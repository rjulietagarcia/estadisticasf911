<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="PlantelesEx_911_8N.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8N.PlantelesEx_911_8N" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    
    <%--Agregado--%>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 16;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">
    <table>
        <tr><td style="width:120px;"></td><td><span>EDUCACI�N NORMAL</span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCiclo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCentroTrabajo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8N',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" ><span>CARACTER�STICAS Y PERSONAL</span></a></li>
        <li onclick="openPage('ACG_911_8N',true)"><a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Total_911_8N',true)"><a href="#" title="" ><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="openPage('TotalAlumn_911_8N',true)"><a href="#" title=""><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="openPage('PlantelesEx_911_8N',true)"><a href="#" title="" class="activo"><span>PLANTELES DE EXTENSI�N</span></a></li>
        <li onclick="openPage('Inmueble_911_8N',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

         <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

          
            <table style="width: 830px">
                        <tr>
                            <td colspan="6" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                                    Text="IV. PLANTELES DE EXTENSI�N" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align:left;">
                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" 
                                    Text="1. Si existen planteles de extenci�n o m�dulos, escriba el n�mero y el total de alumnos que se atendieron, desglose la inscripci�n total, la existencia y los aprobados en todas las asignaturas seg�n el sexo." Width="100%"></asp:Label></td>
                        </tr>
                        
            </table>
            <table style="width:auto;">
            <tr>
                <td colspan="6" style="text-align:left;" ><br />
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="N�MERO DE PLANTELES DE EXTENSI�N: " ></asp:Label>
                    <asp:TextBox ID="txtV537" runat="server" Columns="5" MaxLength="5" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align:left;"><br />
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="ALUMNOS EN PLANTELES DE EXTENSI�N " Width="100%"></asp:Label>
                    <br /><br />
                </td>
            </tr>
            <tr >
                <td class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="80px"></asp:Label>
                </td>
                <td class="linaBajoAlto" style="width: 136px">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEXO"
                        Width="80px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblRojo" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="80px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="EXISTENCIA" Width="100px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="APROADOS EN TODAS LAS AIGNATURAS"
                        Width="100px"></asp:Label>
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width:136px">
                    <asp:Label ID="lblInscTotal1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV538" runat="server" Columns="5" MaxLength="5" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV539" runat="server" Columns="5" MaxLength="5" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV540" runat="server" Columns="5" MaxLength="5" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="lblExistencia1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV541" runat="server" Columns="5" MaxLength="5" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV542" runat="server" Columns="5" MaxLength="5" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV543" runat="server" Columns="5" MaxLength="5" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width:136px">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV544" runat="server" Columns="5" MaxLength="5" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV545" runat="server" Columns="5" MaxLength="5" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV546" runat="server" Columns="5" MaxLength="5" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV547" runat="server" Columns="5" MaxLength="5" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV548" runat="server" Columns="5" MaxLength="5" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV549" runat="server" Columns="5" MaxLength="5" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width:136px">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV550" runat="server" Columns="5" MaxLength="5" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV551" runat="server" Columns="5" MaxLength="5" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV552" runat="server" Columns="5" MaxLength="5" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV553" runat="server" Columns="5" MaxLength="5" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV554" runat="server" Columns="5" MaxLength="5" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV555" runat="server" Columns="5" MaxLength="5" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width:136px">
                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV556" runat="server" Columns="5" MaxLength="5" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV557" runat="server" Columns="5" MaxLength="5" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV558" runat="server" Columns="5" MaxLength="5" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV559" runat="server" Columns="5" MaxLength="5" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV560" runat="server" Columns="5" MaxLength="5" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV561" runat="server" Columns="5" MaxLength="5" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td colspan="2" class="linaBajo">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV562" runat="server" Columns="5" MaxLength="5" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV563" runat="server" Columns="5" MaxLength="5" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV564" runat="server" Columns="5" MaxLength="5" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align:right;" ><br />
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="FECHA DE LLENADO: " ></asp:Label>
                    <asp:TextBox ID="txtV531" runat="server" Columns="5" MaxLength="5" TabIndex="11301" CssClass="lblNegro"></asp:TextBox>
                </td>
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('TotalAlumn_911_8N',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('TotalAlumn_911_8N',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Inmueble_911_8N',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_8N',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center>
         
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 </div>
         </div>
               
                    
          <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
</asp:Content>
