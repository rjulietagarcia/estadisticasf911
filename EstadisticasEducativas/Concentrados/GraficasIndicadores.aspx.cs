using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace EstadisticasEducativas.Concentrados
{
    public partial class GraficasIndicadores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                EducacionBasica();
                RegistraAcceso();
            }
        }

        private void RegistraAcceso()
        {
            SEroot.WsRefSeguridad.ControlSeguridad wsSeg = new SEroot.WsRefSeguridad.ControlSeguridad();
            SEroot.WsRefSeguridad.AccesoLogDP LogDP = new SEroot.WsRefSeguridad.AccesoLogDP();
            LogDP.logId = 0;
            LogDP.opcionId = 17;
            LogDP.usuarioId = 0;
            LogDP.fecha = DateTime.Now.ToString();
            LogDP.ip = this.Request.UserHostAddress;
            string resultado = wsSeg.GuardaAcceso(LogDP);
        }

        protected void rbtNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtNivel.Items[0].Selected == true)
            {
                EducacionBasica();
            }
            if (rbtNivel.Items[1].Selected == true)
            {
                MediaSuperior();
            }
            if (rbtNivel.Items[2].Selected == true)
            {
                Superior();
            }
        }

        private void EducacionBasica()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='cuadro' width='300'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='2' bgcolor ='CCCCCC' class='titulo'><strong>Educaci�n B�sica</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB1.pdf'>Analfabetismo</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB2.pdf'>Grado promedio de escolaridad</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB3.pdf'>Atenci�n a la poblaci�n de 3, 4 y 5 a�os</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB4.pdf'>Atenci�n a la poblaci�n de 5 a�os</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB5.pdf'>Deserci�n en primaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB6.pdf'>Deserci�n en secundaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB7.pdf'>Reprobaci�n en primaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB8.pdf'>Reprobaci�n en secundaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB9.pdf'>Eficiencia terminal en primaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB10.pdf'>Eficiencia Terminal en secundaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB11.pdf'>Absorci�n secundaria</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB12.pdf'>Cobertura primaria (6 a 12 a�os)</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GB13.pdf'>Cobertura secundaria (13 a 15 a�os)</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        private void MediaSuperior()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='cuadro' width='270'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='2' bgcolor ='CCCCCC' class='titulo'><strong>Educaci�n Media Superior</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS1.pdf'>Absorci�n Media Superior</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS2.pdf'>Deserci�n Media Superior</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS3.pdf'>Reprobaci�n Media Superior</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS4.pdf'>Eficiencia Terminal Media Superior</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS5.pdf'>Cobertura Media Superior</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        private void Superior()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='cuadro' width='360'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='2' bgcolor ='CCCCCC' class='titulo'><strong>Educaci�n Superior</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS6.pdf'>Cobertura Educaci�n Superior (Incluye posgrado)</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/GMS7.pdf'>Absorci�n Educaci�n Superior</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

    }
}
