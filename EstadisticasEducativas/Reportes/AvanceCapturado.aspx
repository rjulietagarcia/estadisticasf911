<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true"
    CodeBehind="AvanceCapturado.aspx.cs" Inherits="EstadisticasEducativas.Reportes.AvanceCapturado"
    Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <script language="javascript" type="text/javascript">

        function expandcollapse(id, row) {
            //alert('ctl00_cphMainMaster_gvReporte_ctl'+ row +'_gvNiveles');
            var div = document.getElementById('div' + id);
            var renglon = document.getElementById('td' + id);
            var grid = document.getElementById('ctl00_cphMainMaster_gvReporte');
            var Level = 'ctl00_cphMainMaster_gvReporte_ctl' + row + '_gvNiveles';

            var grid2 = document.getElementById(Level);

            var ancho = grid.offsetWidth

            if (div.style.display == "none") {
                div.style.display = "";
                renglon.style.display = "table-row";

                if (grid2.offsetWidth > ancho) {
                    grid.width = grid2.offsetWidth + 50;
                }

            }
            else {
                div.style.display = "none";
                renglon.style.visibility = "none";

            }
        }

        function expandcollapse2(id, row, row2) {
//            alert('divAnidado' + id);
            var div = document.getElementById('divAnidado' + id);
            var renglon = document.getElementById('trAnidado' + id);
            var grid = document.getElementById('ctl00_cphMainMaster_gvReporte');
            var grid2 = document.getElementById('ctl00_cphMainMaster_gvReporte_ctl' + row + '_gvNiveles');
            var gridAnidado = document.getElementById('ctl00_cphMainMaster_gvReporte_ctl' + row + '_gvNiveles_ctl' + row2 + '_gvAnidado2');

            var ancho = grid2.offsetWidth


            if (div.style.display == "none") {
                div.style.display = "block";
                renglon.style.display = "table-row";
                if (gridAnidado.offsetWidth > ancho) {
                    grid2.width = gridAnidado.offsetWidth + 50;

                    if ((grid2.offsetWidth + 50) > grid.offsetWidth) {
                        grid.width = grid2.offsetWidth + 50;
                    }
                }

            }
            else {
                div.style.display = "none";
                renglon.style.display = "none";
            }
        }

    </script>
    <center>
        <div style="text-align: center; width: 100%">
            <center>
                <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center"
                    border="0">
                    <tr>
                        <td class="EsqSupIzq">
                        </td>
                        <td class="RepSup">
                        </td>
                        <td class="EsqSupDer">
                        </td>
                    </tr>
                    <tr>
                        <td class="RepLatIzq">
                        </td>
                        <td>
                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="Titulo" style="font-weight: bold" align="center">
                                        Reportes
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:UpdatePanel ID="upBusqueda" runat="server">
                                <ContentTemplate>
                                    <table cellspacing="2" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="Label1" runat="server" Text="Ciclo Escolar:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlCiclo" runat="server" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblEntidad" runat="server" Text="Entidad:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlEntidad" runat="server" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged"
                                                        AutoPostBack="True">
                                                        <%--<asp:ListItem Value="0">Seleccione un Estado</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <%-- <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblRegion" runat="server" Text="Regi�n:" CssClass="lblEtiqueta" Font-Size="14px"></asp:Label></td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlRegion" runat="server" Enabled="false" AutoPostBack="True">
                                                        <asp:ListItem Value="-1">Seleccione una Regi�n</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%><tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblNivel" runat="server" Text="Nivel:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlNivel" runat="server" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged"
                                                        AutoPostBack="True">
                                                        <asp:ListItem Value="-1_-1">Todos Los Niveles</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <%-- <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblZona" runat="server" Width="43px" Text="Zona:" CssClass="lblEtiqueta"
                                                        Font-Size="14px"></asp:Label></td>
                                                <td style="width: 120px" align="left">
                                                    <asp:TextBox ID="txtZona" TabIndex="2" runat="server" Width="55px" MaxLength="3"></asp:TextBox></td>
                                            </tr>--%><tr>
                                                <td>
                                                    <asp:Label ID="lblTipoCaptura" runat="server" Text="Tipo de Captura"></asp:Label>
                                                    <asp:RadioButtonList ID="rdLista1" runat="server" OnSelectedIndexChanged="rdLista1_SelectedIndexChanged"
                                                        AutoPostBack="True" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Inicio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fin" Value="2" Selected="True"></asp:ListItem>
                                                        
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td style="text-align: right">
                                                    <img style="cursor: pointer" id="imgExcel" title="Exportar a Excel" onclick="__doPostBack('ctl00$cphMainMaster$lnkExportar','')"
                                                        src="../App_Themes/SEP2010/Imagenes/Fondo/toExcel.JPG" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvReporte" runat="server" OnSelectedIndexChanged="gvReporte_SelectedIndexChanged"
                                                        OnRowDataBound="gvReporte_RowDataBound" ShowFooter="true" AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkFiltro" Text='<%# Bind("Filtro") %>' CommandName="Select">
                                                                    </asp:LinkButton>
                                                                    <asp:Label ID="lblIdFiltro" runat="server" Text='<%# Bind("IdFiltro") %>' Visible="false"></asp:Label>
                                                                    <%--<a onclick="javascript:expandcollapse(<%# Eval("IdFiltro") %>, 'none');">
                                                                    <asp:Label ID="lblFiltro" runat="server" Text='<%# Bind("Filtro") %>'></asp:Label>
                                                                </a>--%>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblFiltroTotal" runat="server" Text="Total"></asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total General">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotales" Text='<%# Bind("Totales","{0:n0}") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblTotalGeneral" runat="server" Text=""></asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td colspan="3" style="border-bottom: solid 1px #EBE9ED; text-align: center;">
                                                                                Oficializados
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 85px; text-align: center;">
                                                                                Capturados
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                Con Motivo
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                Total
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblCapturados" Text='<%# Bind("Capturados","{0:n0}") %>' runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblConMotivo" Text='<%# Bind("ConMotivo","{0:n0}") %>' runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblOficializados" Text='<%# Bind("Oficializados","{0:n0}") %>' runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblCapturadosF" Text="" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblConMotivoF" Text="" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblOficializadosF" Text="" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td colspan="3" style="border-bottom: solid 1px #EBE9ED; text-align: center;">
                                                                                Pendientes
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 85px; text-align: center;">
                                                                                En Captura
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                Pend. Captura
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                Total
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblEnCaptura" Text='<%# Bind("EnCaptura","{0:n0}") %>' runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                <asp:Label ID="lblPendientesCapt" Text='<%# Bind("PendientesCaptura","{0:n0}") %>' runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblPendientesTotales" Text='<%# Bind("PendientesTotales","{0:n0}") %>' runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblEnCapturaF" Text="" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                <asp:Label ID="lblPendientesCapF" Text="" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                <asp:Label ID="lblPendientesTotalesF" Text="" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="% Avance">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblProcentaje1" Text='<%# Bind("Porcentaje") %>' runat="server"></asp:Label>%
                                                                    <tr id="td<%# Eval("IdFiltro") %>" style="background-color: #EEE8AA; display: none">
                                                                        <td colspan="100%">
                                                                            <div id="div<%# Eval("IdFiltro") %>" style="display: none; position: relative; left: 27px;
                                                                                overflow: auto; width: 100%">
                                                                                <asp:GridView ID="gvNiveles" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                                                                                    OnRowDataBound="gvNiveles_RowDataBound" OnSelectedIndexChanged="gvNiveles_SelectedIndexChanged">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ShowHeader="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton runat="server" ID="lnkNivelFiltro" Text='<%# Bind("Filtro") %>' CommandName="Select">
                                                                                                </asp:LinkButton>
                                                                                                <asp:Label ID="lblNivelIdFiltro" runat="server" Text='<%# Bind("IdFiltro") %>' Visible="false"></asp:Label>
                                                                                                <%--<asp:Label ID="lblNivelFiltro" runat="server" Text='<%# Bind("Filtro") %>'></asp:Label>--%>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:Label ID="lblNivelFiltroTotal" runat="server" Text="Total"></asp:Label>
                                                                                            </FooterTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Total General">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblNivel1" Text='<%# Bind("Totales","{0:n0}") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:Label ID="lblNivelTotalGeneral" runat="server" Text=""></asp:Label>
                                                                                            </FooterTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <HeaderTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td colspan="3" style="border-bottom: solid 1px #EBE9ED; text-align: center;">
                                                                                                            Oficializados
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 85px; text-align: center;">
                                                                                                            Capturados
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            Con Motivo
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            Total
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td style="width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelCapturados" Text='<%# Bind("Capturados","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelConMotivo" Text='<%# Bind("ConMotivo","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelOficializados" Text='<%# Bind("Oficializados","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td style="width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelCapturadosF" Text="" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelConMotivoF" Text="" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelOficializadosF" Text="" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </FooterTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <HeaderTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td colspan="3" style="border-bottom: solid 1px #EBE9ED; text-align: center;">
                                                                                                            Pendientes
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 85px; text-align: center;">
                                                                                                            En Captura
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                                            Pend. Captura
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            Total
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td style="width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelEnCaptura" Text='<%# Bind("EnCaptura","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelPendienteCapt" Text='<%# Bind("PendientesCaptura","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelPendienteTot" Text='<%# Bind("PendientesTotales","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td style="width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelEnCapturaF" Text="" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelPendientesCapF" Text="" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                            <asp:Label ID="lblNivelPendientesTotalesF" Text="" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </FooterTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="% Avance">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl1" Text='<%# Bind("Porcentaje") %>' runat="server"></asp:Label>%
                                                                                                <tr id="trAnidado<%# Eval("IdFiltro") %>" style="background-color: #EEE8C8; display: none;">
                                                                                                    <td colspan="100%">
                                                                                                        <div id="divAnidado<%# Eval("IdFiltro") %>" style="display: none; position: relative;
                                                                                                            left: 27px; overflow: auto; width: 100%">
                                                                                                            <asp:GridView ID="gvAnidado2" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                                                                                                                OnRowDataBound="gvAnidado2_RowDataBound">
                                                                                                                <EmptyDataTemplate>
                                                                                                                    No hay detalle
                                                                                                                </EmptyDataTemplate>
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField ShowHeader="false">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:LinkButton runat="server" ID="lnkAnidado2Filtro" Text='<%# Bind("Filtro") %>'
                                                                                                                                CommandName="Select">
                                                                                                                            </asp:LinkButton>
                                                                                                                            <asp:Label ID="lblAnidado2IdFiltro" runat="server" Text='<%# Bind("IdFiltro") %>'
                                                                                                                                Visible="false"></asp:Label>
                                                                                                                            <%--<asp:Label ID="lblNivelFiltro" runat="server" Text='<%# Bind("Filtro") %>'></asp:Label>--%>
                                                                                                                        </ItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                            <asp:Label ID="lblAnidado2FiltroTotal" runat="server" Text="Total"></asp:Label>
                                                                                                                        </FooterTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="Total General">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lblAnidado2Totales" Text='<%# Bind("Totales","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                            <asp:Label ID="lblAnidado2TotalGeneral" runat="server" Text=""></asp:Label>
                                                                                                                        </FooterTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField>
                                                                                                                        <HeaderTemplate>
                                                                                                                            <table width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td colspan="3" style="border-bottom: solid 1px #EBE9ED; text-align: center;">
                                                                                                                                        Oficializados
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 85px; text-align: center;">
                                                                                                                                        Capturados
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        Con Motivo
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        Total
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </HeaderTemplate>
                                                                                                                        <ItemTemplate>
                                                                                                                            <table width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2Capturados" Text='<%# Bind("Capturados","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2ConMotivo" Text='<%# Bind("ConMotivo","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2Oficializados" Text='<%# Bind("Oficializados","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                            <table>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2CapturadosF" Text="" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2ConMotivoF" Text="" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2OficializadosF" Text="" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </FooterTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField>
                                                                                                                        <HeaderTemplate>
                                                                                                                            <table width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td colspan="3" style="border-bottom: solid 1px #EBE9ED; text-align: center;">
                                                                                                                                        Pendientes
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 85px; text-align: center;">
                                                                                                                                        En Captura
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                                                                        Pend. Captura
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        Total
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </HeaderTemplate>
                                                                                                                        <ItemTemplate>
                                                                                                                            <table width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2EnCaptura" Text='<%# Bind("EnCaptura","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2PendienteCapt" Text='<%# Bind("PendientesCaptura","{0:n0}") %>'
                                                                                                                                            runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2PendienteTot" Text='<%# Bind("PendientesTotales","{0:n0}") %>' runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                            <table>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2EnCapturaF" Text="" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 95px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2PendientesCapF" Text="" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="border-left: solid 1px #EBE9ED; width: 85px; text-align: center;">
                                                                                                                                        <asp:Label ID="lblAnidado2PendientesTotalesF" Text="" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </FooterTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="% Avance">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl1" Text='<%# Bind("Porcentaje") %>' runat="server"></asp:Label>%
                                                                                                                            <%-- <tr style="background-color: #EEE8AA">
                                                                                                <td style="visibility:collapse" colspan="100%">
                                                                                                    <div id="divAnidado<%# Eval("IdFiltro") %>" style="display: none; position: relative;
                                                                                                        left: 27px; overflow: auto; width: 100%"></div>
                                                                                                        </td>
                                                                                                        </tr>--%>
                                                                                                                        </ItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                            <asp:Label ID="lblAnidado2PorcentajeF" Text="" runat="server"></asp:Label>
                                                                                                                        </FooterTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                            </asp:GridView>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:Label ID="lblNivelPorcentajeF" Text="" runat="server"></asp:Label>
                                                                                            </FooterTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblPorcentajeF" Text="" runat="server"></asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No se encontraron registros
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnReporte" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                            &nbsp;&nbsp;
                            <asp:Button ID="btnReporte" runat="server" Text="Ver Reporte" TabIndex="4" OnClick="btnReporte_Click" />
                            <asp:LinkButton ID="lnkExportar" runat="server" Text="" OnClick="lnkExportar_Click"></asp:LinkButton><br />
                            <table width="100%">
                                <tr>
                                    <td style="width: 100%; text-align: center">
                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                            <ProgressTemplate>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="../tema/images/loading.gif" />
                                                <!-- this is an animated progress gif that will be shown while progress delay -->
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="RepLatDer">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="EsqInfIzq">
                        </td>
                        <td class="RepInf">
                        </td>
                        <td class="EsqInfDer">
                        </td>
                    </tr>
                </table>
                <div>
                    <br />
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>
                <br />
            </center>
        </div>
    </center>
</asp:Content>
