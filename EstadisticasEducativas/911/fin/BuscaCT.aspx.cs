using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

//using SEroot.WsSESeguridad;
using Mx.Gob.Nl.Educacion;
using SEroot.WsEstadisticasEducativas;


namespace EstadisticasEducativas._911.fin
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadiscticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtRegion.Attributes["onkeypress"] = "return Num(event)";
                txtZona.Attributes["onkeypress"] = "return Num(event)";
                btnBuscar.Attributes["onblur"] = "javascript:_enter=true;";
                btnBuscar.Attributes["onfocus"] = "javascript:_enter=false;";
                Nivel();
                GridVacio();
                Filtros();
                Ciclos();

            }
        }
        
        private void Filtros()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            SEroot.WsCentrosDeTrabajo.ZonaDP zonaDP = ws.Load_Zona(zona);
            string ID_NivelEducacion = usr[7];

            //ddlNivel.Enabled = false;

            switch (ID_NivelTrabajo)
            {
                case 1://SE
                    break;
                case 2://Region
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
                case 3:
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    txtZona.Text = zonaDP.numeroZona.ToString();
                    txtZona.Enabled = false;
                    break;

                case 4:

                    if (centroTrabajo.Length > 1)
                    {
                        txtRegion.Text = "";
                        txtZona.Text = "";
                        txtRegion.Enabled = false;
                        txtZona.Enabled = false;

                        txtClaveCT.Visible = false; txtClaveCT.Text = "";
                        ddlCentroTrabajo.Visible = true;
                        for (int i = 0; i < centroTrabajo.Length; i++)
                        {
                            SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[i]));

                            ddlCentroTrabajo.Items.Add(new ListItem(ctDP.clave, ctDP.clave));
                        }
                        ddlCentroTrabajo.SelectedIndex = 0;
                    }
                    else
                    {

                        ddlNivel.Enabled = false;

                        txtRegion.Text = region.ToString();
                        txtRegion.Enabled = false;
                        txtZona.Text = zonaDP.numeroZona.ToString();
                        txtZona.Enabled = false;

                        SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[0]));
                        txtClaveCT.Visible = true; txtClaveCT.Text = ctDP.clave;

                        ddlCentroTrabajo.Visible = false;
                    }
                    txtClaveCT.Enabled = false;
                    break;
           
                case 5://RegionAVAN
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
                case 6://RegionDES
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
                case 7://RegionDBF
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
               

            }
        }

        private void Nivel()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            //Response.Write(usr[0]);
            if (usr[0] == "352599")
            {
                ddlNivel.Items.Add(new ListItem("CONALEP", "21_20"));
            }
            else if (ID_NivelTrabajo == 1 || ID_NivelTrabajo == 2 || ID_NivelTrabajo == 5 || ID_NivelTrabajo == 6 || ID_NivelTrabajo == 7)
            {
                SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

                SEroot.WsEstadisticasEducativas.CatListaNiveles911DP[] listaNiveles = wsEstadisticas.Lista_CatListaNiveles911(Class911.ID_Fin);
                int secundaria = 0;
                for (int i = 0; i < listaNiveles.Length; i++)
                {
                    string valor;
                    if (listaNiveles[i].ID_Nivel != 13)
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_" + listaNiveles[i].ID_SubNivel.ToString();
                        secundaria = 0;
                    }
                    else
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_0";
                        secundaria++;
                    }
                    if (secundaria < 2)
                    {
                        string texto = listaNiveles[i].Descrip;
                        ddlNivel.Items.Add(new ListItem(texto, valor));
                    }
                }
            }
            else
            {
                ddlNivel.Enabled = false;
                ddlNivel.Items.Add(new ListItem("", "0_0"));
            }
        }

        private void Ciclos()
        {
            SEroot.WSEscolar.WsEscolar wsEscolar = new SEroot.WSEscolar.WsEscolar();
            SEroot.WSEscolar.DsCiclosEscolares dsCiclos = wsEscolar.ListaCicloEscolarCombo(1);//Anual

            ddlCiclo.DataSource = dsCiclos.CicloEscolar;
            ddlCiclo.DataValueField = "Id_CicloEscolar";
            ddlCiclo.DataTextField = "Nombre";
            ddlCiclo.DataBind();

            getUsr();

            ListItem itemActual = ddlCiclo.Items.FindByValue(usr.Selecciones.CicloEscolarSeleccionado.CicloescolarId.ToString());
            if (itemActual != null)
            {
                itemActual.Selected = true;
            }

        }

        protected void GridVacio()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            Session["dsGrid"] = null;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            LlenaGridView_2();
        }

       

        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }

        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.DataSource = SortDataTable(true);
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected DataView SortDataTable(bool isPageIndexChanging)
        {
            //DataSet ds = (DataSet)Session["dsGrid"];
            //if (ds != null)
            if (Session["dsGrid"] != null)
            {
                //DataTable dataTable = new DataTable();
                //dataTable = ds.Tables[0];

                DataTable dataTable = new DataTable();
                dataTable = (DataTable)Session["dsGrid"];

                if (dataTable != null)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
            else
            {
                return new DataView();
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridViewSortExpression = e.SortExpression;
            int pageIndex = GridView1.PageIndex;
            GridView1.DataSource = SortDataTable(false);
            GridView1.DataBind();
            GridView1.PageIndex = pageIndex;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string id_CCT = "";
            //id_CCT = GridView1.SelectedRow.Cells[0].Text.ToString();
        }
        protected void RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            if (row != null)
            {

                int ID_Ciclo = int.Parse(ddlCiclo.SelectedValue);
                string[] usr = User.Identity.Name.Split('|');
                int ID_Usuario = int.Parse(usr[0]);

                Label lbl = (Label)row.FindControl("lblID_CCTNT");

                EstablecerBarrita(0, 0, 0, 0, int.Parse(lbl.Text));
                int id_cctnt = int.Parse(lbl.Text);
                ControlDP controlDP = wsEstadiscticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Fin, ID_Ciclo, ID_Usuario);
                Class911.SetControlSeleccionado(HttpContext.Current, controlDP);

                //Abrir en modal el cuestionario
                //string codigo = "window.open('" + Ruta(controlDP.ID_Cuestionario) + "', 'ventana911', '');"; //status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200');";
                string codigo = "Vent911('" + Ruta(controlDP.ID_Cuestionario) + "');";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "", codigo, true);
            }

        }
      
        protected void LlenaGridView_2()
        {
            SEroot.WsCentrosDeTrabajo.DsBuscaCCT911 Ds = new SEroot.WsCentrosDeTrabajo.DsBuscaCCT911();
            int reg = 0;
            int zon = 0;
            string ct = "";
            if (txtRegion.Text.Trim().Length > 0)
            {
                reg = int.Parse(txtRegion.Text.Trim());
            }
            if (txtZona.Text.Trim().Length > 0)
            {
                zon = int.Parse(txtZona.Text.Trim());
            }
            if (txtClaveCT.Visible)
            {
                if (txtClaveCT.Text.Trim().Length > 0)
                {
                    ct = txtClaveCT.Text.Trim();
                }
            }
            else
            {
                ct = ddlCentroTrabajo.SelectedValue;
            }

            string valor = ddlNivel.SelectedValue;
            int ID_Nivel = int.Parse(valor.Split('_')[0]);

            int ID_SubNivel = int.Parse(valor.Split('_')[1]);

            Ds = ws.ListaEscuelas911(19,reg, zon, ct, ID_Nivel, ID_SubNivel);

            if (Ds != null && Ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = Ds.CCT911;
                GridView1.DataBind();
                Session["dsGrid"] = Ds.CCT911;
                lblMsg.Text = "Registros encontrados: " + Ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                GridVacio();
                lblMsg.Text = "No se encontraron registros";
            }
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtRegion.Text = "";
            //txtZona.Text = "";
            //txtClaveCT.Text = "";
            lblMsg.Text = "";
            GridVacio();
        }
   
        UsuarioSeDP usr = null;
        protected void getUsr()
        {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }
        protected void EstablecerBarrita(int ID_Nviel, int ID_Region, int ID_Sostenimiento, int ID_Zona, int ID_CCTNT)
        {
            //getUsr();
            //SERaiz.Controles.FiltraCCTporZona FiltraCCTporZona1 = (SERaiz.Controles.FiltraCCTporZona)Page.Master.FindControl("FiltraCCTporZona1");

         
            if ((ID_CCTNT == -1 || ID_CCTNT == 0) && (ID_Zona == -1 || ID_Zona == 0) && (ID_Region == -1 || ID_Region == 0))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('¡¡¡Seleccione un Centro de Trabajo!!!');", true);
            }
            else
            {
                if (ID_CCTNT != 0 && ID_CCTNT != -1)
                    SeguridadSE.SetCctNt(this.Page, ID_CCTNT);//Set Centro Trabajo
                else
                    if (ID_Zona != 0 && ID_Zona != -1)
                        SeguridadSE.SetCctNt(this.Page, ID_Zona);//Set Zona
                    else
                        if (ID_Region != 0 && ID_Region != -1)
                            SeguridadSE.SetCctNt(this.Page, ID_Region);//Set Region
                        else
                            if (usr.NivelTrabajo.NiveltrabajoId == 1)
                                SeguridadSE.SetCctNt(this.Page, 2);//Set Secretaria de Educacion
                            else
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('¡¡¡Seleccione un Centro de Trabajo!!!');", true);

                ((Label)Page.Master.FindControl("lblCCTSeleccionado")).Text = SeguridadSE.cctSelected.Clavecct + " - " + SeguridadSE.cctSelected.Nombrecct + " - " + SeguridadSE.cctSelected.Truno;
            }
            if (SeguridadSE.cicloSelected != null)
            {
                if (SeguridadSE.cicloSelected.Nombre != "")
                    ((Label)Page.Master.FindControl("lblCicloEscolar")).Text = SeguridadSE.cicloSelected.Nombre;
            }
            //FiltraCCTporZona1.filtrosPermisos(usr);
        }

        protected void txtNivelEduacionID_TextChanged(object sender, EventArgs e)
        {

        }

        protected void CentroTrabajoID_TextChanged(object sender, EventArgs e)
        {

        }

        private string Ruta(int id_cuestionario)
        {
            string abrirVentana = "";
            switch (id_cuestionario)
            {
                case 1:
                    abrirVentana = "911_2/Identificacion_911_2.aspx";
                    break;
                case 2:
                    abrirVentana = "911_4/Identificacion_911_4.aspx";
                    break;
                case 3:
                    abrirVentana = "911_6/Identificacion_911_6.aspx";
                    break;
                case 4:
                    abrirVentana = "911_8P/Identificacion_911_8P.aspx";
                    break;
                case 5:
                    abrirVentana = "911_8G/Identificacion_911_8G.aspx";
                    break;
                case 6:
                    abrirVentana = "911_8T/Identificacion_911_8T.aspx";
                    break;
                case 7:
                    abrirVentana = "911_ECC_21/Identificacion_911_ECC_21.aspx";
                    break;
                case 8:
                    abrirVentana = "911_ECC_22/Identificacion_911_ECC_22.aspx";
                    break;
                case 9:
                    abrirVentana = "911_CAM_2/Identificacion_911_CAM_2.aspx";
                    break;
                case 11:
                    abrirVentana = "911_USAER_2/Identificacion_911_USAER_2.aspx";
                    break;
                case 12:
                    abrirVentana = "911_EI_NE_2/Identificacion_911_EI_NE_2.aspx";
                    break;
                case 13:
                    abrirVentana = "911_EI_2/Identificacion_911_EI_2.aspx";
                    break;
                case 14:
                    abrirVentana = "911_6C/Identificacion_911_6C.aspx";
                    break;
                default:
                    Response.Write("<br> NO DISPONIBLE");
                    break;

            }
            return abrirVentana;
        }
    }
}
