<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Equipo_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.Equipo_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->


    <h2> IX. EQUIPO </h2>
    <br />
    
    <%--<form id="form1" runat="server">--%>
    <div>
    
    <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
    
    1. Marque si el inmueble cuenta con:
    <br /> <br />
    
        <table>
            <tr>
                <td>
                </td>
                <td style="text-align: center"> No
                </td>
                <td style="text-align: center"> Si
                </td>
            </tr>
            <tr>
                <td>Ventiladores o Climas
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="txtv760" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="No" Text=" "> </asp:ListItem>
                        <asp:ListItem Value="Si" Text=" "> </asp:ListItem>
                    </asp:RadioButtonList> 
                </td>
            </tr>
            <tr>
                <td> Sonido
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="txtv761" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="No" Text=" "> </asp:ListItem>
                        <asp:ListItem Value="Si" Text=" "> </asp:ListItem>
                    </asp:RadioButtonList> 
                </td>
            </tr>
            <tr>
                <td> Enciclomedia
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="txtv762" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="No" Text=" "> </asp:ListItem>
                        <asp:ListItem Value="Si" Text=" "> </asp:ListItem>
                    </asp:RadioButtonList> 
                </td>
            </tr>
        </table>
    
    <br /> <br />
    
    2. Si cuenta con Enciclomedia, escriba la cantidad de equipo que se utiliza <strong> exclusivamente </strong> para el programa.
    <br /> <br />
    
        <table>
            <tr>
                <td> Computadora
                </td>
                <td> <asp:TextBox runat="server" id="txtv763" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Proyector
                </td>
                <td> <asp:TextBox runat="server" id="txtv764" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Impresora
                </td>
                <td> <asp:TextBox runat="server" id="txtv765" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Fuente de Poder
                </td>
                <td> <asp:TextBox runat="server" id="txtv766" Width="60" />
                </td>
            </tr>
        </table>

    
    <br /> <br />
    
    3. Escriba el n&uacute;mero de <strong> pizarrones interactivos </strong> seg&uacute;n la marca.
    <br /> <br />
    
        <table>
            <tr>
                <td> Prometean
                </td>
                <td> <asp:TextBox runat="server" id="txtv767" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Polivisi&oacute;n
                </td>
                <td> <asp:TextBox runat="server" id="txtv768" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Smart Board
                </td>
                <td> <asp:TextBox runat="server" id="txtv769" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Alfher
                </td>
                <td> <asp:TextBox runat="server" id="txtv770" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Otro*
                </td>
                <td> <asp:TextBox runat="server" id="txtv771" Width="60" /> 
                </td>
                <td> *Especifique <asp:TextBox runat="server" id="txtv772" Width="60" />
                </td>
            </tr>
        </table>
    
    <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
    
    </div>
    <%--</form>--%>

 </asp:Content>