using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
    
namespace EstadisticasEducativas._911.fin.ECC_22
{
    public partial class Alumnos2_ECC_22 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV328.Attributes["onkeypress"] = "return Num(event)";
                txtV329.Attributes["onkeypress"] = "return Num(event)";
                txtV330.Attributes["onkeypress"] = "return Num(event)";
                txtV331.Attributes["onkeypress"] = "return Num(event)";
                txtV332.Attributes["onkeypress"] = "return Num(event)";
                txtV333.Attributes["onkeypress"] = "return Num(event)";
                txtV334.Attributes["onkeypress"] = "return Num(event)";
                txtV335.Attributes["onkeypress"] = "return Num(event)";
                txtV336.Attributes["onkeypress"] = "return Num(event)";
                txtV337.Attributes["onkeypress"] = "return Num(event)";
                txtV338.Attributes["onkeypress"] = "return Num(event)";
                txtV339.Attributes["onkeypress"] = "return Num(event)";
                txtV340.Attributes["onkeypress"] = "return Num(event)";
                txtV341.Attributes["onkeypress"] = "return Num(event)";
                txtV342.Attributes["onkeypress"] = "return Num(event)";
                txtV343.Attributes["onkeypress"] = "return Num(event)";
                txtV344.Attributes["onkeypress"] = "return Num(event)";
                txtV345.Attributes["onkeypress"] = "return Num(event)";
                txtV346.Attributes["onkeypress"] = "return Num(event)";
                txtV347.Attributes["onkeypress"] = "return Num(event)";
                txtV348.Attributes["onkeypress"] = "return Num(event)";
                txtV349.Attributes["onkeypress"] = "return Num(event)";
                txtV350.Attributes["onkeypress"] = "return Num(event)";
                txtV351.Attributes["onkeypress"] = "return Num(event)";
                txtV352.Attributes["onkeypress"] = "return Num(event)";
                txtV353.Attributes["onkeypress"] = "return Num(event)";
                txtV354.Attributes["onkeypress"] = "return Num(event)";
                txtV355.Attributes["onkeypress"] = "return Num(event)";
                txtV356.Attributes["onkeypress"] = "return Num(event)";
                txtV357.Attributes["onkeypress"] = "return Num(event)";
                txtV358.Attributes["onkeypress"] = "return Num(event)";
                txtV359.Attributes["onkeypress"] = "return Num(event)";
                txtV360.Attributes["onkeypress"] = "return Num(event)";
                txtV361.Attributes["onkeypress"] = "return Num(event)";
                txtV362.Attributes["onkeypress"] = "return Num(event)";
                txtV363.Attributes["onkeypress"] = "return Num(event)";
                txtV364.Attributes["onkeypress"] = "return Num(event)";
                txtV365.Attributes["onkeypress"] = "return Num(event)";
                txtV366.Attributes["onkeypress"] = "return Num(event)";
                txtV367.Attributes["onkeypress"] = "return Num(event)";
                txtV368.Attributes["onkeypress"] = "return Num(event)";
                txtV369.Attributes["onkeypress"] = "return Num(event)";
                txtV370.Attributes["onkeypress"] = "return Num(event)";
                txtV371.Attributes["onkeypress"] = "return Num(event)";
                txtV372.Attributes["onkeypress"] = "return Num(event)";
                txtV373.Attributes["onkeypress"] = "return Num(event)";
                txtV374.Attributes["onkeypress"] = "return Num(event)";
                txtV375.Attributes["onkeypress"] = "return Num(event)";
                txtV376.Attributes["onkeypress"] = "return Num(event)";
                txtV377.Attributes["onkeypress"] = "return Num(event)";
                txtV378.Attributes["onkeypress"] = "return Num(event)";
                txtV379.Attributes["onkeypress"] = "return Num(event)";
                txtV380.Attributes["onkeypress"] = "return Num(event)";
                txtV381.Attributes["onkeypress"] = "return Num(event)";
                txtV382.Attributes["onkeypress"] = "return Num(event)";
                txtV383.Attributes["onkeypress"] = "return Num(event)";
                txtV384.Attributes["onkeypress"] = "return Num(event)";
                txtV385.Attributes["onkeypress"] = "return Num(event)";
                txtV386.Attributes["onkeypress"] = "return Num(event)";
                txtV387.Attributes["onkeypress"] = "return Num(event)";
                txtV388.Attributes["onkeypress"] = "return Num(event)";
                txtV389.Attributes["onkeypress"] = "return Num(event)";
                txtV390.Attributes["onkeypress"] = "return Num(event)";
                txtV391.Attributes["onkeypress"] = "return Num(event)";
                txtV392.Attributes["onkeypress"] = "return Num(event)";
                txtV393.Attributes["onkeypress"] = "return Num(event)";
                txtV394.Attributes["onkeypress"] = "return Num(event)";
                txtV395.Attributes["onkeypress"] = "return Num(event)";
                txtV396.Attributes["onkeypress"] = "return Num(event)";
                txtV397.Attributes["onkeypress"] = "return Num(event)";
                txtV398.Attributes["onkeypress"] = "return Num(event)";
                txtV399.Attributes["onkeypress"] = "return Num(event)";
                txtV400.Attributes["onkeypress"] = "return Num(event)";
                txtV401.Attributes["onkeypress"] = "return Num(event)";
                txtV402.Attributes["onkeypress"] = "return Num(event)";
                txtV403.Attributes["onkeypress"] = "return Num(event)";
                txtV404.Attributes["onkeypress"] = "return Num(event)";
                txtV405.Attributes["onkeypress"] = "return Num(event)";
                txtV406.Attributes["onkeypress"] = "return Num(event)";
                txtV407.Attributes["onkeypress"] = "return Num(event)";
                txtV408.Attributes["onkeypress"] = "return Num(event)";
                txtV409.Attributes["onkeypress"] = "return Num(event)";
                txtV410.Attributes["onkeypress"] = "return Num(event)";
                txtV411.Attributes["onkeypress"] = "return Num(event)";
                txtV412.Attributes["onkeypress"] = "return Num(event)";
                txtV413.Attributes["onkeypress"] = "return Num(event)";
                txtV414.Attributes["onkeypress"] = "return Num(event)";
                txtV415.Attributes["onkeypress"] = "return Num(event)";
                txtV416.Attributes["onkeypress"] = "return Num(event)";
                txtV417.Attributes["onkeypress"] = "return Num(event)";
                txtV418.Attributes["onkeypress"] = "return Num(event)";
                txtV419.Attributes["onkeypress"] = "return Num(event)";
                txtV420.Attributes["onkeypress"] = "return Num(event)";
                txtV421.Attributes["onkeypress"] = "return Num(event)";
                txtV422.Attributes["onkeypress"] = "return Num(event)";
                txtV423.Attributes["onkeypress"] = "return Num(event)";
                txtV424.Attributes["onkeypress"] = "return Num(event)";
                txtV425.Attributes["onkeypress"] = "return Num(event)";
                txtV426.Attributes["onkeypress"] = "return Num(event)";
                txtV427.Attributes["onkeypress"] = "return Num(event)";
                txtV428.Attributes["onkeypress"] = "return Num(event)";
                txtV429.Attributes["onkeypress"] = "return Num(event)";
                txtV430.Attributes["onkeypress"] = "return Num(event)";
                txtV431.Attributes["onkeypress"] = "return Num(event)";
                txtV432.Attributes["onkeypress"] = "return Num(event)";
                txtV433.Attributes["onkeypress"] = "return Num(event)";
                txtV434.Attributes["onkeypress"] = "return Num(event)";
                txtV435.Attributes["onkeypress"] = "return Num(event)";
                txtV436.Attributes["onkeypress"] = "return Num(event)";
                txtV437.Attributes["onkeypress"] = "return Num(event)";
                txtV438.Attributes["onkeypress"] = "return Num(event)";
                txtV439.Attributes["onkeypress"] = "return Num(event)";
                txtV440.Attributes["onkeypress"] = "return Num(event)";
                txtV441.Attributes["onkeypress"] = "return Num(event)";
                txtV442.Attributes["onkeypress"] = "return Num(event)";
                txtV443.Attributes["onkeypress"] = "return Num(event)";
                txtV444.Attributes["onkeypress"] = "return Num(event)";
                txtV445.Attributes["onkeypress"] = "return Num(event)";
                txtV446.Attributes["onkeypress"] = "return Num(event)";
                txtV447.Attributes["onkeypress"] = "return Num(event)";
                txtV448.Attributes["onkeypress"] = "return Num(event)";
                txtV449.Attributes["onkeypress"] = "return Num(event)";
                txtV450.Attributes["onkeypress"] = "return Num(event)";
                txtV451.Attributes["onkeypress"] = "return Num(event)";
                txtV452.Attributes["onkeypress"] = "return Num(event)";
                txtV453.Attributes["onkeypress"] = "return Num(event)";
                txtV454.Attributes["onkeypress"] = "return Num(event)";
                txtV455.Attributes["onkeypress"] = "return Num(event)";
                txtV456.Attributes["onkeypress"] = "return Num(event)";
                txtV457.Attributes["onkeypress"] = "return Num(event)";
                txtV458.Attributes["onkeypress"] = "return Num(event)";
                txtV459.Attributes["onkeypress"] = "return Num(event)";
                txtV460.Attributes["onkeypress"] = "return Num(event)";
                txtV461.Attributes["onkeypress"] = "return Num(event)";
                txtV462.Attributes["onkeypress"] = "return Num(event)";
                txtV463.Attributes["onkeypress"] = "return Num(event)";
                txtV464.Attributes["onkeypress"] = "return Num(event)";
                txtV465.Attributes["onkeypress"] = "return Num(event)";
                txtV466.Attributes["onkeypress"] = "return Num(event)";
                txtV467.Attributes["onkeypress"] = "return Num(event)";
                txtV468.Attributes["onkeypress"] = "return Num(event)";
                txtV469.Attributes["onkeypress"] = "return Num(event)";
                txtV470.Attributes["onkeypress"] = "return Num(event)";
                txtV471.Attributes["onkeypress"] = "return Num(event)";
                txtV472.Attributes["onkeypress"] = "return Num(event)";
                txtV473.Attributes["onkeypress"] = "return Num(event)";
                txtV474.Attributes["onkeypress"] = "return Num(event)";
                txtV475.Attributes["onkeypress"] = "return Num(event)";
                txtV476.Attributes["onkeypress"] = "return Num(event)";
                txtV477.Attributes["onkeypress"] = "return Num(event)";
                txtV478.Attributes["onkeypress"] = "return Num(event)";
                txtV479.Attributes["onkeypress"] = "return Num(event)";
                txtV480.Attributes["onkeypress"] = "return Num(event)";
                txtV481.Attributes["onkeypress"] = "return Num(event)";
                txtV482.Attributes["onkeypress"] = "return Num(event)";
                txtV483.Attributes["onkeypress"] = "return Num(event)";
                txtV484.Attributes["onkeypress"] = "return Num(event)";
                txtV485.Attributes["onkeypress"] = "return Num(event)";
                txtV486.Attributes["onkeypress"] = "return Num(event)";
                txtV487.Attributes["onkeypress"] = "return Num(event)";
                txtV488.Attributes["onkeypress"] = "return Num(event)";
                txtV489.Attributes["onkeypress"] = "return Num(event)";
                txtV490.Attributes["onkeypress"] = "return Num(event)";
                txtV491.Attributes["onkeypress"] = "return Num(event)";
                txtV492.Attributes["onkeypress"] = "return Num(event)";
                txtV493.Attributes["onkeypress"] = "return Num(event)";
                txtV494.Attributes["onkeypress"] = "return Num(event)";
                txtV495.Attributes["onkeypress"] = "return Num(event)";
                txtV496.Attributes["onkeypress"] = "return Num(event)";
                txtV497.Attributes["onkeypress"] = "return Num(event)";
                txtV498.Attributes["onkeypress"] = "return Num(event)";
                txtV499.Attributes["onkeypress"] = "return Num(event)";
                txtV500.Attributes["onkeypress"] = "return Num(event)";
                txtV501.Attributes["onkeypress"] = "return Num(event)";
                txtV502.Attributes["onkeypress"] = "return Num(event)";
                txtV503.Attributes["onkeypress"] = "return Num(event)";
                txtV504.Attributes["onkeypress"] = "return Num(event)";
                txtV505.Attributes["onkeypress"] = "return Num(event)";
                txtV506.Attributes["onkeypress"] = "return Num(event)";
                txtV507.Attributes["onkeypress"] = "return Num(event)";
                txtV508.Attributes["onkeypress"] = "return Num(event)";
                txtV509.Attributes["onkeypress"] = "return Num(event)";
                txtV510.Attributes["onkeypress"] = "return Num(event)";
                txtV511.Attributes["onkeypress"] = "return Num(event)";
                txtV512.Attributes["onkeypress"] = "return Num(event)";
                txtV513.Attributes["onkeypress"] = "return Num(event)";
                txtV514.Attributes["onkeypress"] = "return Num(event)";
                txtV515.Attributes["onkeypress"] = "return Num(event)";
                txtV516.Attributes["onkeypress"] = "return Num(event)";
                txtV517.Attributes["onkeypress"] = "return Num(event)";
                txtV518.Attributes["onkeypress"] = "return Num(event)";
                txtV519.Attributes["onkeypress"] = "return Num(event)";
                txtV520.Attributes["onkeypress"] = "return Num(event)";
                txtV521.Attributes["onkeypress"] = "return Num(event)";
                txtV522.Attributes["onkeypress"] = "return Num(event)";
                txtV523.Attributes["onkeypress"] = "return Num(event)";
                txtV524.Attributes["onkeypress"] = "return Num(event)";
                txtV525.Attributes["onkeypress"] = "return Num(event)";
                txtV526.Attributes["onkeypress"] = "return Num(event)";
                txtV527.Attributes["onkeypress"] = "return Num(event)";
                txtV528.Attributes["onkeypress"] = "return Num(event)";
                txtV529.Attributes["onkeypress"] = "return Num(event)";
                txtV530.Attributes["onkeypress"] = "return Num(event)";
                txtV531.Attributes["onkeypress"] = "return Num(event)";
                txtV532.Attributes["onkeypress"] = "return Num(event)";
                txtV533.Attributes["onkeypress"] = "return Num(event)";
                txtV534.Attributes["onkeypress"] = "return Num(event)";
                txtV535.Attributes["onkeypress"] = "return Num(event)";
                txtV536.Attributes["onkeypress"] = "return Num(event)";
                txtV537.Attributes["onkeypress"] = "return Num(event)";
                txtV538.Attributes["onkeypress"] = "return Num(event)";
                txtV539.Attributes["onkeypress"] = "return Num(event)";
                txtV540.Attributes["onkeypress"] = "return Num(event)";
                txtV541.Attributes["onkeypress"] = "return Num(event)";
                txtV542.Attributes["onkeypress"] = "return Num(event)";
                txtV543.Attributes["onkeypress"] = "return Num(event)";
                txtV544.Attributes["onkeypress"] = "return Num(event)";
                txtV545.Attributes["onkeypress"] = "return Num(event)";
                txtV546.Attributes["onkeypress"] = "return Num(event)";
                txtV547.Attributes["onkeypress"] = "return Num(event)";
                txtV548.Attributes["onkeypress"] = "return Num(event)";
                txtV549.Attributes["onkeypress"] = "return Num(event)";
                txtV550.Attributes["onkeypress"] = "return Num(event)";
                txtV551.Attributes["onkeypress"] = "return Num(event)";
                txtV552.Attributes["onkeypress"] = "return Num(event)";
                txtV553.Attributes["onkeypress"] = "return Num(event)";
                txtV554.Attributes["onkeypress"] = "return Num(event)";
                txtV555.Attributes["onkeypress"] = "return Num(event)";
                txtV556.Attributes["onkeypress"] = "return Num(event)";
                txtV557.Attributes["onkeypress"] = "return Num(event)";
                txtV558.Attributes["onkeypress"] = "return Num(event)";
                txtV559.Attributes["onkeypress"] = "return Num(event)";
                txtV560.Attributes["onkeypress"] = "return Num(event)";
                txtV561.Attributes["onkeypress"] = "return Num(event)";
                txtV562.Attributes["onkeypress"] = "return Num(event)";
                txtV563.Attributes["onkeypress"] = "return Num(event)";
                txtV564.Attributes["onkeypress"] = "return Num(event)";
                txtV565.Attributes["onkeypress"] = "return Num(event)";
                txtV566.Attributes["onkeypress"] = "return Num(event)";
                txtV567.Attributes["onkeypress"] = "return Num(event)";
                txtV568.Attributes["onkeypress"] = "return Num(event)";
                txtV569.Attributes["onkeypress"] = "return Num(event)";
                txtV570.Attributes["onkeypress"] = "return Num(event)";
                txtV571.Attributes["onkeypress"] = "return Num(event)";
                txtV572.Attributes["onkeypress"] = "return Num(event)";
                txtV573.Attributes["onkeypress"] = "return Num(event)";
                txtV574.Attributes["onkeypress"] = "return Num(event)";
                txtV575.Attributes["onkeypress"] = "return Num(event)";
                txtV576.Attributes["onkeypress"] = "return Num(event)";
                txtV577.Attributes["onkeypress"] = "return Num(event)";
                txtV578.Attributes["onkeypress"] = "return Num(event)";
                txtV579.Attributes["onkeypress"] = "return Num(event)";
                txtV580.Attributes["onkeypress"] = "return Num(event)";
                txtV581.Attributes["onkeypress"] = "return Num(event)";
                txtV582.Attributes["onkeypress"] = "return Num(event)";
                txtV583.Attributes["onkeypress"] = "return Num(event)";
                txtV584.Attributes["onkeypress"] = "return Num(event)";
                txtV585.Attributes["onkeypress"] = "return Num(event)";
                txtV586.Attributes["onkeypress"] = "return Num(event)";
                txtV587.Attributes["onkeypress"] = "return Num(event)";
                txtV588.Attributes["onkeypress"] = "return Num(event)";
                txtV589.Attributes["onkeypress"] = "return Num(event)";
                txtV590.Attributes["onkeypress"] = "return Num(event)";
                txtV591.Attributes["onkeypress"] = "return Num(event)";
                txtV592.Attributes["onkeypress"] = "return Num(event)";
                txtV593.Attributes["onkeypress"] = "return Num(event)";
                txtV594.Attributes["onkeypress"] = "return Num(event)";
                txtV595.Attributes["onkeypress"] = "return Num(event)";
                txtV596.Attributes["onkeypress"] = "return Num(event)";
                txtV597.Attributes["onkeypress"] = "return Num(event)";
                txtV598.Attributes["onkeypress"] = "return Num(event)";
                txtV599.Attributes["onkeypress"] = "return Num(event)";
                txtV600.Attributes["onkeypress"] = "return Num(event)";
                txtV601.Attributes["onkeypress"] = "return Num(event)";
                txtV602.Attributes["onkeypress"] = "return Num(event)";
                txtV603.Attributes["onkeypress"] = "return Num(event)";
                txtV604.Attributes["onkeypress"] = "return Num(event)";
                txtV605.Attributes["onkeypress"] = "return Num(event)";
                txtV606.Attributes["onkeypress"] = "return Num(event)";
                txtV607.Attributes["onkeypress"] = "return Num(event)";
                txtV608.Attributes["onkeypress"] = "return Num(event)";
                txtV609.Attributes["onkeypress"] = "return Num(event)";
                txtV610.Attributes["onkeypress"] = "return Num(event)";
                txtV611.Attributes["onkeypress"] = "return Num(event)";
                txtV612.Attributes["onkeypress"] = "return Num(event)";
                txtV613.Attributes["onkeypress"] = "return Num(event)";
                txtV614.Attributes["onkeypress"] = "return Num(event)";
                txtV615.Attributes["onkeypress"] = "return Num(event)";
                txtV616.Attributes["onkeypress"] = "return Num(event)";
                txtV617.Attributes["onkeypress"] = "return Num(event)";
                txtV618.Attributes["onkeypress"] = "return Num(event)";
                txtV619.Attributes["onkeypress"] = "return Num(event)";
                txtV620.Attributes["onkeypress"] = "return Num(event)";
                txtV621.Attributes["onkeypress"] = "return Num(event)";
                txtV622.Attributes["onkeypress"] = "return Num(event)";
                txtV623.Attributes["onkeypress"] = "return Num(event)";
                txtV624.Attributes["onkeypress"] = "return Num(event)";
                txtV625.Attributes["onkeypress"] = "return Num(event)";
                txtV626.Attributes["onkeypress"] = "return Num(event)";
                txtV627.Attributes["onkeypress"] = "return Num(event)";
                txtV628.Attributes["onkeypress"] = "return Num(event)";
                txtV629.Attributes["onkeypress"] = "return Num(event)";
                txtV630.Attributes["onkeypress"] = "return Num(event)";
                txtV631.Attributes["onkeypress"] = "return Num(event)";
                txtV632.Attributes["onkeypress"] = "return Num(event)";
                txtV633.Attributes["onkeypress"] = "return Num(event)";
                txtV634.Attributes["onkeypress"] = "return Num(event)";
                txtV635.Attributes["onkeypress"] = "return Num(event)";
                txtV636.Attributes["onkeypress"] = "return Num(event)";
                txtV637.Attributes["onkeypress"] = "return Num(event)";
                txtV638.Attributes["onkeypress"] = "return Num(event)";
                txtV639.Attributes["onkeypress"] = "return Num(event)";
                txtV640.Attributes["onkeypress"] = "return Num(event)";
                txtV641.Attributes["onkeypress"] = "return Num(event)";
                txtV642.Attributes["onkeypress"] = "return Num(event)";
                txtV643.Attributes["onkeypress"] = "return Num(event)";
                txtV644.Attributes["onkeypress"] = "return Num(event)";
                txtV645.Attributes["onkeypress"] = "return Num(event)";
                txtV646.Attributes["onkeypress"] = "return Num(event)";
                txtV647.Attributes["onkeypress"] = "return Num(event)";
                txtV648.Attributes["onkeypress"] = "return Num(event)";
                txtV649.Attributes["onkeypress"] = "return Num(event)";
                txtV650.Attributes["onkeypress"] = "return Num(event)";
                txtV651.Attributes["onkeypress"] = "return Num(event)";
                #endregion


                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion


                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************


    }
}
