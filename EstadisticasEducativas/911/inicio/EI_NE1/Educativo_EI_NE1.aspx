<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE1(Educativo)" AutoEventWireup="true" CodeBehind="Educativo_EI_NE1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_NE1.Educativo_EI_NE1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server" Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N INICIAL NO ESCOLARIZADA"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_NE1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Atendidos_EI_NE1',true)"><a href="#" title=""><span>GRUPOS Y NI�OS ATENDIDOS</span></a></li><li onclick="openPage('Personal_EI_NE1',true)"><a href="#" title=""><span>PADRES Y PERSONAL</span></a></li><li onclick="openPage('Educativo_EI_NE1',true)"><a href="#" title="" class="activo"><span>AVANCE Y ESPACIOS</span></a></li><li onclick="openPage('Anexo_EI_NE1',false)"><a href="#" title="" ><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul> 
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
         
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

        
    <table style="width: 900px">
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblCARRERA" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="V. AVANCE DE PROGRAMA"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: justify">
                <asp:Label ID="lblInstruccionI1" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero de grupos seg�n el grado de avance del programa, por delegaci�n o municipio y localidad o colonia."
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: center">
    <table>
        <tr valign="top">
            <td >
            </td>
            <td >
                <asp:Label  ID="lblGradoA1" runat="server" CssClass="lblGrisTit" Text="INVESTIGACI�N DE CAMPO"
                    Width="90px"></asp:Label></td>
            <td >
                <asp:Label ID="lblGradoA2" runat="server" CssClass="lblGrisTit" Text="FORMACI�N DE COMIT�S" Width="80px"></asp:Label></td>
            <td >
                <asp:Label ID="lblGradoA3" runat="server" CssClass="lblGrisTit" Text="RECLUTAMIENTO DE EDUCADORES COMUNITARIOS"
                    Width="100px"></asp:Label></td>
            <td >
                <asp:Label ID="lblGradoA4" runat="server" CssClass="lblGrisTit" Text="CAPACITACI�N A EDUCADORES COMUNITARIOS" Width="100px"></asp:Label></td>
            <td >
                <asp:Label ID="lblGradoA5" runat="server" CssClass="lblGrisTit" Text="FORMACI�N DE GRUPOS DE PADRES"
                    Width="100px"></asp:Label></td>
            <td >
                <asp:Label ID="lblGradoA6" runat="server" CssClass="lblGrisTit" Text="SESI�N DE CAPACITACI�N PERMANENTE" Width="90px"></asp:Label></td>
            <td >
                <asp:Label ID="lblGradoA7" runat="server" CssClass="lblGrisTit" Text="CLAUSURA" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV1" runat="server" CssClass="lblGrisTit" Text="1"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV420" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV421" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV422" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV423" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV424" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV425" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV426" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
        </tr>
     
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV2" runat="server" CssClass="lblGrisTit" Text="2"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV427" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV428" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV429" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV430" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV431" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV432" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV433" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV3" runat="server" CssClass="lblGrisTit" Text="3"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV434" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV435" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV436" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV437" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV438" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV439" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV440" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV4" runat="server" CssClass="lblGrisTit" Text="4"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV441" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV442" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV443" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV444" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV445" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV446" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV447" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV5" runat="server" CssClass="lblGrisTit" Text="5"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV448" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV449" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV450" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV451" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV452" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV453" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV454" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right; height: 29px;">
                <asp:Label ID="lblV6" runat="server" CssClass="lblGrisTit" Text="6"></asp:Label></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV455" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV456" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV457" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV458" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV459" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV460" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
            <td style="text-align: center; height: 29px;">
                <asp:TextBox ID="txtV461" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV7" runat="server" CssClass="lblGrisTit" Text="7"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV462" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV463" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV464" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV465" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV466" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV467" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV468" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV8" runat="server" CssClass="lblGrisTit" Text="8"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV469" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV470" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV471" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV472" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV473" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV474" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV475" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV9" runat="server" CssClass="lblGrisTit" Text="9"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV476" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV477" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV478" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV479" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV480" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV481" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV482" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV10" runat="server" CssClass="lblGrisTit" Text="10"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV483" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV484" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV485" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV486" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV487" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV488" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV489" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV11" runat="server" CssClass="lblGrisTit" Text="11"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV490" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV491" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV492" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV493" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV494" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV495" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV496" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV12" runat="server" CssClass="lblGrisTit" Text="12"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV497" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV498" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV499" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV500" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV501" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV502" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV503" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV13" runat="server" CssClass="lblGrisTit" Text="13"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV504" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV505" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV506" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV507" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV508" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV509" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV510" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV14" runat="server" CssClass="lblGrisTit" Text="14"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV511" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV512" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV513" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV514" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV515" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV516" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV517" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV15" runat="server" CssClass="lblGrisTit" Text="15"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV518" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV519" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV520" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV521" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV522" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV523" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV524" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV16" runat="server" CssClass="lblGrisTit" Text="16"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV525" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV526" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV527" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV528" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV529" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV530" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV531" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV17" runat="server" CssClass="lblGrisTit" Text="17"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV532" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV533" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV534" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV535" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV536" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV537" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV538" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblV18" runat="server" CssClass="lblGrisTit" Text="18"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV539" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV540" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV541" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV542" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV543" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV544" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV545" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="height: 15px">
                <asp:Label ID="lblVTotal" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV546" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV547" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV548" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV549" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV550" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV551" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
            <td style="height: 15px; text-align: center;">
                <asp:TextBox ID="txtV552" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
        </tr>
        
    </table>
            </td>
        </tr>
        <tr>
            <td style="text-align:justify">
            <br />
                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="VI. ESPACIOS EDUCATIVOS"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero de espacios educativos seg�n su tipo, por delegaci�n o municipio y localidad o colonia."
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: center">
    <table>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td colspan="3" style="text-align: center">
                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblEspacio1" runat="server" CssClass="lblGrisTit" Height="17px" Text="PATIO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblEspacio2" runat="server" CssClass="lblGrisTit" Text="AULA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblEspacio3" runat="server" CssClass="lblGrisTit" Height="40px" Text="CASA PARTICULAR"
                    Width="80px"></asp:Label></td>
            <td>
                <asp:Label ID="lblEspacio4" runat="server" CssClass="lblGrisTit" Text="COMISAR�A" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblEspacio5" runat="server" CssClass="lblGrisTit" Text="CASA DE CULTURA"
                    Width="80px"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV553" runat="server" Columns="20" MaxLength="30" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV554" runat="server" Columns="20" MaxLength="30" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV555" runat="server" Columns="20" MaxLength="30" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI1" runat="server" CssClass="lblGrisTit" Text="1"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV556" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV557" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV558" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV559" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV560" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV561" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV562" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV563" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI2" runat="server" CssClass="lblGrisTit" Text="2"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV564" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV565" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV566" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12203"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV567" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12204"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV568" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12205"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV569" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12206"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV570" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12207"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV571" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12208"></asp:TextBox></td>
        </tr> <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI3" runat="server" CssClass="lblGrisTit" Text="3"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV572" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV573" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV574" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12303"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV575" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12304"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV576" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12305"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV577" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12306"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV578" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12307"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV579" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12308"></asp:TextBox></td>
        </tr> <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI4" runat="server" CssClass="lblGrisTit" Text="4"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV580" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV581" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV582" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12403"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV583" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12404"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV584" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12405"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV585" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12406"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV586" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12407"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV587" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12408"></asp:TextBox></td>
        </tr> <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI5" runat="server" CssClass="lblGrisTit" Text="5"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV588" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV589" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV590" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12503"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV591" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12504"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV592" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12505"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV593" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12506"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV594" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12507"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV595" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12508"></asp:TextBox></td>
        </tr> <tr>
            <td style="text-align: right; height: 23px;">
                <asp:Label ID="lblVI6" runat="server" CssClass="lblGrisTit" Text="6"></asp:Label></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV596" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12601"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV597" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12602"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV598" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12603"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV599" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12604"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV600" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12605"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV601" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12606"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV602" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12607"></asp:TextBox></td>
            <td style="text-align: center; height: 23px;">
                <asp:TextBox ID="txtV603" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12608"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI7" runat="server" CssClass="lblGrisTit" Text="7"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV604" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV605" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV606" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12703"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV607" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12704"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV608" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12705"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV609" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12706"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV610" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12707"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV611" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12708"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI8" runat="server" CssClass="lblGrisTit" Text="8"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV612" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV613" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV614" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12803"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV615" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12804"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV616" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12805"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV617" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12806"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV618" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12807"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV619" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12808"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI9" runat="server" CssClass="lblGrisTit" Text="9"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV620" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV621" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV622" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12903"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV623" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12904"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV624" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12905"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV625" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12906"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV626" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12907"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV627" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12908"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI10" runat="server" CssClass="lblGrisTit" Text="10"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV628" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV629" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13002"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV630" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13003"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV631" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13004"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV632" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13005"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV633" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13006"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV634" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13007"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV635" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13008"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI11" runat="server" CssClass="lblGrisTit" Text="11"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV636" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV637" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV638" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13103"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV639" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13104"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV640" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13105"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV641" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13106"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV642" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13107"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV643" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13108"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI12" runat="server" CssClass="lblGrisTit" Text="12"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV644" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV645" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV646" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13203"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV647" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13204"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV648" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13205"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV649" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13206"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV650" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13207"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV651" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13208"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI13" runat="server" CssClass="lblGrisTit" Text="13"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV652" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV653" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV654" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13303"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV655" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13304"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV656" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13305"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV657" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13306"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV658" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13307"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV659" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13308"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI14" runat="server" CssClass="lblGrisTit" Text="14"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV660" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV661" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV662" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13403"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV663" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13404"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV664" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13405"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV665" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13406"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV666" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13407"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV667" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13408"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI15" runat="server" CssClass="lblGrisTit" Text="15"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV668" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV669" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV670" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13503"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV671" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13504"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV672" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13505"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV673" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13506"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV674" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13507"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV675" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13508"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI16" runat="server" CssClass="lblGrisTit" Text="16"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV676" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV677" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13602"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV678" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13603"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV679" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13604"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV680" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13605"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV681" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13606"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV682" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13607"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV683" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13608"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI17" runat="server" CssClass="lblGrisTit" Text="17"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV684" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV685" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV686" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13703"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV687" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13704"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV688" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13705"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV689" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13706"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV690" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13707"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV691" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13708"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVI18" runat="server" CssClass="lblGrisTit" Text="18"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV692" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV693" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV694" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13803"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV695" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13804"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV696" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13805"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV697" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13806"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV698" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13807"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV699" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13808"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblVITotal" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV700" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV701" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV702" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13903"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV703" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13904"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV704" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13905"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV705" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13906"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV706" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13907"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV707" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13908"></asp:TextBox></td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_EI_NE1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_EI_NE1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_EI_NE1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_EI_NE1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div id="divResultado" class="divResultado"  ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 10;
                MaxRow = 40;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV553'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV554'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV555'));
                    openPage(ventana);
                    }
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
