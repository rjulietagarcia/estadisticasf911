<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true"
    CodeBehind="FiltroReportes.aspx.cs" Inherits="EstadisticasEducativas.Reportes.FiltroReportes"
    Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <center>
        <div style="text-align: center; width: 100%">
            <center>
                <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center"
                    border="0">
                    <tr>
                        <td class="EsqSupIzq">
                        </td>
                        <td class="RepSup">
                        </td>
                        <td class="EsqSupDer">
                        </td>
                    </tr>
                    <tr>
                        <td class="RepLatIzq">
                        </td>
                        <td>
                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="Titulo" style="font-weight: bold" align="center">
                                        Reportes
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:UpdatePanel ID="upBusqueda" runat="server">
                                <ContentTemplate>
                                    <table cellspacing="2" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="Label1" runat="server" Text="Ciclo Escolar:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlCiclo" runat="server" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblEntidad" runat="server" Text="Entidad:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlEntidad" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblRegion" runat="server" Text="Regi�n:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlRegion" runat="server" Enabled="false" AutoPostBack="True">
                                                        <asp:ListItem Value="-1">Seleccione una Regi�n</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td colspan = 2>
                                            <hr />
                                            </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text="Tipo de Captura :" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                    <asp:RadioButtonList ID="rdLista1" runat="server" AutoPostBack="True" 
                                                        EnableTheming="True" OnSelectedIndexChanged="rdLista1_SelectedIndexChanged" 
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="Inicio" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Fin" Value="2" Selected="True"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblNivel" runat="server" Text="Cuestionario :" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:DropDownList ID="ddlNivel" runat="server" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                           <%-- <tr>
                                                <td style="width: 133px" align="left">
                                                    <asp:Label ID="lblZona" runat="server" Width="43px" Text="Zona:" Font-Size="14px"
                                                        CssClass="lblEtiqueta"></asp:Label>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <asp:TextBox ID="txtZona" TabIndex="2" runat="server" Width="55px" MaxLength="3"></asp:TextBox>
                                                </td>
                                            </tr>--%>
                                           
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnReporte" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                            &nbsp;&nbsp;
                            <asp:Button ID="btnReporte" runat="server" Text="Ver Reporte" Width="75px" TabIndex="4"
                                OnClick="btnReporte_Click" /><br />
                        </td>
                        <td class="RepLatDer">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="EsqInfIzq">
                        </td>
                        <td class="RepInf">
                        </td>
                        <td class="EsqInfDer">
                        </td>
                    </tr>
                </table>
                <div>
                    <br />
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>
            </center>
        </div>
    </center>
</asp:Content>
