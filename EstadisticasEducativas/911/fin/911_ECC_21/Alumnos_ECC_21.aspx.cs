using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     

namespace EstadisticasEducativas._911.fin.ECC_21
{
    public partial class Alumnos_ECC_21 : System.Web.UI.Page, ICallbackEventHandler
    {

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;


        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV5.Attributes["onkeypress"] = "return Num(event)";
                txtV6.Attributes["onkeypress"] = "return Num(event)";
                txtV7.Attributes["onkeypress"] = "return Num(event)";
                txtV8.Attributes["onkeypress"] = "return Num(event)";
                txtV9.Attributes["onkeypress"] = "return Num(event)";
                txtV11.Attributes["onkeypress"] = "return Num(event)";
                txtV12.Attributes["onkeypress"] = "return Num(event)";
                txtV13.Attributes["onkeypress"] = "return Num(event)";
                txtV14.Attributes["onkeypress"] = "return Num(event)";
                txtV15.Attributes["onkeypress"] = "return Num(event)";
                txtV16.Attributes["onkeypress"] = "return Num(event)";
                txtV17.Attributes["onkeypress"] = "return Num(event)";
                txtV18.Attributes["onkeypress"] = "return Num(event)";
                txtV19.Attributes["onkeypress"] = "return Num(event)";
                txtV20.Attributes["onkeypress"] = "return Num(event)";
                txtV21.Attributes["onkeypress"] = "return Num(event)";
                txtV22.Attributes["onkeypress"] = "return Num(event)";
                txtV23.Attributes["onkeypress"] = "return Num(event)";
                txtV24.Attributes["onkeypress"] = "return Num(event)";
                txtV25.Attributes["onkeypress"] = "return Num(event)";
                txtV26.Attributes["onkeypress"] = "return Num(event)";
                txtV27.Attributes["onkeypress"] = "return Num(event)";
                txtV28.Attributes["onkeypress"] = "return Num(event)";
                txtV29.Attributes["onkeypress"] = "return Num(event)";
                txtV30.Attributes["onkeypress"] = "return Num(event)";
                txtV31.Attributes["onkeypress"] = "return Num(event)";
                txtV32.Attributes["onkeypress"] = "return Num(event)";
                txtV33.Attributes["onkeypress"] = "return Num(event)";
                txtV34.Attributes["onkeypress"] = "return Num(event)";
                txtV35.Attributes["onkeypress"] = "return Num(event)";
                txtV36.Attributes["onkeypress"] = "return Num(event)";
                txtV37.Attributes["onkeypress"] = "return Num(event)";
                txtV38.Attributes["onkeypress"] = "return Num(event)";
                #endregion


                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                {
                    pnlOficializado.Visible = true;
                    optV1.Enabled = false;
                    optV2.Enabled = false;
                    optV3.Enabled = false;
                    optV4.Enabled = false;
                    optV40.Enabled = false;
                    optV41.Enabled = false;
                }


                optV1.Attributes.Add("onclick", "OPTs2Txt()");
                optV2.Attributes.Add("onclick", "OPTs2Txt()");
                optV3.Attributes.Add("onclick", "OPTs2Txt()");
                optV4.Attributes.Add("onclick", "OPTs2Txt()");
                optV40.Attributes.Add("onclick", "OPTs2Txt()");
                optV41.Attributes.Add("onclick", "OPTs2Txt()");
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************


    }
}
