<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Alumnos Inicial)" AutoEventWireup="true" CodeBehind="AINI_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.AINI_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
    
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li><li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title="" class="activo"><span>INICIAL</span></a></li><li onclick="openPage('APRE_911_USAER_1',false)"><a href="#" title=""><span>PREESCOLAR</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PRIMARIA</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>SECUNDARIA</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
     <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
   <center> 
       
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td >
                    <table id="TABLE1" style="text-align: center; ">
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Escriba la poblaci�n total con necesidades educativas especiales atendida en educaci�n inicial, por escuela y sexo."
                                    Width="720px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="8" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblInicial" runat="server" CssClass="lblRojo" Text="INICIAL" Width="500px"></asp:Label>
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL"
                                    Width="500px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; width: 67px; height: 3px;
                                text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 3px; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="text-align: center; width: 67px;">
                                &nbsp;</td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2">
                                &nbsp;<asp:Label ID="lblTotal1" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                </td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                </td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV45" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV46" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV47" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV48" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV49" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV50" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" colspan="2">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV51" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV52" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV53" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" colspan="2">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV54" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV55" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV56" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV57" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV58" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV59" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV60" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV61" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV62" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV63" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV64" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV65" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV66" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV67" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV68" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV69" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV70" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV71" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV72" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV73" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV74" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV75" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="2" style="text-align: center">
                                <asp:TextBox ID="txtV76" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:TextBox ID="txtV77" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" 
                                Text="De los alumnos reportados en el rubro anterior, 
                                escriba la cantidad de alumnos 
                                con discapacidad, aptitudes sobresalientes u otras condiciones, 
                                seg�n las definiciones establecidas en el glosario."
                                    Width="1233px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                            <td colspan="10" style="text-align: center">
                                &nbsp;<asp:Label ID="lblAlumnosEsc" runat="server" CssClass="lblRojo" Text="ALUMNOS POR ESCUELA"
                                    Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres22" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres22" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal22" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc12" runat="server" CssClass="lblGrisTit" Text="1" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc22" runat="server" CssClass="lblGrisTit" Text="2" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc32" runat="server" CssClass="lblGrisTit" Text="3" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc42" runat="server" CssClass="lblGrisTit" Text="4" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc52" runat="server" CssClass="lblGrisTit" Text="5" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc62" runat="server" CssClass="lblGrisTit" Text="6" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc72" runat="server" CssClass="lblGrisTit" Text="7" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc82" runat="server" CssClass="lblGrisTit" Text="8" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc92" runat="server" CssClass="lblGrisTit" Text="9" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc102" runat="server" CssClass="lblGrisTit" Text="10" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV78" runat="server" Columns="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV79" runat="server" Columns="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV80" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV81" runat="server" Columns="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV82" runat="server" Columns="2" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV83" runat="server" Columns="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV84" runat="server" Columns="2" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV85" runat="server" Columns="2" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV86" runat="server" Columns="2" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV87" runat="server" Columns="2" TabIndex="20110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV88" runat="server" Columns="2" TabIndex="20111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV89" runat="server" Columns="2" TabIndex="20112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV90" runat="server" Columns="2" TabIndex="20113" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV91" runat="server" Columns="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV92" runat="server" Columns="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV93" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV94" runat="server" Columns="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV95" runat="server" Columns="2" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV96" runat="server" Columns="2" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV97" runat="server" Columns="2" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV98" runat="server" Columns="2" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV99" runat="server" Columns="2" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV100" runat="server" Columns="2" TabIndex="20210" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV101" runat="server" Columns="2" TabIndex="20211" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV102" runat="server" Columns="2" TabIndex="20212" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV103" runat="server" Columns="2" TabIndex="20213" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV104" runat="server" Columns="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV105" runat="server" Columns="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV106" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV107" runat="server" Columns="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV108" runat="server" Columns="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV109" runat="server" Columns="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV110" runat="server" Columns="2" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV111" runat="server" Columns="2" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV112" runat="server" Columns="2" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV113" runat="server" Columns="2" TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV114" runat="server" Columns="2" TabIndex="20311" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV115" runat="server" Columns="2" TabIndex="20312" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV116" runat="server" Columns="2" TabIndex="20313" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV117" runat="server" Columns="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV118" runat="server" Columns="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV119" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV120" runat="server" Columns="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV121" runat="server" Columns="2" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV122" runat="server" Columns="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV123" runat="server" Columns="2" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV124" runat="server" Columns="2" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV125" runat="server" Columns="2" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV126" runat="server" Columns="2" TabIndex="20410" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV127" runat="server" Columns="2" TabIndex="20411" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV128" runat="server" Columns="2" TabIndex="20412" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV129" runat="server" Columns="2" TabIndex="20413" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV130" runat="server" Columns="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV131" runat="server" Columns="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV132" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV133" runat="server" Columns="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV134" runat="server" Columns="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV135" runat="server" Columns="2" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV136" runat="server" Columns="2" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV137" runat="server" Columns="2" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV138" runat="server" Columns="2" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV139" runat="server" Columns="2" TabIndex="20510" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV140" runat="server" Columns="2" TabIndex="20511" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV141" runat="server" Columns="2" TabIndex="20512" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV142" runat="server" Columns="2" TabIndex="20513" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV143" runat="server" Columns="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV144" runat="server" Columns="2" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV145" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV146" runat="server" Columns="2" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV147" runat="server" Columns="2" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV148" runat="server" Columns="2" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV149" runat="server" Columns="2" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV150" runat="server" Columns="2" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV151" runat="server" Columns="2" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV152" runat="server" Columns="2" TabIndex="20610" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV153" runat="server" Columns="2" TabIndex="20611" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV154" runat="server" Columns="2" TabIndex="20612" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV155" runat="server" Columns="2" TabIndex="20613" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV156" runat="server" Columns="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV157" runat="server" Columns="2" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV158" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV159" runat="server" Columns="2" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV160" runat="server" Columns="2" TabIndex="20705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV161" runat="server" Columns="2" TabIndex="20706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV162" runat="server" Columns="2" TabIndex="20707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV163" runat="server" Columns="2" TabIndex="20708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV164" runat="server" Columns="2" TabIndex="20709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV165" runat="server" Columns="2" TabIndex="20710" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV166" runat="server" Columns="2" TabIndex="20711" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV167" runat="server" Columns="2" TabIndex="20712" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV168" runat="server" Columns="2" TabIndex="20713" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV169" runat="server" Columns="2" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV170" runat="server" Columns="2" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV171" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV172" runat="server" Columns="2" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV173" runat="server" Columns="2" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV174" runat="server" Columns="2" TabIndex="20806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV175" runat="server" Columns="2" TabIndex="20807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV176" runat="server" Columns="2" TabIndex="20808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV177" runat="server" Columns="2" TabIndex="20809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV178" runat="server" Columns="2" TabIndex="20810" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV179" runat="server" Columns="2" TabIndex="20811" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV180" runat="server" Columns="2" TabIndex="20812" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV181" runat="server" Columns="2" TabIndex="20813" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblTotal23" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV182" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV183" runat="server" Columns="3" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV184" runat="server" Columns="3" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV185" runat="server" Columns="3" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV186" runat="server" Columns="3" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV187" runat="server" Columns="3" TabIndex="20906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV188" runat="server" Columns="3" TabIndex="20907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV189" runat="server" Columns="3" TabIndex="20908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV190" runat="server" Columns="3" TabIndex="20909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV191" runat="server" Columns="3" TabIndex="20910" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV192" runat="server" Columns="3" TabIndex="20911" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV193" runat="server" Columns="3" TabIndex="20912" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV194" runat="server" Columns="3" TabIndex="20913" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 14px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AE_911_USAER_1')"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AE_911_USAER_1')"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('APRE_911_USAER_1')"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('APRE_911_USAER_1')"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
                MaxCol = 17;
                MaxRow = 31;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
        </script> 
 
</asp:Content>
