<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG1_911_112.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_112.AG1_911_112" Title="911.112(1�)" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
     
    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1250px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_112',true)"><a href="#" title="" class="activo"><span>1�</span></a></li><li onclick="openPage('AG2_911_112',false)"><a href="#" title=""><span>2�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>3�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>4�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>5�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>6�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br />
    <br />
    <br />
    <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
        
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
				     
                        <table style="text-align:center; font-weight: bold; width: 1000px;"> 
                            <tr>
                            <td>
                            <table>
                                <tr>
                                    <td colspan="14" style="padding-bottom: 10px; text-align: left">
                                        <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="I. ALUMNOS Y GRUPOS"
                                            ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="14" style="text-align: left">
                                        <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="1. Marque con una X el tipo de servicio (s�lo uno)."
                                            ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="width: 400px">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="Escuela albergue"></asp:Label>
                                         <asp:RadioButton ID="optV1" onkeydown="return Arrows(event,this.tabIndex)" runat="server" GroupName="servicio" TabIndex="10101" />
                                         <asp:TextBox ID="txtV1" runat="server" style="visibility:hidden; width:5px;" ></asp:TextBox>
                                    
                                    </td>
                                    <td colspan="2" style="width: 400px"  >
                                        
                                        <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Text="Primaria ind�gena"></asp:Label>
                                         <asp:RadioButton ID="optV2" onkeydown="return Arrows(event,this.tabIndex)" runat="server" GroupName="servicio" TabIndex="10102" />
                    <asp:TextBox ID="txtV2" runat="server"  style="visibility:hidden; width:5px;" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="14">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="14" style="padding-bottom: 20px; text-align: left">
                                        <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="2. En esta p�gina y en las siguientes escriba el total de alumnos, desglos�ndolo por grado, sexo, inscripci�n total, existencia, aprobados, edad y el n�mero de grupos existentes por grado. Verifique que la suma de los subtotales de los alumnos por edad sea igual al total."
                                            ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="14" style="padding-bottom: 20px; text-align:center">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblRojo" 
                                        Text="Estad�stica de alumnos por grado, sexo, nuevo ingreso, repetidores y edad"
                                            ></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="14" style="padding-bottom: 20px; text-align: center">
                                        <table>
                                            <tr>
                                                <td style="text-align: justify">
                                                </td>
                                                <td style="text-align: justify">
                                <asp:Label ID="lbl1" runat="server" Text="1�" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl_6" runat="server" Text="Menos de 6 a�os" CssClass="lblRojo" Width="50px" ></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="Label1" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl7" runat="server" Text="7 a�os" CssClass="lblRojo"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl8" runat="server" Text="8 a�os" CssClass="lblRojo"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os y m�s" Width="50px"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" style="text-align: justify">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                                                <td style="text-align: justify">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="NUEVO INGRESO" CssClass="lblGrisTit" Width="96px"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV4" runat="server" Columns="3"  MaxLength="3" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV5" runat="server" Columns="3"  TabIndex="20102" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV6" runat="server" Columns="3"  TabIndex="20103" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV7" runat="server" Columns="3"  TabIndex="20104" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV8" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20105"  CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV9" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20106"  CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV10" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20107" CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV11" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20108"  CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV12" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20109"  CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20110"  CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV14" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20111"  CssClass="lblNegro">0</asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV15" runat="server" Columns="4"  TabIndex="20112" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="REPETIDORES" CssClass="lblGrisTit"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtVblack1" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                             BorderStyle="Solid" Columns="3" Enabled="true" ReadOnly="true" TabIndex="20201" MaxLength="3"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV16" runat="server" Columns="3"  TabIndex="20202" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV17" runat="server" Columns="3"  TabIndex="20203" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV18" runat="server" Columns="3"  TabIndex="20204" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV19" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20205"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV20" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20206"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV21" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20207"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV22" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20208"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV23" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20209"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV24" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20210"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV25" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20211"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV26" runat="server" Columns="4"  TabIndex="20212" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                </td>
                                                <td style="text-align: justify">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" style="text-align: justify">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                                                <td style="text-align: justify">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="NUEVO INGRESO" CssClass="lblGrisTit"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV27" runat="server" Columns="3"  TabIndex="20301" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV28" runat="server" Columns="3"  TabIndex="20302" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV29" runat="server" Columns="3"  TabIndex="20303" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV30" runat="server" Columns="3"  TabIndex="20304" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV31" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20305"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV32" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20306"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV33" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20307"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV34" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV35" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20309"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV36" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV37" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20311"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV38" runat="server" Columns="4"  TabIndex="20312" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="REPETIDORES" CssClass="lblGrisTit"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtVblack2" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="3" Enabled="true" ReadOnly="true" TabIndex="20401" MaxLength="3"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV39" runat="server" Columns="3"  TabIndex="20402" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV40" runat="server" Columns="3"  TabIndex="20403" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV41" runat="server" Columns="3"  TabIndex="20404" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV42" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20405"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV43" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20406"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV44" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20407"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV45" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20408"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV46" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20409"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV47" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20410"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV48" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20411"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV49" runat="server" Columns="4"  TabIndex="20412" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                </td>
                                                <td style="text-align: justify">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                </td>
                                                <td style="text-align: justify">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="SUBTOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV50" runat="server" Columns="3"  TabIndex="20501" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV51" runat="server" Columns="3"  TabIndex="20502" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV52" runat="server" Columns="3"  TabIndex="20503" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV53" runat="server" Columns="3"  TabIndex="20504" MaxLength="3"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV54" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20505"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV55" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20506"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV56" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20507"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV57" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV58" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20509"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV59" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20510"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV60" runat="server" Columns="3" MaxLength="3" 
                                    TabIndex="20511"  CssClass="lblNegro"></asp:TextBox></td>
                                                <td style="width: 60px">
                                <asp:TextBox ID="txtV61" runat="server" Columns="4"  TabIndex="20512" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        <tr></tr>
                    </table>
                            </td>
                        </tr>
                    </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('AG2_911_112',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG2_911_112',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
        				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
    <script type="text/javascript" language="javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
        GetTabIndexes();
        
        var CambiarPagina = "";
        var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
        Disparador(<%=this.hidDisparador.Value %>);   
        
        function PintaOPTs(){
                     marcar('V1');
                     marcar('V2');
                }  
                function marcar(variable){
                     var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                     if (txtv != null) {
                         var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                         if (txtv.value == 'X'){
                             chk.checked = true;
                         } else {
                             chk.checked = false;
                         } 
                     }
                }
        function OPTs2Txt(){
                     marcarTXT('V1');
                     marcarTXT('V2');
                      
                }   
                
          function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                } 
                PintaOPTs(); 
        
        
    </script>
</asp:Content>
