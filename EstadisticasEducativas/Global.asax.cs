using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.IO;


namespace EstadisticasEducativas
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            try
            {
                DoTask();
            }
            catch (Exception ex)
            { 
            
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        static void DoTask()
        {
            string path = HttpContext.Current.Server.MapPath("~/PDFReports");
                string[] filePaths = Directory.GetFiles(path);
                foreach (string filePath in filePaths)
                    File.Delete(filePath);
        }


    }
}