using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;
using EstadisticasEducativas._911.fin.Reportes;
using System.Text;


using System.Data.SqlClient;
using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
//using EstadisticasEducativas._911.fin.Reportes.DataSets;
//using EstadisticasEducativas._911.fin.Reportes.Crystals;

namespace EstadisticasEducativas._911.fin.Reportes
{
    public partial class Reporte : System.Web.UI.Page
    {


        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
                     
            try
            {
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);

                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
                int PDF = 0;
                if (Request.Params["OCE"] != null)
                {
                    int.TryParse(Request.Params["OCE"], out PDF);
                }
                //  1->Comprobante     2->Vacio            3->Lleno              4-> Desoficializar
                byte[] reporte = null;
                switch (PDF)
                {
                    case 1:
                        reporte = wsEstadisticas.ImprimeComprobanteCaptura911(controlDP);
                        break;
                    case 2:
                        reporte = wsEstadisticas.ImprimeCuestionario911(controlDP, false);
                        // ImprimirQuiestionario(controlDP, false);
                        break;
                    case 3:
                        reporte = wsEstadisticas.ImprimeCuestionario911(controlDP, true);
                        //ImprimirQuiestionario(controlDP, true);
                        break;
                    case 4:
                        Label1.Visible = false;
                        controlDP.Estatus = 0;
                        wsEstadisticas.Oficializar_Cuestionario(controlDP, int.Parse(User.Identity.Name.Split('|')[0]));
                        Response.Write("La estad�stica fue desoficializada");
                        break;
                    default:
                        Response.Write("no se localiz� tipo de documento a imprimir");
                        break;
                }
                if (reporte != null)
                    ImprimeReporte(reporte);
                else
                    throw new Exception("Ocurri� un error al obtener el Reporte");

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + " \\n " + ex.StackTrace);
            }
        }
     
       
        //private byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        //{
        //    int b1;
        //    System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
        //    while ((b1 = theStream.ReadByte()) != -1)
        //    {
        //        tempStream.WriteByte(((byte)b1));
        //    }
        //    return tempStream.ToArray();
        //}

        protected void DoDeleteFile()
        {
            if (Session["lGUID"] != null)
            {
                string lGUID = Session["lGUID"].ToString();
                if (System.IO.File.Exists(Server.MapPath("~/PDFReports/EE" + lGUID + ".pdf")))
                    System.IO.File.Delete(Server.MapPath("~/PDFReports/EE" + lGUID + ".pdf"));
            }
        }

        private void ImprimeReporte(byte[] aArchivo)
        {
            DoDeleteFile();
            System.Guid lGUID = Guid.NewGuid();
            System.IO.FileStream lFileStream = new System.IO.FileStream(Server.MapPath("~/PDFReports/EE" + lGUID.ToString() + ".pdf"), System.IO.FileMode.CreateNew);
            try
            {
                lFileStream.Write(aArchivo, 0, aArchivo.Length - 1);
            }
            finally
            {
                lFileStream.Flush();
                lFileStream.Close();
            }
            Session["lGUID"] = lGUID.ToString();
            Response.Redirect("~/PDFReports/EE" + lGUID.ToString() + ".pdf");
        }

        //private SqlConnection cnn_DB911()
        //{
        //    AppSettingsReader reader = new AppSettingsReader();
        //    SqlConnection cnn = new System.Data.SqlClient.SqlConnection();
        //    cnn.ConnectionString = reader.GetValue("DB911ConnectionString", typeof(string)).ToString();
        //    cnn.FireInfoMessageEventOnUserErrors = false;
        //    return cnn;
        //}

        //private SqlConnection cnn_SENLMaster()
        //{
        //    AppSettingsReader reader = new AppSettingsReader();
        //    SqlConnection cnn = new System.Data.SqlClient.SqlConnection();
        //    cnn.ConnectionString = reader.GetValue("SENLMasterConnectionString", typeof(string)).ToString();
        //    cnn.FireInfoMessageEventOnUserErrors = false;
        //    return cnn;
        //}

        /// ImprimeReporte y crea la session que lo maneja
    }
}
