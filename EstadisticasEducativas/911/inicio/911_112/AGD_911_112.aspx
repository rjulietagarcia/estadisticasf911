<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.112(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_112.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_112.AGD_911_112" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header">


    <table style="width:100%">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1250px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_112',true)"><a href="#" title=""><span>1�</span></a></li><li onclick="openPage('AG2_911_112',true)"><a href="#" title=""><span>2�</span></a></li><li onclick="openPage('AG3_911_112',true)"><a href="#" title=""><span>3�</span></a></li><li onclick="openPage('AG4_911_112',true)"><a href="#" title=""><span>4�</span></a></li><li onclick="openPage('AG5_911_112',true)"><a href="#" title=""><span>5�</span></a></li><li onclick="openPage('AG6_911_112',true)"><a href="#" title=""><span>6�</span></a></li><li onclick="openPage('AGT_911_112',true)"><a href="#" title="" ><span>TOTAL</span></a></li><li onclick="openPage('AGD_911_112',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li><li onclick="openPage('Personal_911_112',false)"><a href="#" title=""><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
<center>
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq">&nbsp; </td>
				    <td>
            <%--a aqui--%>


        
                    <table  style="width: 1100px;">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                        <div style="text-align:justify">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="3. Escriba la cantidad de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, desglos�ndola por sexo."
                        Width="100%"></asp:Label>
                                        </div>
                                        <br />
                                            <table>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CEGUERA"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV345" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10101"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV346" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10102"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV347" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10103"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="DISCAPACIDAD VISUAL"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV348" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10201"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV349" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10202"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV350" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10203"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SORDERA"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV351" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10301"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV352" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10302"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV353" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10303"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="DISCAPACIDAD AUDITIVA"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV354" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10401"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV355" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10402"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV356" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10403"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="DISCAPACIDAD MOTRIZ"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV357" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10501"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV358" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10502"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV359" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10503"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="DISCAPACIDAD INTELECTUAL"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV360" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10601"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV361" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10602"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV362" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10603"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRA DISCAPACIDAD"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV363" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10701"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV364" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10702"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV365" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10703"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV366" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10801"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV367" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10802"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV368" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10803"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV369" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10901"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV370" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10902"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV371" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10903"></asp:TextBox></td>
                                    </tr>
                                </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div style="text-align:justify">
                                            <asp:Label ID="Label13" CssClass="lblRojo" Font-Bold="True" runat="server" Text="4. Escriba la cantidad de alumnos con Necesidades Educativas Especiales (NEE).Independientemente de que presenten o no alguna discapacidad, desglos�ndola por sexo."></asp:Label>
                                            </div>
                                            <br />
                        <table style="width: 156px; height: 65px">
                        <tr>
                        <td align="center">
                        <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                                Width="70%" Height="17px"></asp:Label>
                        </td>
                        <td style="width: 73px" align="center">
                            <asp:TextBox ID="txtV372" runat="server" Width="33px" TabIndex="11001"></asp:TextBox></td>
                        </tr>
                        <tr>
                        <td align="center">
                            <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                                Width="68%" Height="17px"></asp:Label>
                        </td>
                        <td style="width: 73px" align="center">
                            <asp:TextBox ID="txtV373" runat="server" Width="33px" TabIndex="11101"></asp:TextBox></td>
                        </tr>
                        <tr>
                        <td style="text-align:center;" align="center">
                            <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                Text="TOTAL" Width="41%"></asp:Label>
                        </td>
                         <td style="width: 73px" align="center">
                         <asp:TextBox ID="txtV374" runat="server" Width="33px" TabIndex="11201"></asp:TextBox></td>
                        </tr>
                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div style="text-align:justify">
                             <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. Del total de alumnos inscritos en primer grado de primaria, escriba la cantidad de los que cursaron educacion preescolar, seg�n los a�os cursados, desglos�ndola por sexo."></asp:Label>
                             </div>
                             <br />
                         <table>
                             <tr><td colspan="2"></td><td align="center">
                                 <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                     Text="HOMBRES"></asp:Label></td><td align="center">
                                 <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                     Text="MUJERES"></asp:Label></td><td align="center" style="width: 41px">
                                 <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                     Text="TOTAL"></asp:Label></td></tr>
                             <tr>
                                 <td rowspan="2" style="width: 73px">
                                     <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="UN A�O CURSADO" Width="78%"></asp:Label></td>
                                 <td align="left" style="width: 150px">
                                     <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="17px"
                                         Text="NUEVO INGRESO" Width="97%"></asp:Label></td><td align="center">
                                             <asp:TextBox ID="txtV375" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11301"></asp:TextBox></td><td align="center">
                                             <asp:TextBox ID="txtV376" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11302"></asp:TextBox></td><td align="center" style="width: 41px">
                                             <asp:TextBox ID="txtV377" runat="server" Height="16px" Width="26px" MaxLength="3" TabIndex="11303"></asp:TextBox></td></tr>
                             <tr><td align="left">
                                 <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="17px"
                                     Text="REPETIDORES" Width="38%"></asp:Label></td><td align="center">
                                         <asp:TextBox ID="txtV378" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11401"></asp:TextBox ></td><td align="center">
                                         <asp:TextBox ID="txtV379" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11402"></asp:TextBox></td><td align="center" style="width: 41px">
                                         <asp:TextBox ID="txtV380" runat="server" Height="16px" Width="26px" MaxLength="3" TabIndex="11403"></asp:TextBox></td></tr>
                             <tr>
                                 <td rowspan="2" style="width: 73px">
                                     <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="DOS A�OS CURSADOS" Width="89%"></asp:Label></td>
                                 <td align="left">
                                     <asp:Label ID="Label23" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="17px"
                                         Text="NUEVO INGRESO" Width="95%"></asp:Label></td><td align="center">
                                             <asp:TextBox ID="txtV381" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11501"></asp:TextBox></td><td align="center">
                                             <asp:TextBox ID="txtV382" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11502"></asp:TextBox></td><td align="center" style="width: 41px">
                                             <asp:TextBox ID="txtV383" runat="server" Height="16px" Width="26px" MaxLength="3" TabIndex="11503"></asp:TextBox></td></tr>
                             <tr><td align="left">
                                 <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="17px"
                                     Text="REPETIDORES" Width="38%"></asp:Label></td><td align="center">
                                         <asp:TextBox ID="txtV384" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11601"></asp:TextBox></td><td align="center">
                                         <asp:TextBox ID="txtV385" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11602"></asp:TextBox></td><td align="center" style="width: 41px">
                                         <asp:TextBox ID="txtV386" runat="server" Height="16px" Width="26px" MaxLength="3" TabIndex="11603"></asp:TextBox></td></tr>
                             <tr>
                                 <td rowspan="2" style="width: 73px">
                                     <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="TRES A�OS CURSADOS" Width="97%"></asp:Label></td>
                                 <td align="left">
                                     <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="17px"
                                         Text="NUEVO INGRESO" Width="96%"></asp:Label></td><td align="center">
                                             <asp:TextBox ID="txtV387" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11701"></asp:TextBox></td><td align="center">
                                             <asp:TextBox ID="txtV388" runat="server" Height="16px" Width="26px" MaxLength="2" TabIndex="11702"></asp:TextBox></td><td align="center" style="width: 41px">
                                             <asp:TextBox ID="txtV389" runat="server" Height="16px" Width="26px" MaxLength="3" TabIndex="11703"></asp:TextBox></td></tr>
                             <tr><td align="left">
                                 <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="17px"
                                     Text="REPETIDORES" Width="38%"></asp:Label></td><td align="center">
                                         <asp:TextBox ID="txtV390" runat="server" Width="26px" MaxLength="2" TabIndex="11801"></asp:TextBox></td><td align="center">
                                         <asp:TextBox ID="txtV391" runat="server" Height="16px" Width="26px" TabIndex="11802" MaxLength="2"></asp:TextBox></td><td align="center" style="width: 41px">
                                         <asp:TextBox ID="txtV392" runat="server" Height="16px" Width="26px" TabIndex="11803" MaxLength="3"></asp:TextBox></td></tr>
                             <tr><td colspan="3"></td><td align="center">
                                 <asp:Label ID="Label30" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                     Text="TOTAL" Width="78%"></asp:Label></td><td align="center" style="width: 41px">
                                 <asp:TextBox ID="txtV393" runat="server" MaxLength="3" Width="26px" TabIndex="11901"></asp:TextBox></td></tr>
                         </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div style="text-align:justify">
                             <asp:Label ID="Label31" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6. Registre el total de grupos existentes, por grado."></asp:Label>
                                        </div>
                             <br />
                         <table>
                             <tr><td style="width: 68px; height: 19px;" align="left"></td><td style="height: 19px; width: 68px;">
                                 <asp:Label ID="Label32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                     Text="GRUPOS"></asp:Label></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label33" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="PRIMERO" Width="97%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV394" runat="server" MaxLength="2" Width="26px" TabIndex="12001"></asp:TextBox></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label34" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="SEGUNDO" Width="86%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV395" runat="server" MaxLength="2" Width="26px" TabIndex="12101"></asp:TextBox></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="TERCERO" Width="93%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV396" runat="server" MaxLength="2" Width="26px" TabIndex="12201"></asp:TextBox></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label36" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="CUARTO" Width="90%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV397" runat="server" MaxLength="2" Width="26px" TabIndex="12301"></asp:TextBox></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label37" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="QUINTO" Width="84%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV398" runat="server" MaxLength="2" Width="26px" TabIndex="12401"></asp:TextBox></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label38" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                         Text="SEXTO" Width="71%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV399" runat="server" MaxLength="2" Width="26px" TabIndex="12501"></asp:TextBox></td>
                             </tr>
                             <tr>
                                 <td style="width: 68px" align="left">
                                     <asp:Label ID="Label39" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                         Text="TOTAL" Width="38%"></asp:Label></td>
                                 <td style="width: 68px">
                                     <asp:TextBox ID="txtV400" runat="server" MaxLength="3" Width="26px" TabIndex="12601"></asp:TextBox></td></tr>
                         </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div style="text-align:justify">
                            <asp:Label ID="Label48" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7. Escriba la cantidad de directivos, docentes y promotores con grupo, por grado.IMPORTANTE: Si un profesor atiende a m�s de un grado, an�telo en el rubro correspondiente; el total debe coincidir con la suma de directivo con grupo m�s personal docente m�s promotores de la pregunta 1 de la secci�n III.PERSONAL POR FUNCI�N."></asp:Label>
                            </div>
                            <br />
                                            <table style="width: 144px; height: 168px">
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label41" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="PRIMERO" Width="50%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV401" runat="server" MaxLength="2" Width="26px" TabIndex="12701"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label42" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="SEGUNDO" Width="51%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV402" runat="server" MaxLength="2" Width="26px" TabIndex="12801"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label43" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="TERCERO" Width="52%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV403" runat="server" MaxLength="2" Width="26px" TabIndex="12901"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label44" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="CUARTO" Width="50%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV404" runat="server" MaxLength="2" Width="26px" TabIndex="13001"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label45" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="QUINTO" Width="49%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV405" runat="server" MaxLength="2" Width="26px" TabIndex="13101"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label46" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="SEXTO" Width="49%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV406" runat="server" MaxLength="2" Width="26px" TabIndex="13201"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label40" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                            Text="MAS DE UN GRADO" Width="92%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV407" runat="server" MaxLength="2" Width="26px" TabIndex="13301"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 86px" align="left">
                                        <asp:Label ID="Label47" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                            Text="TOTAL" Width="38%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV408" runat="server" MaxLength="3" Width="26px" TabIndex="13401"></asp:TextBox></td>
                                </tr>
                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 10px">
                            </td>
                            <td style="vertical-align: text-top; text-align: center">
                                <table>
                                    <tr>
                                        <td style="width: 521px">
                                        <div style="text-align:justify">
                                        <asp:Label ID="Label63" runat="server" CssClass="lblGrisTit" Font-Size="16px" Font-Bold="True" Text="II.LENGUA MATERNA"></asp:Label>
                                        <br /><br />
                                        
                            <asp:Label ID="Label49" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Lengua materna. Escriba el nombre de las lenguas maternas habladas por los docentes, seg�n la tabla del reverso de la p�gina anterior, y la cantidad de maestros que hablan cada una de ellas."></asp:Label>
                                        </div>
                                        <br />
                                            <table>
                                                <tr>
                                                    <td  >
                                                        <asp:Label ID="Label57" runat="server" CssClass="lblGrisTit" Text="Clave" Width="100%"></asp:Label></td>
                                                    <td   style="width: 378px">
                                                        <asp:Label ID="lbl3a" runat="server" CssClass="lblGrisTit" Text="Lengua Materna"
                                                            Width="100%"></asp:Label></td>
                                                    <td  >
                                                        <asp:Label ID="lbl4a" runat="server" CssClass="lblGrisTit" Text="N�mero de maestros"
                                                            Width="100%"></asp:Label></td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                               <asp:TextBox ReadOnly="true" ID="txtV409" runat="server" Width="48px" TabIndex="13501"></asp:TextBox></td>
                                                    <td   style="width: 378px">
                                                        <asp:TextBox ID="txtV410" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV410" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV410','ctl00_cphMainMaster_txtV409')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                   <asp:TextBox ID="txtV411" runat="server" Width="48px" TabIndex="13502"></asp:TextBox></td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                               <asp:TextBox ReadOnly="true" ID="txtV412" runat="server" Width="48px" TabIndex="13601"></asp:TextBox></td>
                                                    <td   style="width: 378px">
                                                        <asp:TextBox ID="txtV413" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV413" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV413','ctl00_cphMainMaster_txtV412')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                   <asp:TextBox ID="txtV414" runat="server" Width="48px" TabIndex="13602"></asp:TextBox></td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                               <asp:TextBox ReadOnly="true" ID="txtV415" runat="server" Width="48px" TabIndex="13701"></asp:TextBox></td>
                                                    <td   style="width: 378px">
                                                        <asp:TextBox ID="txtV416" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV416" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV416','ctl00_cphMainMaster_txtV415')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                   <asp:TextBox ID="txtV417" runat="server" Width="48px" TabIndex="13702"></asp:TextBox></td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                               <asp:TextBox ReadOnly="true" ID="txtV418" runat="server" Width="48px" TabIndex="13801"></asp:TextBox></td>
                                                    <td   style="width: 378px">
                                                        <asp:TextBox ID="txtV419" runat="server" ReadOnly="true" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV419" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV419','ctl00_cphMainMaster_txtV418')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                   <asp:TextBox ID="txtV420" runat="server" Width="48px" TabIndex="13802"></asp:TextBox></td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 521px">
                                        <br />
                                        <div style="text-align:justify">
                             <asp:Label ID="Label50" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Lenguas de la comunidad. Escriba el nombre de la(s) lengua(s) materna(s) que se habla(n) en la comunidad, seg�n la tabla del reverso de la p�gina anterior."></asp:Label>
                                        </div>
                                        <br />
                                            <table>
                                                <tr>
                                                    <td  >
                                                        <asp:Label ID="Label58" runat="server" CssClass="lblGrisTit" Text="Clave" Width="100%"></asp:Label></td>
                                                    <td   style="width: 381px">
                                                        <asp:Label ID="Label59" runat="server" CssClass="lblGrisTit" Text="Lengua Materna"
                                                            Width="100%"></asp:Label></td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                                     <asp:TextBox ReadOnly="true" ID="txtV421" runat="server" Width="48px" TabIndex="13901"></asp:TextBox></td>
                                                    <td   style="width: 381px">
                                                        <asp:TextBox ID="txtV422" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV422" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV422','ctl00_cphMainMaster_txtV421')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                                     <asp:TextBox ReadOnly="true" ID="txtV423" runat="server" Width="48px" TabIndex="14001"></asp:TextBox></td>
                                                    <td   style="width: 381px">
                                                        <asp:TextBox ID="txtV424" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV424" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV424','ctl00_cphMainMaster_txtV423')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                                     <asp:TextBox ReadOnly="true" ID="txtV425" runat="server" Width="48px" TabIndex="14101"></asp:TextBox></td>
                                                    <td   style="width: 381px">
                                                        <asp:TextBox ID="txtV426" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV426" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV426','ctl00_cphMainMaster_txtV425')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >
                                     <asp:TextBox ReadOnly="true" ID="txtV427" runat="server" Width="48px" TabIndex="14201"></asp:TextBox></td>
                                                    <td   style="width: 381px">
                                                        <asp:TextBox ID="txtV428" runat="server" ReadOnly="True" Style="visibility: hidden"
                                                            Width="5px">0</asp:TextBox>
                                                        <asp:DropDownList ID="optV428" runat="server" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV428','ctl00_cphMainMaster_txtV427')"
                                                            Width="340px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  >
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 521px">
                                        <br />
                                        <div style="text-align:justify">
                             <asp:Label ID="Label51" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Dominio de la lengua. Escriba la cantidad de personal que habla, lee y escribe la lengua materna de la comunidad."></asp:Label>
                                        </div>
                             <br />
                             <table style="width: 274px">
                                <tr><td style="width: 146px" align="left"></td><td style="width: 41px">
                                    <asp:Label ID="Label60" runat="server" CssClass="lblGrisTit" Text="Habla" Width="100%"></asp:Label></td><td style="width: 40px">
                                    <asp:Label ID="Label61" runat="server" CssClass="lblGrisTit" Text="Lee" Width="100%"></asp:Label></td><td>
                                    <asp:Label ID="Label62" runat="server" CssClass="lblGrisTit" Text="Escribe" Width="100%"></asp:Label></td></tr>
                                <tr><td style="width: 146px" align="left">
                                    <asp:Label ID="Label52" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                        Text="DIRECTOR CON GRUPO" Width="99%"></asp:Label></td><td style="width: 41px">
                                    <asp:TextBox ID="txtV429" runat="server" MaxLength="2" Width="26px" TabIndex="14301"></asp:TextBox></td><td style="width: 40px">
                                    <asp:TextBox ID="txtV430" runat="server" MaxLength="2" Width="26px" TabIndex="14302"></asp:TextBox></td><td>
                                    <asp:TextBox ID="txtV431" runat="server" MaxLength="2" Width="26px" TabIndex="14303"></asp:TextBox></td></tr>
                                <tr><td style="width: 146px" align="left">
                                    <asp:Label ID="Label53" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                        Text="DIRECTOR SIN GRUPO" Width="96%"></asp:Label></td><td style="width: 41px">
                                    <asp:TextBox ID="txtV432" runat="server" MaxLength="2" Width="26px" TabIndex="14401"></asp:TextBox></td><td style="width: 40px">
                                    <asp:TextBox ID="txtV433" runat="server" MaxLength="2" Width="26px" TabIndex="14402"></asp:TextBox></td><td>
                                    <asp:TextBox ID="txtV434" runat="server" MaxLength="2" Width="26px" TabIndex="14403"></asp:TextBox></td></tr>
                                <tr><td style="width: 146px" align="left">
                                    <asp:Label ID="Label54" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                        Text="DOCENTE" Width="53%"></asp:Label></td><td style="width: 41px">
                                    <asp:TextBox ID="txtV435" runat="server" MaxLength="2" Width="26px" TabIndex="14501"></asp:TextBox></td><td style="width: 40px">
                                    <asp:TextBox ID="txtV436" runat="server" MaxLength="2" Width="26px" TabIndex="14502"></asp:TextBox></td><td>
                                    <asp:TextBox ID="txtV437" runat="server" MaxLength="2" Width="26px" TabIndex="14503"></asp:TextBox></td></tr>
                                <tr><td style="width: 146px" align="left">
                                    <asp:Label ID="Label55" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                        Text="PROMOTOR" Width="58%"></asp:Label></td><td style="width: 41px">
                                    <asp:TextBox ID="txtV438" runat="server" MaxLength="2" Width="26px" TabIndex="14601"></asp:TextBox></td><td style="width: 40px">
                                    <asp:TextBox ID="txtV439" runat="server" MaxLength="2" Width="26px" TabIndex="14602"></asp:TextBox></td><td>
                                    <asp:TextBox ID="txtV440" runat="server" MaxLength="2" Width="26px" TabIndex="14603"></asp:TextBox></td></tr>
                                <tr><td style="width: 146px" align="left">
                                    <asp:Label ID="Label56" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                        Text="TOTAL" Width="38%"></asp:Label></td><td style="width: 41px">
                                    <asp:TextBox ID="txtV441" runat="server" MaxLength="3" Width="26px" TabIndex="14701"></asp:TextBox></td><td style="width: 40px">
                                    <asp:TextBox ID="txtV442" runat="server" MaxLength="3" Width="26px" TabIndex="14702"></asp:TextBox></td><td>
                                    <asp:TextBox ID="txtV443" runat="server" MaxLength="3" Width="26px" TabIndex="14703"></asp:TextBox></td></tr>
                             </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                
        <br />
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGT_911_112',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_112',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px">&nbsp;</td>
                <td ><span  onclick="openPage('Personal_911_112',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_112',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"> </div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       
        
        <script type="text/javascript" language="javascript">
                MaxCol = 10;
                MaxRow = 48;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		        Disparador(<%=hidDisparador.Value %>);
        </script>
         
</asp:Content>
