using System;
using System.Data;
using System.Configuration;
using System.Web;
using SEroot.WsEstadisticasEducativas;
using System.Web.UI.WebControls;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using System.Text.RegularExpressions;

using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.Web.Security;
//using System.Web.SessionState;


namespace EstadisticasEducativas._911
{
    public class Class911
    {
        // *** Asignar Tiempo para disparo de guardado de pantallas autmoatico ****

        public static string TiempoAutoGuardado()
        {
            SEroot.Seguridad.Seguridad seg = new SEroot.Seguridad.Seguridad();
            string tiempoAutomatico = seg.LoadParametroSistema(401).Valor;

            return tiempoAutomatico;
        }

        //*** Metodos basico que ejecutan las paginas  >>>>>>>>>>>>>>>>>>>>>>>>>

        #region Session controlDP
        public static ControlDP GetControlSeleccionado(HttpContext pagina)
        {
            ControlDP controlDP = null;
            if (pagina.Session["controlDP"] != null)
            {
                controlDP = (ControlDP)pagina.Session["controlDP"];
            }
            else
            {
                pagina.Response.Redirect("../../SinControl.aspx");
            }
            return controlDP;
        }
        public static void SetControlSeleccionado(HttpContext pagina,ControlDP controlDP)
        {
            pagina.Session["controlDP"] = controlDP;
        }
        #endregion

       
        #region Lista Cajas
        public static string ListaCajasAnexos(System.Web.UI.Page Page)
        {
            string salida = "";
            ContentPlaceHolder mainContent = (ContentPlaceHolder)Page.Master.FindControl("cphMainMaster");
            for (int NoVariable = 0; NoVariable < 700; NoVariable++)
            {
                TextBox txtV = (TextBox)mainContent.FindControl("txtVANX" + NoVariable.ToString());
                if (txtV != null)
                {
                    salida = salida + txtV.ClientID + "," + NoVariable.ToString() + "|";
                }
            }
            return salida;
        }
        public static string ListaCajas_Carreras(System.Web.UI.Page Page)
        {
            string salida = "";
            ContentPlaceHolder mainContent = (ContentPlaceHolder)Page.Master.FindControl("cphMainMaster");
            for (int NoVariable = 0; NoVariable < 60; NoVariable++)
            {
                TextBox txtVA = (TextBox)mainContent.FindControl("txtVA" + NoVariable.ToString());
                if (txtVA != null)
                {
                    salida = salida + txtVA.ClientID + "," + NoVariable.ToString() + "|";
                }
            }
            return salida;
        }
        public static string ListaCajas(System.Web.UI.Page Page, ControlDP controlDP)
        {
            string salida = "";
            ContentPlaceHolder mainContent = (ContentPlaceHolder)Page.Master.FindControl("cphMainMaster");
            for (int NoVariable = 0; NoVariable < controlDP.CuestionarioDP.TotalVariables; NoVariable++)
            {

                TextBox txtV = (TextBox)mainContent.FindControl("txtV" + NoVariable.ToString());
                if (txtV != null)
                {
                    salida = salida + txtV.ClientID + "," + NoVariable.ToString() + "|";
                }
            }
            return salida;
        }
        #endregion

        public static void LlenarDatosDB11(System.Web.UI.Page Pagina, ControlDP controlDP, int Documento, int ID_Carrera, string Lista_cajas)
        {
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();

            string[] Cajas = Lista_cajas.Split('|');

            StringBuilder listaNumerosVariables = new StringBuilder();
            for (int NoVariable = 0; NoVariable < Cajas.Length - 1; NoVariable++)
            {
                if (Cajas[NoVariable].Split(',')[1] != "0")
                    listaNumerosVariables.Append(Cajas[NoVariable].Split(',')[1]);
                if (NoVariable < Cajas.Length - 2 && listaNumerosVariables.Length > 0)
                    listaNumerosVariables.Append(",");
            }
            VariablesDP[] lista_VariableDP = wsEstadisiticas.LeerVariableEncuesta_Lista(controlDP, Documento, ID_Carrera, listaNumerosVariables.ToString());

            foreach (VariablesDP varDP in lista_VariableDP)
            {
                string NombreCaja = ""; //1... Variable Custrionario 2...Especial 3... Anexo  4... Carreras
                if (Documento == 1) NombreCaja = "txtV";
                if (Documento == 3) NombreCaja = "txtVANX";
                if (Documento == 4) NombreCaja = "txtVA";
                if (Documento == Doc_AnexoInicio) NombreCaja = "txtVANX";

                ContentPlaceHolder mainContent = (ContentPlaceHolder)Pagina.Master.FindControl("cphMainMaster");
                TextBox txtV = (TextBox)mainContent.FindControl(NombreCaja + varDP.var);
                if (txtV != null)
                {
                    txtV.Text = "";

                    txtV.Attributes["onkeydown"] = "return Arrows(event,this.tabIndex)";

                    if (varDP.valor != null)
                    {
                        if (varDP.valor.ToString() != "0")
                            txtV.Text = varDP.valor.ToString();
                        else
                        { txtV.Text = "0"; }
                        if (varDP.valor.ToString() != "")
                        {
                            txtV.Text = varDP.valor.ToString().ToUpper();
                            
                        }
                        else
                        { txtV.Text = "_"; }
                        
                    }
                    else
                    {
                        if (varDP.tipo == Var_TipoNumerico)
                        {
                            txtV.Text = "0";
                        }
                        else
                        {
                            txtV.Text = "_";
                        }
                    }

                    if (varDP.tipo == Var_TipoNumerico)
                        txtV.Attributes["onkeypress"] = "return Num(event)";

                    //txtV.ReadOnly = varDP.ReadOnly;
                    txtV.ReadOnly = false;
                    if (controlDP.Estatus != 0)
                    {
                        txtV.ReadOnly = varDP.ReadOnly;
                    }
                    if (varDP.PreLlenado)
                    {
                        txtV.CssClass = "txtVarSW";
                    }
                    else
                    {
                        txtV.CssClass = "txtVarCorrecto";
                    }
                    txtV.Font.Bold = true;
                }
            }

        }

        public static string CargaInicialCuestionario(ControlDP controlDP, int Seccion)
        {
            string res = "";
            if (controlDP.ID_SubNivel != 11)
            {
                ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
                res = wsEstadisiticas.CargaIncialWebServiceDatos(controlDP, Seccion);
            }
            return res;
        }

        public static string RaiseCallbackEvent(String eventArgument, int Documento, HttpContext pagina)
        {
            //1... Variable Custrionario 2...Especial 3... Anexo  4... Carreras
            string resultado = "";

            string[] paramFromClient = eventArgument.Split('!');
           
            paramFromClient[0] = paramFromClient[0].Replace("==", "=''=");
            ////Guardado de la información
            ////paramFromClient[0] = "v1=12|v2=3|v3=54";
            ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
            //ControlDP controlDP = wsEstadisticas.ObtenerDatosEncuestaID(int.Parse(paramFromClient[2]));
            ControlDP controlDP = Class911.GetControlSeleccionado(pagina);
            if (controlDP.Estatus == 0)
            {
                string[] rows = paramFromClient[0].Split('|');
               
                int ID_Carrera = 0;

                System.Collections.ArrayList listaVariablesDP_validar = new System.Collections.ArrayList();
                System.Collections.ArrayList listaVariablesDP_Guardar = new System.Collections.ArrayList();
                for (int i = 1; i < rows.Length - 1; i++)
                {
                    string[] items = rows[i].Split('=');

                    if (int.Parse(items[0]) == 0)
                        ID_Carrera = int.Parse(items[1]);
                    else
                    {
                        VariablesDP variableDP = new VariablesDP();
                        variableDP.ID_Control = controlDP.ID_Control;
                        variableDP.valor = items[1] == "" ? "0" : items[1].ToUpper();
                        variableDP.var = int.Parse(items[0]);
                        variableDP.ID_Carrera = ID_Carrera;
                        variableDP.ReadOnly = !bool.Parse(items[2]);
                        variableDP.Documento = Documento;
                        listaVariablesDP_validar.Add(variableDP);
                        if (!variableDP.ReadOnly)
                        {
                            listaVariablesDP_Guardar.Add(variableDP);
                        }

                    }
                }
                //***Guardado***
                if (listaVariablesDP_Guardar.Count > 0)
                {
                    resultado = wsEstadisticas.Guardar_ListaVariablesEncuesta(controlDP, Documento, (VariablesDP[])listaVariablesDP_Guardar.ToArray(typeof(VariablesDP)));
                }
                //***Validacion***
                //paramFromClient[1] = "Ejecutar/NoEjecutar";
                if (paramFromClient[1] == "Ejecutar")
                {
                    //Validación de la información    "NoVariable!Comentario1#Comentario2#Comentario3|NoVariable!Comentario1#Comentario2#Comentario3|"
                    SEroot.Seguridad.Seguridad seg = new SEroot.Seguridad.Seguridad();

                    bool ShowSqlValidatosion = seg.LoadParametroSistema(402).Valor == "1";
                    resultado = wsEstadisticas.Vaidar_ListaVariableEncuesta(controlDP, (VariablesDP[])listaVariablesDP_validar.ToArray(typeof(VariablesDP)), ShowSqlValidatosion);

                }
            }
            return resultado;

        }



        #region Metodos para generar Ruta Ligas
        public static ConfigCapturaDP[] CargarDatosConfigCatura(ControlDP controlDP)
        {
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
            ConfigCapturaDP[] listaConfigCaptura = wsEstadisiticas.Load_B(0, controlDP);

            //HttpContext.Current.Session["listaConfigCaptura"] = listaConfigCaptura;
            return listaConfigCaptura;
        }
        ///Ligas
        public static string Liga_ModificacionDatosAlumno(ControlDP controlDP)
        {
            return "~/../ESCOLARABC/escolar/listadoOpciones.aspx";

        }
        public static string Liga_ControlEscolar(ControlDP controlDP)
        {
            //return "http://planeacioneducativa.nl.gob.mx/se/ESCOLARABC/Escolar/Listado.aspx?" +
            return "~/../ESCOLARABC/Escolar/Listado.aspx";
        }
        public static string Liga_PlantillaPersonal(ControlDP controlDP)
        {
            return "~/../MANEJOPERSONAL/Asignacion/ListaAsignacionFuncion.aspx";
        }
        public static string Liga_AsignacionDeGrupos(ControlDP controlDP)
        {
            return "~/../MANEJOPERSONAL/Asignacion/AsignacionGrupo.aspx";
        }
        public static string Liga_Calificaciones(ControlDP controlDP)
        {
            return "~/../CALIFICACIONES/Reportes/opcionImpreGrupo.aspx";
        }

        #endregion


        #region Control de Seccion de Imnueble
        public static string GuardarDatosInmueble(string datos) { return ""; }
        public static string GuardarDatosInmueble(HttpContext page, string datos)
        {
            
            StringBuilder Resultado = new StringBuilder();
            ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
            InmuebleDP inmuebleDP = new InmuebleDP();
            
                //a=1|b=0|c=0
                string[] info = datos.Split('|');
                for (int i = 0; i < info.Length; i++)
                {
                    string registro = info[i];
                    string[] campos = registro.Split('=');
                    if (campos[1] != "-1")
                    {
                        switch (campos[0])
                        {
                            case "a":
                                inmuebleDP.a = campos[1] == "1" ? true : false;
                                break;
                            case "b":
                                inmuebleDP.b = campos[1] == "1" ? true : false;
                                break;
                            case "c":
                                inmuebleDP.c = campos[1] == "1" ? true : false;
                                break;
                            case "d":
                                inmuebleDP.d = campos[1] == "1" ? true : false;
                                break;
                            case "e":
                                inmuebleDP.e = campos[1] == "1" ? true : false;
                                break;
                            case "especifique":
                                inmuebleDP.especifique = campos[1].Trim().Replace("_", "");
                                break;
                            case "ID_Control":
                                inmuebleDP.controlId = int.Parse(campos[1]);
                                break;
                        }
                    }
                    else
                    {
                        Resultado.Append("<li>No se tiene respuesta para:  " + campos[0].ToUpper() + "</li>");
                    }

                }
                ControlDP controlDP = Class911.GetControlSeleccionado(page);
                if (controlDP.Estatus == 0)
                {
                    wsEstadisticas.GuardaCuestionarioInmueble(inmuebleDP);
                }
          
            return Resultado.ToString();
        }

        #endregion

        public static int CicloActual()
        {
            SEroot.WSEscolar.CicloEscolarActualDP actualDP = null;

            SEroot.WSEscolar.WsEscolar wsEscolar = new SEroot.WSEscolar.WsEscolar();    
            actualDP = wsEscolar.LoadCiclo(1);

            return int.Parse(actualDP.cicloActual.nombre.Split('-')[0]);
        }

        static int idtipoCuestionario = 0;
        /// <summary>
        /// Actualiza el encabezado para cuestionarios de inicio de cursos
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="controlDP"></param>
        /// <param name="inicioCursos">debe ser true</param>
       

        public static void ActualizaEncabezado(System.Web.UI.Page Page, ControlDP controlDP)
        {
            int añoCicloActual = CicloActual();
            añoCicloActual = idtipoCuestionario == 0 ? añoCicloActual : añoCicloActual + 1;

            ContentPlaceHolder mainContent = (ContentPlaceHolder)Page.Master.FindControl("cphMainMaster");
            Label lblCiclo = (Label)mainContent.FindControl("lblCiclo");
            if (lblCiclo != null)
                lblCiclo.Text = "CICLO ESCOLAR " + (añoCicloActual).ToString() + "-" + (añoCicloActual + 1).ToString();

            Label lblCentroTrabajo = (Label)mainContent.FindControl("lblCentroTrabajo");
            if (lblCentroTrabajo != null)
            {
                string turno="";
                switch (controlDP.ID_Turno)
                {
                    case 1: turno = "MATUTINO"; break;
                    case 2: turno = "VESPERTINO"; break;
                    case 3: turno = "NOCTURNO"; break;
                    case 4: turno = "CONTINUO"; break;
                    case 5: turno = "MIXTO"; break;
                }
                lblCentroTrabajo.Text = controlDP.Clave + " - \"" + controlDP.Nombre + "\" " + "TURNO: " + turno;
            }

        }


        #region PDFs
        public static string GenerarComprobante_PDF2(int id_control)
        {
            return "Reportes/Reporte.aspx?OCE=1&ctrl=" + id_control.ToString();
        }
        public static string GenerarComprobante_PDF(int id_control)
        {
            return "../Reportes/Reporte.aspx?OCE=1&ctrl=" + id_control.ToString();
        }
        public static string GenerarCuestionario_Bacio_PDF(int id_control)
        {
            return "../Reportes/Reporte.aspx?OCE=2&ctrl=" + id_control.ToString();
        }
        public static string GenerarCuestionario_PDF(int id_control)
        {
            return "../Reportes/Reporte.aspx?OCE=3&ctrl=" + id_control.ToString();
        }
        public static string Desoficializar(int id_control)
        {
            return "../Reportes/Reporte.aspx?OCE=4&ctrl=" + id_control.ToString();
        }

        //  1->Comprobante     2->Bacio            3->Lleno         4-> Desoficializar
        #endregion

        //ID_TipoCuestionario  1..INICIO   2..FIN
        public static readonly int ID_Inicio = 1;
        public static readonly int ID_Fin = 2;

        // Tipo Documento
        public static readonly int Doc_Cuestionario = 1;
        //public static readonly int Doc_Especial = 2;
        public static readonly int Doc_Anexo = 3;
        public static readonly int Doc_Carreras = 4;
        public static readonly int Doc_AnexoInicio = 5;

        //Tipo Variable
        public static readonly char Var_TipoNumerico = 'N';
        public static readonly char Var_TipoString = 'C';

       
    }
     

}
