using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
 
namespace EstadisticasEducativas._911.fin._911_USAER_2
{
    public partial class ASEC_911_USAER_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV448.Attributes["onkeypress"] = "return Num(event)";
                txtV449.Attributes["onkeypress"] = "return Num(event)";
                txtV450.Attributes["onkeypress"] = "return Num(event)";
                txtV451.Attributes["onkeypress"] = "return Num(event)";
                txtV452.Attributes["onkeypress"] = "return Num(event)";
                txtV453.Attributes["onkeypress"] = "return Num(event)";
                txtV454.Attributes["onkeypress"] = "return Num(event)";
                txtV455.Attributes["onkeypress"] = "return Num(event)";
                txtV456.Attributes["onkeypress"] = "return Num(event)";
                txtV457.Attributes["onkeypress"] = "return Num(event)";
                txtV458.Attributes["onkeypress"] = "return Num(event)";
                txtV459.Attributes["onkeypress"] = "return Num(event)";
                txtV460.Attributes["onkeypress"] = "return Num(event)";
                txtV461.Attributes["onkeypress"] = "return Num(event)";
                txtV462.Attributes["onkeypress"] = "return Num(event)";
                txtV463.Attributes["onkeypress"] = "return Num(event)";
                txtV464.Attributes["onkeypress"] = "return Num(event)";
                txtV465.Attributes["onkeypress"] = "return Num(event)";
                txtV466.Attributes["onkeypress"] = "return Num(event)";
                txtV467.Attributes["onkeypress"] = "return Num(event)";
                txtV468.Attributes["onkeypress"] = "return Num(event)";
                txtV469.Attributes["onkeypress"] = "return Num(event)";
                txtV470.Attributes["onkeypress"] = "return Num(event)";
                txtV471.Attributes["onkeypress"] = "return Num(event)";
                txtV472.Attributes["onkeypress"] = "return Num(event)";
                txtV473.Attributes["onkeypress"] = "return Num(event)";
                txtV474.Attributes["onkeypress"] = "return Num(event)";
                txtV475.Attributes["onkeypress"] = "return Num(event)";
                txtV476.Attributes["onkeypress"] = "return Num(event)";
                txtV477.Attributes["onkeypress"] = "return Num(event)";
                txtV478.Attributes["onkeypress"] = "return Num(event)";
                txtV479.Attributes["onkeypress"] = "return Num(event)";
                txtV480.Attributes["onkeypress"] = "return Num(event)";
                txtV481.Attributes["onkeypress"] = "return Num(event)";
                txtV482.Attributes["onkeypress"] = "return Num(event)";
                txtV483.Attributes["onkeypress"] = "return Num(event)";
                txtV484.Attributes["onkeypress"] = "return Num(event)";
                txtV485.Attributes["onkeypress"] = "return Num(event)";
                txtV486.Attributes["onkeypress"] = "return Num(event)";
                txtV487.Attributes["onkeypress"] = "return Num(event)";
                txtV488.Attributes["onkeypress"] = "return Num(event)";
                txtV489.Attributes["onkeypress"] = "return Num(event)";
                txtV490.Attributes["onkeypress"] = "return Num(event)";
                txtV491.Attributes["onkeypress"] = "return Num(event)";
                txtV492.Attributes["onkeypress"] = "return Num(event)";
                txtV493.Attributes["onkeypress"] = "return Num(event)";
                txtV494.Attributes["onkeypress"] = "return Num(event)";
                txtV495.Attributes["onkeypress"] = "return Num(event)";
                txtV496.Attributes["onkeypress"] = "return Num(event)";
                txtV497.Attributes["onkeypress"] = "return Num(event)";
                txtV498.Attributes["onkeypress"] = "return Num(event)";
                txtV499.Attributes["onkeypress"] = "return Num(event)";
                txtV500.Attributes["onkeypress"] = "return Num(event)";
                txtV501.Attributes["onkeypress"] = "return Num(event)";
                txtV502.Attributes["onkeypress"] = "return Num(event)";
                txtV503.Attributes["onkeypress"] = "return Num(event)";
                txtV504.Attributes["onkeypress"] = "return Num(event)";
                txtV505.Attributes["onkeypress"] = "return Num(event)";
                txtV506.Attributes["onkeypress"] = "return Num(event)";
                txtV507.Attributes["onkeypress"] = "return Num(event)";
                txtV508.Attributes["onkeypress"] = "return Num(event)";
                txtV509.Attributes["onkeypress"] = "return Num(event)";
                txtV510.Attributes["onkeypress"] = "return Num(event)";
                txtV511.Attributes["onkeypress"] = "return Num(event)";
                txtV512.Attributes["onkeypress"] = "return Num(event)";
                txtV513.Attributes["onkeypress"] = "return Num(event)";
                txtV514.Attributes["onkeypress"] = "return Num(event)";
                txtV515.Attributes["onkeypress"] = "return Num(event)";
                txtV516.Attributes["onkeypress"] = "return Num(event)";
                txtV517.Attributes["onkeypress"] = "return Num(event)";
                txtV518.Attributes["onkeypress"] = "return Num(event)";
                txtV519.Attributes["onkeypress"] = "return Num(event)";
                txtV520.Attributes["onkeypress"] = "return Num(event)";
                txtV521.Attributes["onkeypress"] = "return Num(event)";
                txtV522.Attributes["onkeypress"] = "return Num(event)";
                txtV523.Attributes["onkeypress"] = "return Num(event)";
                txtV524.Attributes["onkeypress"] = "return Num(event)";
                txtV525.Attributes["onkeypress"] = "return Num(event)";
                txtV526.Attributes["onkeypress"] = "return Num(event)";
                txtV527.Attributes["onkeypress"] = "return Num(event)";
                txtV528.Attributes["onkeypress"] = "return Num(event)";
                txtV529.Attributes["onkeypress"] = "return Num(event)";
                txtV530.Attributes["onkeypress"] = "return Num(event)";
                txtV531.Attributes["onkeypress"] = "return Num(event)";
                txtV532.Attributes["onkeypress"] = "return Num(event)";
                txtV533.Attributes["onkeypress"] = "return Num(event)";
                txtV534.Attributes["onkeypress"] = "return Num(event)";
                txtV535.Attributes["onkeypress"] = "return Num(event)";
                txtV536.Attributes["onkeypress"] = "return Num(event)";
                txtV537.Attributes["onkeypress"] = "return Num(event)";
                txtV538.Attributes["onkeypress"] = "return Num(event)";
                txtV539.Attributes["onkeypress"] = "return Num(event)";
                txtV540.Attributes["onkeypress"] = "return Num(event)";
                txtV541.Attributes["onkeypress"] = "return Num(event)";
                txtV542.Attributes["onkeypress"] = "return Num(event)";
                txtV543.Attributes["onkeypress"] = "return Num(event)";
                txtV544.Attributes["onkeypress"] = "return Num(event)";
                txtV545.Attributes["onkeypress"] = "return Num(event)";
                txtV546.Attributes["onkeypress"] = "return Num(event)";
                txtV547.Attributes["onkeypress"] = "return Num(event)";
                txtV548.Attributes["onkeypress"] = "return Num(event)";
                txtV549.Attributes["onkeypress"] = "return Num(event)";
                txtV550.Attributes["onkeypress"] = "return Num(event)";
                txtV551.Attributes["onkeypress"] = "return Num(event)";
                txtV552.Attributes["onkeypress"] = "return Num(event)";
                txtV553.Attributes["onkeypress"] = "return Num(event)";
                txtV554.Attributes["onkeypress"] = "return Num(event)";
                txtV555.Attributes["onkeypress"] = "return Num(event)";
                txtV556.Attributes["onkeypress"] = "return Num(event)";
                txtV557.Attributes["onkeypress"] = "return Num(event)";
                txtV558.Attributes["onkeypress"] = "return Num(event)";
                txtV559.Attributes["onkeypress"] = "return Num(event)";
                txtV560.Attributes["onkeypress"] = "return Num(event)";
                txtV561.Attributes["onkeypress"] = "return Num(event)";
                txtV562.Attributes["onkeypress"] = "return Num(event)";
                txtV563.Attributes["onkeypress"] = "return Num(event)";
                txtV564.Attributes["onkeypress"] = "return Num(event)";
                txtV565.Attributes["onkeypress"] = "return Num(event)";
                txtV566.Attributes["onkeypress"] = "return Num(event)";
                txtV567.Attributes["onkeypress"] = "return Num(event)";
                txtV568.Attributes["onkeypress"] = "return Num(event)";
                txtV569.Attributes["onkeypress"] = "return Num(event)";
                txtV570.Attributes["onkeypress"] = "return Num(event)";
                txtV571.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************


    }
}
