using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Mx.Gob.Nl.Educacion;
using SEroot.WsEstadisticasEducativas;

namespace EstadisticasEducativas._911.inicio
{
    public partial class VerComentarios : System.Web.UI.Page
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtRegion.Attributes["onkeypress"] = "return Num(event)";
                txtZona.Attributes["onkeypress"] = "return Num(event)";
                btnBuscar.Attributes["onblur"] = "javascript:_enter=true;";
                btnBuscar.Attributes["onfocus"] = "javascript:_enter=false;";
                Nivel();
                Filtros();
                Estatus();
                DesplegarCampos();
                //GridVacio();
            }
        }

        private void DesplegarCampos()
        {
            chkCampos.Items.Add(new ListItem("VER COMPROBANTE DE CAPTURA", "CompCaptura"));
            chkCampos.Items.Add(new ListItem("VER CUESTIONARIO 911 LLENO", "Cuestionario"));
            chkCampos.Items.Add(new ListItem("DESOFICIALIZAR", "Desoficializar"));
            chkCampos.Items.Add(new ListItem("IR A CAPTURA 911", "Detalle"));
            chkCampos.Items.Add(new ListItem("CLAVE DEL CENTRO DE TRABAJO", "CCT"));
            chkCampos.Items.Add(new ListItem("NOMBRE DE LA ESCUELA", "NOMBRE"));
            chkCampos.Items.Add(new ListItem("TURNO", "ID_TURNO"));
            chkCampos.Items.Add(new ListItem("TELEFONO", "TELEFONO"));
            chkCampos.Items.Add(new ListItem("REGION", "REGION"));
            chkCampos.Items.Add(new ListItem("ZONA", "ZONA"));
            chkCampos.Items.Add(new ListItem("NIVEL", "NIVEL"));
            chkCampos.Items.Add(new ListItem("ESTATUS", "ESTATUS"));
            chkCampos.Items.Add(new ListItem("OBSERVACIONES", "OBSERVACIONES"));

            foreach (ListItem li in chkCampos.Items)
            {
                if (li.Value == "Detalle") { li.Selected = true; }
                if (li.Value == "CCT") { li.Selected = true; }
                if (li.Value == "NOMBRE") { li.Selected = true; }
                if (li.Value == "TELEFONO") { li.Selected = true; }
                if (li.Value == "ESTATUS") { li.Selected = true; }
                if (li.Value == "OBSERVACIONES") { li.Selected = true; }
            }
        }

        private void Estatus()
        {
            ddlEstatus.Items.Add(new ListItem("", "TODO"));
            ddlEstatus.Items.Add(new ListItem("OFICIALIZADO", "SI"));
            ddlEstatus.Items.Add(new ListItem("NO OFICIALIZADO", "NO"));
            ddlEstatus.Items.Add(new ListItem("OFICIALIZADO CON MOTIVO DE NO CAPTURA", "MC"));
        }

        private void Filtros()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            SEroot.WsCentrosDeTrabajo.ZonaDP zonaDP = ws.Load_Zona(zona);
            string ID_NivelEducacion = usr[7];

            //ddlNivel.Enabled = false;

            switch (ID_NivelTrabajo)
            {
                case 1://SE
                    break;
                case 2://Region
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
                case 3:
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    txtZona.Text = zonaDP.numeroZona.ToString();
                    txtZona.Enabled = false;
                    break;
                case 4:

                    if (centroTrabajo.Length > 1)
                    {
                        txtRegion.Text = "";
                        txtZona.Text = "";
                        txtRegion.Enabled = false;
                        txtZona.Enabled = false;

                        txtClaveCT.Visible = false; txtClaveCT.Text = "";
                        ddlCentroTrabajo.Visible = true;
                        for (int i = 0; i < centroTrabajo.Length; i++)
                        {
                            SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[i]));

                            ddlCentroTrabajo.Items.Add(new ListItem(ctDP.clave, ctDP.clave));
                        }
                        ddlCentroTrabajo.SelectedIndex = 0;
                    }
                    else
                    {

                        ddlNivel.Enabled = false;

                        txtRegion.Text = region.ToString();
                        txtRegion.Enabled = false;
                        txtZona.Text = zonaDP.numeroZona.ToString();
                        txtZona.Enabled = false;

                        SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[0]));
                        txtClaveCT.Visible = true; txtClaveCT.Text = ctDP.clave;

                        ddlCentroTrabajo.Visible = false;
                    }
                    txtClaveCT.Enabled = false;
                    break;
                case 5://Region
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
                case 6://Region
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
                case 7://Region
                    txtRegion.Text = region.ToString();
                    txtRegion.Enabled = false;
                    break;
            }
        }

        private void Nivel()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            //Response.Write(usr[0]);
            if (usr[0] == "352599")
            {
                ddlNivel.Items.Add(new ListItem("CONALEP", "21_20"));
            }
            else if (ID_NivelTrabajo == 1 || ID_NivelTrabajo == 2 || ID_NivelTrabajo == 5 || ID_NivelTrabajo == 6 || ID_NivelTrabajo == 7)
            {
                SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

                SEroot.WsEstadisticasEducativas.CatListaNiveles911DP[] listaNiveles = wsEstadisticas.Lista_CatListaNiveles911(Class911.ID_Inicio);
                int secundaria = 0;
                for (int i = 0; i < listaNiveles.Length; i++)
                {
                    string valor;
                    if (listaNiveles[i].ID_Nivel != 13)
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_" + listaNiveles[i].ID_SubNivel.ToString();
                        secundaria = 0;
                    }
                    else
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_0";
                        secundaria++;
                    }
                    if (secundaria < 2)
                    {
                        string texto = listaNiveles[i].Descrip;
                        ddlNivel.Items.Add(new ListItem(texto, valor));
                    }
                }
            }
            else
            {
                ddlNivel.Enabled = false;
                ddlNivel.Items.Add(new ListItem("", "0_0"));
            }
        }

        protected void GridVacio()
        {
            SEroot.WsEstadisticasEducativas.DsComentarios Ds = new SEroot.WsEstadisticasEducativas.DsComentarios();
            Ds.Comentarios.Rows.Add(0, 0, 0,0, "", "", 0, "", 0, 0, "", 0, "", "", "");
            GridView1.DataSource = Ds;
            GridView1.DataBind();
            Session["dsGrid"] = null;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            LlenaGridView(0);
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false; 
                e.Row.Cells[4].Visible = false; // ID_CONTROL
                e.Row.Cells[5].Visible = false; // ID_CENTROTRABAJO
                e.Row.Cells[6].Visible = false; // ID_INMUEBLE
                e.Row.Cells[7].Visible = false; // ID_cctnt
                e.Row.Cells[8].Visible = false; // CCT
                e.Row.Cells[9].Visible = false; // NOMBRE
                e.Row.Cells[10].Visible = false; // ID_TURNO
                e.Row.Cells[11].Visible = false; // TELEFONO
                e.Row.Cells[12].Visible = false; // REGION
                e.Row.Cells[13].Visible = false; // ID_ZONA
                e.Row.Cells[14].Visible = false; // ZONA
                e.Row.Cells[15].Visible = false; // ID_NIVEL
                e.Row.Cells[16].Visible = false; // NIVEL
                e.Row.Cells[17].Visible = false; // ESTATUS
                e.Row.Cells[18].Visible = false; // OBSERVACIONES

                foreach (ListItem li in chkCampos.Items)
                {
                    if (li.Value == "CompCaptura" && li.Selected == true) { e.Row.Cells[0].Visible = true; } // CompCaptura
                    if (li.Value == "Cuestionario" && li.Selected == true) { e.Row.Cells[1].Visible = true; } // Cuestionario
                    if (li.Value == "Desoficializar" && li.Selected == true) { e.Row.Cells[2].Visible = true; } // Desoficializar
                    if (li.Value == "Detalle" && li.Selected == true) { e.Row.Cells[3].Visible = true; } // Detalle
                    if (li.Value == "CCT" && li.Selected == true) { e.Row.Cells[8].Visible = true; } // CCT
                    if (li.Value == "NOMBRE" && li.Selected == true) { e.Row.Cells[9].Visible = true; } // NOMBRE
                    if (li.Value == "ID_TURNO" && li.Selected == true) { e.Row.Cells[10].Visible = true; } // ID_TURNO
                    if (li.Value == "TELEFONO" && li.Selected == true) { e.Row.Cells[11].Visible = true; } // TELEFONO
                    if (li.Value == "REGION" && li.Selected == true) { e.Row.Cells[12].Visible = true; } // REGION
                    if (li.Value == "ZONA" && li.Selected == true) { e.Row.Cells[14].Visible = true; } // ZONA
                    if (li.Value == "NIVEL" && li.Selected == true) { e.Row.Cells[16].Visible = true; } // NIVEL
                    if (li.Value == "ESTATUS" && li.Selected == true) { e.Row.Cells[17].Visible = true; } // ESTATUS
                    if (li.Value == "OBSERVACIONES" && li.Selected == true) { e.Row.Cells[18].Visible = true; } // OBSERVACIONES
                }
            }
        }

        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }

        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.DataSource = SortDataTable(true);
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected DataView SortDataTable(bool isPageIndexChanging)
        {
            //DataSet ds = (DataSet)Session["dsGrid"];
            //if (ds != null)
            if (Session["dsGrid"] != null)
            {
                //DataTable dataTable = new DataTable();
                //dataTable = ds.Tables[0];

                DataTable dataTable = new DataTable();
                dataTable = (DataTable)Session["dsGrid"];

                if (dataTable != null)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
            else
            {
                return new DataView();
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridViewSortExpression = e.SortExpression;
            int pageIndex = GridView1.PageIndex;
            GridView1.DataSource = SortDataTable(false);
            GridView1.DataBind();
            GridView1.PageIndex = pageIndex;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string id_CCT = "";
            //id_CCT = GridView1.SelectedRow.Cells[0].Text.ToString();
        }

        protected void LlenaGridView(int pag)
        {
            SEroot.WsEstadisticasEducativas.DsComentarios Ds = new SEroot.WsEstadisticasEducativas.DsComentarios();
            int reg = 0;
            int zon = 0;
            string ct = "";
            if (txtRegion.Text.Trim().Length > 0)
            {
                reg = int.Parse(txtRegion.Text.Trim());
            }
            if (txtZona.Text.Trim().Length > 0)
            {
                zon = int.Parse(txtZona.Text.Trim());
            }
            if (txtClaveCT.Visible)
            {
                if (txtClaveCT.Text.Trim().Length > 0)
                {
                    ct = txtClaveCT.Text.Trim();
                }
            }
            else
            {
                ct = ddlCentroTrabajo.SelectedValue;
            }

            string valor = ddlNivel.SelectedValue;
            int ID_Nivel = int.Parse(valor.Split('_')[0]);
            int ID_SubNivel = int.Parse(valor.Split('_')[1]);
            string vddlEstatus = ddlEstatus.SelectedItem.Value;

            Ds = wsEstadisticas.VerComentarios(reg, zon, ct, ID_Nivel, ID_SubNivel, vddlEstatus, 1);

            if (Ds != null && Ds.Tables[0].Rows.Count > 0)
            {
                if (pag == 0)
                {
                    if (chkPaginado.Checked)
                    {
                        GridView1.AllowPaging = true;
                    }
                    else
                    {
                        GridView1.AllowPaging = false;
                    }
                }
                else
                {
                    GridView1.AllowPaging = false;
                }
                GridView1.DataSource = Ds.Comentarios;
                GridView1.DataBind();
                Session["dsGrid"] = Ds.Comentarios;
                lblMsg.Text = "Registros encontrados: " + Ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                //GridVacio();
                lblMsg.Text = "No se encontraron registros";
            }
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtRegion.Text = "";
            //txtZona.Text = "";
            //txtClaveCT.Text = "";
            lblMsg.Text = "";
            //GridVacio();
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            LlenaGridView(1);
            StringBuilder sb_bus = new StringBuilder();
            sb_bus.Append("<table border='1'>");
            sb_bus.Append("<tr>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>ESTATUS</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>REGION</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>ZONA</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>NIVEL</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>CCT</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>NOMBRE</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>TURNO</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>TELEFONO</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>OBSERVACIONES</td>");
            sb_bus.Append("</tr>");
            foreach (GridViewRow row in GridView1.Rows)
            {
                sb_bus.Append("<tr>");
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[17].Text + "</td>"); // ESTATUS
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[12].Text + "</td>"); // REGION
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[14].Text + "</td>"); // ZONA
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[16].Text + "</td>"); // NIVEL
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[8].Text + "</td>"); // CCT
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[9].Text + "</td>"); // NOMBRE
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[10].Text + "</td>"); // TURNO
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[11].Text + "</td>"); // TELEFONO
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[18].Text + "</td>"); // OBSERVACIONES
                sb_bus.Append("</tr>");
            }
            sb_bus.Append("</table>");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-Disposition", "attachment;filename=Datos.xls");
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.Default;
            //Response.Write(" <table><tr><td align='center' colspan='15'><h4>titulo</h4></td></tr></table>");
            //Response.Write(" <table><tr><td align='center' colspan='15'><h4>subtitulo</h4></td></tr></table><br>");
            Response.Write(sb_bus.ToString());
            Response.End();
        }

        protected void RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            if (row != null)
            {
                int ID_Ciclo = 5; //int.Parse(ddlCiclo.SelectedValue);
                string[] usr = User.Identity.Name.Split('|');
                int ID_Usuario = int.Parse(usr[0]);

                Label lbl = (Label)row.FindControl("lblID_CCTNT");

                EstablecerBarrita(0, 0, 0, 0, int.Parse(lbl.Text));
                int id_cctnt = int.Parse(lbl.Text);
                ControlDP controlDP = wsEstadisticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Inicio, ID_Ciclo, ID_Usuario);
                Class911.SetControlSeleccionado(HttpContext.Current, controlDP);

                //Abrir en modal el cuestionario
                string codigo = "";
                if (e.CommandName == "cc")
                {
                    codigo = "AbrirPDF('" + Class911.GenerarComprobante_PDF(controlDP.ID_Control) + "',1);";
                }
                else if (e.CommandName == "frm")
                {
                    codigo = "AbrirPDF('" + Class911.GenerarCuestionario_PDF(controlDP.ID_Control) + "',2);";
                }
                else if (e.CommandName == "des")
                {
                    codigo = "AbrirPDF('" + Class911.Desoficializar(controlDP.ID_Control) + "',3);";
                }
                else if (e.CommandName == "cap")
                {
                    codigo = "Vent911('" + Ruta(controlDP.ID_Cuestionario) + "');";
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "", codigo, true);
            }
        }

        UsuarioSeDP usr = null;
        protected void getUsr()
        {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }
        protected void EstablecerBarrita(int ID_Nviel, int ID_Region, int ID_Sostenimiento, int ID_Zona, int ID_CCTNT)
        {
            getUsr();
            //SERaiz.Controles.FiltraCCTporZona FiltraCCTporZona1 = (SERaiz.Controles.FiltraCCTporZona)Page.Master.FindControl("FiltraCCTporZona1");


            if ((ID_CCTNT == -1 || ID_CCTNT == 0) && (ID_Zona == -1 || ID_Zona == 0) && (ID_Region == -1 || ID_Region == 0))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('¡¡¡Seleccione un Centro de Trabajo!!!');", true);
            }
            else
            {
                if (ID_CCTNT != 0 && ID_CCTNT != -1)
                    SeguridadSE.SetCctNt(this.Page, ID_CCTNT);//Set Centro Trabajo
                else
                    if (ID_Zona != 0 && ID_Zona != -1)
                        SeguridadSE.SetCctNt(this.Page, ID_Zona);//Set Zona
                    else
                        if (ID_Region != 0 && ID_Region != -1)
                            SeguridadSE.SetCctNt(this.Page, ID_Region);//Set Region
                        else
                            if (usr.NivelTrabajo.NiveltrabajoId == 1)
                                SeguridadSE.SetCctNt(this.Page, 2);//Set Secretaria de Educacion
                            else
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('¡¡¡Seleccione un Centro de Trabajo!!!');", true);

                ((Label)Page.Master.FindControl("lblCCTSeleccionado")).Text = SeguridadSE.cctSelected.Clavecct + " - " + SeguridadSE.cctSelected.Nombrecct + " - " + SeguridadSE.cctSelected.Truno;
            }
            if (SeguridadSE.cicloSelected != null)
            {
                if (SeguridadSE.cicloSelected.Nombre != "")
                    ((Label)Page.Master.FindControl("lblCicloEscolar")).Text = SeguridadSE.cicloSelected.Nombre;
            }
            //FiltraCCTporZona1.filtrosPermisos(usr);
        }


        private string Ruta(int id_cuestionario)
        {
            string abrirVentana = "";
            switch (id_cuestionario)
            {
                case 15:
                    abrirVentana = "EI_1/Identificacion_EI_1.aspx";
                    break;
                case 16:
                    abrirVentana = "EI_NE1/Identificacion_EI_NE1.aspx";
                    break;
                case 17:
                    abrirVentana = "911_USAER_1/Identificacion_911_USAER_1.aspx";
                    break;
                case 18:
                    abrirVentana = "CAM_1/Identificacion_CAM_1.aspx";
                    break;
                case 19:
                    abrirVentana = "911_1/Identificacion_911_1.aspx";
                    break;
                case 20:
                    abrirVentana = "ECC_11/Identificacion_ECC_11.aspx";
                    break;
                case 22:
                    abrirVentana = "911_3/Identificacion_911_3.aspx";
                    break;
                case 23:
                    abrirVentana = "ECC_12/Identificacion_ECC_12.aspx";
                    break;
                case 25:
                    abrirVentana = "911_5/Identificacion_911_5.aspx";
                    break;
                case 26:
                    abrirVentana = "911_7G/Identificacion_911_7G.aspx";
                    break;
                case 27:
                    abrirVentana = "911_7T/Identificacion_911_7T.aspx";
                    break;
                case 28:
                    abrirVentana = "911_7P/Identificacion_911_7P.aspx";
                    break;
                default:
                    abrirVentana = "Reportes/Reporte.aspx";
                    break;

            }
            return abrirVentana;
        }

    }
}
