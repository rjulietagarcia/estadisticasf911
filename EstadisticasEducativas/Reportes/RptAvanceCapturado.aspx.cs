using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;
using System.IO;
namespace EstadisticasEducativas.Reportes
{
    public partial class RptAvanceCapturado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ServiceEstadisticas ws = new ServiceEstadisticas();
                int idCiclo = int.Parse(Request.Params["ciclo"] == null ? "-1" : Request.Params["ciclo"]);
                int tipo = int.Parse(Request.Params["tipo"] == null ? "-1" : Request.Params["tipo"]);

                //int idNivel = int.Parse(Request.Params["niv"] == null ? "-1" : Request.Params["niv"]);
                //int idSubNivel = int.Parse(Request.Params["sub"] == null ? "-1" : Request.Params["sub"]);

                int idCuestionario = int.Parse(Request.Params["c"] == null ? "-1" : Request.Params["c"]);

                int idEntidad = int.Parse(Request.Params["ent"] == null ? "-1" : Request.Params["ent"]);
                int idRegion = int.Parse(Request.Params["region"] == null ? "-1" : Request.Params["region"]);
                //int idZona = int.Parse(Request.Params["zon"] == null ? "-1" : Request.Params["zon"]);

                SEroot.WSEscolar.WsEscolar wsEsc = new SEroot.WSEscolar.WsEscolar();
                SEroot.WSEscolar.CicloEscolarRowSet[] cicloRow = wsEsc.LoadCicloEscolarRowset(idCiclo, 1);

                string nombreCiclo = cicloRow[0].Nombre;

                byte[] reporte = null;

                if (idEntidad == -1)
                {
                    reporte = ws.GetAvanceCapturadoPorEstadoRpt(idCiclo, "Ciclo Escolar "+ nombreCiclo, tipo, idCuestionario);
                }
                else
                {
                    reporte = ws.GetAvanceCapturadoPorNivelRpt(idCiclo, "Ciclo Escolar " + nombreCiclo, tipo, idEntidad, idRegion, -1 );
                }

                if (reporte != null && reporte.Length > 0)
                {
                    DoDeleteFile();
                    ImprimeReporte(reporte);
                }
                else
                {
                    HttpContext.Current.Trace.Warn("Consulta GetAvanceCapturadoPorNivel regres� null o len <=0");
                }


            }
        }

        /// Elimina el documento PDF anterior, siempre y cuando exista la session
        protected void DoDeleteFile()
        {
            if (Session["lGUID"] != null)
            {
                string lGUID = Session["lGUID"].ToString();
                if (File.Exists(HttpContext.Current.Server.MapPath("~/PDFReports/" + lGUID + ".pdf")))
                    File.Delete(HttpContext.Current.Server.MapPath("~/PDFReports/" + lGUID + ".pdf"));
            }
        }

        /// ImprimeReporte y crea la session que lo maneja
        private void ImprimeReporte(byte[] aArchivo)
        {
            DoDeleteFile();
            System.Guid lGUID = Guid.NewGuid();
            FileStream lFileStream = new FileStream(HttpContext.Current.Server.MapPath("~/PDFReports/" + lGUID.ToString() + ".pdf"), FileMode.CreateNew);
            try
            {
                lFileStream.Write(aArchivo, 0, aArchivo.Length - 1);
            }
            finally
            {
                lFileStream.Flush();
                lFileStream.Close();
            }

            Session["lGUID"] = lGUID.ToString();

            Response.Redirect("../PDFReports/" + lGUID.ToString() + ".pdf");
        }
    }
}
