﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.ComponentModel;
using AjaxControlToolkit;
using System.Configuration;
using System.Collections.Specialized;
using System.Web.Security;
using SEroot.WsSESeguridad;
using System.IO;
using SEroot;
namespace Estructuras
{
    /// <summary>
    /// Descripción breve de Catalagos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    [ToolboxItem(false)]
    
    public class Catalagos : System.Web.Services.WebService
    {
        // Reemplazar con la clase del Web Services.
        private WsSESeguridad ws;
        private string seccionConfiguracion = "WebServiceSeguridad";

        /// <summary>
        /// Constructor del catálogo
        /// <remarks>Recuerda que hay que cambiar la clase del Web Services a consultar</remarks>
        /// </summary>
        public Catalagos()
        {
            // Inicializo el Web Services Aqui
            ws = new WsSESeguridad();
            ws.Url = ConfigurationManager.AppSettings[seccionConfiguracion];
        }

        private CascadingDropDownNameValue[] GetCategoriaLocalidad(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCategoriaLocalidad(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCategoriaLocalidad(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            // string llavePadreString = knownCategoryValuesDictionary["LlavePadre"];

            //int llavePadre = 0;
            //int.TryParse(llavePadreString, out llavePadre);

            cantidad = Convert.ToInt32(ws.CategoriaLocalidadQryCountForDescripcion(filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CategoriaLocalidadQryDP[] res = ws.CategoriaLocalidadQryLoadListForDescripcion(0, cantidad, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CategoriaLocalidadQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.CategorialocalidadId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetPais(StringDictionary knownCategoryValuesDictionary)
        {
            return GetPais(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetPais(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            // string llavePadreString = knownCategoryValuesDictionary["LlavePadre"];

            //int llavePadre = 0;
            //int.TryParse(llavePadreString, out llavePadre);

            cantidad = Convert.ToInt32(ws.PaisQryCountForDescripcion(filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            PaisQryDP[] res = ws.PaisQryLoadListForDescripcion(0, cantidad, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (PaisQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.PaisId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetEntidad(StringDictionary knownCategoryValuesDictionary)
        {
            return GetEntidad(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetEntidad(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string paisString = knownCategoryValuesDictionary["PAIS"];

            short llavePais = 0;
            short.TryParse(paisString, out llavePais);

            cantidad = Convert.ToInt32(ws.EntidadQryCountForDescripcion(llavePais, filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            EntidadQryDP[] res = ws.EntidadQryLoadListForDescripcion(0, cantidad, llavePais, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (EntidadQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.EntidadId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetMunicipio(StringDictionary knownCategoryValuesDictionary)
        {
            return GetMunicipio(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetMunicipio(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string paisString = knownCategoryValuesDictionary["PAIS"];
            string entidadString = knownCategoryValuesDictionary["ENTIDAD"];

            short llavePais = 0;
            short.TryParse(paisString, out llavePais);
            short llaveEntidad = 0;
            short.TryParse(entidadString, out llaveEntidad);

            cantidad = Convert.ToInt32(ws.MunicipioQryCountForDescripcion(llavePais, llaveEntidad, filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            MunicipioQryDP[] res = ws.MunicipioQryLoadListForDescripcion(0, cantidad, llavePais, llaveEntidad, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (MunicipioQryDP linea in res)
            {
                string descripcion = linea.MunicipioId.ToString("000") + "-" + linea.Nombre;
                string valor = linea.MunicipioId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetTipoEducacion(StringDictionary knownCategoryValuesDictionary)
        {
            return GetTipoEducacion(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetTipoEducacion(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            // string paisString = knownCategoryValuesDictionary["PAIS"];
            // string entidadString = knownCategoryValuesDictionary["ENTIDAD"];

            // short llavePais = 0;
            //short.TryParse(paisString, out llavePais);
            //short llaveEntidad = 0;
            //short.TryParse(entidadString, out llaveEntidad);

            cantidad = Convert.ToInt32(ws.TipoEducacionQryCountForDescripcion(filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            TipoEducacionQryDP[] res = ws.TipoEducacionQryLoadListForDescripcion(0, cantidad, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (TipoEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.TipoeducacionId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetNivelEducacion(StringDictionary knownCategoryValuesDictionary)
        {
            return GetNivelEducacion(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetNivelEducacion(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string tipoEducacionString = knownCategoryValuesDictionary["TIPOEDUCACION"];
            // string entidadString = knownCategoryValuesDictionary["ENTIDAD"];

            short llaveTipoEducacion = 0;
            short.TryParse(tipoEducacionString, out llaveTipoEducacion);
            //short llaveEntidad = 0;
            //short.TryParse(entidadString, out llaveEntidad);

            cantidad = Convert.ToInt32(ws.NivelEducacionQryCountForDescipcion(llaveTipoEducacion, filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            NivelEducacionQryDP[] res = ws.NivelEducacionQryLoadListForDescipcion(0, cantidad, llaveTipoEducacion, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (NivelEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.NivelId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCentroTrabajo(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCentroTrabajo(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCentroTrabajo(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string zonaString = knownCategoryValuesDictionary["ZONA"];
            string sostenimientoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];

            short llaveZona = 0;
            short.TryParse(zonaString, out llaveZona);
            byte llaveSostenimiento = 0;
            byte.TryParse(sostenimientoString, out llaveSostenimiento);

            cantidad = Convert.ToInt32(ws.CentroTrabajoQryCountForDescripcionIdZonayIdSostenimiento(filtro, llaveZona, llaveSostenimiento));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CentroTrabajoQryDP[] res = ws.CentroTrabajoQryLoadListForDescripcionIdZonayIdSostenimiento(0, cantidad, filtro, llaveZona, llaveSostenimiento);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CentroTrabajoQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.CentrotrabajoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetNivelTrabajo(StringDictionary knownCategoryValuesDictionary)
        {
            return GetNivelTrabajo(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetNivelTrabajo(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            //string tipoEducacionString = knownCategoryValuesDictionary["TIPOEDUCACION"];
            // string entidadString = knownCategoryValuesDictionary["ENTIDAD"];

            //short llaveTipoEducacion = 0;
            //short.TryParse(tipoEducacionString, out llaveTipoEducacion);
            //short llaveEntidad = 0;
            //short.TryParse(entidadString, out llaveEntidad);

            cantidad = Convert.ToInt32(ws.NivelTrabajoQryCountForDescripcion(filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            NivelTrabajoQryDP[] res = ws.NivelTrabajoQryLoadListForDescripcion(0, cantidad, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (NivelTrabajoQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.NiveltrabajoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetRegion(StringDictionary knownCategoryValuesDictionary)
        {
            return GetRegion(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetRegion(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string nivelEducacionString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            // string entidadString = knownCategoryValuesDictionary["ENTIDAD"];

            byte llavenivelEducacion = 0;
            byte.TryParse(nivelEducacionString, out llavenivelEducacion);
            //short llaveEntidad = 0;
            //short.TryParse(entidadString, out llaveEntidad);

            cantidad = Convert.ToInt32(ws.RegionNivelEducacionQryCountForDescripcion(filtro, llavenivelEducacion));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            RegionNivelEducacionQryDP[] res = ws.RegionNivelEducacionQryLoadListForDescripcion(0, cantidad, filtro, llavenivelEducacion);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (RegionNivelEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.RegionId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetSostenimiento(StringDictionary knownCategoryValuesDictionary)
        {
            return GetSostenimiento(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetSostenimiento(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string regionString = knownCategoryValuesDictionary["REGIONPORNIVELEDUCACION"];

            short llaveRegion = 0;
            short.TryParse(regionString, out llaveRegion);

            cantidad = Convert.ToInt32(ws.SostenimientoQryCountForDescripcion(filtro, llaveRegion));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            SostenimientoQryDP[] res = ws.SostenimientoQryLoadListForDescripcion(0, cantidad, filtro, llaveRegion);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (SostenimientoQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.SostenimientoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetZona(StringDictionary knownCategoryValuesDictionary)
        {
            return GetZona(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetZona(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string sostenimientoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];

            byte llaveSostenimiento = 0;
            byte.TryParse(sostenimientoString, out llaveSostenimiento);

            cantidad = Convert.ToInt32(ws.ZonaQryCountForDescripcionyidsostenimiento(filtro, llaveSostenimiento));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            ZonaQryDP[] res = ws.ZonaQryLoadListForDescripcionyidsostenimiento(0, cantidad, filtro, llaveSostenimiento);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (ZonaQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.ZonaId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetPerfilPorNivelTrabajo(StringDictionary knownCategoryValuesDictionary)
        {
            return GetPerfilPorNivelTrabajo(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetPerfilPorNivelTrabajo(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string nivelTrabajoString = knownCategoryValuesDictionary["NIVELTRABAJO"];

            byte llaveNivelTrabajo = 0;
            byte.TryParse(nivelTrabajoString, out llaveNivelTrabajo);

            cantidad = Convert.ToInt32(ws.PerfilQryCountForNombreYNivelTrabajo(filtro, llaveNivelTrabajo));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            PerfilQryDP[] res = ws.PerfilQryLoadListForNombreYNivelTrabajo(0, cantidad, filtro, llaveNivelTrabajo);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (PerfilQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.PerfilId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetNivelEducacionCCT(StringDictionary knownCategoryValuesDictionary)
        {
            return GetNivelEducacionCCT(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetNivelEducacionCCT(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            cantidad = Convert.ToInt32(ws.NivelEducacionQryCount());
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            NivelEducacionQryDP[] res = ws.NivelEducacionQryLoadList(0, cantidad);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (NivelEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.NivelId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetRegionPorNivelEducacion(StringDictionary knownCategoryValuesDictionary)
        {
            return GetRegionPorNivelEducacion(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetRegionPorNivelEducacion(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string tipoEducacionString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            // string entidadString = knownCategoryValuesDictionary["ENTIDAD"];

            //byte llaveNivelEducacion = 0;
            //byte.TryParse(tipoEducacionString, out llaveNivelEducacion);
            //short llaveEntidad = 0;
            //short.TryParse(entidadString, out llaveEntidad);

            cantidad = Convert.ToInt32(ws.RegionQryCount());

            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            RegionQryDP[] res = ws.RegionQryLoadList(0, cantidad);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (RegionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.RegionId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }
        private CascadingDropDownNameValue[] GetSostenimientos(StringDictionary knownCategoryValuesDictionary)
        {
            return GetSostenimientos(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetSostenimientos(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            //string regionString = knownCategoryValuesDictionary["REGION"];

            //short llaveRegion = 0;
            //short.TryParse(regionString, out llaveRegion);

            cantidad = Convert.ToInt32(ws.SostenimientofullQryCountForDescripcion(filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            SostenimientofullQryDP[] res = ws.SostenimientofullQryLoadListForDescripcion(0, cantidad, filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (SostenimientofullQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.SostenimientoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetZonaPorRegion(StringDictionary knownCategoryValuesDictionary)
        {
            return GetZonaPorRegion(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetZonaPorRegion(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string sostenimientoString = knownCategoryValuesDictionary["REGIONPORNIVELEDUCACION"];

            byte llaveRegion = 0;
            byte.TryParse(sostenimientoString, out llaveRegion);

            cantidad = Convert.ToInt32(ws.ZonaPorRegionQryCount());
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            ZonaPorRegionQryDP[] res = ws.ZonaPorRegionQryLoadListForDescripcion(0, cantidad, filtro, llaveRegion);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (ZonaPorRegionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.ZonaId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCentroTrabajoPorSost(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCentroTrabajoPorSost(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCentroTrabajoPorSost(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string nivelString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            string regionString = knownCategoryValuesDictionary["REGIONPORNIVELEDUCACION"];
            string zonaString = knownCategoryValuesDictionary["ZONAPORREGION"];
            string sostenimientoString = knownCategoryValuesDictionary["SOSTENIMIENTOS"];

            short llaveNivel = 0;
            short.TryParse(nivelString, out llaveNivel);
            short llaveRegion = 0;
            short.TryParse(regionString, out llaveRegion);
            short llaveZona = 0;
            short.TryParse(zonaString, out llaveZona);
            byte llaveSostenimiento = 0;
            byte.TryParse(sostenimientoString, out llaveSostenimiento);


            cantidad = Convert.ToInt32(ws.CctPorSostenimientoQryCountForDescripcion(filtro, llaveNivel, llaveRegion, llaveZona, llaveSostenimiento));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CctPorSostenimientoQryDP[] res = ws.CctPorSostenimientoQryLoadListForDescripcion(0, cantidad, filtro, llaveNivel, llaveRegion, llaveZona, llaveSostenimiento);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CctPorSostenimientoQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.CentrotrabajoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetZonaCCTEscolar(StringDictionary knownCategoryValuesDictionary)
        {
            return GetZonaCCTEscolar(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetZonaCCTEscolar(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string nivelString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            string regionString = knownCategoryValuesDictionary["REGIONPORNIVELEDUCACION"];
            string sosteniminetoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];

            byte llaveNivel = 0;
            byte llaveRegion = 0;
            byte llaveSostenimiento = 0;

            byte.TryParse(nivelString, out llaveNivel);
            byte.TryParse(regionString, out llaveRegion);
            byte.TryParse(sosteniminetoString, out llaveSostenimiento);

            cantidad = Convert.ToInt32(ws.CttEscolarQryCountForDescripcion(llaveNivel, llaveRegion, llaveSostenimiento, "E", filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CttEscolarQryDP[] res = ws.CttEscolarQryLoadListForDescripcion(0, cantidad, llaveNivel, llaveRegion, llaveSostenimiento, "E", filtro);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CttEscolarQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.ZonaId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCentroTrabajoPorZona(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCentroTrabajoPorZona(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCentroTrabajoPorZona(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string zonaString = knownCategoryValuesDictionary["ZONAPORREGIONNIVEL"];
            string niveleducacionString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            string regionString = knownCategoryValuesDictionary["Regionporniveleducacion"];
            string sostenimientoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];

            short llaveZona = 0;
            short.TryParse(zonaString, out llaveZona);
            byte llaveNiveleducacion = 0;
            byte.TryParse(niveleducacionString, out llaveNiveleducacion);
            short llaveRegion = 0;
            short.TryParse(regionString, out llaveRegion);
            byte llaveSostenimiento = 0;
            byte.TryParse(sostenimientoString, out llaveSostenimiento);

            cantidad = Convert.ToInt32(ws.CctPorZonaQryCountForDescripcion(filtro, llaveNiveleducacion, llaveRegion, llaveSostenimiento, llaveZona));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CctPorZonaQryDP[] res = ws.CctPorZonaQryLoadListForDescripcion(0, cantidad, filtro, llaveNiveleducacion, llaveRegion, llaveSostenimiento, llaveZona);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CctPorZonaQryDP linea in res)
            {
                string descripcion = linea.Nombre + " - " + linea.Nombreturno;
                string valor = linea.CentrotrabajoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCicloEscolar(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCicloEscolar(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCicloEscolar(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string tipoCicloString = "1";

            short llaveTipoCiclo = 1;
            short.TryParse(tipoCicloString, out llaveTipoCiclo);

            cantidad = Convert.ToInt32(ws.CicloPorTipoEducacionQryCountForDescripcion(filtro, llaveTipoCiclo));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CicloPorTipoEducacionQryDP[] res = ws.CicloPorTipoEducacionQryLoadListForDescripcion(0, cantidad, filtro, llaveTipoCiclo);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CicloPorTipoEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.CicloescolarId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCCTsdeUsuario(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCCTsdeUsuario(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCCTsdeUsuario(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            string username = User.Identity.Name;
            string[] opciones = username.Split('|');
            int usuarioId = Convert.ToInt32(opciones[0]);
            int cantidad = 0;

            cantidad = Convert.ToInt32(ws.CctntUsrQryCountForUsuario(usuarioId));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CctntUsrQryDP[] res = ws.CctntUsrQryLoadListForUsuario(0, cantidad, usuarioId);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CctntUsrQryDP linea in res)
            {
                string descripcion = linea.Clavecct + " - " + linea.Nombrecct + " - " + linea.Truno;
                string valor = linea.CctntId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            return resultado;
        }


        /// <summary>
        /// Helper web service method
        /// </summary>
        /// <param name="knownCategoryValues">private storage format string</param>
        /// <param name="category">category of DropDownList to populate</param>
        /// <returns>list of content items</returns>
        [WebMethod(EnableSession=true)]        
        public CascadingDropDownNameValue[] GetDropDownContents(string knownCategoryValues, string category)
        {
            return GetDropDownContentsFilter(knownCategoryValues, category, "");
        }

        [WebMethod(EnableSession = true)]
        public CascadingDropDownNameValue[] GetDropDownContentsFilter(string knownCategoryValues, string category, string filter)
        {
            if (category.IndexOf(';') != -1)
            {
                if (knownCategoryValues == "")
                {
                    knownCategoryValues = category + ":-1";
                    string[] categorias = category.Split(';');
                    category = categorias[categorias.Length - 1];
                }
            }
            // Get a dictionary of known category/value pairs
            StringDictionary knownCategoryValuesDictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[0];
            switch (category.ToUpper())
            {
                case "CATEGORIALOCALIDAD":
                    resultado = GetCategoriaLocalidad(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "PAIS":
                    resultado = GetPais(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "ENTIDAD":
                    resultado = GetEntidad(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "MUNICIPIO":
                    resultado = GetMunicipio(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "TIPOEDUCACION":
                    resultado = GetTipoEducacion(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "NIVELEDUCACION":
                    resultado = GetNivelEducacion(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "NIVELTRABAJO":
                    resultado = GetNivelTrabajo(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "CENTROTRABAJO":
                    resultado = GetCentroTrabajo(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "REGION":
                    resultado = GetRegion(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "ZONA":
                    resultado = GetZona(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "PERFILPORNIVELTRABAJO":
                    resultado = GetPerfilPorNivelTrabajo(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "SOSTENIMIENTOS":
                    resultado = GetSostenimientos(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "ZONAPORREGION":
                    resultado = GetZonaPorRegion(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "CCTPORSOST":
                    resultado = GetCentroTrabajoPorSost(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                //case "CCTSDEUSUARIO":
                //    resultado = GetCCTsdeUsuario(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                //    break;
                default:
                    FiltroPrincipales flt = new FiltroPrincipales();
                    resultado = flt.GetDropDownContentsFilter(knownCategoryValues, category, filter);
                    break;


            }

            // Perform a simple query against the data document
            return resultado;        
        }

        //[WebMethod]
        //public string GetReport(string aParametros)
        //{
        //    //System.Diagnostics.EventLog.WriteEntry("CELFOS", "ENTRO", System.Diagnostics.EventLogEntryType.Information);
        //    string[] parametros1 = aParametros.Split('\\');
        //    string[] valores = parametros1[2].Split('|');
        //    int ciclo = int.Parse(valores[0]);
        //    int centroTrabajo = int.Parse(valores[1]);
        //    int nivel = int.Parse(valores[2]);
        //    int Sostenimiento = int.Parse(valores[3]);
        //    int turno = int.Parse(valores[4]);

        //    byte[] archivo = null;

        //    SEroot.WSEstructuras.WSEstructuras ws = new SEroot.WSEstructuras.WSEstructuras();

        //    SEroot.WSEscolar.WsEscolar wsEsc = new SEroot.WSEscolar.WsEscolar();

        //    SEroot.WSEscolar.CentroTrabajoNivelTurnoDP cctnt = wsEsc.GetCentroTrabajoNivelTurno(centroTrabajo, nivel, turno);

        //    archivo = ws.ReporteGrupoPlantillaOcupacional(ciclo, nivel, centroTrabajo, turno, Sostenimiento, cctnt.centroTrabajoNivelTurnoId);

        //    string resultado = "";
        //    string serverPath = Server.MapPath("~/PDFReports/");
        //    if (archivo != null)
        //    {
        //        if (archivo.Length > 1)
        //        {
        //            Guid nombreArchivo = Guid.NewGuid();
        //            resultado = nombreArchivo.ToString() + ".PDF";
        //            if (File.Exists(serverPath + resultado))
        //                File.Delete(serverPath + resultado);
        //            FileStream fs = new FileStream(serverPath + resultado, FileMode.CreateNew);
        //            try
        //            {
        //                resultado = "";
        //                fs.Write(archivo, 0, Convert.ToInt32(archivo.Length));
        //                fs.Flush();
        //                resultado = nombreArchivo.ToString() + ".PDF";
        //            }
        //            finally
        //            {
        //                fs.Close();
        //            }
        //        }
        //    }

        //    return "../PDFReports/"+resultado;
        //}

        [WebMethod]
        public Boolean EliminaReporte(string aReportName)
        {
            string serverPath = Server.MapPath("~/PDFReports/");
            System.Threading.Thread.Sleep(10000);
            while (File.Exists(serverPath + aReportName))
            {
                try
                {
                    File.Delete(serverPath + aReportName);
                }
                catch
                {
                }
            }
            return true;
        }


    }
}
