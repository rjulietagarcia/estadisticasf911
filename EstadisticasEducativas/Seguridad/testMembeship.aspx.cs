using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace EstadisticasEducativas.Seguridad
{
    public partial class testMembeship : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btntest_Click(object sender, EventArgs e)
        {
            if (System.Web.Security.Membership.ValidateUser(txtUsuario.Text.ToUpper(), txtContra.Text.ToUpper()))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "er", "alert('Usuario valido');",true);
            }
            else
            {
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "er2", "alert('no valido');",true);
            }
        }
    }
}
