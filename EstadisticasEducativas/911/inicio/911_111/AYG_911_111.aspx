<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AYG_911_111.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_111.AYG_911_111" Title="911.111(Alumnos y grupos)" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
<div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_111',true)">  <a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AYG_911_111',true)">             <a href="#" title="" class="activo"><span>ALUMNOS Y GRUPOS</span></a></li><li onclick="openPage('LMD_911_111',false)">            <a href="#" title="" ><span>LENGUA MATERNA</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>PERSONAL POR FUNCION</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>CARRERA Y AULAS</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>OFICIALIZACION</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
        <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>   
                        <table >
                            <tr>
                                <td style="text-align:justify;">
                            <asp:Label ID="Label25" Font-Size="16px" runat="server" CssClass="lblGrisTit" Text="I. ALUMNOS Y GRUPOS"></asp:Label><br />
                                </td>
                            </tr>
                            <tr>
                                <td  style="text-align:justify;width:800px">
                                    &nbsp;<asp:Label ID="lblInstrucion1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de alumnos inscritos a partir de la fecha de inicio de cursos, sumando las altas y restando las bajas hasta el 30 de septiembre, clasific�ndolos por grado, sexo y edad. Escriba adem�s el n�mero de grupos por grado. Verifique que la suma de los subtotales de los alumnos por edad sea igual al total." ></asp:Label></td>
                            </tr>
                             <tr>
                                <td  style="text-align:center;width:800px">
                                <br />
                                
                                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" 
                                    Text="Estad�stica de alumnos por grado, sexo, edad y grupos" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td >
                                <table style="width: 700px" align="center">
                            <tr>
                                <td colspan="2">
                                </td>
                                <td  >
                                    <asp:Label ID="lbl3a" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="lbl4a" runat="server" CssClass="lblGrisTit" Text="4 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="lbl5a" runat="server" CssClass="lblGrisTit" Text="5 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="lbl6a" runat="server" CssClass="lblGrisTit" Text="6 a�os" Width="100%"></asp:Label></td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS" Width="100%"></asp:Label></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan="3"  >
                                    <asp:Label ID="lbl1" runat="server" CssClass="lblNegro" Text="1�" Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="height: 26px; text-align: left;"  >
                                    <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                <td style="height: 26px"  >
                                    <asp:TextBox ID="txtV1" runat="server" Columns="4"  MaxLength="4" TabIndex="10101" CssClass="lblNegro" ></asp:TextBox></td>
                                <td style="height: 26px"  >
                                    <asp:TextBox ID="txtV2" runat="server" Columns="4"  MaxLength="4" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="height: 26px"  >
                                    <asp:TextBox ID="txtV3" runat="server" Columns="4"  MaxLength="4" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="height: 26px"  >
                                    <asp:TextBox ID="txtV4" runat="server" Columns="4"  MaxLength="4" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px; height: 26px;"  >
                                    <asp:TextBox ID="txtV5" runat="server" Columns="4"  MaxLength="4" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                                <td rowspan="2" style="width: 1px"  >
                                    &nbsp;</td>
                                <td rowspan="2" style="width: 5px"  >
                                    &nbsp;</td>
                                <td   rowspan="2" style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV6" runat="server" Columns="4"  MaxLength="4" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV7" runat="server" Columns="4"  MaxLength="4" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV8" runat="server" Columns="4"  MaxLength="4" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV9" runat="server" Columns="4"  MaxLength="4" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV10" runat="server" Columns="4"  MaxLength="4" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="height: 25px; text-align: left;"  >
                                    <asp:Label ID="lblSubtotal1" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL" Width="100%"></asp:Label></td>
                                <td style="height: 25px"  >
                                    <asp:TextBox ID="txtV11" runat="server" Columns="4"  MaxLength="4" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="height: 25px"  >
                                    <asp:TextBox ID="txtV12" runat="server" Columns="4"  MaxLength="4" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="height: 25px"  >
                                    <asp:TextBox ID="txtV13" runat="server" Columns="4"  MaxLength="4" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="height: 25px"  >
                                    <asp:TextBox ID="txtV14" runat="server" Columns="4"  MaxLength="4" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px; height: 25px;"  >
                                    <asp:TextBox ID="txtV15" runat="server" Columns="4"  MaxLength="4" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 1px; height: 25px"  >
                                    &nbsp;</td>
                                <td style="width: 5px; height: 25px;"  >
                                    <asp:TextBox ID="txtV16" runat="server" Columns="2"  MaxLength="2" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
                                <td   style="width: 5px; height: 25px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="9" rowspan="1" style="height: 10px">
                                </td>
                                <td colspan="1" rowspan="1" style="height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td  >
                                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="4 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Text="5 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Text="6 a�os" Width="100%"></asp:Label></td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Text="GRUPOS" Width="100%"></asp:Label></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan="3"  >
                                    <asp:Label ID="lbl2" runat="server" CssClass="lblNegro" Text="2�" Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV17" runat="server" Columns="4"  MaxLength="4" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV18" runat="server" Columns="4"  MaxLength="4" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV19" runat="server" Columns="4"  MaxLength="4" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV20" runat="server" Columns="4"  MaxLength="4" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV21" runat="server" Columns="4"  MaxLength="4" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                                <td rowspan="2" style="width: 1px"  >
                                    &nbsp;</td>
                                <td rowspan="2" style="width: 5px"  >
                                    &nbsp;</td>
                                <td   rowspan="2" style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV22" runat="server" Columns="4"  MaxLength="4" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV23" runat="server" Columns="4"  MaxLength="4" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV24" runat="server" Columns="4"  MaxLength="4" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV25" runat="server" Columns="4"  MaxLength="4" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV26" runat="server" Columns="4"  MaxLength="4" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblSubtotal2" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV27" runat="server" Columns="4"  MaxLength="4" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV28" runat="server" Columns="4"  MaxLength="4" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV29" runat="server" Columns="4"  MaxLength="4" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV30" runat="server" Columns="4"  MaxLength="4" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV31" runat="server" Columns="4"  MaxLength="4" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV32" runat="server" Columns="2"  MaxLength="2" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="9" style="height: 10px">
                                </td>
                                <td colspan="1" style="height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td  >
                                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Text="4 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Text="5 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Text="6 a�os" Width="100%"></asp:Label></td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Text="GRUPOS" Width="100%"></asp:Label></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan="3"  >
                                    <br />
                                    <asp:Label ID="lbl3" runat="server" CssClass="lblNegro" Text="3�" Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="text-align: left"  >
                                    &nbsp;<asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV33" runat="server" Columns="4"  MaxLength="4" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV34" runat="server" Columns="4"  MaxLength="4" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV35" runat="server" Columns="4"  MaxLength="4" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV36" runat="server" Columns="4"  MaxLength="4" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV37" runat="server" Columns="4"  MaxLength="4" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                                <td rowspan="2" style="width: 1px"  >
                                    &nbsp;</td>
                                <td rowspan="2" style="width: 5px"  >
                                    &nbsp;</td>
                                <td   rowspan="2" style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV38" runat="server" Columns="4"  MaxLength="4" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV39" runat="server" Columns="4"  MaxLength="4" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV40" runat="server" Columns="4"  MaxLength="4" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV41" runat="server" Columns="4"  MaxLength="4" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV42" runat="server" Columns="4"  MaxLength="4" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblSubtotal3" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV43" runat="server" Columns="4"  MaxLength="4" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV44" runat="server" Columns="4"  MaxLength="4" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV45" runat="server" Columns="4"  MaxLength="4" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV46" runat="server" Columns="4"  MaxLength="4" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV47" runat="server" Columns="4"  MaxLength="4" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV48" runat="server" Columns="2"  MaxLength="2" TabIndex="10906" CssClass="lblNegro"></asp:TextBox></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="9" style="height: 10px">
                                </td>
                                <td colspan="1" style="height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td  >
                                    <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Text="4 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Text="5 a�os" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Text="6 a�os" Width="100%"></asp:Label></td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="Label23" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Text="GRUPOS" Width="100%"></asp:Label></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan="3"  >
                                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblNegro" Text="TOTAL" Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblHombresT" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV49" runat="server" Columns="4"  MaxLength="4" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV50" runat="server" Columns="4"  MaxLength="4" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV51" runat="server" Columns="4"  MaxLength="4" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV52" runat="server" Columns="4"  MaxLength="4" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV53" runat="server" Columns="4"  MaxLength="4" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                                <td rowspan="2" style="width: 1px"  >
                                    &nbsp;</td>
                                <td rowspan="2" style="width: 5px"  >
                                    &nbsp;</td>
                                <td   rowspan="2" style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblMujeresT" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV54" runat="server" Columns="4"  MaxLength="4" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV55" runat="server" Columns="4"  MaxLength="4" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV56" runat="server" Columns="4"  MaxLength="4" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV57" runat="server" Columns="4"  MaxLength="4" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV58" runat="server" Columns="4"  MaxLength="4" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left"  >
                                    <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:TextBox ID="txtV59" runat="server" Columns="4"  MaxLength="4" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV60" runat="server" Columns="4"  MaxLength="4" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV61" runat="server" Columns="4"  MaxLength="4" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >
                                    <asp:TextBox ID="txtV62" runat="server" Columns="4"  MaxLength="4" TabIndex="11204" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV63" runat="server" Columns="4"  MaxLength="4" TabIndex="11205" CssClass="lblNegro"></asp:TextBox></td>
                                <td style="width: 1px"  >
                                    &nbsp;</td>
                                <td style="width: 5px"  >
                                    <asp:TextBox ID="txtV64" runat="server" Columns="2"  MaxLength="2" TabIndex="11206" CssClass="lblNegro"></asp:TextBox></td>
                                <td   style="width: 5px">
                                    &nbsp;</td>
                            </tr>
                        </table>
                                </td>
                            </tr>
                            </table>
                                          <br />
                            <table style="width:600px" >
                            <tr>
                                <td valign="top" style="text-align:justify;width:50%">
                                    &nbsp;<asp:Label ID="lblTit6" runat="server" CssClass="lblRojo" 
                                    Text="2. Escriba la cantidad de alumnos con discapacidad, 
                                    aptitudes sobresalientes u otras condiciones, desglos�ndolos por sexo." Width="100%"></asp:Label>
                                                <table style="width: 355px">
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblSituacion" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO" Width="100%" Font-Bold="True"></asp:Label></td>
                                                        <td style="width: 61px">
                                                            <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                                        <td style="width: 58px">
                                                            <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:Label ID="lblTotal6" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV65" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20101"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV66" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20102"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV67" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20103"
                                                               ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV68" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20201"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV69" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20202"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV70" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20203"
                                                               ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV71" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20301"
                                                                ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV72" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20302"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV73" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20303"
                                                                ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; height: 25px; text-align: left">
                                                            <asp:Label ID="lblAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; height: 25px; text-align: center">
                                                            <asp:TextBox ID="txtV74" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20401"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; height: 25px; text-align: center">
                                                            <asp:TextBox ID="txtV75" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20402"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 51px; height: 25px; text-align: center">
                                                            <asp:TextBox ID="txtV76" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20403"
                                                              ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV77" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20501"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV78" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20502"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV79" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20503"
                                                               ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV80" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20601"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV81" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20602"
                                                                ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV82" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20603"
                                                              ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblSobresalientes" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV83" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20701"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV84" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20702"
                                                             ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV85" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20703"
                                                              ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; text-align: left">
                                                            <asp:Label ID="lblOtros6" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; text-align: center">
                                                            <asp:TextBox ID="txtV86" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20801"
                                                               ></asp:TextBox></td>
                                                        <td style="width: 58px; text-align: center">
                                                            <asp:TextBox ID="txtV87" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2" TabIndex="20802"
                                                             ></asp:TextBox></td>
                                                        <td style="width: 51px; text-align: center">
                                                            <asp:TextBox ID="txtV88" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20803"
                                                            ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 169px; height: 25px; text-align: left">
                                                            <asp:Label ID="lblTotalT6" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                                        <td style="width: 61px; height: 25px; text-align: center">
                                                            <asp:TextBox ID="txtV89" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20901"
                                                          ></asp:TextBox></td>
                                                        <td style="width: 58px; height: 25px; text-align: center">
                                                            <asp:TextBox ID="txtV90" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20902"
                                                             ></asp:TextBox></td>
                                                        <td style="width: 51px; height: 25px; text-align: center">
                                                            <asp:TextBox ID="txtV91" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20903"
                                                             ></asp:TextBox></td>
                                                    </tr>
                                                </table>
                                </td>
                                <td>&nbsp;&nbsp;</td>
                                <td style="text-align:justify;width:50%;" valign="top">
                                    &nbsp;<asp:Label ID="lblTit5" runat="server" CssClass="lblRojo" Text="3. Escriba la cantidad de alumnos con Necesidades Educativas Especiales (NEE), independientemente de que presenten o no alguna discapacidad, desglos�ndola por sexo." Width="100%"></asp:Label>
                                                <table style="width: 415px">
                                                    <tr>
                                                        <td>
                                                            &nbsp;<asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV92" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="21001"
                                                                ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;<asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV93" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="21101"
                                                               ></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;<asp:Label ID="lblTotal5" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV94" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="21201"
                                                                ></asp:TextBox></td>
                                                    </tr>
                                                </table>
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <table align="center">
                            <tr>
                                <td ><span  onclick="openPage('Identificacion_911_111',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                                <td ><span  onclick="openPage('Identificacion_911_111',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                <td style="width: 330px;">
                                   &nbsp;
                                </td>
                                <td ><span  onclick="openPage('LMD_911_111',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                <td ><span  onclick="openPage('LMD_911_111',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                            </tr>
                        </table>
                        
                        <div id="divResultado" class="divResultado" ></div>
                       
                        <br />
                        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
                        <input id="hidIdCtrl" type="hidden" runat= "server" />
                        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
                        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
                          <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                                <div class="fondoDegradado">
                                 <br /><br /><br /><br />
                                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;background:#000;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                                 Este proceso puede tardar....</span>
                                 <br />
                                </div>
                         </div>
                           <asp:Panel ID="pnlFallas" runat="server">
                           </asp:Panel>
                           
                    <!--/Contenido dentro del area de trabajo-->
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			</table>
			
        <script type="text/javascript" language="javascript">
                MaxCol = 10;
                MaxRow = 30;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		        Disparador(<%=hidDisparador.Value %>);
        </script>
    </center>
</asp:Content>
