<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6(Total)" AutoEventWireup="true" CodeBehind="AGT_911_6.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6.AGT_911_6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_6',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_6',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_6',true)"><a href="#" title="" ><span>3�</span></a></li>
        <li onclick="openPage('AGT_911_6',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_6',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Revisar la cantidad de alumnos inscritos, existentes y aprobados por grado, edad y g�nero, y en caso de observar alguna inconsistencia en la informaci�n favor de realizar los ajustes pertinentes en el sistema de Control Escolar.</p>
    <p>La cantidad de alumnos aprobados se mostrar� cuando se haya concluido el ingreso de las calificaciones del quinto bimestre. Por lo tanto no deber� oficializar la estad�stica hasta que todas las calificaciones est�n en el sistema y la informaci�n en este cuestionario haya sido debidamente validada.</p>
    <p>Para calcular la edad de los alumnos se ha considerado a�os cumplidos al 8 de julio del a�o en curso, que es la fecha en que termina el ciclo escolar.</p>
    <p>En esta pantalla est�n disponibles los links para acceder al Control Escolar y realizar correcciones en los datos de los alumnos, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         <table style="text-align:center">
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="TOTAL" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl_12" runat="server" Text="Menos de 12 a�os" CssClass="lblRojo" Width="48px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="Label1" runat="server" Text="12 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" Text="13 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" Text="14 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl16" runat="server" CssClass="lblRojo" Text="16 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl17" runat="server" CssClass="lblRojo" Text="17 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl18" runat="server" CssClass="lblRojo" Text="18 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV220" runat="server" Columns="4" ReadOnly="True" MaxLength="4" TabIndex="10101" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV221" runat="server" Columns="4" ReadOnly="True" TabIndex="10102" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV222" runat="server" Columns="4" ReadOnly="True" TabIndex="10103" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV223" runat="server" Columns="4" ReadOnly="True" TabIndex="10104" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV224" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV225" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV226" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV227" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10108" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV228" runat="server" Columns="5" ReadOnly="True" TabIndex="10109" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV229" runat="server" Columns="4" ReadOnly="True" TabIndex="10201" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV230" runat="server" Columns="4" ReadOnly="True" TabIndex="10202" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV231" runat="server" Columns="4" ReadOnly="True" TabIndex="10203" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV232" runat="server" Columns="4" ReadOnly="True" TabIndex="10204" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV233" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV234" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV235" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV236" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10208" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV237" runat="server" Columns="5" ReadOnly="True" TabIndex="10209" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosH" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV238" runat="server" Columns="4" ReadOnly="True" TabIndex="10301" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>                        
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV239" runat="server" Columns="4" ReadOnly="True" TabIndex="10302" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV240" runat="server" Columns="4" ReadOnly="True" TabIndex="10303" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV241" runat="server" Columns="4" ReadOnly="True" TabIndex="10304" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV242" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV243" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV244" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV245" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10308" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV246" runat="server" Columns="5" ReadOnly="True" TabIndex="10309" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                         </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV247" runat="server" Columns="4" ReadOnly="True" TabIndex="10401" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV248" runat="server" Columns="4" ReadOnly="True" TabIndex="10402" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV249" runat="server" Columns="4" ReadOnly="True" TabIndex="10403" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV250" runat="server" Columns="4" ReadOnly="True" TabIndex="10404" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV251" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV252" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV253" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV254" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10408" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV255" runat="server" Columns="5" ReadOnly="True" TabIndex="10409" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV256" runat="server" Columns="4" ReadOnly="True" TabIndex="10501" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV257" runat="server" Columns="4" ReadOnly="True" TabIndex="10502" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV258" runat="server" Columns="4" ReadOnly="True" TabIndex="10503" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV259" runat="server" Columns="4" ReadOnly="True" TabIndex="10504" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV260" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV261" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV262" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV263" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10508" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV264" runat="server" Columns="5" ReadOnly="True" TabIndex="10509" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosM" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV265" runat="server" Columns="4" ReadOnly="True" TabIndex="10601" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV266" runat="server" Columns="4" ReadOnly="True" TabIndex="10602" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV267" runat="server" Columns="4" ReadOnly="True" TabIndex="10603" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV268" runat="server" Columns="4" ReadOnly="True" TabIndex="10604" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV269" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10605" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV270" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10606" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV271" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10607" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV272" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10608" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV273" runat="server" Columns="5" ReadOnly="True" TabIndex="10609" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td style="height: 102px">
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV274" runat="server" Columns="4" ReadOnly="True" TabIndex="10701" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV275" runat="server" Columns="4" ReadOnly="True" TabIndex="10702" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV276" runat="server" Columns="4" ReadOnly="True" TabIndex="10703" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV277" runat="server" Columns="4" ReadOnly="True" TabIndex="10704" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV278" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10705" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV279" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10706" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV280" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10707" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV281" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10708" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV282" runat="server" Columns="5" ReadOnly="True" TabIndex="10709" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV283" runat="server" Columns="4" ReadOnly="True" TabIndex="10801" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV284" runat="server" Columns="4" ReadOnly="True" TabIndex="10802" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV285" runat="server" Columns="4" ReadOnly="True" TabIndex="10803" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV286" runat="server" Columns="4" ReadOnly="True" TabIndex="10804" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV287" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10805" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV288" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10806" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV289" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10807" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV290" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10808" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV291" runat="server" Columns="5" ReadOnly="True" TabIndex="10809" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosS" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV292" runat="server" Columns="4" ReadOnly="True" TabIndex="10901" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV293" runat="server" Columns="4" ReadOnly="True" TabIndex="10902" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV294" runat="server" Columns="4" ReadOnly="True" TabIndex="10903" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV295" runat="server" Columns="4" ReadOnly="True" TabIndex="10904" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV296" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10905" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV297" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10906" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV298" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10907" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV299" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10908" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV300" runat="server" Columns="5" ReadOnly="True" TabIndex="10909" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px; height: 16px">
                            </td>
                            <td style="width: 132px; height: 16px">
                                &nbsp;</td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 81px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 81px">
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV301" runat="server" Columns="3" ReadOnly="True" TabIndex="11101" CssClass="lblNegro" MaxLength="3">0</asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG3_911_6',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG3_911_6',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGD_911_6',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_6',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
      <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
          <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
        GetTabIndexes();  
        </script> 
</asp:Content>
