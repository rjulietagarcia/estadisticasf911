<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="ECC-21(Alumnos)" AutoEventWireup="true" CodeBehind="Alumnos_ECC_21.aspx.cs" Inherits="EstadisticasEducativas._911.fin.ECC_21.Alumnos_ECC_21" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 6;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

<%--EDUCACI�N COMUNITARIA RURAL PREESCOLAR--%>
   <div id="logo"></div>
    <div style="min-width:900px; height:65px;">
    <div id="header">
        <table style="width: 100%;">
                <tr>
                    <td>
                        <span>EDUCACI�N COMUNITARIA RURAL PREESCOLAR</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
    </div>
    </div>

    <div id="menu" style="min-width:900px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_ECC_21',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos_ECC_21',true)"><a href="#" title="" class="activo"><span>ALUMNOS</span></a></li>
        <li onclick="openPage('Anexo_ECC_21',false)"><a href="#" title=""><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td>
                <asp:Label ID="lblMsg" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="20px"
                        Text="IMPORTANTE:  AL CONTESTAR ESTE CUESTIONARIO NO CONSIDERE LA INFORMACI�N DE PRIMARIA." Width="100%">
                        </asp:Label> <br /><br />
                    <asp:Label ID="lblALUMNOS" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="I. ALUMNOS" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblInstruccionI1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Marque el tipo de servicio."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblPreescolar" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Preescolar comunitario"
                        Width="100%"></asp:Label></td>
                <td style="width: 108px; text-align: left">
                    <asp:RadioButton ID="optV1" runat="server" GroupName="servicio" />
                    <asp:TextBox ID="txtV1" runat="server" style="visibility:hidden; width:20px;" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblPAEPI" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Ind�gena (PAEPI)"
                        Width="100%"></asp:Label></td>
                <td style="width: 108px; text-align: left">
                    <asp:RadioButton ID="optV2" runat="server" GroupName="servicio" />
                    <asp:TextBox ID="txtV2" runat="server"  style="visibility:hidden; width:20px;" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCIC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Centro infantil Comunitario (CIC)"
                        Width="100%"></asp:Label></td>
                <td style="width: 108px; text-align: left">
                    <asp:RadioButton ID="optV3" runat="server" GroupName="servicio" />
                    <asp:TextBox ID="txtV3" runat="server"  style="visibility:hidden; width:20px;" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblPAEPIAM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Infantil Agr�cola Migrante (PAEPIAM)"
                        Width="100%"></asp:Label></td>
                <td style="width: 108px; text-align: left">
                    <asp:RadioButton ID="optV40" runat="server" GroupName="servicio" />
                    <asp:TextBox ID="txtV40" runat="server"  style="visibility:hidden; width:20px;" ></asp:TextBox></td>
            </tr>
        </table>
                </td>
            </tr>
            <tr>
                <td>
                                <asp:Label ID="lblInstruccionI2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. �El proyecto es de aulas compartidas?"
                                    Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 600px">
                        <tr>
                            <td style="text-align: right">
                                <asp:RadioButton ID="optV4" runat="server" GroupName="aulasCompartidas" Text="S�" />
                                <asp:TextBox ID="txtV4" runat="server" style="visibility:hidden; width:20px;" ></asp:TextBox></td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtV41" runat="server" style="visibility:hidden; width:20px;" ></asp:TextBox>
                                <asp:RadioButton ID="optV41" runat="server" GroupName="aulasCompartidas" Text="NO" />
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblInstruccionI3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el total de alumnos, desglos�ndolo por sexo, inscripci�n total, existencia y edad. Verifique que la suma de los alumnos por edad sea igual al total."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="lbl3a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3 a�os"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl4a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4 a�os"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl5a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5 a�os"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl6a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblTotal1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: left">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left">
                    <asp:Label ID="lblInscripcionTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV5" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV6" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV7" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10103"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtV8" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV9" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblExistenciaH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV10" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV11" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV12" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV14" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: left">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left">
                    <asp:Label ID="lblInscripcionTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV15" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV16" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV17" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV18" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV19" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblExistenciasM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV20" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV21" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV22" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV23" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV24" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: left">
                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left">
                    <asp:Label ID="InscripcionTT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV25" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV26" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV27" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV28" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV29" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblExistenciaT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV30" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV31" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV32" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV33" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV34" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
            </tr>
        </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: justify">
                                <asp:Label ID="lblInstruccionI4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba, por sexo, la cantidad de alumnos que fueron promovidos a primaria."
                                    Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblPromovidos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROMOVIDOS A PRIMARIA"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                <asp:Label ID="lblPromovidosH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV35" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
            <asp:Label ID="lblPromovidosM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV36" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
            <asp:Label ID="lblPromovidosT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV37" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="II. PERSONAL" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: justify">
                                <asp:Label ID="lblInstruccionII1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el n�mero de instructores comunitarios o agentes educativos."
                                    Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="lblPersonalT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV38" runat="server" Columns="3" MaxLength="1" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_ECC_21',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_ECC_21',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Anexo_ECC_21',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_ECC_21',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
           <<div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center> 
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function PintaOPTs(){
                     marcar('V1');
                     marcar('V2');
                     marcar('V3');
                     marcar('V4');
                     marcar('V40');
                     marcar('V41');
                }  
                
                function marcar(variable){
                     var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                     if (txtv != null) {
                         var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                         if (txtv.value == 'X'){
                             chk.checked = true;
                         } else {
                             chk.checked = false;
                         } 
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('V1');
                     marcarTXT('V2');
                     marcarTXT('V3');
                     marcarTXT('V4');
                     marcarTXT('V40');
                     marcarTXT('V41');
                }    
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                } 
                PintaOPTs();
                Disparador(<%=hidDisparador.Value %>);            
        </script>  
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>