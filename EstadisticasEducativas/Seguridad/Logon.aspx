<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasic.Master" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="EstadisticasEducativas.Seguridad.Logon" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <span style="color: #FF0000"><strong style="background-color: #000000">Periodo de pruebas para la captura del 
    ciclo Fin 2012-2013<script type="text/javascript" src="../tema/js/jquery.tools.min.js"></script><script type="text/javascript" src="../tema/js/sep04.js"></script></strong></span><link href="../tema/css/Styles.css" rel="stylesheet" type="text/css" /><span 
        style="color: #FF0000"><strong style="background-color: #000000">, del 20 de mayo al 31 de mayo, </strong>
    </span><strong>
    <br style="color: #FF0000" />
    </strong><span style="color: #FF0000"><strong style="background-color: #000000">una vez terminado este periodo los 
    datos se eliminaran totalmente.</strong></span><br />
<br />
 <center>
            <table id="Table1" cellspacing="0" cellpadding="0" border="0" style="width: 276px;
                height: 91%; text-align: center;">
                <tr>
                    <td>   
                    <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0" class="Logon">
                        <tr>
                            <td style="width: 440px;">
                                <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate"
                                 FailureText="Acceso denegado!, verifique usuario y contrase�a">
                                        <TitleTextStyle CssClass="TextTitleLogon" HorizontalAlign="Left" />
                                        <CheckBoxStyle CssClass="chekLogon" />
                                        <TextBoxStyle CssClass="TextboxesLogon" />
                                        <LoginButtonStyle CssClass="ButtomLogon" />
                                        <LabelStyle CssClass="lblLogn" />
                                        <ValidatorTextStyle CssClass="ValidadorLogon" />
                                        
                                    <LayoutTemplate>
                                        <table border="0" cellpadding="1" cellspacing="0" style="border-collapse: collapse">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="1">
                                                        <tr>
                                                            <td align="center" colspan="2" class="TextTitleLogon">
                                                                Iniciar sesi�n</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="lblLogn">
                                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="lblLogn">Nombre de usuario:</asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="UserName" runat="server" CssClass="TextboxesLogon"></asp:TextBox>
                                                                <asp:RequiredFieldValidator CssClass="ValidadorLogon" ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                    ErrorMessage="El nombre de usuario es obligatorio." ToolTip="El nombre de usuario es obligatorio."
                                                                    ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="lblLogn">
                                                                <asp:Label CssClass="lblLogn" ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contrase�a:</asp:Label></td>
                                                            <td>
                                                                <asp:TextBox CssClass="TextboxesLogon" ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                                                <asp:RequiredFieldValidator CssClass="ValidadorLogon" ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                                    ErrorMessage="La contrase�a es obligatoria." ToolTip="La contrase�a es obligatoria."
                                                                    ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                               <%-- <asp:CheckBox ID="RememberMe" runat="server" Text="Record�rmelo la pr�xima vez." />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2" style="color: red">
                                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="2">
                                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Inicio de sesi�n"
                                                                    ValidationGroup="Login1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                </asp:Login>
                            </td>
                        </tr>
                    </table>
    </td>
    </tr>
    </table>
    </center>

</asp:Content>

