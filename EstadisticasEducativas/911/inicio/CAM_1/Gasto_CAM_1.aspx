<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Gasto" AutoEventWireup="true" CodeBehind="Gasto_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.Gasto_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="true"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li><li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li><li onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li><li onclick="openPage('Desgolse_CAM_1',true)"><a href="#" title=""><span>DESGLOSE</span></a></li><li onclick="openPage('Personal1_CAM_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li><li onclick="openPage('Personal2_CAM_1',true)"><a href="#" title=""><span>PERSONAL(CONT)</span></a></li><li onclick="openPage('CMYA_CAM_1',true)"><a href="#" title=""><span>CARRERA MAG Y AULAS</span></a></li><li onclick="openPage('Gasto_CAM_1',true)"><a href="#" title="" class="activo"><span>GASTO</span></a></li><li onclick="openPage('Anexo_CAM_1',false)"><a href="#" title="" ><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>



   <table style="WIDTH: 800px">
   <tbody>
        <tr>
            <td style="padding-bottom: 20px;text-align:justify"><asp:Label id="lblGASTOS" runat="server" Text="V. GASTOS DE LAS FAMILIAS EN EDUCACI�N" Font-Size="16px" CssClass="lblGrisTit" Width="100%"></asp:Label></td></tr><tr><td style="text-align: justify"><asp:Label id="lblInstruccionA" runat="server" Text="a) La informaci�n de esta secci�n ser� utilizada exclusivamente para obtener indicadores de gasto educativo." CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblInstruccionB" runat="server" Text="b) El punto n�mero 1 deber� ser constestado por las escuelas de todos sostenimientos. El punto n�mero 2 �nicamente por las escuelas con sostenimiento particular." CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblInstruccionC" runat="server" Text="c) Presente las cifras en pesos; no utilice decimales." CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblInstruccionD" runat="server" Text="d) Para contestar, considere las definiciones siguientes. Si no cuenta con cantidades exactas, d� una aproximaci�n de las mismas." CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblDefinicion1" runat="server" Text="GASTO PROMEDIO ANUAL. Es el monto promedio de dinero que gasta cada alumno (o los padres del alumno) en un determinado concepto, durante el ciclo escolar. Se aplica a los siguientes conceptos: inscripci�n, paquetes de �tiles y libros (cuando �stos se soliciten) y uniformes. Asimismo, se aplican a cuotas que requieran un desembolso para las familias; por ejemplo, las aportaciones a la asociaci�n de padres de familia o alguna ayuda para el arreglo de la escuela o para equipar laboratorios y talleres, etc�tera." CssClass="lblGrisTit" Width="100%" Height="68px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblDefinicion2" runat="server" Text="GASTO PROMEDIO MENSUAL. Es el monto promedio de dinero que gasta cada alumno (o los padres del alumno) en septiembre por conceptos de colegiatura y(o) transporte escolar en escuelas particulares. Es el resultado de dividir el total de los ingresos de la escuela en septiembre entre el total de alumnos. Por ejemplo: si el ingreso de la escuela por colegiaturas pagadas por las familias durante septiembre fue de $15,000 y el n�mero de alumnos es de 100, el gasto promedio mensual en colegiaturas es de $150, cantidad que se reportar� en el rubro correspondiente." CssClass="lblGrisTit" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px; text-align: justify;"><asp:Label id="lblInciso1" runat="server" Text="1. ESCUELAS DE TODOS LOS SOSTENIMIENTOS" Font-Bold="true" CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblInstruccion1" runat="server" Text="En el caso de escuelas particulares, considere los gastos y compras que los alumnos y(o) padres de familia hacen directamente en la instituci�n, as� como fuera de ella." Font-Bold="true" CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
            <table width="100%">
                <tbody>
                    <tr>
                    
                        <td style="text-align: left"><asp:Label id="lblGasto11" runat="server" Text="GASTO PROMEDIO ANUAL EN EL PAQUETE DE �TILES Y LIBROS QUE SE SUGIERE ADQUIERA EL ALUMNO" Font-Bold="true" CssClass="lblGrisTit" Width="600px"></asp:Label>
                        </td>
                        <td style="text-align: right; width: 61px;"><asp:TextBox id="txtV1182" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10101"></asp:TextBox> 
                        </td>
                     </tr>
                     <tr>
                        <td style="text-align: left"><asp:Label id="lblGasto12" runat="server" Text="GASTO PROMEDIO ANUAL EN UNIFORMES QUE SE SUGIERE ADQUIERA EL ALUMNO" Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label>
                        </td>
                        <td style="text-align: right; width: 61px;"><asp:TextBox id="txtV1183" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10201"></asp:TextBox>
                        </td>
                     </tr>
                     <tr>
                        <td style="text-align: left"><asp:Label id="lblGasto13" runat="server" Text="GASTO PROMEDIO ANUAL EN CUOTAS" Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label>
                        </td>
                        <td style="text-align: right; width: 61px;"><asp:TextBox id="txtV1184" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10301"></asp:TextBox>
                        </td>
                     </tr>
                 </tbody>
             </table>
             </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblInciso2" runat="server" Text="2. ESCUELAS PARTICULARES" Font-Bold="true" CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left" width="100%">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left"><asp:Label id="lblGasto21" runat="server" Text="GASTO PROMEDIO ANUAL EN INSCRIPCI�N" Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label>
                        </td>
                        <td style="text-align: right" colspan="2"><asp:TextBox id="txtV1185" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10401"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><asp:Label id="lblGasto22" runat="server" Text="GASTO PROMEDIO MENSUAL EN COLEGIATURA" Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label>
                        </td>
                        <td style="text-align: right" colspan="2"><asp:TextBox id="txtV1186" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10501"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><asp:Label id="lblGasto23" runat="server" Text="N�MERO DE MENSUALIDADES QUE SE PAGAN" Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label>
                        </td>
                        <td style="text-align: right" colspan="2"><asp:TextBox id="txtV1187" runat="server" Columns="5" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><asp:Label id="lblGasto24" runat="server" Text="�LA ESCUELA OFRECE SERVICIO DE TRANSPORTE ESCOLAR, PROPIO O CONCESIONADO?" Font-Bold="true" CssClass="lblGrisTit" Width="580px"></asp:Label>
                        </td>
                        <td style="text-align: right;" colspan="2">
                        <asp:TextBox ID="txtV1188" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" style="visibility:hidden; width:1px;" ></asp:TextBox><asp:TextBox ID="txtV1189" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" style="visibility:hidden; width:1px;"></asp:TextBox>
                            <asp:RadioButton id="optV1188" onclick="OPTs2Txt();" runat="server" Text="SI" GroupName="trANSPORTE" TabIndex="10701" onkeydown="return Arrows(event,this.tabIndex)"></asp:RadioButton>
                            <asp:RadioButton id="optV1189" onclick="OPTs2Txt();" runat="server" Text="NO" GroupName="trANSPORTE" TabIndex="10801" onkeydown="return Arrows(event,this.tabIndex)"></asp:RadioButton>
                            </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: justify"><asp:Label id="lblInstrucion2" runat="server" Text="Si la respuesta es S�, conteste lo siguiente:" Font-Bold="true" CssClass="lblRojo" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td style="height: 23px; text-align: left">
                            </td>
                            <td style="HEIGHT: 23px; text-align: left"><asp:Label id="lblGasto25" runat="server" Text="GASTO PROMEDIO MENSUAL EN SERVICIO DE trANSPORTE" Font-Bold="true" CssClass="lblGrisTit" Width="600px"></asp:Label>
                            </td>
                            <td style="HEIGHT: 23px; text-align: right"><asp:TextBox id="txtV1190" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10901"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                &nbsp;</td>
                            <td style="text-align: left">
                                <asp:Label id="lblGasto26" runat="server" Text="N�MERO DE MENSUALIDADES QUE SE PAGAN" Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label></td>
                            <td style="text-align: right">
                                &nbsp;<asp:TextBox id="txtV1191" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11001"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                &nbsp;</td>
                            <td style="text-align: left">
                                <asp:Label id="lblGasto27" runat="server" Text="N�MERO DE ALUMNOS QUE UTILIZAN EL SERVICIO." Font-Bold="true" CssClass="lblGrisTit" Width="100%"></asp:Label></td>
                            <td style="text-align: right">
                                &nbsp;<asp:TextBox id="txtV1192" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td></tr></tbody></table></td></tr>
     </tbody>
     </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('CMYA_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('CMYA_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatder">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</tbody>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 2;
                MaxRow = 12;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function OPTs2Txt(){
                     marcarTXT('1188');
                     marcarTXT('1189'); 
                } 
                function PintaOPTs(){
                     marcar('1188');
                     marcar('1189');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                 PintaOPTs();                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
