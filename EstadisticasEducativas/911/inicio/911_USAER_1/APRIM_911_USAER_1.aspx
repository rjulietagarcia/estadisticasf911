<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Alumnos Primaria)" AutoEventWireup="true" CodeBehind="APRIM_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.APRIM_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
    
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title="" class="activo"><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_1',false)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center> 
            
   
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td >
                    <table id="TABLE2" style="text-align: center;">
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lbl4" runat="server" CssClass="lblRojo" Text="4. Escriba la poblaci�n total con necesidades educativas especiales atendida en educaci�n primaria, por escuela, grado y sexo."
                                    Width="750px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPrimaria" runat="server" CssClass="lblRojo" Text="PRIMARIA" Width="500px"></asp:Label>
                                </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="12" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL POR GRADO"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPobTotal" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL (SUMA)"
                                    Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 3px; width: 67px;">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="2">
                                <asp:Label ID="lbl41" runat="server" CssClass="lblRojo" Text="1�" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="2">
                                <asp:Label ID="lbl42" runat="server" CssClass="lblRojo" Text="2�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="lbl43" runat="server" CssClass="lblRojo" Text="3�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2">
                                &nbsp;<asp:Label ID="lbl44" runat="server" CssClass="lblRojo" Text="4�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="lbl45" runat="server" CssClass="lblRojo" Text="5�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="lbl46" runat="server" CssClass="lblRojo" Text="6�" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;">
                                &nbsp;<asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 69px;">
                                <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblHombresTot" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblMujeresTot" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblTotal41" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV444" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV445" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV446" runat="server" Columns="2" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV447" runat="server" Columns="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV448" runat="server" Columns="2" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV449" runat="server" Columns="2" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV450" runat="server" Columns="2" TabIndex="10107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV451" runat="server" Columns="2" TabIndex="10108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV452" runat="server" Columns="2" TabIndex="10109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV453" runat="server" Columns="2" TabIndex="10110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV454" runat="server" Columns="2" TabIndex="10111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV455" runat="server" Columns="2" TabIndex="10112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV456" runat="server" Columns="3" TabIndex="10113" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV457" runat="server" Columns="3" TabIndex="10114" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV458" runat="server" Columns="3" TabIndex="10115" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV459" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV460" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV461" runat="server" Columns="2" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV462" runat="server" Columns="2" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV463" runat="server" Columns="2" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV464" runat="server" Columns="2" TabIndex="10206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV465" runat="server" Columns="2" TabIndex="10207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV466" runat="server" Columns="2" TabIndex="10208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV467" runat="server" Columns="2" TabIndex="10209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV468" runat="server" Columns="2" TabIndex="10210" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV469" runat="server" Columns="2" TabIndex="10211" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV470" runat="server" Columns="2" TabIndex="10212" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV471" runat="server" Columns="3" TabIndex="10213" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV472" runat="server" Columns="3" TabIndex="10214" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV473" runat="server" Columns="3" TabIndex="10215" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV474" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV475" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV476" runat="server" Columns="2" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV477" runat="server" Columns="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV478" runat="server" Columns="2" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV479" runat="server" Columns="2" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV480" runat="server" Columns="2" TabIndex="10307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV481" runat="server" Columns="2" TabIndex="10308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV482" runat="server" Columns="2" TabIndex="10309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV483" runat="server" Columns="2" TabIndex="10310" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV484" runat="server" Columns="2" TabIndex="10311" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV485" runat="server" Columns="2" TabIndex="10312" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV486" runat="server" Columns="3" TabIndex="10313" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV487" runat="server" Columns="3" TabIndex="10314" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV488" runat="server" Columns="3" TabIndex="10315" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV489" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV490" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV491" runat="server" Columns="2" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV492" runat="server" Columns="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV493" runat="server" Columns="2" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV494" runat="server" Columns="2" TabIndex="10406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV495" runat="server" Columns="2" TabIndex="10407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV496" runat="server" Columns="2" TabIndex="10408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV497" runat="server" Columns="2" TabIndex="10409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV498" runat="server" Columns="2" TabIndex="10410" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV499" runat="server" Columns="2" TabIndex="10411" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV500" runat="server" Columns="2" TabIndex="10412" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV501" runat="server" Columns="3" TabIndex="10413" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV502" runat="server" Columns="3" TabIndex="10414" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV503" runat="server" Columns="3" TabIndex="10415" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV504" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV505" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV506" runat="server" Columns="2" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV507" runat="server" Columns="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV508" runat="server" Columns="2" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV509" runat="server" Columns="2" TabIndex="10506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV510" runat="server" Columns="2" TabIndex="10507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV511" runat="server" Columns="2" TabIndex="10508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV512" runat="server" Columns="2" TabIndex="10509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV513" runat="server" Columns="2" TabIndex="10510" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV514" runat="server" Columns="2" TabIndex="10511" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV515" runat="server" Columns="2" TabIndex="10512" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV516" runat="server" Columns="3" TabIndex="10513" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV517" runat="server" Columns="3" TabIndex="10514" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV518" runat="server" Columns="3" TabIndex="10515" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV519" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV520" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV521" runat="server" Columns="2" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV522" runat="server" Columns="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV523" runat="server" Columns="2" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV524" runat="server" Columns="2" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV525" runat="server" Columns="2" TabIndex="10607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV526" runat="server" Columns="2" TabIndex="10608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV527" runat="server" Columns="2" TabIndex="10609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV528" runat="server" Columns="2" TabIndex="10610" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV529" runat="server" Columns="2" TabIndex="10611" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV530" runat="server" Columns="2" TabIndex="10612" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV531" runat="server" Columns="3" TabIndex="10613" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV532" runat="server" Columns="3" TabIndex="10614" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV533" runat="server" Columns="3" TabIndex="10615" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV534" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV535" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV536" runat="server" Columns="2" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV537" runat="server" Columns="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV538" runat="server" Columns="2" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV539" runat="server" Columns="2" TabIndex="10706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV540" runat="server" Columns="2" TabIndex="10707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV541" runat="server" Columns="2" TabIndex="10708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV542" runat="server" Columns="2" TabIndex="10709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV543" runat="server" Columns="2" TabIndex="10710" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV544" runat="server" Columns="2" TabIndex="10711" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV545" runat="server" Columns="2" TabIndex="10712" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV546" runat="server" Columns="3" TabIndex="10713" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV547" runat="server" Columns="3" TabIndex="10714" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV548" runat="server" Columns="3" TabIndex="10715" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV549" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV550" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV551" runat="server" Columns="2" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV552" runat="server" Columns="2" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV553" runat="server" Columns="2" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV554" runat="server" Columns="2" TabIndex="10806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV555" runat="server" Columns="2" TabIndex="10807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV556" runat="server" Columns="2" TabIndex="10808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV557" runat="server" Columns="2" TabIndex="10809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV558" runat="server" Columns="2" TabIndex="10810" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV559" runat="server" Columns="2" TabIndex="10811" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV560" runat="server" Columns="2" TabIndex="10812" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV561" runat="server" Columns="3" TabIndex="10813" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV562" runat="server" Columns="3" TabIndex="10814" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV563" runat="server" Columns="3" TabIndex="10815" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV564" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV565" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV566" runat="server" Columns="2" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV567" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV568" runat="server" Columns="2" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV569" runat="server" Columns="2" TabIndex="10906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV570" runat="server" Columns="2" TabIndex="10907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV571" runat="server" Columns="2" TabIndex="10908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV572" runat="server" Columns="2" TabIndex="10909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV573" runat="server" Columns="2" TabIndex="10910" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV574" runat="server" Columns="2" TabIndex="10911" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV575" runat="server" Columns="2" TabIndex="10912" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV576" runat="server" Columns="3" TabIndex="10913" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV577" runat="server" Columns="3" TabIndex="10914" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV578" runat="server" Columns="3" TabIndex="10915" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV579" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV580" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV581" runat="server" Columns="2" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV582" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV583" runat="server" Columns="2" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV584" runat="server" Columns="2" TabIndex="11006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV585" runat="server" Columns="2" TabIndex="11007" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV586" runat="server" Columns="2" TabIndex="11008" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV587" runat="server" Columns="2" TabIndex="11009" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV588" runat="server" Columns="2" TabIndex="11010" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV589" runat="server" Columns="2" TabIndex="11011" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV590" runat="server" Columns="2" TabIndex="11012" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV591" runat="server" Columns="3" TabIndex="11013" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV592" runat="server" Columns="3" TabIndex="11014" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV593" runat="server" Columns="3" TabIndex="11015" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblTotal42" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV594" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV595" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV596" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV597" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV598" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV599" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV600" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV601" runat="server" Columns="3" TabIndex="11108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV602" runat="server" Columns="3" TabIndex="11109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;">
                                <asp:TextBox ID="txtV603" runat="server" Columns="3" TabIndex="11110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV604" runat="server" Columns="3" TabIndex="11111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV605" runat="server" Columns="3" TabIndex="11112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV606" runat="server" Columns="4" TabIndex="11113" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV607" runat="server" Columns="4" TabIndex="11114" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV608" runat="server" Columns="4" TabIndex="11115" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 69px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" 
                                Text="De los alumnos reportados en el rubro anterior, 
                                escriba la cantidad de alumnos 
                                con discapacidad, aptitudes sobresalientes u otras condiciones, 
                                seg�n las definiciones establecidas en el glosario."
                                    Width="1136px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 69px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                            <td colspan="10" style="text-align: center">
                                &nbsp;<asp:Label ID="lblAlumnosEsc" runat="server" CssClass="lblRojo" Text="ALUMNOS POR ESCUELA"
                                    Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresAl" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresAl" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotalAl" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc12" runat="server" CssClass="lblGrisTit" Text="1" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc22" runat="server" CssClass="lblGrisTit" Text="2" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc32" runat="server" CssClass="lblGrisTit" Text="3" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc42" runat="server" CssClass="lblGrisTit" Text="4" Width="67px"></asp:Label></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc52" runat="server" CssClass="lblGrisTit" Text="5" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc62" runat="server" CssClass="lblGrisTit" Text="6" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc72" runat="server" CssClass="lblGrisTit" Text="7" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc82" runat="server" CssClass="lblGrisTit" Text="8" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc92" runat="server" CssClass="lblGrisTit" Text="9" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc102" runat="server" CssClass="lblGrisTit" Text="10" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 26px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV609" runat="server" Columns="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV610" runat="server" Columns="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV611" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV612" runat="server" Columns="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV613" runat="server" Columns="2" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV614" runat="server" Columns="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV615" runat="server" Columns="2" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV616" runat="server" Columns="2" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV617" runat="server" Columns="2" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV618" runat="server" Columns="2" TabIndex="20110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV619" runat="server" Columns="2" TabIndex="20111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV620" runat="server" Columns="2" TabIndex="20112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV621" runat="server" Columns="2" TabIndex="20113" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV622" runat="server" Columns="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV623" runat="server" Columns="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV624" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV625" runat="server" Columns="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV626" runat="server" Columns="2" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV627" runat="server" Columns="2" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV628" runat="server" Columns="2" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV629" runat="server" Columns="2" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV630" runat="server" Columns="2" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV631" runat="server" Columns="2" TabIndex="20210" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV632" runat="server" Columns="2" TabIndex="20211" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV633" runat="server" Columns="2" TabIndex="20212" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV634" runat="server" Columns="2" TabIndex="20213" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV635" runat="server" Columns="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV636" runat="server" Columns="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV637" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV638" runat="server" Columns="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV639" runat="server" Columns="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV640" runat="server" Columns="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV641" runat="server" Columns="2" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV642" runat="server" Columns="2" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV643" runat="server" Columns="2" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV644" runat="server" Columns="2" TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV645" runat="server" Columns="2" TabIndex="20311" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV646" runat="server" Columns="2" TabIndex="20312" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV647" runat="server" Columns="2" TabIndex="20313" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV648" runat="server" Columns="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV649" runat="server" Columns="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV650" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV651" runat="server" Columns="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV652" runat="server" Columns="2" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV653" runat="server" Columns="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV654" runat="server" Columns="2" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV655" runat="server" Columns="2" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV656" runat="server" Columns="2" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV657" runat="server" Columns="2" TabIndex="20410" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV658" runat="server" Columns="2" TabIndex="20411" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV659" runat="server" Columns="2" TabIndex="20412" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV660" runat="server" Columns="2" TabIndex="20413" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV661" runat="server" Columns="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV662" runat="server" Columns="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV663" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV664" runat="server" Columns="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV665" runat="server" Columns="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV666" runat="server" Columns="2" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV667" runat="server" Columns="2" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV668" runat="server" Columns="2" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV669" runat="server" Columns="2" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV670" runat="server" Columns="2" TabIndex="20510" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV671" runat="server" Columns="2" TabIndex="20511" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV672" runat="server" Columns="2" TabIndex="20512" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV673" runat="server" Columns="2" TabIndex="20513" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV674" runat="server" Columns="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV675" runat="server" Columns="2" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV676" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV677" runat="server" Columns="2" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV678" runat="server" Columns="2" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV679" runat="server" Columns="2" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV680" runat="server" Columns="2" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV681" runat="server" Columns="2" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV682" runat="server" Columns="2" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV683" runat="server" Columns="2" TabIndex="20610" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV684" runat="server" Columns="2" TabIndex="20611" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV685" runat="server" Columns="2" TabIndex="20612" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV686" runat="server" Columns="2" TabIndex="20613" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV687" runat="server" Columns="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV688" runat="server" Columns="2" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV689" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV690" runat="server" Columns="2" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV691" runat="server" Columns="2" TabIndex="20705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV692" runat="server" Columns="2" TabIndex="20706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV693" runat="server" Columns="2" TabIndex="20707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV694" runat="server" Columns="2" TabIndex="20708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV695" runat="server" Columns="2" TabIndex="20709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV696" runat="server" Columns="2" TabIndex="20710" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV697" runat="server" Columns="2" TabIndex="20711" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV698" runat="server" Columns="2" TabIndex="20712" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV699" runat="server" Columns="2" TabIndex="20713" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV700" runat="server" Columns="2" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV701" runat="server" Columns="2" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV702" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV703" runat="server" Columns="2" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV704" runat="server" Columns="2" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV705" runat="server" Columns="2" TabIndex="20806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV706" runat="server" Columns="2" TabIndex="20807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV707" runat="server" Columns="2" TabIndex="20808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV708" runat="server" Columns="2" TabIndex="20809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV709" runat="server" Columns="2" TabIndex="20810" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV710" runat="server" Columns="2" TabIndex="20811" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV711" runat="server" Columns="2" TabIndex="20812" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV712" runat="server" Columns="2" TabIndex="20813" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblTotal43" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV713" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV714" runat="server" Columns="3" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV715" runat="server" Columns="3" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV716" runat="server" Columns="3" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV717" runat="server" Columns="3" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV718" runat="server" Columns="3" TabIndex="20906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV719" runat="server" Columns="3" TabIndex="20907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 69px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV720" runat="server" Columns="3" TabIndex="20908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV721" runat="server" Columns="3" TabIndex="20909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV722" runat="server" Columns="3" TabIndex="20910" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV723" runat="server" Columns="3" TabIndex="20911" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV724" runat="server" Columns="3" TabIndex="20912" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV725" runat="server" Columns="3" TabIndex="20913" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 14px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 69px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('ASEC_911_USAER_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('ASEC_911_USAER_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
        MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
        GetTabIndexes();
        </script> 
      
</asp:Content>
