<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CAM-2(Inicial y Preescolar)" AutoEventWireup="true" CodeBehind="AG1_CAM_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin.CAM_2.AG1_CAM_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <%--<link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
<%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 42;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
     <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N ESPECIAL - CENTRO DE ATENCI�N M�LTIPLE</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1500px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_CAM_2',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_2',true)"><a href="#" title="" class="activo"><span>INICIAL Y PREESCOLAR</span></a></li>
        <li onclick="openPage('AG2_CAM_2',false)"><a href="#" title=""><span>PRIMARIA Y SECUNDARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CAP. P/TRABAJO Y ATENCI�N COMP.</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL DOCENTE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
       <%-- <li onclick="openPage('Anexo')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>


            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td colspan="15" rowspan="1" style="text-align: justify">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba el n�mero de alumnos por sexo y el n�mero de grupos de educaci�n inicial, seg�n los rubros que se indican."
                        Width="100%"></asp:Label></td>
                <td colspan="1" rowspan="1" style="text-align: justify">
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionI1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N INICIAL"
                        Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblBajas1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="BAJAS"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTermino" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T�RMINO DE ATENCI�N"
                        Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblIntegrados1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INTEGRADOS A EDUCACI�N REGULAR"
                        Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblContinuan1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="CONTIN�AN CON ATENCI�N"
                        Width="100px"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblGrupos1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
                <td class="Orila" rowspan="2" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                
             </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1" runat="server" Columns="3" MaxLength="3" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV2" runat="server" Columns="3" MaxLength="3" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV3" runat="server" Columns="3" MaxLength="3" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV4" runat="server" Columns="3" MaxLength="3" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV5" runat="server" Columns="3" MaxLength="2" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV6" runat="server" Columns="3" MaxLength="2" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV7" runat="server" Columns="3" MaxLength="3" TabIndex="10107" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV8" runat="server" Columns="3" MaxLength="3" TabIndex="10108" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV9" runat="server" Columns="3" MaxLength="2" TabIndex="10109" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV10" runat="server" Columns="3" MaxLength="2" TabIndex="10110" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV11" runat="server" Columns="3" MaxLength="2" TabIndex="10111" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV12" runat="server" Columns="3" MaxLength="2" TabIndex="10112" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" TabIndex="10113" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV14" runat="server" Columns="3" MaxLength="3" TabIndex="10114" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV15" runat="server" Columns="3" MaxLength="2" TabIndex="10115" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
                    
            </tr>
            
        </table>
        <br />
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion1b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro de inscripcion total, escriba el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion1" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal12" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td  style="text-align: left">
                <asp:Label ID="lblCeguera1" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV16" runat="server" Columns="4" MaxLength="3" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV17" runat="server" Columns="4" MaxLength="3" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV18" runat="server" Columns="4" MaxLength="4" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVisual1" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV19" runat="server" Columns="4" MaxLength="3" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV20" runat="server" Columns="4" MaxLength="3" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV21" runat="server" Columns="4" MaxLength="4" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSordera1" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV22" runat="server" Columns="4" MaxLength="3" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV23" runat="server" Columns="4" MaxLength="3" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV24" runat="server" Columns="4" MaxLength="4" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuditiva1" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV25" runat="server" Columns="4" MaxLength="3" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV26" runat="server" Columns="4" MaxLength="3" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV27" runat="server" Columns="4" MaxLength="4" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMotriz1" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV28" runat="server" Columns="4" MaxLength="3" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV29" runat="server" Columns="4" MaxLength="3" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV30" runat="server" Columns="4" MaxLength="4" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblIntelectual1" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV31" runat="server" Columns="4" MaxLength="3" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV32" runat="server" Columns="4" MaxLength="3" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV33" runat="server" Columns="4" MaxLength="4" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="nowrap"  style="text-align: left">
                <asp:Label ID="lblSobresalientes1" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV34" runat="server" Columns="4" MaxLength="3" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV35" runat="server" Columns="4" MaxLength="3" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV36" runat="server" Columns="4" MaxLength="4" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td  style="text-align: left">
                <asp:Label ID="lblOtros1" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV37" runat="server" Columns="4" MaxLength="3" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV38" runat="server" Columns="4" MaxLength="3" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV39" runat="server" Columns="4" MaxLength="4" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td  style="text-align: left">
                <asp:Label ID="lblTotal13" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV40" runat="server" Columns="4" MaxLength="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV41" runat="server" Columns="4" MaxLength="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV42" runat="server" Columns="4" MaxLength="4" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
        <br/>
    
    
    <table>
        <tr>
            <td colspan="18" style="text-align: justify">
                <asp:Label ID="lblInstruccion2" runat="server" CssClass="lblRojo" Font-Bold="True"
                    Text="2. Escriba el n�mero de alumnos por sexo y el n�mero de grupos de educaci�n preescolar, seg�n los rubros que se indican (no utilice las �reas sombreadas)."
                    Width="100%"></asp:Label></td>
            <td colspan="1" style="text-align: justify">
            </td>
        </tr>
        <tr>
            <td colspan="1" style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblInscripcionI2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N INICIAL"
                    Width="100px"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblInscripcionT2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                    Width="100px"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblBajas2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="BAJAS"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblExistencia2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblPromovidosGrado2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PROMOVIDOS AL PR�XIMO GRADO"
                    Width="100px"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblPromovidosPrim2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PROMOVIDOS A PRIMARIA"
                    Width="100px"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblIntegrados2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INTEGRADOS A EDUCACI�N REGULAR"
                    Width="100px"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblContinuan2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="CONTIN�AN CON ATENCI�N"
                    Width="100px"></asp:Label></td>
            <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblGrupos2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                    Width="100%"></asp:Label></td>
            <td class="Orila" rowspan="2" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
            </td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom23" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj23" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblHom28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblMuj28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV43" runat="server" Columns="3" MaxLength="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV44" runat="server" Columns="3" MaxLength="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV45" runat="server" Columns="3" MaxLength="2" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV46" runat="server" Columns="3" MaxLength="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV47" runat="server" Columns="3" MaxLength="2" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV48" runat="server" Columns="3" MaxLength="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV49" runat="server" Columns="3" MaxLength="2" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV50" runat="server" Columns="3" MaxLength="2" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV51" runat="server" Columns="3" MaxLength="2" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV52" runat="server" Columns="3" MaxLength="2" TabIndex="20110" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" bgcolor="darkgray" class="linaBajoAlto">
            </td>
            <td style="text-align: center; height: 27px;" bgcolor="darkgray" class="linaBajoAlto">
            </td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV53" runat="server" Columns="3" MaxLength="2" TabIndex="20111" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV54" runat="server" Columns="3" MaxLength="2" TabIndex="20112" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV55" runat="server" Columns="3" MaxLength="3" TabIndex="20113" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV56" runat="server" Columns="3" MaxLength="3" TabIndex="20114" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 27px;" class="linaBajoAlto">
                <asp:TextBox ID="txtV57" runat="server" Columns="3" MaxLength="2" TabIndex="20115" CssClass="lblNegro"></asp:TextBox></td>
            <td class="Orila" style="text-align: center; height: 27px;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV58" runat="server" Columns="3" MaxLength="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV59" runat="server" Columns="3" MaxLength="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV60" runat="server" Columns="3" MaxLength="2" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV61" runat="server" Columns="3" MaxLength="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV62" runat="server" Columns="3" MaxLength="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV63" runat="server" Columns="3" MaxLength="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV64" runat="server" Columns="3" MaxLength="2" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV65" runat="server" Columns="3" MaxLength="2" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV66" runat="server" Columns="3" MaxLength="2" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV67" runat="server" Columns="3" MaxLength="2" TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV68" runat="server" Columns="3" MaxLength="2" TabIndex="20311" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV69" runat="server" Columns="3" MaxLength="2" TabIndex="20312" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV70" runat="server" Columns="3" MaxLength="2" TabIndex="20313" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV71" runat="server" Columns="3" MaxLength="2" TabIndex="20314" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV72" runat="server" Columns="3" MaxLength="3" TabIndex="20315" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV73" runat="server" Columns="3" MaxLength="3" TabIndex="20316" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV74" runat="server" Columns="3" MaxLength="2" TabIndex="20317" CssClass="lblNegro"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV75" runat="server" Columns="3" MaxLength="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV76" runat="server" Columns="3" MaxLength="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV77" runat="server" Columns="3" MaxLength="2" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV78" runat="server" Columns="3" MaxLength="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV79" runat="server" Columns="3" MaxLength="2" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV80" runat="server" Columns="3" MaxLength="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV81" runat="server" Columns="3" MaxLength="2" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV82" runat="server" Columns="3" MaxLength="2" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" bgcolor="darkgray" class="linaBajoAlto">
            </td>
            <td style="text-align: center" bgcolor="darkgray" class="linaBajoAlto">
            </td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV83" runat="server" Columns="3" MaxLength="2" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV84" runat="server" Columns="3" MaxLength="2" TabIndex="20410" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV85" runat="server" Columns="3" MaxLength="2" TabIndex="20411" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV86" runat="server" Columns="3" MaxLength="2" TabIndex="20412" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV87" runat="server" Columns="3" MaxLength="3" TabIndex="20413" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV88" runat="server" Columns="3" MaxLength="3" TabIndex="20414" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV89" runat="server" Columns="3" MaxLength="2" TabIndex="20415" CssClass="lblNegro"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV90" runat="server" Columns="3" MaxLength="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV91" runat="server" Columns="3" MaxLength="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV92" runat="server" Columns="3" MaxLength="2" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV93" runat="server" Columns="3" MaxLength="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV94" runat="server" Columns="3" MaxLength="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV95" runat="server" Columns="3" MaxLength="2" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV96" runat="server" Columns="3" MaxLength="2" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV97" runat="server" Columns="3" MaxLength="2" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV98" runat="server" Columns="3" MaxLength="2" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV99" runat="server" Columns="3" MaxLength="2" TabIndex="20510" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV100" runat="server" Columns="3" MaxLength="2" TabIndex="20511" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV101" runat="server" Columns="3" MaxLength="2" TabIndex="20512" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV102" runat="server" Columns="3" MaxLength="2" TabIndex="20513" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV103" runat="server" Columns="3" MaxLength="2" TabIndex="20514" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV104" runat="server" Columns="3" MaxLength="3" TabIndex="20515" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV105" runat="server" Columns="3" MaxLength="3" TabIndex="20516" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoAlto">
                <asp:TextBox ID="txtV106" runat="server" Columns="3" MaxLength="2" TabIndex="20517" CssClass="lblNegro"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion2b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro de inscripcion total, escriba el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion2" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal22" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblCeguera2" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV107" runat="server" Columns="4" MaxLength="3" TabIndex="30101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV108" runat="server" Columns="4" MaxLength="3" TabIndex="30102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV109" runat="server" Columns="4" MaxLength="4" TabIndex="30103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVisual2" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV110" runat="server" Columns="4" MaxLength="3" TabIndex="30201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV111" runat="server" Columns="4" MaxLength="3" TabIndex="30202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV112" runat="server" Columns="4" MaxLength="4" TabIndex="30203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSordera2" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV113" runat="server" Columns="4" MaxLength="3" TabIndex="30301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV114" runat="server" Columns="4" MaxLength="3" TabIndex="30302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV115" runat="server" Columns="4" MaxLength="4" TabIndex="30303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuditiva2" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV116" runat="server" Columns="4" MaxLength="3" TabIndex="30401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV117" runat="server" Columns="4" MaxLength="3" TabIndex="30402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV118" runat="server" Columns="4" MaxLength="4" TabIndex="30403" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMotriz2" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV119" runat="server" Columns="4" MaxLength="3" TabIndex="30501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV120" runat="server" Columns="4" MaxLength="3" TabIndex="30502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV121" runat="server" Columns="4" MaxLength="4" TabIndex="30503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblIntelectual2" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV122" runat="server" Columns="4" MaxLength="3" TabIndex="30601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV123" runat="server" Columns="4" MaxLength="3" TabIndex="30602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV124" runat="server" Columns="4" MaxLength="4" TabIndex="30603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="text-align: left">
                <asp:Label ID="lblSobresalientes2" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV125" runat="server" Columns="4" MaxLength="3" TabIndex="30701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV126" runat="server" Columns="4" MaxLength="3" TabIndex="30702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV127" runat="server" Columns="4" MaxLength="4" TabIndex="30703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblOtros2" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV128" runat="server" Columns="4" MaxLength="3" TabIndex="30801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV129" runat="server" Columns="4" MaxLength="3" TabIndex="30802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV130" runat="server" Columns="4" MaxLength="4" TabIndex="30803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblTotal23" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV131" runat="server" Columns="4" MaxLength="3" TabIndex="30901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV132" runat="server" Columns="4" MaxLength="3" TabIndex="30902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV133" runat="server" Columns="4" MaxLength="4" TabIndex="30903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_CAM_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_CAM_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG2_CAM_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG2_CAM_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
       
       
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
