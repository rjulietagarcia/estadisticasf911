/****** Script para Validaciones  SEP_F911  ******/
SELECT *  FROM [SEP_911].[dbo].[Tb_Validaciones] WHERE  NombreHoja='BACH GEN' AND Variable='VAR40' AND Id_Cuestionario=5
  
  /*Actualizacion de validaciones Ciclo Fin 2012-2013*/
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR40) = (@VAR38 + @VAR39) AND  (@VAR40) <= (@VAR18 + @VAR22))',Elementos='@VAR40@VAR38@VAR39@VAR18@VAR22', ElementosSimp='@VAR40,@VAR38,@VAR39,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR40' AND Id_Cuestionario=5
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR44) = (@VAR42 + @VAR43) AND  (@VAR44) <= (@VAR18 + @VAR22))',Elementos='@VAR44@VAR42@VAR43@VAR18@VAR22', ElementosSimp='@VAR44,@VAR42,@VAR43,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR44' AND Id_Cuestionario=5
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR48) = (@VAR46 + @VAR47) AND  (@VAR48) <= (@VAR18 + @VAR22))',Elementos='@VAR48@VAR46@VAR47@VAR18@VAR22',ElementosSimp='@VAR48,@VAR46,@VAR47,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR48' AND Id_Cuestionario=5  
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR52) = (@VAR50 + @VAR51) AND  (@VAR52) <= (@VAR18 + @VAR22))',Elementos='@VAR52@VAR50@VAR51@VAR18@VAR22', ElementosSimp='@VAR52,@VAR50,@VAR51,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR52' AND Id_Cuestionario=5  
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR56) = (@VAR54 + @VAR55) AND  (@VAR56) <= (@VAR18 + @VAR22))',Elementos='@VAR56@VAR54@VAR55@VAR18@VAR22', ElementosSimp='@VAR56,@VAR54,@VAR55,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR56' AND Id_Cuestionario=5  
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR60) = (@VAR58 + @VAR59) AND  (@VAR60) <= (@VAR18 + @VAR22))',Elementos='@VAR60@VAR58@VAR59@VAR18@VAR22', ElementosSimp='@VAR60,@VAR58,@VAR59,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR60' AND Id_Cuestionario=5  
 
 
  
  /*ORIGINALES*/
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='(@VAR40) = (@VAR38 + @VAR39)',Elementos='', ElementosSimp='@VAR40,@VAR38,@VAR39' WHERE  NombreHoja='BACH GEN' AND Variable='VAR40' AND Id_Cuestionario=5
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='(@VAR44) = (@VAR42 + @VAR43)',Elementos='', ElementosSimp='@VAR44,@VAR42,@VAR43' WHERE  NombreHoja='BACH GEN' AND Variable='VAR44' AND Id_Cuestionario=5
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='(@VAR48) = (@VAR46 + @VAR47)',Elementos='',ElementosSimp='@VAR48,@VAR46,@VAR47' WHERE  NombreHoja='BACH GEN' AND Variable='VAR48' AND Id_Cuestionario=5  
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='(@VAR52) = (@VAR50 + @VAR51) ',Elementos='',ElementosSimp='@VAR52,@VAR50,@VAR51' WHERE  NombreHoja='BACH GEN' AND Variable='VAR52' AND Id_Cuestionario=5  
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='(@VAR56) = (@VAR54 + @VAR55)',Elementos='',ElementosSimp='@VAR56,@VAR54,@VAR55' WHERE  NombreHoja='BACH GEN' AND Variable='VAR56' AND Id_Cuestionario=5  
 UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='(@VAR60) = (@VAR58 + @VAR59)',Elementos='',ElementosSimp='@VAR60,@VAR58,@VAR59' WHERE  NombreHoja='BACH GEN' AND Variable='VAR60' AND Id_Cuestionario=5  
 
  
 /* Datos originales
  (@VAR44) = (@VAR42 + @VAR43)
  ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
714	VAR40	5	BACH GEN	(@VAR40) = (@VAR38 + @VAR39)		6	LA VARIABLE V40 DEBE DE SER IGUAL A LA SUMA	@VAR40,@VAR38,@VAR39
ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
715	VAR44	5	BACH GEN	(@VAR44) = (@VAR42 + @VAR43)		6	LA VARIABLE DEBE DE SER IGUAL A LA SUMA	@VAR44,@VAR42,@VAR43
ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
716	VAR48	5	BACH GEN	(@VAR48) = (@VAR46 + @VAR47)		6	LA VARIABLE DEBE DE SER IGUAL A LA SUMA	@VAR48,@VAR46,@VAR47
ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
717	VAR52	5	BACH GEN	(@VAR52) = (@VAR50 + @VAR51)		6	LA VARIABLE DEBE DE SER IGUAL A LA SUMA	@VAR52,@VAR50,@VAR51
ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
718	VAR56	5	BACH GEN	(@VAR56) = (@VAR54 + @VAR55)		6	LA VARIABLE DEBE DE SER IGUAL A LA SUMA	@VAR56,@VAR54,@VAR55
ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
719	VAR60	5	BACH GEN	(@VAR60) = (@VAR58 + @VAR59)		6	LA VARIABLE DEBE DE SER IGUAL A LA SUMA	@VAR58,@VAR59,@VAR60

ID	Variable	Id_Cuestionario	NombreHoja	Validacion	Elementos	NumErrs	Detalle	ElementosSimp
8334	VAR61	5	BACH GEN	(@VAR61 = @VAR40+@VAR44+@VAR48+@VAR52+@VAR56+@VAR60)	@VAR61@VAR40@VAR44@VAR48@VAR52@VAR56@VAR60	6,,,	EL TOTAL DEBE SER IGUAL A LA EXISTENCIA DE HOMBRES Y MUJERES EN TERCER GRADO	@VAR61,@VAR40,@VAR44,@VAR48,@VAR52,@VAR56,@VAR60
*/

UPDATE [SEP_911].[dbo].[Tb_Validaciones] SET Validacion='((@VAR61)=(@VAR40+@VAR44+@VAR48+@VAR52+@VAR56+@VAR60) AND (@VAR61)<=(@VAR18 + @VAR22))', ElementosSimp='@VAR61,@VAR40,@VAR44,@VAR48,@VAR52,@VAR56,@VAR60,@VAR18,@VAR22' WHERE  NombreHoja='BACH GEN' AND Variable='VAR61' AND Id_Cuestionario=5  
