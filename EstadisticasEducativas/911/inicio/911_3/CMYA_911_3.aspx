<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.3(Carrera Magisterial y Aulas)" AutoEventWireup="true" CodeBehind="CMYA_911_3.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_3.CMYA_911_3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
      
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header"> 
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N PRIMARIA"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1400px; width:expression(document.body.clientWidth < 1401? '1400px': 'auto' );">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_3',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_3',true)"><a href="#" title=""><span>4�, 5� y 6�</span></a></li>
        <li onclick="openPage('AGT_911_3',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_3',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_3',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('CMYA_911_3',true)"><a href="#" title="" class="activo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="openPage('Gasto_911_3',false)"><a href="#" title=""><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>  
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
    

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 100%; text-align: center">
            <tr>
                <td style="width: 100%; text-align: center">
                    <table id="TABLE1"  style="text-align: center">
                        <tr>
                            <td colspan="5" rowspan="1" style="vertical-align: top; text-align: center">
                                <table style="width: 455px; text-align: center">
                          
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 20px; text-align:justify">
                                            <asp:Label ID="lblCarrera" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="III. CARRERA MAGISTERIAL"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; width: 3px; text-align: left">
                                            <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de profesores que se encuentran en el programa de carrera magisterial"
                                                Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: right">
                                            <asp:TextBox ID="txtV876" runat="server" Columns="3" TabIndex="010101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: left">
                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Desglose la cantidad anotada en el inciso anterior, seg�n la vertiente y el nivel en que se encuentran los profesores." Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl1aVertiente" runat="server" CssClass="lblRojo" Text="1a. VERTIENTE"
                                                Width="160px" Height="21px"></asp:Label>
                                            <br />
                                            <asp:Label ID="lbl1aVertiente2" runat="server"
                                                    CssClass="lblRojo" Height="21px" Text="(Profesores frente a grupo)" Width="160px"></asp:Label></td>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA1" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV877" runat="server" Columns="2" TabIndex="10201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB1" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV878" runat="server" Columns="2" TabIndex="10301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC1" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV879" runat="server" Columns="2" TabIndex="10401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC1" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV880" runat="server" Columns="2" TabIndex="10501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD1" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV881" runat="server" Columns="2" TabIndex="10601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE1" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV882" runat="server" Columns="2" TabIndex="10701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl2aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="2a. VERTIENTE"
                                                Width="160px"></asp:Label>
                                            <asp:Label ID="lbl2aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en funciones directivas y de supervisi�n)"
                                                Width="160px"></asp:Label></td>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA2" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV883" runat="server" Columns="2" TabIndex="10801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB2" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV884" runat="server" Columns="2" TabIndex="10901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC2" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV885" runat="server" Columns="2" TabIndex="11001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC2" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV886" runat="server" Columns="2" TabIndex="11101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD2" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV887" runat="server" Columns="2" TabIndex="11201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE2" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV888" runat="server" Columns="2" TabIndex="11301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl3aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="3a. VERTIENTE"
                                                Width="160px"></asp:Label>
                                            <asp:Label ID="lbl3aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en actividades t�cno-pedag�gicas)"
                                                Width="160px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA3" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV889" runat="server" Columns="2" TabIndex="11401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB3" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV890" runat="server" Columns="2" TabIndex="11501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC3" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV891" runat="server" Columns="2" TabIndex="11601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC3" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV892" runat="server" Columns="2" TabIndex="11701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD3" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV893" runat="server" Columns="2" TabIndex="11801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE3" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV894" runat="server" Columns="2" TabIndex="11901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="padding-bottom: 20px; text-align: center">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="lblAulas" runat="server" CssClass="lblRojo" Font-Size="16px" Text="IV. AULAS"
                                                Width="150px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 50px;
                                            text-align: left">
                                            <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Registre el n�mero de aulas por grado, seg�n su tipo."
                                                Width="450px"></asp:Label>
                                            <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Text="Notas:" Width="450px"></asp:Label>
                                            <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) El reporte de aulas debe ser por turno."
                                                Width="450px"></asp:Label>
                                            <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si un aula se utiliza para impartir clases a m�s de un grado, an�tela en el rubro correspondiente"
                                                Width="450px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lblAulas1" runat="server" CssClass="lblRojo" Text="AULAS" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Text="1�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Text="2�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Text="3�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lbl4o" runat="server" CssClass="lblGrisTit" Text="4�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lbl5o" runat="server" CssClass="lblGrisTit" Text="5�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lbl6o" runat="server" CssClass="lblGrisTit" Text="6�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lblMasUnGdo" runat="server" CssClass="lblGrisTit" Text="M�S DE UN GRADO"
                                                Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL" Width="50px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:Label ID="lblExistentes" runat="server" CssClass="lblGrisTit" Text="EXISTENTES" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                            <asp:TextBox ID="txtV895" runat="server" Columns="3" TabIndex="12001" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:Label ID="lblEnUso" runat="server" CssClass="lblGrisTit" Text="EN USO" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV896" runat="server" Columns="2" TabIndex="12101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV897" runat="server" Columns="2" TabIndex="12102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV898" runat="server" Columns="2" TabIndex="12103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV899" runat="server" Columns="2" TabIndex="12104" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV900" runat="server" Columns="2" TabIndex="12105" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV901" runat="server" Columns="2" TabIndex="12106" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV902" runat="server" Columns="2" TabIndex="12107" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                            <asp:TextBox ID="txtV903" runat="server" Columns="3" TabIndex="12108" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 50px;
                                            padding-top: 10px; text-align: left">
                                            <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="De las reportadas en uso, indique el n�mero de las adaptadas."
                                                Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:Label ID="lblAdaptadas" runat="server" CssClass="lblGrisTit" Text="ADAPTADAS" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV904" runat="server" Columns="2" TabIndex="12201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV905" runat="server" Columns="2" TabIndex="12202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV906" runat="server" Columns="2" TabIndex="12203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV907" runat="server" Columns="2" TabIndex="12204" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV908" runat="server" Columns="2" TabIndex="12205" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV909" runat="server" Columns="2" TabIndex="12206" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV910" runat="server" Columns="2" TabIndex="12207" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                            <asp:TextBox ID="txtV911" runat="server" Columns="3" TabIndex="12208" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_911_3',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_3',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Gasto_911_3',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Gasto_911_3',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"  ></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 40;
                MaxRow = 30;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
