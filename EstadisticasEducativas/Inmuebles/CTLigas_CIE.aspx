<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="CTLigas_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.CTLigas_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
    
<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->
    
    <h2> VI. CLAVE Y TURNO DE LOS CENTROS DE TRABAJOS Y LOCALES UTILIZADOS </h2>
    <br />

    <%--<form id="claves" runat="server">--%>
    <div>
    
    
    <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
    
    1. Escriba la clave y turno de los centros de trabajo que funcionan en el inmueble y el n�mero de aulas, laboratorios y talleres que son utilizados por cada uno de ellos; 
    incluya en el primer lugar de esta secci&oacute; la clave del centro de trabajo que aparece en la secci&oacute;n de identificaci&oacute;n.
    
        <table>
            <tr>
                <td rowspan="2"> Clave del Centro de Trabajo
                </td>
                <td rowspan="2" style="text-align: center"> Turno
                </td>
                <td colspan="3" style="text-align: center"> Utilizaci&oacute;n de:
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> Aulas
                </td>
                <td style="text-align: center"> Laboratorios
                </td>
                <td style="text-align: center"> Talleres
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv609" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv610" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv611" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv612" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv613" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv614" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv615" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv616" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txt617" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txt618" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv619" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv620" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv621" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv622" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv623" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv624" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv625" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv626" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv627" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv628" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv629" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv630" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv631" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv632" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv633" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv634" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv635" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv636" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv637" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv638" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv639" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv640" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv641" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv642" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv643" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv644" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv645" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv646" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv647" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv648" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv649" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv650" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv651" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv652" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv653" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv654" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv655" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv656" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv657" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv658" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv659" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv660" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv661" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv662" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv663" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv664" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv665" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv666" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv667" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv668" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv669" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv670" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv671" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv672" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv673" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv674" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv675" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv676" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv677" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv678" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv679" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv680" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv681" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv682" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv683" Width="40" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv684" Width="100" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv685" Width="45" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv686" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv687" Width="40" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv688" Width="40" />
                </td>
            </tr>
        </table>    
    
        <%--<input id="Submit7" type="submit" value="Continuar" /> --%>
           
           
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
           
    </div>
    <%--</form>--%>
 </asp:Content>
