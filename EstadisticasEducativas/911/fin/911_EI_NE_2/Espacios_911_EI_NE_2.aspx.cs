using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
   
namespace EstadisticasEducativas._911.fin._911_EI_NE_2
{
    public partial class Espacios_911_EI_NE_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV425.Attributes["onkeypress"] = "return Num(event)";
                txtV426.Attributes["onkeypress"] = "return Num(event)";
                txtV427.Attributes["onkeypress"] = "return Num(event)";
                txtV428.Attributes["onkeypress"] = "return Num(event)";
                txtV429.Attributes["onkeypress"] = "return Num(event)";
                txtV430.Attributes["onkeypress"] = "return Num(event)";
                txtV431.Attributes["onkeypress"] = "return Num(event)";
                txtV432.Attributes["onkeypress"] = "return Num(event)";
                txtV433.Attributes["onkeypress"] = "return Num(event)";
                txtV434.Attributes["onkeypress"] = "return Num(event)";
                txtV435.Attributes["onkeypress"] = "return Num(event)";
                txtV436.Attributes["onkeypress"] = "return Num(event)";
                txtV437.Attributes["onkeypress"] = "return Num(event)";
                txtV438.Attributes["onkeypress"] = "return Num(event)";
                txtV439.Attributes["onkeypress"] = "return Num(event)";
                txtV440.Attributes["onkeypress"] = "return Num(event)";
                txtV441.Attributes["onkeypress"] = "return Num(event)";
                txtV442.Attributes["onkeypress"] = "return Num(event)";
                txtV443.Attributes["onkeypress"] = "return Num(event)";
                txtV444.Attributes["onkeypress"] = "return Num(event)";
                txtV445.Attributes["onkeypress"] = "return Num(event)";
                txtV446.Attributes["onkeypress"] = "return Num(event)";
                txtV447.Attributes["onkeypress"] = "return Num(event)";
                txtV448.Attributes["onkeypress"] = "return Num(event)";
                txtV449.Attributes["onkeypress"] = "return Num(event)";
                txtV450.Attributes["onkeypress"] = "return Num(event)";
                txtV451.Attributes["onkeypress"] = "return Num(event)";
                txtV452.Attributes["onkeypress"] = "return Num(event)";
                txtV453.Attributes["onkeypress"] = "return Num(event)";
                txtV454.Attributes["onkeypress"] = "return Num(event)";
                txtV455.Attributes["onkeypress"] = "return Num(event)";
                txtV456.Attributes["onkeypress"] = "return Num(event)";
                txtV457.Attributes["onkeypress"] = "return Num(event)";
                txtV458.Attributes["onkeypress"] = "return Num(event)";
                txtV459.Attributes["onkeypress"] = "return Num(event)";
                txtV460.Attributes["onkeypress"] = "return Num(event)";
                txtV461.Attributes["onkeypress"] = "return Num(event)";
                txtV462.Attributes["onkeypress"] = "return Num(event)";
                txtV463.Attributes["onkeypress"] = "return Num(event)";
                txtV464.Attributes["onkeypress"] = "return Num(event)";
                txtV465.Attributes["onkeypress"] = "return Num(event)";
                txtV466.Attributes["onkeypress"] = "return Num(event)";
                txtV467.Attributes["onkeypress"] = "return Num(event)";
                txtV468.Attributes["onkeypress"] = "return Num(event)";
                txtV469.Attributes["onkeypress"] = "return Num(event)";
                txtV470.Attributes["onkeypress"] = "return Num(event)";
                txtV471.Attributes["onkeypress"] = "return Num(event)";
                txtV472.Attributes["onkeypress"] = "return Num(event)";
                txtV473.Attributes["onkeypress"] = "return Num(event)";
                txtV474.Attributes["onkeypress"] = "return Num(event)";
                txtV475.Attributes["onkeypress"] = "return Num(event)";
                txtV476.Attributes["onkeypress"] = "return Num(event)";
                txtV477.Attributes["onkeypress"] = "return Num(event)";
                txtV478.Attributes["onkeypress"] = "return Num(event)";
                txtV479.Attributes["onkeypress"] = "return Num(event)";
                txtV480.Attributes["onkeypress"] = "return Num(event)";
                txtV481.Attributes["onkeypress"] = "return Num(event)";
                txtV482.Attributes["onkeypress"] = "return Num(event)";
                txtV483.Attributes["onkeypress"] = "return Num(event)";
                txtV484.Attributes["onkeypress"] = "return Num(event)";
                txtV485.Attributes["onkeypress"] = "return Num(event)";
                txtV486.Attributes["onkeypress"] = "return Num(event)";
                txtV487.Attributes["onkeypress"] = "return Num(event)";
                txtV488.Attributes["onkeypress"] = "return Num(event)";
                txtV489.Attributes["onkeypress"] = "return Num(event)";
                txtV490.Attributes["onkeypress"] = "return Num(event)";
                txtV491.Attributes["onkeypress"] = "return Num(event)";
                txtV492.Attributes["onkeypress"] = "return Num(event)";
                txtV493.Attributes["onkeypress"] = "return Num(event)";
                txtV494.Attributes["onkeypress"] = "return Num(event)";
                txtV495.Attributes["onkeypress"] = "return Num(event)";
                txtV496.Attributes["onkeypress"] = "return Num(event)";
                txtV497.Attributes["onkeypress"] = "return Num(event)";
                txtV498.Attributes["onkeypress"] = "return Num(event)";
                txtV499.Attributes["onkeypress"] = "return Num(event)";
                txtV500.Attributes["onkeypress"] = "return Num(event)";
                txtV501.Attributes["onkeypress"] = "return Num(event)";
                txtV502.Attributes["onkeypress"] = "return Num(event)";
                txtV503.Attributes["onkeypress"] = "return Num(event)";
                txtV504.Attributes["onkeypress"] = "return Num(event)";
                txtV505.Attributes["onkeypress"] = "return Num(event)";
                txtV506.Attributes["onkeypress"] = "return Num(event)";
                txtV507.Attributes["onkeypress"] = "return Num(event)";
                txtV508.Attributes["onkeypress"] = "return Num(event)";
                txtV509.Attributes["onkeypress"] = "return Num(event)";
                txtV510.Attributes["onkeypress"] = "return Num(event)";
                txtV511.Attributes["onkeypress"] = "return Num(event)";
                txtV512.Attributes["onkeypress"] = "return Num(event)";
                txtV513.Attributes["onkeypress"] = "return Num(event)";
                txtV514.Attributes["onkeypress"] = "return Num(event)";
                txtV515.Attributes["onkeypress"] = "return Num(event)";
                txtV516.Attributes["onkeypress"] = "return Num(event)";
                txtV517.Attributes["onkeypress"] = "return Num(event)";
                txtV518.Attributes["onkeypress"] = "return Num(event)";
                txtV519.Attributes["onkeypress"] = "return Num(event)";
                txtV520.Attributes["onkeypress"] = "return Num(event)";
                txtV521.Attributes["onkeypress"] = "return Num(event)";
                txtV522.Attributes["onkeypress"] = "return Num(event)";
                txtV523.Attributes["onkeypress"] = "return Num(event)";
                txtV524.Attributes["onkeypress"] = "return Num(event)";
                txtV525.Attributes["onkeypress"] = "return Num(event)";
                txtV526.Attributes["onkeypress"] = "return Num(event)";
                txtV527.Attributes["onkeypress"] = "return Num(event)";
                txtV528.Attributes["onkeypress"] = "return Num(event)";
                txtV529.Attributes["onkeypress"] = "return Num(event)";
                txtV530.Attributes["onkeypress"] = "return Num(event)";
                txtV531.Attributes["onkeypress"] = "return Num(event)";
                txtV532.Attributes["onkeypress"] = "return Num(event)";
                txtV533.Attributes["onkeypress"] = "return Num(event)";
                txtV534.Attributes["onkeypress"] = "return Num(event)";
                txtV535.Attributes["onkeypress"] = "return Num(event)";
                txtV536.Attributes["onkeypress"] = "return Num(event)";
                txtV537.Attributes["onkeypress"] = "return Num(event)";
                txtV538.Attributes["onkeypress"] = "return Num(event)";
                txtV539.Attributes["onkeypress"] = "return Num(event)";
                txtV540.Attributes["onkeypress"] = "return Num(event)";
                txtV541.Attributes["onkeypress"] = "return Num(event)";
                txtV542.Attributes["onkeypress"] = "return Num(event)";
                txtV543.Attributes["onkeypress"] = "return Num(event)";
                txtV544.Attributes["onkeypress"] = "return Num(event)";
                txtV545.Attributes["onkeypress"] = "return Num(event)";
                txtV546.Attributes["onkeypress"] = "return Num(event)";
                txtV547.Attributes["onkeypress"] = "return Num(event)";
                txtV548.Attributes["onkeypress"] = "return Num(event)";
                txtV549.Attributes["onkeypress"] = "return Num(event)";
                txtV550.Attributes["onkeypress"] = "return Num(event)";
                txtV551.Attributes["onkeypress"] = "return Num(event)";
                txtV552.Attributes["onkeypress"] = "return Num(event)";
                txtV719.Attributes["onkeypress"] = "return Num(event)";
                txtV720.Attributes["onkeypress"] = "return Num(event)";
                txtV721.Attributes["onkeypress"] = "return Num(event)";
                txtV722.Attributes["onkeypress"] = "return Num(event)";
                txtV723.Attributes["onkeypress"] = "return Num(event)";
                txtV724.Attributes["onkeypress"] = "return Num(event)";
                txtV725.Attributes["onkeypress"] = "return Num(event)";
                txtV726.Attributes["onkeypress"] = "return Num(event)";
                txtV727.Attributes["onkeypress"] = "return Num(event)";
                txtV728.Attributes["onkeypress"] = "return Num(event)";
                txtV729.Attributes["onkeypress"] = "return Num(event)";
                txtV730.Attributes["onkeypress"] = "return Num(event)";
                txtV731.Attributes["onkeypress"] = "return Num(event)";
                txtV732.Attributes["onkeypress"] = "return Num(event)";
                txtV733.Attributes["onkeypress"] = "return Num(event)";
                txtV734.Attributes["onkeypress"] = "return Num(event)";
                txtV553.Attributes["onkeypress"] = "return Num(event)";
                txtV554.Attributes["onkeypress"] = "return Num(event)";
                txtV555.Attributes["onkeypress"] = "return Num(event)";
                txtV556.Attributes["onkeypress"] = "return Num(event)";
                txtV557.Attributes["onkeypress"] = "return Num(event)";
                txtV558.Attributes["onkeypress"] = "return Num(event)";
                txtV559.Attributes["onkeypress"] = "return Num(event)";
                txtV560.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
                //ActivarTRs(controlDP);
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************

        void ActivarTRs(ControlDP controlDP)
        {
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
            string lista = "txtV3,3|txtV6,6|txtV9,9|txtV12,12|txtV15,15|txtV18,18|txtV21,21|txtV24,24|txtV27,27|txtV30,30|txtV33,33|txtV36,36|txtV39,39|txtV42,42|txtV45,45|txtV48,48|txtV564,564|txtV567,567|";
            string[] Cajas = lista.Split('|');

            System.Text.StringBuilder listaNumerosVariables = new System.Text.StringBuilder();
            for (int NoVariable = 0; NoVariable < Cajas.Length - 1; NoVariable++)
            {
                if (Cajas[NoVariable].Split(',')[1] != "0")
                    listaNumerosVariables.Append(Cajas[NoVariable].Split(',')[1]);
                if (NoVariable < Cajas.Length - 2 && listaNumerosVariables.Length > 0)
                    listaNumerosVariables.Append(",");
            }
            VariablesDP[] lista_VariableDP = wsEstadisiticas.LeerVariableEncuesta_Lista(controlDP, Class911.Doc_Cuestionario, 0, listaNumerosVariables.ToString());

            foreach (VariablesDP varDP in lista_VariableDP)
            {
                int x = 0;
                if (varDP.valor == DBNull.Value)
                    varDP.valor = x;
                if (varDP.valor.ToString() == "0")
                {

                    switch (varDP.var)
                    {
                        case 3:
                            txt_disabled(ref tr1);
                            break;
                        case 6:
                            txt_disabled(ref tr2);
                            break;
                        case 9:
                            txt_disabled(ref tr3);
                            break;
                        case 12:
                            txt_disabled(ref tr4);
                            break;
                        case 15:
                            txt_disabled(ref tr5);
                            break;
                        case 18:
                            txt_disabled(ref tr6);
                            break;
                        case 21:
                            txt_disabled(ref tr7);
                            break;
                        case 24:
                            txt_disabled(ref tr8);
                            break;
                        case 27:
                            txt_disabled(ref tr9);
                            break;
                        case 30:
                            txt_disabled(ref tr10);
                            break;
                        case 33:
                            txt_disabled(ref tr11);
                            break;
                        case 36:
                            txt_disabled(ref tr12);
                            break;
                        case 39:
                            txt_disabled(ref tr13);
                            break;
                        case 42:
                            txt_disabled(ref tr14);
                            break;
                        case 45:
                            txt_disabled(ref tr15);
                            break;
                        case 48:
                            txt_disabled(ref tr16);
                            break;
                        case 564:
                            txt_disabled(ref tr17);
                            break;
                        case 567:
                            txt_disabled(ref tr18);
                            break;
                    }
                }
            }
        }

        void txt_disabled(ref HtmlTableRow row)
        {
            row.Disabled = true;
            for (int i = 1; i < row.Cells.Count - 1; i++)
            {
                Control control = row.Cells[i].Controls[1];
                if (control.ID.Substring(0, 4) == "txtV")
                {
                    TextBox txt = (TextBox)control;
                    txt.ReadOnly = true;
                }
            }
        }
    }
}
