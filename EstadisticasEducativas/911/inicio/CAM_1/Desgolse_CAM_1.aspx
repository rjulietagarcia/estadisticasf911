<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Desglose" AutoEventWireup="true" CodeBehind="Desgolse_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.Desgolse_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li><li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li><li onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li><li onclick="openPage('Desgolse_CAM_1',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li><li onclick="openPage('Personal1_CAM_1',false)"><a href="#" title="" ><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL(CONT)</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAG Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
                            <tr>
                    <td colspan="16" nowrap="nowrap" style="height: 20px; text-align: justify">
                        <asp:Label ID="lblInstruccion7" runat="server" CssClass="lblRojo" Font-Bold="True"
                            Text="7. Escriba el total de los alumnos, desglos�ndolos por servicio o nivel, edad a la fecha del llenado del cuestionario y sexo."
                            Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td style="text-align: center">
                    </td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblInicial7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EDUCACI�N INICIAL"
                            Width="100%"></asp:Label></td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblPreescolar7" runat="server" CssClass="lblNegro" Font-Bold="True"
                            Text="PREESCOLAR" Width="100%"></asp:Label></td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblPrimaria7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PRIMARIA"
                            Width="100%"></asp:Label></td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblSecundaria7" runat="server" CssClass="lblNegro" Font-Bold="True"
                            Text="SECUNDARIA" Width="100%"></asp:Label></td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblCapacitacion7" runat="server" CssClass="lblNegro" Font-Bold="True"
                            Text="FORMACI�N PARA EL TRABAJO" Width="100%"></asp:Label></td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblAtencion7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="APOYO COMPLEMENTARIO"
                            Width="100%"></asp:Label></td>
                    <td colspan="2" style="text-align: center">
                        <asp:Label ID="lblTotal71" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                    </td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom71" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj71" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom73" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj73" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom74" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj74" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom75" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj75" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom76" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj76" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblHom77" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblMuj77" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label ID="lblTota72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                            Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl45a1" runat="server" CssClass="lblGrisTit" Text="45 d�as a 1 a�o"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV451" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV452" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV453" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV454" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV455" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV456" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV457" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV458" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV459" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV460" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV461" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10111"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV462" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10112"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV463" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10113"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV464" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10114"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV465" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10115"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl2a" runat="server" CssClass="lblGrisTit" Text="2 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV466" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV467" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV468" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV469" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV470" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV471" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV472" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV473" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV474" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV475" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV476" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10211"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV477" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10212"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV478" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10213"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV479" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10214"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV480" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10215"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl3a" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV481" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV482" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV483" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV484" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV485" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV486" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV487" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV488" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV489" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV490" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV491" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10311"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV492" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10312"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV493" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10313"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV494" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10314"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV495" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10315"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl3a6m" runat="server" CssClass="lblGrisTit" Text="3 a�os 6 meses a 3 a�os 11 meses"
                            Width="100px"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV496" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV497" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV498" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV499" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV500" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV501" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV502" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV503" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV504" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV505" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV506" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10411"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV507" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10412"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV508" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10413"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV509" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10414"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV510" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10415"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl4a" runat="server" CssClass="lblGrisTit" Text="4 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV511" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV512" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV513" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV514" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV515" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV516" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV517" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV518" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV519" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV520" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV521" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10511"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV522" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10512"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV523" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10513"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV524" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10514"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV525" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10515"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl5a" runat="server" CssClass="lblGrisTit" Text="5 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV526" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV527" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV528" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV529" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV530" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV531" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV532" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV533" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV534" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV535" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV536" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10611"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV537" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10612"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV538" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10613"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV539" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10614"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV540" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10615"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl6a" runat="server" CssClass="lblGrisTit" Text="6 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV541" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV542" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV543" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV544" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV545" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV546" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV547" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV548" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV549" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV550" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV551" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10711"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV552" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10712"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV553" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10713"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV554" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10714"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV555" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10715"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl7a" runat="server" CssClass="lblGrisTit" Text="7 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV556" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV557" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV558" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV559" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV560" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV561" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV562" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV563" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV564" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10809"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV565" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10810"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV566" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10811"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV567" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10812"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV568" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10813"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV569" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10814"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV570" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10815"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl8a" runat="server" CssClass="lblGrisTit" Text="8 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV571" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV572" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV573" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV574" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV575" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV576" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV577" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV578" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV579" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV580" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10910"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV581" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10911"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV582" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10912"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV583" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10913"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV584" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10914"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV585" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10915"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl9a" runat="server" CssClass="lblGrisTit" Text="9 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV586" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV587" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV588" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV589" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV590" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV591" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV592" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV593" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV594" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV595" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11010"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV596" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11011"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV597" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11012"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV598" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11013"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV599" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11014"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV600" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11015"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl10a" runat="server" CssClass="lblGrisTit" Text="10 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV601" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV602" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV603" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV604" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV605" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV606" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV607" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV608" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV609" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV610" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11110"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV611" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11111"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV612" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11112"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV613" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11113"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV614" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11114"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV615" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11115"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl11a" runat="server" CssClass="lblGrisTit" Text="11 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV616" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV617" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV618" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV619" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV620" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV621" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV622" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV623" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV624" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV625" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11210"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV626" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11211"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV627" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11212"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV628" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11213"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV629" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11214"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV630" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11215"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl12a" runat="server" CssClass="lblGrisTit" Text="12 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV631" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV632" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV633" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV634" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV635" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV636" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV637" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV638" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV639" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV640" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11310"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV641" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11311"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV642" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11312"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV643" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11313"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV644" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11314"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV645" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11315"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl13a" runat="server" CssClass="lblGrisTit" Text="13 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV646" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV647" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV648" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV649" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV650" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV651" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV652" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV653" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV654" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV655" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11410"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV656" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11411"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV657" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11412"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV658" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11413"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV659" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11414"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV660" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11415"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl14a" runat="server" CssClass="lblGrisTit" Text="14 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV661" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV662" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV663" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV664" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV665" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV666" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV667" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV668" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV669" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV670" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV671" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11511"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV672" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11512"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV673" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11513"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV674" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11514"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV675" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11515"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl15a" runat="server" CssClass="lblGrisTit" Text="15 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV676" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV677" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV678" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV679" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV680" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV681" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV682" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV683" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV684" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV685" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11610"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV686" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11611"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV687" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11612"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV688" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11613"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV689" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11614"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV690" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11615"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl16a" runat="server" CssClass="lblGrisTit" Text="16 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV691" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV692" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV693" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV694" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV695" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV696" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV697" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV698" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV699" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV700" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11710"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV701" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11711"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV702" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11712"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV703" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11713"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV704" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11714"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV705" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11715"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl17a" runat="server" CssClass="lblGrisTit" Text="17 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV706" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV707" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV708" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV709" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV710" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV711" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV712" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV713" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV714" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV715" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11810"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV716" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11811"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV717" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11812"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV718" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11813"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV719" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11814"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV720" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11815"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl18a" runat="server" CssClass="lblGrisTit" Text="18 a�os" Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV721" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV722" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV723" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV724" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV725" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV726" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV727" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV728" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV729" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV730" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11910"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV731" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11911"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV732" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11912"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV733" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11913"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV734" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11914"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV735" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11915"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lbl19a" runat="server" CssClass="lblGrisTit" Text="19 o mas a�os"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV736" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV737" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV738" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV739" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV740" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV741" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV742" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV743" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV744" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV745" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12010"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV746" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12011"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV747" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12012"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV748" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12013"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV749" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12014"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV750" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12015"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Label ID="lblSubTotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"
                            Width="100%"></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV751" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV752" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV753" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV754" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV755" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV756" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV757" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV758" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV759" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV760" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12110"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV761" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12111"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV762" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12112"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV763" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12113"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV764" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12114"></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtV765" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12115"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="1" style="text-align: left">
                        <asp:Label ID="lblTota73" runat="server" CssClass="lblGrisTit" Text="TOTAL (Suma de HOM y MUJ)"
                            Width="90px"></asp:Label></td>
                    <td colspan="2" style="text-align:center">
                        <asp:TextBox ID="txtV766" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                    <td colspan="2" style="text-align: center">
                        <asp:TextBox ID="txtV767" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                    <td colspan="2" style="text-align: center">
                        <asp:TextBox ID="txtV768" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                    <td colspan="2" style="text-align: center">
                        <asp:TextBox ID="txtV769" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
                    <td colspan="2" style="text-align: center">
                        <asp:TextBox ID="txtV770" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
                    <td colspan="2" style="text-align: center">
                        <asp:TextBox ID="txtV771" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
                    <td colspan="3" style="text-align: right">
                        <asp:TextBox ID="txtV772" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="16" nowrap="nowrap" style="height: 20px">
                    </td>
                </tr>
                <tr>
                    <td colspan="16">
                        <asp:Label ID="lblInstruccion8" runat="server" CssClass="lblRojo" Font-Bold="True"
                            Text="8. Registre por nivel o servicio educativo el n�mero de alumnos beneficiados en educaci�n regular, y la cantidad de padres orientados y maestros asesorados, al inicio del ciclo escolar."
                            Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="16">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                        <asp:Label ID="lblBeneficiados" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="ALUMNOS BENEFICIADOS" Width="200px"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:Label ID="lblOrientados" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="PADRES ORIENTADOS" Width="200px"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:Label ID="lblAsesorados" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="MAESTROS ASESORADOS" Width="200px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblInicial8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="EDUCACI�N INICIAL" Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV773" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30101"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV774" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30102"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV775" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30103"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblPreescolar8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="PREESCOLAR" Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV776" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30201"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV777" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30202"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV778" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30203"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblPrimaria8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="PRIMARIA" Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV779" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30301"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV780" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30302"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV781" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30303"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblSecundaria8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="SECUNDARIA" Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV782" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30401"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV783" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30402"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV784" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30403"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblCapacitacion" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="FORMACI�N PARA EL TRABAJO" Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV785" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30501"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV786" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30502"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV787" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30503"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTota8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV788" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30601"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV789" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30602"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV790" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30603"></asp:TextBox></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            
        </table>
       
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Personal1_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal1_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
      <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 16;
                MaxRow = 23;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
