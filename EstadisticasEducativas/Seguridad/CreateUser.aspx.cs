using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsSESeguridad;
using SEroot.WsRefSeguridad;
using Mx.Gob.Nl.Educacion;
using SEroot.WsEstadisticasEducativas;

namespace EstadisticasEducativas.Seguridad
{
    public partial class CreateUser : System.Web.UI.Page
    {

        SEroot.WsRefSeguridad.ControlSeguridad wsNt = new SEroot.WsRefSeguridad.ControlSeguridad();
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadiscticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                TextBox resultado = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("resul");
                DropDownList ddlNivelT = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivelT");
                CheckBoxList permisos = (CheckBoxList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("CheckBoxList1");

                for (int i = 0; i < permisos.Items.Count; i++)
                {

                    if (permisos.Items[0].Selected && permisos.Items[1].Selected)
                    {
                        int regionavan = 6;
                        //Valores += permisos.Items[i].Value + ";";
                        resultado.Text = "";
                        resultado.Text = regionavan.ToString();
                    }
                }

                TextBox Password = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("Password");
                TextBox ConfirmPassword = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ConfirmPassword");
                Password.Attributes.Add("onkeyup", "this.value=this.value.toUpperCase();");
                ConfirmPassword.Attributes.Add("onkeyup", "this.value=this.value.toUpperCase();");

                TextBox txtZona = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtZona");
                //txtZona.Attributes["onkeypress"] = "return keyRestrict(event, '1234567890')";
                //btnBuscar.Attributes["onblur"] = "javascript:_enter=true;";
                //btnBuscar.Attributes["onfocus"] = "javascript:_enter=false;";

                Entidad();
                Nivel();
                vaciaDropCCTs();
                Filtros();
                //Ciclos();
            }
        }

        private void Filtros()
        {
            DropDownList ddlEntidad = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlEntidad");
            DropDownList ddlRegion = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlRegion");
            TextBox txtZona = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtZona");
            TextBox txtClaveCT = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtClaveCT");
            DropDownList ddlCentroTrabajo = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlCentroTrabajo");
            DropDownList ddlNivel = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivel");
            DropDownList ddlCCTs = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlCCTs");
            DropDownList ddlNivelT = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivelT");
           

            UsuarioSeDP nuevoUser = SeguridadSE.GetUsuario(HttpContext.Current);

            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int ID_Entidad = nuevoUser.EntidadDP.EntidadId;
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            SEroot.WsCentrosDeTrabajo.ZonaDP zonaDP = ws.Load_Zona(zona);
            string ID_NivelEducacion = usr[7];

            ListItem itnt = null;
            switch (ID_NivelTrabajo)
            {
                case 0://FEDERAL
                    itnt = new ListItem("Federal", "0");
                    ddlNivelT.Items.Add(itnt);
                    itnt = new ListItem("Estado", "1");
                    ddlNivelT.Items.Add(itnt);
                    itnt = new ListItem("Regi�n", "2");
                    ddlNivelT.Items.Add(itnt);
                    itnt = new ListItem("Zona", "3");
                    ddlNivelT.Items.Add(itnt);
                    itnt = new ListItem("CCT", "4");
                    ddlNivelT.Items.Add(itnt);
                    //itnt = new ListItem("Regi�n Avan", "5");
                    //ddlNivelT.Items.Add(itnt);
                    //  itnt = new ListItem("Regi�n Des", "6");
                    //ddlNivelT.Items.Add(itnt);
                    //  itnt = new ListItem("Regi�n DBF", "7");
                    //ddlNivelT.Items.Add(itnt);
                    break;
                case 1://SE ESTATAL
                    //Bloquear el estado y mostrar solamente el estado al cual pertenece
                    ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    ddlEntidad.Enabled = false;
                    itnt = new ListItem("Regi�n", "2");
                    ddlNivelT.Items.Add(itnt);
                    itnt = new ListItem("Zona", "3");
                    ddlNivelT.Items.Add(itnt); 
                    break;
                case 2://Region
                     
                    ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    ddlEntidad.Enabled = false;
                    ddlRegion.SelectedValue = region.ToString();
                    ddlRegion.Enabled = false;
                    itnt = new ListItem("Zona", "3");
                    ddlNivelT.Items.Add(itnt); 
                  
                    break;
                case 3://ZONA
                    Response.Write(@"<script type='text/javascript'>alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);</script>");    
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", "alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);", true);
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "onload", " alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);",true);
                     break;
                case 4://CCT
                    Response.Write(@"<script type='text/javascript'>alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);</script>");        
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", "alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);", true);
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "onload", " alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);",true);
                    break;
                case 5://Region

                    ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    ddlEntidad.Enabled = false;
                    ddlRegion.SelectedValue = region.ToString();
                    ddlRegion.Enabled = false;
                    itnt = new ListItem("Zona", "3");
                    ddlNivelT.Items.Add(itnt);

                    break;
                case 6://Region

                    ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    ddlEntidad.Enabled = false;
                    ddlRegion.SelectedValue = region.ToString();
                    ddlRegion.Enabled = false;
                    itnt = new ListItem("Zona", "3");
                    ddlNivelT.Items.Add(itnt);

                    break;
                case 7://Region

                    ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    ddlEntidad.Enabled = false;
                    ddlRegion.SelectedValue = region.ToString();
                    ddlRegion.Enabled = false;
                    itnt = new ListItem("Zona", "3");
                    ddlNivelT.Items.Add(itnt);

                    break;


            }
            
        }
            
        protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            string pass = CreateUserWizard1.Password;
            DropDownList ddlCCTs = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlCCTs");

            ControlSeguridad wscs=new ControlSeguridad();
            SEroot.WsRefSeguridad.PersonaDP personaDP=new SEroot.WsRefSeguridad.PersonaDP();
            SEroot.WsRefSeguridad.UsuarioDP usuarioDP=new SEroot.WsRefSeguridad.UsuarioDP();
            SEroot.WsRefSeguridad.UsuarioLugarTrabajoDP uLTrDP=new SEroot.WsRefSeguridad.UsuarioLugarTrabajoDP();
            SEroot.WsRefSeguridad.UsuarioPerfilDP uPerDP=new SEroot.WsRefSeguridad.UsuarioPerfilDP();

            WsSESeguridad wse = new WsSESeguridad();
            CentrotrabajoPk ctPK=new CentrotrabajoPk();
            CentrotrabajoDP ctDP=new CentrotrabajoDP();
            ctPK.CentrotrabajoId=int.Parse(ddlCCTs.SelectedValue);
            ctDP=wse.CentrotrabajoLoad(ctPK);

            string resultadoUsuarioSave = "0";
            if (ctDP != null)
            {
                TextBox resultado = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("resul");
                TextBox txtNom = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtNombre");
                TextBox txtAP = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtApPaterno");
                TextBox txtAM = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtApMaterno");
                    
                personaDP.fechaNacimiento = DateTime.Now.ToString("dd/MM/yyyy");
                personaDP.nombre = txtNom.Text;
                personaDP.apellidoPaterno= txtAP.Text;
                personaDP.apellidoMaterno = txtAM.Text;

                uLTrDP.bitActivo = true;
                uLTrDP.centroTrabajoId = int.Parse(ddlCCTs.SelectedValue);
                uLTrDP.consecutivoId = 0;
                uLTrDP.entidadId = ctDP.EntidadId;
                uLTrDP.fechaActualizacion = DateTime.Now.ToString("dd/MM/yyyy");
            
                uLTrDP.nivelTrabajoId = int.Parse(resultado.Text);
                uLTrDP.paisId = ctDP.PaisId;
                uLTrDP.regionId = ctDP.RegionId;
                uLTrDP.sectorId = ctDP.SectorId;
                uLTrDP.usuarioId = int.Parse(User.Identity.Name.Split('|')[0]);
                uLTrDP.usuarioIdLt = 0;
                uLTrDP.usuarioLugarTrabajoCNSC = 0;
                uLTrDP.zonaId = ctDP.ZonaId;

                usuarioDP.bitActivo = true;
                usuarioDP.bitActivo2 = true;
                usuarioDP.bitBloqueado = false;
                usuarioDP.fechaActualizacion = DateTime.Now.ToString("dd/MM/yyyy");
                usuarioDP.login = CreateUserWizard1.UserName;
                usuarioDP.password = CreateUserWizard1.Password;
                usuarioDP.personaId = 0;
                usuarioDP.usuarioId = 0;
                usuarioDP.usuarioId2 = int.Parse(User.Identity.Name.Split('|')[0]);

                uPerDP.bitActivo = true;

                resultadoUsuarioSave=wscs.GuardaUsuarioTransaccional(personaDP, usuarioDP, uLTrDP, uPerDP);
            }
            if (resultadoUsuarioSave != "1")
            {
                e.Cancel = true;
                Literal lblError = (Literal)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ErrorMessage");
                lblError.Text = "No se pudo crear el nuevo usuario, intentelo m�s tarde";
            }
           
        }

        protected void ddlEntidad_SelectedIndexChanged(object sender, EventArgs e)
        {


            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();

            DropDownList ddlEntidad = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlEntidad");
            DropDownList ddlRegion = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlRegion");
            
            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);
            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    ddlRegion.Items.Clear();
                    ddlRegion.Items.Add(new ListItem("Todas las regiones", "0"));
                    ddlRegion.AppendDataBoundItems = true;
                    ddlRegion.DataSource = Ds;
                    ddlRegion.DataMember = "Region";
                    ddlRegion.DataTextField = "Nombre";
                    ddlRegion.DataValueField = "ID_Region";
                    //this.ddlRegion.SelectedIndex = 1;
                    ddlRegion.DataBind();
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                ddlRegion.Items.Clear();
                ddlRegion.Items.Add(new ListItem("Seleccione un Estado", "0"));
            }


        }
        protected void ddlNivelT_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlNivelT = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivelT");
            CheckBoxList permisos = (CheckBoxList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("CheckBoxList1");
            Label seleccion = (Label)CreateUserWizardStep1.ContentTemplateContainer.FindControl("Permisos");
            Button agregar = (Button)CreateUserWizardStep1.ContentTemplateContainer.FindControl("agregar");
            TextBox resultado = (TextBox) CreateUserWizardStep1.ContentTemplateContainer.FindControl("resul");
           
            int ID_Nivelt = int.Parse(ddlNivelT.SelectedValue);
            if (ID_Nivelt == 2)
            {
                permisos.Visible = true;
                seleccion.Visible = true;
                agregar.Visible= true;
                resultado.Visible = false;
                
            }
            else
            {
                permisos.Visible = false;
                seleccion.Visible = false;
                agregar.Visible= false;
                resultado.Visible= false;
                

            }
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label lblMsg = (Label)CreateUserWizardStep1.ContentTemplateContainer.FindControl("lblMsg");
            lblMsg.Text = "";
            vaciaDropCCTs();
        }

        private void Entidad()
        {
            DropDownList ddlEntidad = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlEntidad");

            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.CatListaEntidad911DP[] listaEntidades = wsEstadisticas.Lista_CatListaEntidad911(223);//EL ID PAIS = 223 es M�XICO
            for (int i = 0; i < listaEntidades.Length; i++)
            {
                string valor = listaEntidades[i].ID_Entidad.ToString();
                string texto = listaEntidades[i].Nombre;
                ddlEntidad.Items.Add(new ListItem(texto, valor));

            }
        }

        protected void cargaCCTs()
        {
            DropDownList ddlEntidad = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlEntidad");
            DropDownList ddlRegion = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlRegion");
            TextBox txtZona = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtZona");
            TextBox txtClaveCT = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtClaveCT");
            DropDownList ddlCentroTrabajo = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlCentroTrabajo");
            DropDownList ddlNivel = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivel");
            DropDownList ddlCCTs = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlCCTs");
            Label lblMsg = (Label)CreateUserWizardStep1.ContentTemplateContainer.FindControl("lblMsg");

            SEroot.WsCentrosDeTrabajo.DsBuscaCCT911 Ds = new SEroot.WsCentrosDeTrabajo.DsBuscaCCT911();
            int reg = 0;
            int zon = 0;
            string ct = "";
            //if (txtRegion.Text.Trim().Length > 0)
            //{
            //    reg = int.Parse(txtRegion.Text.Trim());
            //}
            //Ahora la region la obtiene del dropdownlist

            reg = int.Parse(ddlRegion.SelectedValue);

            if (txtZona.Text.Trim().Length > 0)
            {
                zon = int.Parse(txtZona.Text.Trim());
            }
            if (txtClaveCT.Visible)
            {
                if (txtClaveCT.Text.Trim().Length > 0)
                {
                    ct = txtClaveCT.Text.Trim();
                }
            }
            else
            {
                ct = ddlCentroTrabajo.SelectedValue;
            }

            string valor = ddlNivel.SelectedValue;

            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);
            
            if (ddlNivel.SelectedValue == "0")
                Ds = ws.ListaEscuelas911(ID_Entidad, reg, zon, ct, 0, 0);
            else
            {
                int ID_Nivel = int.Parse(valor.Split('_')[0]);
                int ID_SubNivel = int.Parse(valor.Split('_')[1]);
                Ds = ws.ListaEscuelas911(ID_Entidad, reg, zon, ct, ID_Nivel, ID_SubNivel);
            }
            
            if (Ds != null && Ds.Tables[0].Rows.Count > 0)
            {
                ddlCCTs.DataSource = Ds.CCT911;
                ddlCCTs.DataTextField = "CveCT";
                ddlCCTs.DataValueField = "ID_CT";
                ddlCCTs.DataBind();
                Session["dsCCTs"] = Ds.CCT911;
                lblMsg.Text = "Registros encontrados: " + Ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                //GridVacio();
                lblMsg.Text = "No se encontraron registros";
            }
        }
        private void Nivel()
        {
            DropDownList ddlNivel = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivel");

            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            //Response.Write(usr[0]);
            if (usr[0] == "352599")
            {
                ddlNivel.Items.Add(new ListItem("CONALEP", "21_20"));
            }
            else if (ID_NivelTrabajo == 0 || ID_NivelTrabajo == 1 || ID_NivelTrabajo == 2 || ID_NivelTrabajo == 3 || ID_NivelTrabajo == 5 || ID_NivelTrabajo == 6 || ID_NivelTrabajo == 7)
            {
                SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

                SEroot.WsEstadisticasEducativas.CatListaNiveles911DP[] listaNiveles = wsEstadisticas.Lista_CatListaNiveles911(EstadisticasEducativas._911.Class911.ID_Fin);
                int secundaria = 0;
                for (int i = 0; i < listaNiveles.Length; i++)
                {
                    string valor;
                    if (listaNiveles[i].ID_Nivel != 13)
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_" + listaNiveles[i].ID_SubNivel.ToString();
                        secundaria = 0;
                    }
                    else
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_0";
                        secundaria++;
                    }
                    if (secundaria < 2)
                    {
                        string texto = listaNiveles[i].Descrip;
                        ddlNivel.Items.Add(new ListItem(texto, valor));
                    }
                }
            }
            else
            {
                ddlNivel.Enabled = false;
                ddlNivel.Items.Add(new ListItem("", "0_0"));
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            vaciaDropCCTs();
            cargaCCTs();
        }

        private void vaciaDropCCTs()
        {
            DropDownList ddlCCTs = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlCCTs");
            ddlCCTs.Items.Clear();
        }

        public void setRegion(int ID_Entidad)
        {
            DropDownList ddlRegion = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlRegion");
            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();

            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    ddlRegion.Items.Clear();
                    ddlRegion.Items.Add(new ListItem("Todas las regiones", "0"));
                    ddlRegion.AppendDataBoundItems = true;
                    ddlRegion.DataSource = Ds;
                    ddlRegion.DataMember = "Region";
                    ddlRegion.DataTextField = "Nombre";
                    ddlRegion.DataValueField = "ID_Region";
                    //ddlRegion.SelectedIndex = 1;
                    ddlRegion.DataBind();

                    ddlRegion.Enabled = true;//Una vez que tiene contenido, desplegar
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                ddlRegion.Items.Clear();
                ddlRegion.Items.Add(new ListItem("Seleccione una Regi�n", "0"));
                ddlRegion.Enabled = false;
            }



        }

      

      

        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox resultado = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("resul");
            DropDownList ddlNivelT = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ddlNivelT");
            CheckBoxList permisos = (CheckBoxList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("CheckBoxList1");
            for (int i = 0; i < permisos.Items.Count; i++)
            {
                
                if (permisos.Items[0].Selected && permisos.Items[1].Selected)
                {
                    int regionavan = 5;
                    //Valores += permisos.Items[i].Value + ";";
                    resultado.Text = "";
                    resultado.Text = regionavan.ToString();
                }
                if (permisos.Items[0].Selected && permisos.Items[1].Selected && permisos.Items[2].Selected)
                {
                    //Valores += permisos.Items[i].Value + ";";
                    int regiondes = 6;
                    resultado.Text = "";
                    resultado.Text = regiondes.ToString();
                }
                if (permisos.Items[0].Selected && permisos.Items[1].Selected && permisos.Items[3].Selected)
                {
                    //Valores += permisos.Items[i].Value + ";";
                    int regiondbf = 7;
                    resultado.Text = "";
                    resultado.Text = regiondbf.ToString();
                }
                if (permisos.Items[0].Selected && permisos.Items[1].Selected && permisos.Items[2].Selected && permisos.Items[3].Selected)
                {
                    int regionall = 2;
                    //Valores += permisos.Items[i].Value + ";";
                    resultado.Text = "";
                    resultado.Text = regionall.ToString();
                }
                
            }
        }
           
    }

        
}
