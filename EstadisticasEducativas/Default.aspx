﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master"  AutoEventWireup="true"
    Codebehind="Default.aspx.cs" Inherits="EscolarABC.Default" Title="Página sin título" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="conLogOn" ContentPlaceHolderID="cphMainMaster" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <table style="width: 100%; height: 2px" cellpadding="0" cellspacing="0">
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%" align="center" cellpadding="0" cellspacing="0">
        <tr class="Titulo">
            <td style="text-align: center; width: 100%; height: 19px;">
                <asp:Label ID="lblHeaderOpciones" Text="Seleccione un Sistema" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 38px">
                <br />
            </td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%; height: 222px;">
                <center>
                    <table id="Table1" class="fondot" cellspacing="0" cellpadding="0" border="0" style="text-align: center">
                        <tr>
                            <td class="EsqSupIzq" style="height: 35px;">
                            </td>
                            <td class="RepSup">
                            </td>
                            <td class="EsqSupDer" style="height: 18px; width: 18px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="RepLatIzq">
                            </td>
                            <td align="left">
                                <!-- Contenido -->
                                <asp:Panel ID="pnlIconos" runat="server" Width="100%">
                                    <asp:DataList ID="dlMenu" runat="server" DataSourceID="smdsSistemasDisponibles" RepeatColumns="2"
                                        SkinID="mnuIconos">
                                        <ItemTemplate>
                                                    <table class="tabla_default" onmouseover="tabla(this, 'over')" onmouseout="tabla(this, 'out')" onclick="ventana('<%# Eval("Url") %>')"
                                                        border="0" cellpadding="4" cellspacing="0">  <%--  esto hiba a dentro del onclik pero marcaba error .ToString().Replace("~","").Replace("/","")--%>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="imgMenu" runat="server" ImageUrl='<%# "~/App_Themes/SEP2010/MenuSistemas/"+ Eval("Description").ToString().Replace("/","").Replace(".","").ToUpper() + ".jpg" %>'
                                                                    PostBackUrl='<%# Eval("Url") %>' CommandName='<%# Eval("Url") %>' OnCommand="DoCommand" />
                                                            </td>
                                                            <td style="vertical-align: middle; width: 100%">
                                                                <asp:HyperLink ID="lnkMenu" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("Url") %>'>Sistema</asp:HyperLink>
                                                            </td>
                                                        </tr>
                                                    </table>
                                        </ItemTemplate>
                                    </asp:DataList></asp:Panel>
                                <asp:Panel ID="pnlOpciones" runat="server">
                                    <asp:Repeater ID="rptOpciones" runat="server">
                                        <ItemTemplate>
                                            ►&nbsp;&nbsp;
                                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Description") %>'
                                                NavigateUrl='<%# Eval("Url") %>' Width="350">
                                            </asp:HyperLink>
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                                <asp:SiteMapDataSource ID="smdsSistemasDisponibles" runat="server" ShowStartingNode="False" />
                            </td>
                            <td class="RepLatDer">
                            </td>
                        </tr>
                        <tr>
                            <td class="EsqInfIzq" style="height: 18px">
                            </td>
                            <td class="RepInf" style="height: 18px">
                            </td>
                            <td class="EsqInfDer" style="height: 18px; width: 18px">
                            </td>
                        </tr>
                    </table>
                    		<p align="center" >
            Es necesario Acrobat Reader para mostrar algunos documentos, si no lo tiene instalado
            puede descargarlo <a href="http://www.adobe.com/es/products/acrobat/readstep2.html" target="_blank">
            <br />
            <img alt="" border="0" title="Descargar Adobe Acrobat Reader" src="http://www.adobe.com/images/shared/download_buttons/get_adobe_reader.gif" /></a></p>
                </center>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
