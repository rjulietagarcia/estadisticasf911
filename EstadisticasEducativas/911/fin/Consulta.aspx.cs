using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Mx.Gob.Nl.Educacion;
using SEroot.WsEstadisticasEducativas;
using SEroot.WSEscolar;

namespace EstadisticasEducativas._911.fin
{
    public partial class Consulta : System.Web.UI.Page
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadiscticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

        private SEroot.WSEscolar.CentroTrabajoNivelTurnoDP CCTNT
        {
            get { return (CentroTrabajoNivelTurnoDP)ViewState["cctnt"]; }
            set { ViewState["cctnt"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //txtRegion.Attributes["onkeypress"] = "return Num(event)";
                txtZona.Attributes["onkeypress"] = "return Num(event)";
                btnBuscar.Attributes["onblur"] = "javascript:_enter=true;";
                btnBuscar.Attributes["onfocus"] = "javascript:_enter=false;";
                Entidad();
                Nivel();
                GridVacio();
                Filtros();
                Ciclos();
                Estatus();
            }
        }

        private void Filtros()
        {
            UsuarioSeDP nuevoUser = SeguridadSE.GetUsuario(HttpContext.Current);
            //nuevoUser.Selecciones.CentroTrabajoSeleccionado.

            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int ID_Entidad = nuevoUser.EntidadDP.EntidadId;
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            SEroot.WsCentrosDeTrabajo.ZonaDP zonaDP = ws.Load_Zona(zona);
            string ID_NivelEducacion = usr[7];

            //ddlNivel.Enabled = false;

            switch (ID_NivelTrabajo)
            {
                case 0://FEDERAL
                    break;
                case 1://SE ESTATAL
                    //Bloquear el estado y mostrar solamente el estado al cual pertenece
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);

                    this.ddlEntidad.Enabled = false;

                    break;
                case 2://Region
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    break;
                case 3://ZONA
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;

                    this.txtZona.Text = zona.ToString();
                    this.txtZona.Enabled = false;





                    break;
               
                case 4://CCT
                    this.btnBuscar.Enabled = false;
                    this.btnExportar.Enabled = false;
                    break;
                case 5://Regionavan
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    break;
                case 6://Regiondes
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    break;
                case 7://Regiondbf
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    break;
            }
        }
        private void Entidad()
        {
            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.CatListaEntidad911DP[] listaEntidades = wsEstadisticas.Lista_CatListaEntidad911(223);//EL ID PAIS = 223 es M�XICO
            for (int i = 0; i < listaEntidades.Length; i++)
            {
                string valor = listaEntidades[i].ID_Entidad.ToString();
                string texto = listaEntidades[i].Nombre;
                ddlEntidad.Items.Add(new ListItem(texto, valor));

            }

        }

        private void Nivel()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            if (usr[0] == "352599")
            {
                ddlNivel.Items.Add(new ListItem("CONALEP", "21_20"));
            }
            else if (ID_NivelTrabajo == 0 || ID_NivelTrabajo == 1 || ID_NivelTrabajo == 2 || ID_NivelTrabajo == 3 || ID_NivelTrabajo == 5 || ID_NivelTrabajo == 6 || ID_NivelTrabajo == 7)
            {
                SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
                int id_tipoCuestionario = 0;
                if (rdLista1.SelectedValue == "1")
                    id_tipoCuestionario = 1;
                else if (rdLista1.SelectedValue == "2")
                    id_tipoCuestionario = 2;
                SEroot.WsEstadisticasEducativas.CatListaNiveles911DP[] listaNiveles = wsEstadisticas.Lista_CatListaNiveles911(id_tipoCuestionario);
                int secundaria = 0;
                for (int i = 0; i < listaNiveles.Length; i++)
                {
                    string valor;
                    if (listaNiveles[i].ID_Nivel != 13)
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_" + listaNiveles[i].ID_SubNivel.ToString();
                        secundaria = 0;
                    }
                    else
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_0";
                        secundaria++;
                    }
                    if (secundaria < 2)
                    {
                        string texto = listaNiveles[i].Descrip;
                        ddlNivel.Items.Add(new ListItem(texto, valor));
                    }
                }
            }
            else
            {
                ddlNivel.Enabled = false;
                ddlNivel.Items.Add(new ListItem("", "0_0"));
            }
        }

        private void Estatus()
        {
            ddlEstatus.Items.Add(new ListItem("TODOS", "TODO"));
            ddlEstatus.Items.Add(new ListItem("OFICIALIZADO", "SI"));
            ddlEstatus.Items.Add(new ListItem("NO OFICIALIZADO", "NO"));
            ddlEstatus.Items.Add(new ListItem("OFICIALIZADO CON MOTIVO DE NO CAPTURA", "MC"));
        }

        private void Ciclos()
        {
            SEroot.WSEscolar.WsEscolar wsEscolar = new SEroot.WSEscolar.WsEscolar();
            SEroot.WSEscolar.DsCiclosEscolares dsCiclos = wsEscolar.ListaCicloEscolarCombo(1);//Anual

            ddlCiclo.DataSource = dsCiclos.CicloEscolar;
            ddlCiclo.DataValueField = "Id_CicloEscolar";
            ddlCiclo.DataTextField = "Nombre";
            ddlCiclo.DataBind();

            getUsr();

            ListItem itemActual = ddlCiclo.Items.FindByValue(usr.Selecciones.CicloEscolarSeleccionado.CicloescolarId.ToString());
            if (itemActual != null)
            {
                itemActual.Selected = true;
            }

        }

        //protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        DataRowView rowView = (DataRowView)e.Row.DataItem;
        //        ImageButton ib = new ImageButton();
        //        ib.ID = "est9111";
        //        ib.ImageUrl = "../../tema/images/iconEsc.gif";
        //        ib.CommandName = "ImageButton";
        //        ib.OnClientClick = "window.open('911_1/Identificacion_911_1.aspx?cnt=" + rowView["ID_CCTNT"].ToString() + "&cic=" + ddlCiclo.SelectedValue + "', 'ventana911', 'width=924,height=800,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');return false;";
        //        ib.Visible = true;
        //        e.Row.Cells[0].Controls.Add(ib);
        //        e.Row.Cells[0].Visible = true;
        //    }
        //}

        protected void GridVacio()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            Session["dsGrid"] = null;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //if (ddlNivel.SelectedValue == "0")
            //{
            //    lblMsg.Text = "Seleccione un Nivel";
            //}
            //else
            //{
                LlenaGridView_2();
            //}
        }



        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }

        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.DataSource = SortDataTable(true);
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected DataView SortDataTable(bool isPageIndexChanging)
        {
            //DataSet ds = (DataSet)Session["dsGrid"];
            //if (ds != null)
            if (Session["dsGrid"] != null)
            {
                //DataTable dataTable = new DataTable();
                //dataTable = ds.Tables[0];

                DataTable dataTable = new DataTable();
                dataTable = (DataTable)Session["dsGrid"];

                if (dataTable != null)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
            else
            {
                return new DataView();
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridViewSortExpression = e.SortExpression;
            int pageIndex = GridView1.PageIndex;
            GridView1.DataSource = SortDataTable(false);
            GridView1.DataBind();
            GridView1.PageIndex = pageIndex;
        }


        protected void RowCommand(object sender, GridViewCommandEventArgs e)
        {

            int index = Convert.ToInt32(e.CommandArgument);
            
            
            GridViewRow row = GridView1.Rows[index];
            if (row != null)
            {
                int ID_Ciclo =  int.Parse(this.ddlCiclo.SelectedValue);
                string[] usr = User.Identity.Name.Split('|');
                int ID_Usuario = int.Parse(usr[0]);

                Label lbl = (Label)row.FindControl("lblID_CCTNT");

                EstablecerBarrita(0, 0, 0, 0, int.Parse(lbl.Text));
                int id_cctnt = int.Parse(lbl.Text);
                ControlDP controlDP=null;
                if(rdLista1.SelectedValue=="1")
                    controlDP = wsEstadiscticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Inicio, ID_Ciclo, ID_Usuario);
                else
                    controlDP = wsEstadiscticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Fin, ID_Ciclo, ID_Usuario);
                Class911.SetControlSeleccionado(HttpContext.Current, controlDP);

                SEroot.WSEscolar.WsEscolar wse = new SEroot.WSEscolar.WsEscolar();
                CCTNT = wse.GetCentroTrabajoNivelTurno(controlDP.ID_CentroTrabajo,
                                            controlDP.ID_Nivel,
                                            controlDP.ID_Turno);


                //Abrir en modal el cuestionario
                string codigo = "";
                if (e.CommandName == "des")
                {
                    controlDP.Estatus = 0;
                    wsEstadiscticas.Oficializar_Cuestionario(controlDP, int.Parse(User.Identity.Name.Split('|')[0]));

                    //codigo = "AbrirPDF('" + Class911.Desoficializar(controlDP.ID_Control).Substring(3) + "',3);";
                }


                LlenaGridView_2();
            }
           



        }

        protected void LlenaGridView_2()
        {
            SEroot.WsEstadisticasEducativas.DsAvanceCaptura Ds = new SEroot.WsEstadisticasEducativas.DsAvanceCaptura();
            int reg = 0;
            int zon = 0;
            string ct = "";

            reg = int.Parse(ddlRegion.SelectedValue);

            if (txtZona.Text.Trim().Length > 0)
            {
                zon = int.Parse(txtZona.Text.Trim());
            }
            if (txtClaveCT.Visible)
            {
                if (txtClaveCT.Text.Trim().Length > 0)
                {
                    ct = txtClaveCT.Text.Trim();
                }
            }
            else
            {
                ct = ddlCentroTrabajo.SelectedValue;
            }

            string valor = ddlNivel.SelectedValue;

            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);

            int ID_Nivel = 0;
            int ID_SubNivel = 0;

            string[] arreglo = valor.Split('_');
            if (arreglo.Length > 1)
            {
                ID_Nivel = int.Parse(valor.Split('_')[0]);

                ID_SubNivel = int.Parse(valor.Split('_')[1]);
            }

            int tipoCuestionario = int.Parse(rdLista1.SelectedValue);

            string vddlEstatus = ddlEstatus.SelectedItem.Value;
            try
            {
                Ds = wsEstadiscticas.ReporteDeAvanceCaptura(ID_Entidad, reg, zon, ct, ID_Nivel, ID_SubNivel, vddlEstatus, tipoCuestionario);

                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {

                    GridView1.DataSource = Ds.avance;
                    GridView1.DataBind();
                    Session["dsGrid"] = Ds.avance;
                    //lblMsg.Text = "Registros encontrados: " + Ds.Tables[0].Rows.Count.ToString();
                    lblMsg.Text = "Solo se mostrar�n los primeros 50 registros encontrados";
                    btnExportar.Enabled = true;
                }
                else
                {
                    GridVacio();
                    lblMsg.Text = "No se encontraron registros";
                    btnExportar.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                GridVacio();
                lblMsg.Text = "No se encontraron registros";
            }
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtRegion.Text = "";
            //txtZona.Text = "";
            //txtClaveCT.Text = "";
            lblMsg.Text = "";
            GridVacio();
        }

        UsuarioSeDP usr = null;
        protected void getUsr()
        {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }
        protected void EstablecerBarrita(int ID_Nviel, int ID_Region, int ID_Sostenimiento, int ID_Zona, int ID_CCTNT)
        {
            getUsr();
            //SERaiz.Controles.FiltraCCTporZona FiltraCCTporZona1 = (SERaiz.Controles.FiltraCCTporZona)Page.Master.FindControl("FiltraCCTporZona1");


            if ((ID_CCTNT == -1 || ID_CCTNT == 0) && (ID_Zona == -1 || ID_Zona == 0) && (ID_Region == -1 || ID_Region == 0))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('���Seleccione un Centro de Trabajo!!!');", true);
            }
            else
            {
                if (ID_CCTNT != 0 && ID_CCTNT != -1)
                    SeguridadSE.SetCctNt(this.Page, ID_CCTNT);//Set Centro Trabajo
                else
                    if (ID_Zona != 0 && ID_Zona != -1)
                        SeguridadSE.SetCctNt(this.Page, ID_Zona);//Set Zona
                    else
                        if (ID_Region != 0 && ID_Region != -1)
                            SeguridadSE.SetCctNt(this.Page, ID_Region);//Set Region
                        else
                            if (usr.NivelTrabajo.NiveltrabajoId == 1)
                                SeguridadSE.SetCctNt(this.Page, 2);//Set Secretaria de Educacion
                            else
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('���Seleccione un Centro de Trabajo!!!');", true);

                ((Label)Page.Master.FindControl("lblCCTSeleccionado")).Text = SeguridadSE.cctSelected.Clavecct + " - " + SeguridadSE.cctSelected.Nombrecct + " - " + SeguridadSE.cctSelected.Truno;
            }
            if (SeguridadSE.cicloSelected != null)
            {
                if (SeguridadSE.cicloSelected.Nombre != "")
                    ((Label)Page.Master.FindControl("lblCicloEscolar")).Text = SeguridadSE.cicloSelected.Nombre;
            }
            //FiltraCCTporZona1.filtrosPermisos(usr);
        }


    

        protected void ddlEntidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);
            setRegion(ID_Entidad);
        }

        public void setRegion(int ID_Entidad)
        {
            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();

            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlRegion.Items.Clear();
                    this.ddlRegion.Items.Add(new ListItem("Todas las regiones", "0"));
                    this.ddlRegion.AppendDataBoundItems = true;
                    this.ddlRegion.DataSource = Ds;
                    this.ddlRegion.DataMember = "Region";
                    this.ddlRegion.DataTextField = "Nombre";
                    this.ddlRegion.DataValueField = "ID_Region";
                    //this.ddlRegion.SelectedIndex = 1;
                    this.ddlRegion.DataBind();

                    this.ddlRegion.Enabled = true;//Una vez que tiene contenido, desplegar
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                this.ddlRegion.Items.Clear();
                this.ddlRegion.Items.Add(new ListItem("Seleccione una Regi�n", "0"));
                this.ddlRegion.Enabled = false;
            }



        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = true; 
                e.Row.Cells[1].Visible = false; 
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false; 
                e.Row.Cells[4].Visible = false; 
                e.Row.Cells[5].Visible = true; 
                e.Row.Cells[6].Visible = true; 
                e.Row.Cells[7].Visible = true; 
                e.Row.Cells[8].Visible = false; 
                e.Row.Cells[9].Visible = true; 
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[11].Visible = true;
                e.Row.Cells[12].Visible = false;
                e.Row.Cells[13].Visible = false;
                e.Row.Cells[14].Visible = false;
                e.Row.Cells[15].Visible = false;
                e.Row.Cells[16].Visible = false;
                e.Row.Cells[17].Visible = true;
                e.Row.Cells[18].Visible = true;
                 
               
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            LlenaGridView_2();
            
            StringBuilder sb_bus = new StringBuilder();
            sb_bus.Append("<table border='1'>");
            sb_bus.Append("<tr>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>ESTATUS</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>REGION</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>ZONA</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>NIVEL</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>CCT</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>NOMBRE</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>TURNO</td>");
            //sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>TELEFONO</td>");
            sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>SUBNIVEL</td>");
            //sb_bus.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #009904;font-weight: bold;vertical-align: middle;'>SOSTENIMIENTO</td>");
            sb_bus.Append("</tr>");

            foreach (GridViewRow row in GridView1.Rows)
            {
                sb_bus.Append("<tr>");
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[18].Text + "</td>"); // ESTATUS
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[9].Text + "</td>"); // REGION
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[11].Text + "</td>"); // ZONA
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[13].Text + "</td>"); // NIVEL
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[5].Text + "</td>"); // CCT
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[6].Text + "</td>"); // NOMBRE
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[7].Text + "</td>"); // TURNO
                //sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[8].Text + "</td>"); // TELEFONO
                sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[15].Text + "</td>"); // SUBNIVEL
                //sb_bus.Append("<td style='font-size: 11px;color: #000000;font-family: Verdana, Tahoma, Arial;background-color: #ffffff;'>" + row.Cells[17].Text + "</td>"); // SOSTENIMIENTO
                sb_bus.Append("</tr>");
            }
            sb_bus.Append("</table>");


            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-Disposition", "attachment;filename=Datos.xls");
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.Default;
            //Response.Write(" <table><tr><td align='center' colspan='15'><h4>titulo</h4></td></tr></table>");
            //Response.Write(" <table><tr><td align='center' colspan='15'><h4>subtitulo</h4></td></tr></table><br>");
            string contenido = sb_bus.ToString();
            Response.Write(contenido);
            Response.End();
           // string codigoJS = "window.open('" + path + "', 'window','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');";
           // ScriptManager.RegisterStartupScript(this, this.GetType(), "SC123", codigoJS, true);
        }
        protected void rdLista1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlNivel.Items.Clear();
            ddlNivel.Items.Add(new ListItem("Seleccione un Nivel", "0"));
            Nivel();
        }
    }
}
