<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master"  Title="911.2(Inmueble)" AutoEventWireup="true" CodeBehind="Inmueble_911_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_2.Inmueble_911_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

     

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACIÓN PREESCOLAR</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_2',true)"><a href="#" title=""><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('AG1_911_2',true)"><a href="#" title=""><span>1°</span></a></li>
        <li onclick="openPage('AG2_911_2',true)"><a href="#" title=""><span>2°</span></a></li>
        <li onclick="openPage('AG3_911_2',true)"><a href="#" title="" ><span>3°</span></a></li>
        <li onclick="openPage('AGT_911_2',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_2',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_2',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('Inmueble_911_2',true)"><a href="#" title="" class="activo"><span>INMUEBLE</span></a></li>
        <li onclick="openPage('Anexo_911_2',false)"><a href="#" title=""><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACIÓN</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>

   <div id="tooltipayuda" class="balloonstyle">
    <p>Favor de contestar el cuestionario referente al inmueble escolar y dar clic en la opción siguiente para continuar con la generación de las Estadísticas Educativas.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
   
      <table class="fondot"  cellspacing="0" cellpadding="0" align="center" border="0">
		<tr>
			<td class="EsqSupIzq"></td>
			<td class="RepSup"></td>
			<td class="EsqSupDer"></td>
		</tr>
		<tr>
			<td class="RepLatIzq"> </td>
		    <td>
                            <table>
                                <tr>
                                    <td style="text-align:center">
                                        <table style="width:625px">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblTitulo" runat="server" CssClass="titulopagina" Font-Bold="True"
                                                        Font-Size="16px" Text="III. MODIFICACIONES EN EL INMUEBLE"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblCambios" runat="server" Text="¿Hubo cambios en el inmueble durante el ciclo escolar, como alguno de los siguientes?" CssClass="lblGris" Font-Size="14px" Width="614px"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;<table style="width: 426px">
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblPregunta1" runat="server" CssClass="lblRojo" Text="a) Rehabilitación o reparación de locales (aulas, laboratorios o talleres) y/o anexos."></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:RadioButtonList ID="rblPregunta1" RepeatLayout="Table" RepeatDirection="Horizontal" runat="server" CssClass="lblGrisTit">
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblPregunta2" runat="server" CssClass="lblRojo" Text="B) Ampliación del inmueble, por construcción de locales y/o anexos."></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:RadioButtonList ID="rblPregunta2" RepeatLayout="Table" RepeatDirection="Horizontal" runat="server" CssClass="lblGrisTit">
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblPregunta3" runat="server" CssClass="lblRojo" Text="c) Reducción del inmueble, por desastres o desmonte de locales y/o anexos."></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:RadioButtonList ID="rblPregunta3" RepeatLayout="Table" RepeatDirection="Horizontal" runat="server" CssClass="lblGrisTit">
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblPregunta4" runat="server" CssClass="lblRojo" Text="d) Instalación de servicios (agua, luz, drenaje, etcétera) en la localidad y/o en el inmueble."></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:RadioButtonList ID="rblPregunta4" RepeatLayout="Table" RepeatDirection="Horizontal" runat="server" CssClass="lblGrisTit">
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblPregunta5" runat="server" CssClass="lblRojo" Text="e) Otro tipo de modificación."></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:RadioButtonList ID="rblPregunta5" RepeatLayout="Table" RepeatDirection="Horizontal" runat="server" CssClass="lblGrisTit">
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblEspecifique" runat="server" CssClass="lblRojo" Text="Especifique:"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:TextBox ID="txtEspecifique" runat="server" Columns="100" CssClass="lblNegro" MaxLength="100"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>                
                                    </td>
                                </tr>
                            </table>
                                   
			               
                        <hr />
                        <table align="center">
                            <tr>
                                <td ><span  onclick="openPage('Personal_911_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir página previa" /></a></span></td> 
                                <td ><span  onclick="openPage('Personal_911_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                <td style="width: 330px;">&nbsp;
                    </td>
                                <td ><span  onclick="openPage('Anexo_911_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                <td ><span  onclick="openPage('Anexo_911_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir página siguiente" /></a></span></td>
                            </tr>
                        </table> 
                       <div class="divResultado" id="divResultado"></div> 
                   </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			     <!--</TBODY>-->
	      </table>
       
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
          <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando información por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
   

<script type="text/javascript">
            var CambiarPagina = "";
            var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
            
        function __ReceiveServerData_Variables(rValue){
            if(document.getElementById('ctl00_cphMainMaster_pnlOficializado')!=null)
            {
                rValue="";
            }
                if (rValue != null)
                {
                    document.getElementById("divWait").style.visibility = "hidden";
                    var Cambiar = "No";
                    if (CambiarPagina != "" && rValue != ""){
                        var obj = document.getElementById("divResultado");
                        obj.innerHTML = "<ul>" + rValue + "</li>";
                        if(ir1)
                        {
                           Cambiar = "Si";
                        }
                        else
                        {
                            alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                            Cambiar = "No";
                        }
                    }
                
                    else if(rValue == "" && CambiarPagina != ""){
                        Cambiar = "Si";
                    }
                    if (Cambiar == "Si"){
                        window.location.href=CambiarPagina+".aspx";
                    }
                }
            
       }
            function EnviarDatosServer(){
            try
              {
                 if (document.getElementById("divWait").style.visibility == "visible"){
                     alert("Se ejecuto mientras trabajaba, se omitir"+'\u00e1'+" operaci"+'\u00f3'+"n. Deber"+'\u00e1'+" de volver a realizar su ultimo movimiento.");
                 }
                 else{
                document.getElementById("divWait").style.visibility = "visible";
               //alert(document.getElementById('rblPregunta1_0').checked);
                var a = ValorSeleccionado('rblPregunta1');
                var b = ValorSeleccionado('rblPregunta2');
                var c = ValorSeleccionado('rblPregunta3');
                var d = ValorSeleccionado('rblPregunta4');
                var e = ValorSeleccionado('rblPregunta5');
                var txtEspecifique = "_";
                if (document.getElementById('ctl00_cphMainMaster_txtEspecifique').value!="")
                      txtEspecifique = document.getElementById('ctl00_cphMainMaster_txtEspecifique').value;
                    
                var Salida = "a=" + a + "|b=" + b + "|c=" + c + "|d=" + d + "|e=" + e +  "|especifique=" + txtEspecifique + "|ID_Control=" + document.getElementById("ctl00_cphMainMaster_hidIdCtrl").value;
                
                CallServer_Variables(Salida,"");
                }
              }
            catch(err)
              {
                  alert("fallas "  + err);
              }
            }
   
           
           function ValorSeleccionado(Opt){
               var salida = -1;
               if (document.getElementById('ctl00_cphMainMaster_' + Opt + '_0').checked == true)
                     salida = 1;
               if (document.getElementById('ctl00_cphMainMaster_' + Opt + '_1').checked == true)
                     salida = 0;
                return salida;         
           }
          function ActivarCaja(){
          
               // truo para que funcione bien
               //WebForm_InitCallback();
               //Sys.Application.initialize();
               
               var obj = document.getElementById('ctl00_cphMainMaster_rblPregunta5_0');
               //document.getElementById('rblPregunta5_0').checked, document.getElementById('rblPregunta5_0').disabled
               if (obj.disabled){
                  document.getElementById('ctl00_cphMainMaster_txtEspecifique').disabled = true;
               }else{
                   if (obj.checked) document.getElementById('ctl00_cphMainMaster_txtEspecifique').disabled = false;
                   else  {
                      document.getElementById('ctl00_cphMainMaster_txtEspecifique').disabled = true;
                   }
               }
           }
   
            function foco()
            {
              try{
                 document.getElementById("ctl00_cphMainMaster_rblPregunta1").focus();
              }
              catch(err){
                  err;
              }
            }
            //Arrancador de Iniciales
            //foco();
            
            ActivarCaja();
            Disparador(<%=hidDisparador.Value %>);

  </script>

 </asp:Content> 