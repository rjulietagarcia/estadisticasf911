<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8P(PROCEIES)" AutoEventWireup="true" CodeBehind="PROCEIES_911_8P.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8P.PROCEIES_911_8P" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 14;
        MaxRow = 4;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>PROFESIONAL T�CNICO MEDIO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8P',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('AG1_911_8P',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_8P',true)"><a href="#" title=""><span>4�, 5� y TOTAL</span></a></li>
        <li onclick="openPage('PROCEIES_911_8P',true)"><a href="#" title="" class="activo"><span>PROCEIES</span></a></li>
        <li onclick="openPage('Personal_911_8P',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

                <center>
          
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table  style="text-align: center" >
                        <tr>
                            <td colspan="15" rowspan="1" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Size="16px" Height="17px"
                                    Text="III. ALUMNOS APROBADOS EN PROCEIES (SOL� CONALEP)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="15" rowspan="1" style="height: 3px; text-align: left">
                                <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Height="17px" Text="1. Escriba el total de alumnos, que aprobaron todas las asignaturas optativas del Programa de Complementaci�n de Estudios para el Ingreso a la Educaci�n Superior (PROCEIES), desglos�ndolo por sexo y edad."></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="height: 3px; text-align: center; width: 61px;">
                            </td>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 3px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 54px; height: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 61px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lbl6_Menos" runat="server" CssClass="lblRojo" Text="14 a�os y menos"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="15 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="16 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="17 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="18 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="19 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="20 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="21 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="22 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="23 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lbl15_Mas" runat="server" CssClass="lblRojo" Text="24 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="25 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="PROCEIES"></asp:Label></td>
                            <td rowspan="1" style="width: 61px; height: 26px; text-align: left">
                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV505" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV506" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV507" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV508" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV509" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV510" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV511" runat="server" Columns="4" TabIndex="10107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV512" runat="server" Columns="4" TabIndex="10108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV513" runat="server" Columns="4" TabIndex="10109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV514" runat="server" Columns="4" TabIndex="10110" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV515" runat="server" Columns="4" TabIndex="10111" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV516" runat="server" Columns="4" TabIndex="10112" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV517" runat="server" Columns="4" TabIndex="10113" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 61px; height: 3px; text-align: center">
                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV518" runat="server" Columns="4" TabIndex="10201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV519" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV520" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV521" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV522" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV523" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV524" runat="server" Columns="4" TabIndex="10207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV525" runat="server" Columns="4" TabIndex="10208" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV526" runat="server" Columns="4" TabIndex="10209" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV527" runat="server" Columns="4" TabIndex="10210" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV528" runat="server" Columns="4" TabIndex="10211" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV529" runat="server" Columns="4" TabIndex="10212" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV530" runat="server" Columns="4" TabIndex="10213" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 61px; height: 26px; text-align: center">
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV531" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV532" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV533" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV534" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV535" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV536" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV537" runat="server" Columns="4" TabIndex="10307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV538" runat="server" Columns="4" TabIndex="10308" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV539" runat="server" Columns="4" TabIndex="10309" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV540" runat="server" Columns="4" TabIndex="10310" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV541" runat="server" Columns="4" TabIndex="10311" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV542" runat="server" Columns="4" TabIndex="10312" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV543" runat="server" Columns="4" TabIndex="10313" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                    </table>
                
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG4_911_8P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG4_911_8P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_8P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_8P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
     <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>
