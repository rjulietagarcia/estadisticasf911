using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;
using System.Text;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
using EstadisticasEducativas._911;
namespace EstadisticasEducativasInicio._911.inicio.CAM_1
{
    public partial class Personal1_CAM_1 : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                //lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);

                if (controlDP.Estatus == 0)
                {
                    pnlOficializado.Visible = false;
                    Cargar_Niveles();
                    Cargar_Especialidades();
                }
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }

        //*********************************
        void Cargar_Niveles()
        {
            ddl_Nivel.Items.Add(new ListItem("", ""));
            ddl_Nivel.Items.Add(new ListItem("PRIMARIA", "1"));
            ddl_Nivel.Items.Add(new ListItem("SECUNDARIA", "2"));
            ddl_Nivel.Items.Add(new ListItem("PROFESIONAL MEDIO", "3"));
            ddl_Nivel.Items.Add(new ListItem("BACHILLERATO", "4"));
            ddl_Nivel.Items.Add(new ListItem("NORMAL DE PREESCOLAR", "5"));
            ddl_Nivel.Items.Add(new ListItem("NORMAL DE PRIMARIA", "6"));
            ddl_Nivel.Items.Add(new ListItem("NORMAL SUPERIOR", "7"));
            ddl_Nivel.Items.Add(new ListItem("LICENCIATURA", "8"));
            ddl_Nivel.Items.Add(new ListItem("MAESTRIA", "9"));
            ddl_Nivel.Items.Add(new ListItem("DOCTORADO", "10"));

            ListItem item = ddl_Nivel.Items.FindByValue((txtV793.Text == "_") ? "" : txtV793.Text);
            if (item != null)
                item.Selected = true;



        }


        void Cargar_Especialidades()
        {
            ddl_Especialidad.Items.Add(new ListItem("", ""));
            ddl_Especialidad.Items.Add(new ListItem("PSICOLOGO", "1"));
            ddl_Especialidad.Items.Add(new ListItem("PEDAGOGO", "2"));
            ddl_Especialidad.Items.Add(new ListItem("DEFICIENCIA MENTAL", "3"));
            ddl_Especialidad.Items.Add(new ListItem("AUDICION Y LENGUAJE", "4"));
            ddl_Especialidad.Items.Add(new ListItem("PROBLEMAS DE APRENDIZAJE", "5"));
            ddl_Especialidad.Items.Add(new ListItem("INADAPTADO SOCIAL", "6"));
            ddl_Especialidad.Items.Add(new ListItem("MAESTRO DE PRIMARIA", "7"));
            ddl_Especialidad.Items.Add(new ListItem("APARATO LOCOMOTOR", "8"));
            ddl_Especialidad.Items.Add(new ListItem("DEFICIENCIA VISUAL", "9"));
            ddl_Especialidad.Items.Add(new ListItem("COMUNICACION HUMANA", "10"));
            ddl_Especialidad.Items.Add(new ListItem("NORMAL SUPERIOR", "11"));
            ddl_Especialidad.Items.Add(new ListItem("SEXOLOGIA", "12"));
            ddl_Especialidad.Items.Add(new ListItem("ADMINISTRACION EDUCATIVA", "13"));
            ddl_Especialidad.Items.Add(new ListItem("ODONTOLOGIA", "14"));
            ddl_Especialidad.Items.Add(new ListItem("AUTISMO", "15"));
            ddl_Especialidad.Items.Add(new ListItem("EDUCACION ESPECIAL", "16"));
            ddl_Especialidad.Items.Add(new ListItem("INNOVACIONES EDUCATIVAS", "17"));
            ddl_Especialidad.Items.Add(new ListItem("INTERVENCION TEMPRANA", "18"));
            ddl_Especialidad.Items.Add(new ListItem("MATEMATICAS", "19"));
            ddl_Especialidad.Items.Add(new ListItem("SOCIOLOGIA", "20"));
            ddl_Especialidad.Items.Add(new ListItem("INGENIERO MECANICO", "21"));
            ddl_Especialidad.Items.Add(new ListItem("ENSE�ANZA SUPERIOR", "22"));
            ddl_Especialidad.Items.Add(new ListItem("EDUCACION", "23"));

            ListItem item = ddl_Especialidad.Items.FindByValue((txtV795.Text == "_") ? "" : txtV795.Text);
            if (item != null)
                item.Selected = true;
        }


    }
}
