<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Capacitaci�n Laboral y Atenci�n Complementaria" AutoEventWireup="true" CodeBehind="AG3_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.AG3_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li><li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li><li onclick="openPage('AG3_CAM_1',true)"><a href="#" title="" class="activo"><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li><li onclick="openPage('Desgolse_CAM_1',false)"><a href="#" title="" ><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL(CONT)</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAG Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
        <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 1200px">
            <tr>
                <td valign="top" style="width: 568px">
                    <asp:Label ID="lblCAPACITACION" runat="server" CssClass="lblRojo" Font-Bold="True" Text="CAPACITACI�N PARA EL TRABAJO"
                        Width="100%"></asp:Label></td>
                <td valign="top">
                    <asp:Label ID="lblATENCION" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ATENCI�N COMPLEMENTARIA"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td valign="top" style="width: 568px">
                    <table>
                        <tr>
                            <td colspan="7" rowspan="1" style="text-align: justify">
                                <asp:Label ID="lblInstruccion5" runat="server" CssClass="lblRojo" Text="5. Escriba el n�mero de alumnos por sexo y el n�mero de grupos de formaci�n para el trabajo, de acuerdo con el taller al que asisten, seg�n los rubros que se indican. Si se imparten otros talleres, escr�balos en las lineas seg�n la tabla del reverso de la p�gina anterior."
                                    Width="500px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="7" nowrap="nowrap" rowspan="1" style="height: 20px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="text-align: center">
                                <asp:Label ID="lblTaller" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TALLER"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblNuevoIngreso5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="NUEVO INGRESO"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblReingreso5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="REINGRESO"
                                    Width="100%"></asp:Label></td>
                            <td rowspan="2" style="text-align: center">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                            <td rowspan="2" style="text-align: center">
                                <asp:Label ID="lblGrupos5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblCocina" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="COCINA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11001" ID="txtV205" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11002" ID="txtV206" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11003" ID="txtV207" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11004" ID="txtV208" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11005" ID="txtV209" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11006" ID="txtV210" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblCorte" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CORTE Y CONFECCI�N"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11101" ID="txtV211" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11102" ID="txtV212" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11103" ID="txtV213" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11104" ID="txtV214" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11105" ID="txtV215" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11106" ID="txtV216" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblArtesanias" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ARTESAN�AS"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11201" ID="txtV217" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11202" ID="txtV218" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11203" ID="txtV219" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11204" ID="txtV220" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11205" ID="txtV221" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11206" ID="txtV222" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblElectricidad" runat="server" CssClass="lblGrisTit" Text="ELECTRICIDAD"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11301" ID="txtV223" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11302" ID="txtV224" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11303" ID="txtV225" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11304" ID="txtV226" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11305" ID="txtV227" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11306" ID="txtV228" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblTejido" runat="server" CssClass="lblGrisTit" Text="TEJIDO" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11401" ID="txtV229" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11402" ID="txtV230" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11403" ID="txtV231" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11404" ID="txtV232" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11405" ID="txtV233" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11406" ID="txtV234" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblCarpinteria" runat="server" CssClass="lblGrisTit" Text="CARPINTER�A" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11501" ID="txtV235" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11502" ID="txtV236" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11503" ID="txtV237" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11504" ID="txtV238" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11505" ID="txtV239" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11506" ID="txtV240" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblHerreria" runat="server" CssClass="lblGrisTit" Text="HERRER�A" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11601" ID="txtV241" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11602" ID="txtV242" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11603" ID="txtV243" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11604" ID="txtV244" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11605" ID="txtV245" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11606" ID="txtV246" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblBelleza" runat="server" CssClass="lblGrisTit" Text="BELLEZA" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11701" ID="txtV247" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11702" ID="txtV248" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11703" ID="txtV249" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11704" ID="txtV250" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11705" ID="txtV251" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11706" ID="txtV252" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1194" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10101"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt253" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV253','ctl00_cphMainMaster_txtV1194')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV253" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11801" ID="txtV254" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11802" ID="txtV255" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11803" ID="txtV256" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11804" ID="txtV257" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11805" ID="txtV258" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11806" ID="txtV259" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1195" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10201"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt260" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV260','ctl00_cphMainMaster_txtV1195')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV260" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11901" ID="txtV261" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11902" ID="txtV262" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11903" ID="txtV263" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11904" ID="txtV264" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11905" ID="txtV265" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="11906" ID="txtV266" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1196" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10301"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt267" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV267','ctl00_cphMainMaster_txtV1196')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV267" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12001" ID="txtV268" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12002" ID="txtV269" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12003" ID="txtV270" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12004" ID="txtV271" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12005" ID="txtV272" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12006" ID="txtV273" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1197" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10401"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt274" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV274','ctl00_cphMainMaster_txtV1197')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV274" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12101" ID="txtV275" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12102" ID="txtV276" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12103" ID="txtV277" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12104" ID="txtV278" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12105" ID="txtV279" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12106" ID="txtV280" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1198" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10501"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt281" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV281','ctl00_cphMainMaster_txtV1198')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV281" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12201" ID="txtV282" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12202" ID="txtV283" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12203" ID="txtV284" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12204" ID="txtV285" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12205" ID="txtV286" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12206" ID="txtV287" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1199" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10601"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt288" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV288','ctl00_cphMainMaster_txtV1199')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV288" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12301" ID="txtV289" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12302" ID="txtV290" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12303" ID="txtV291" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12304" ID="txtV292" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12305" ID="txtV293" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12306" ID="txtV294" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1200" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10701"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt295" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV295','ctl00_cphMainMaster_txtV1200')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV295" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12401" ID="txtV296" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12402" ID="txtV297" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12403" ID="txtV298" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12404" ID="txtV299" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12405" ID="txtV300" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12406" ID="txtV301" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1201" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10801"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt302" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV302','ctl00_cphMainMaster_txtV1201')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV302" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12501" ID="txtV303" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12502" ID="txtV304" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12503" ID="txtV305" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12504" ID="txtV306" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12505" ID="txtV307" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12506" ID="txtV308" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV1202" runat="server" Columns="2" CssClass="lblNegro" Enabled="False"
                                    MaxLength="2" TabIndex="10901"></asp:TextBox>
                                
                                <asp:DropDownList ID="opt309" onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV309','ctl00_cphMainMaster_txtV1202')" runat="server" CssClass="lblNegro" Width="220px">
                                </asp:DropDownList>
                                <asp:TextBox  ID="txtV309" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12601" ID="txtV310" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12602" ID="txtV311" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12603" ID="txtV312" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12604" ID="txtV313" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12605" ID="txtV314" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12606" ID="txtV315" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblTotal52" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12701" ID="txtV316" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12702" ID="txtV317" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12703" ID="txtV318" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12704" ID="txtV319" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12705" ID="txtV320" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12706" ID="txtV321" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td colspan="7" rowspan="1" style="text-align: justify">
                                <asp:Label ID="lblInstruccion6" runat="server" CssClass="lblRojo" Font-Bold="True" 
                                Text="6. Registre por nivel o servicio educativo el n�mero total 
                                de alumnos que reciben apoyo complementario, por grado y sexo, 
                                seg�n los rubros que se indican."
                                    Width="500px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="7" nowrap="nowrap" rowspan="1" style="height: 20px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="text-align: center">
                            </td>
                            <td rowspan="2" style="text-align: center">
                            </td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblInician" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INICIAN ATENCI�N"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblContinuan" runat="server" CssClass="lblNegro" Font-Bold="True" Text="CONTIN�AN CON ATENCI�N"
                                    Width="100%"></asp:Label></td>
                            <td rowspan="2" style="text-align: center">
                                <asp:Label ID="lblTotal61" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInicial" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INICIAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20101" ID="txtV349" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20102" ID="txtV350" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20103" ID="txtV351" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20104" ID="txtV352" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20105" ID="txtV353" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="text-align: left">
                                <asp:Label ID="lblPreescolar" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PREESCOLAR"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lbl11o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20201" ID="txtV354" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20202" ID="txtV355" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20203" ID="txtV356" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20204" ID="txtV357" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20205" ID="txtV358" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl12o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20301" ID="txtV359" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20302" ID="txtV360" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20303" ID="txtV361" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20304" ID="txtV362" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20305" ID="txtV363" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl13o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20401" ID="txtV364" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20402" ID="txtV365" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20403" ID="txtV366" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20404" ID="txtV367" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20405" ID="txtV368" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="6" style="text-align: left">
                                <asp:Label ID="lblPrimaria" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PRIMARIA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lbl21o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20501" ID="txtV369" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20502" ID="txtV370" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20503" ID="txtV371" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20504" ID="txtV372" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20505" ID="txtV373" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl22o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20601" ID="txtV374" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20602" ID="txtV375" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20603" ID="txtV376" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20604" ID="txtV377" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20605" ID="txtV378" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl23o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20701" ID="txtV379" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20702" ID="txtV380" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20703" ID="txtV381" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20704" ID="txtV382" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20705" ID="txtV383" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl24o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20801" ID="txtV384" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20802" ID="txtV385" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20803" ID="txtV386" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20804" ID="txtV387" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20805" ID="txtV388" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl25o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20901" ID="txtV389" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20902" ID="txtV390" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20903" ID="txtV391" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20904" ID="txtV392" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="20905" ID="txtV393" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl26o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="6o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21001" ID="txtV394" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21002" ID="txtV395" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21003" ID="txtV396" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21004" ID="txtV397" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21005" ID="txtV398" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="text-align: left">
                                <asp:Label ID="lblSecundaria" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SECUNDARIA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lbl31o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21101" ID="txtV399" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21102" ID="txtV400" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21103" ID="txtV401" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21104" ID="txtV402" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21105" ID="txtV403" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl32o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21201" ID="txtV404" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21202" ID="txtV405" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21203" ID="txtV406" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21204" ID="txtV407" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21205" ID="txtV408" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lbl33o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21301" ID="txtV409" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21302" ID="txtV410" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21303" ID="txtV411" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21304" ID="txtV412" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21305" ID="txtV413" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblCapacitacion2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="CAPACITACI�N LABORAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21401" ID="txtV414" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21402" ID="txtV415" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21403" ID="txtV416" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21404" ID="txtV417" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21405" ID="txtV418" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblTotal62" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21501" ID="txtV419" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21502" ID="txtV420" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21503" ID="txtV421" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21504" ID="txtV422" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21505" ID="txtV423" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 568px">
                    <table>
                        <tr>
                            <td colspan="4" style="text-align:justify">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba la cantidad de alumnos con 
                discapacidad, aptitudes sobresalientes u otras condiciones, 
                seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSituacion5" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="75px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="75px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotal53" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="75px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblCeguera5" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12801" ID="txtV322" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12802" ID="txtV323" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12803" ID="txtV324" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblVisual5" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12901" ID="txtV325" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12902" ID="txtV326" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="12903" ID="txtV327" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblSordera5" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13001" ID="txtV328" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13002" ID="txtV329" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13003" ID="txtV330" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblAuditiva5" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13101" ID="txtV331" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13102" ID="txtV332" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13103" ID="txtV333" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblMotriz5" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13201" ID="txtV334" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13202" ID="txtV335" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13203" ID="txtV336" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblIntelectual5" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13301" ID="txtV337" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13302" ID="txtV338" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13303" ID="txtV339" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblSobresalientes5" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13401" ID="txtV340" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13402" ID="txtV341" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13403" ID="txtV342" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblOtros5" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13501" ID="txtV343" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13502" ID="txtV344" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13503" ID="txtV345" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTotal54" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13601" ID="txtV346" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13602" ID="txtV347" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="13603" ID="txtV348" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td colspan="4" style="text-align:justify">
                                <asp:Label ID="lblInstruccion2b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba la cantidad de alumnos con 
                discapacidad, aptitudes sobresalientes u otras condiciones, 
                seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" nowrap="nowrap" style="height: 20px" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSituacion6" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="75px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="75px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotal63" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="75px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblCeguera6" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21601" ID="txtV424" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21602" ID="txtV425" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21603" ID="txtV426" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblVisual6" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21701" ID="txtV427" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21702" ID="txtV428" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21703" ID="txtV429" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblSordera6" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21801" ID="txtV430" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21802" ID="txtV431" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21803" ID="txtV432" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblAuditiva6" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21901" ID="txtV433" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21902" ID="txtV434" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="21903" ID="txtV435" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblMotriz6" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22001" ID="txtV436" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22002" ID="txtV437" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22003" ID="txtV438" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblIntelectual6" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22101" ID="txtV439" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22102" ID="txtV440" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22103" ID="txtV441" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblSobresalientes6" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22201" ID="txtV442" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22202" ID="txtV443" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22203" ID="txtV444" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblOtros6" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22301" ID="txtV445" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22302" ID="txtV446" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22303" ID="txtV447" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTotal64" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22401" ID="txtV448" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22402" ID="txtV449" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox TabIndex="22403" ID="txtV450" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Desgolse_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Desgolse_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 20;
                MaxRow = 50;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                
                
                
 		        Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
