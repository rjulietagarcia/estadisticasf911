<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="TotalAlumn_911_8N.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8N.TotalAlumn_911_8N" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <script type="text/javascript">
        var _enter=true;
    </script>

    <%--Agregado--%>
            <script type="text/javascript">
                MaxCol = 14;
                MaxRow = 46;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
            </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">
    <table>
        <tr><td style="width:120px;"></td><td><span>EDUCACI�N NORMAL</span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCiclo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCentroTrabajo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8N',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" ><span>CARACTER�STICAS Y PERSONAL</span></a></li>
        <li onclick="openPage('ACG_911_8N',true)"><a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Total_911_8N',true)"><a href="#" title="" ><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="openPage('TotalAlumn_911_8N',true)"><a href="#" title="" class="activo"><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="openPage('PlantelesEx_911_8N',false)"><a href="#" title="" ><span>PLANTELES DE EXTENSI�N</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td colspan="16">
                    <asp:Label ID="lblTitulo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="ESTAD�STICA DE ALUMNOS POR GRADO, SEXO, INSCRIPCI�N TOTAL, EXISTENCIA, APROBADOS Y EDAD" Width="100%"></asp:Label></td>
                <td colspan="1">
                </td>
            </tr>
            <tr>
                <td colspan="16" style="text-align:left;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True" 
                    Text="1. En esta p�gina y en la siguiente, escriba el total de alumnos de las licenciaturas o programas de posgrado, desglos�ndolo por grado, sexo, inscripci�n total, existencia, aprobados y edad. Verifique que la suma de los subtotales de los alumnos por edad sea igual al total."
                        Width="100%"></asp:Label>
                    <asp:Label ID="Label53" runat="server" CssClass="lblGrisTit" Font-Bold="True" 
                    Text="Nota: Incluya a los alumnos que se atendieron en los planteles e extensi�n o m�dulos "
                        Width="100%"></asp:Label>
                 </td>
                <td colspan="1">
                </td>
            </tr>
            
            <tr>
                <td >
                </td>
                <td >
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl16a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl17a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl18a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl19a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl20a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl21a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lbl25a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y m�s"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="90px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV81" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV82" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV83" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV84" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV85" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV86" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV87" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV88" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV89" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV90" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblExistencia1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV91" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV92" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV93" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV94" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV95" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV96" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV97" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV98" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV99" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV100" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobados1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV101" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV102" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV103" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV104" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV105" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV106" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV107" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV108" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV109" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV110" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblInscripcion1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV111" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV112" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV113" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV114" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV115" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV116" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV117" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV118" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV119" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV120" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblExistencia1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV121" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV122" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV123" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV124" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV125" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV126" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV127" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV128" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV129" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV130" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobados1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV131" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV132" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV133" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV134" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV135" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV136" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV137" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV138" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV139" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV140" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV141" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV142" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV143" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV144" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV145" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV146" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV147" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV148" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV149" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV150" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV151" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV152" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV153" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV154" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV155" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV156" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV157" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV158" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV159" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV160" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10810"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label23" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV161" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV162" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV163" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV164" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV165" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV166" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV167" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV168" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV169" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV170" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10910"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
            <td>&nbsp;</td>
            </tr>
            <%--2o--%>
            <tr>
                <td rowspan="9" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="90px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV171" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV172" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV173" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV174" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV175" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV176" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV177" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV178" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV179" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV180" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11010"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblExistencia2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
          
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV181" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV182" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV183" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV184" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV185" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV186" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV187" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV188" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV189" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV190" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11110"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr> 
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobados2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
             
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV191" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV192" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV193" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV194" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV195" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV196" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV197" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV198" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV199" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV200" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11210"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblInscripcion2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV201" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV202" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV203" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV204" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV205" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV206" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV207" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV208" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV209" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV210" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11310"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblExistencia2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
            
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV211" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV212" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV213" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV214" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV215" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV216" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV217" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV218" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV219" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV220" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11410"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobados2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
               
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV221" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV222" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV223" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV224" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV225" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV226" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV227" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV228" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV229" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV230" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV231" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV232" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV233" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV234" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV235" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV236" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV237" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV238" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV239" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV240" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11610"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV241" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV242" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV243" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV244" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV245" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV246" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV247" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV248" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV249" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV250" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11710"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV251" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV252" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV253" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV254" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV255" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV256" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV257" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV258" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV259" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV260" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11810"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
            <td>&nbsp;</td>
            </tr>  
            <%--3o--%> 
            <tr>
                <td rowspan="9" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="90px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV261" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV262" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV263" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV264" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV265" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV266" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV267" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV268" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV269" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV270" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11910"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
          
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV271" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV272" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV273" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV274" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV275" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV276" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV277" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV278" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV279" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV280" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12010"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr> 
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
             
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV281" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV282" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV283" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV284" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV285" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV286" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV287" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV288" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV289" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV290" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12110"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV291" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV292" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV293" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV294" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV295" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV296" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV297" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV298" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV299" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12209"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV300" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12210"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
            
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV301" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV302" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV303" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV304" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV305" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV306" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV307" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV308" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV309" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12309"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV310" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12310"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobados3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
               
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV311" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV312" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV313" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV314" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV315" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV316" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV317" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV318" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV319" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12409"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV320" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12410"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label102" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label202" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV321" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV322" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV323" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV324" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV325" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV326" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV327" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV328" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV329" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV330" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12510"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label302" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV331" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV332" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV333" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV334" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV335" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV336" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV337" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV338" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV339" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV340" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12610"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label402" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV341" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV342" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV343" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV344" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV345" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV346" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV347" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV348" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV349" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV350" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12710"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
            <td>&nbsp;</td>
            </tr>
            <%--4o--%>   
            <tr>
                <td rowspan="9" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="90px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV351" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV352" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV353" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV354" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV355" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV356" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV357" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV358" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV359" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV360" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12810"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
          
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV361" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV362" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV363" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV364" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV365" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV366" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV367" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV368" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV369" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12909"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV370" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12910"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr> 
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
             
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV371" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV372" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV373" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV374" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV375" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV376" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV377" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV378" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV379" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV380" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13010"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV381" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV382" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV383" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV384" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV385" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV386" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV387" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV388" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV389" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13109"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV390" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13110"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
            
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV391" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV392" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV393" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV394" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV395" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV396" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV397" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV398" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV399" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13209"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV400" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13210"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobados4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
               
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV401" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV402" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV403" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV404" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV405" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV406" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV407" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV408" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV409" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13309"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV410" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13310"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label101" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label201" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV411" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV412" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV413" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV414" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV415" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV416" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV417" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV418" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV419" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV420" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13410"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label301" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV421" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV422" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV423" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV424" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV425" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV426" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV427" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV428" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV429" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV430" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13510"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label400" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV431" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV432" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV433" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV434" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV435" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV436" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV437" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV438" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV439" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV440" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13610"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
            <td>&nbsp;</td>
            </tr>
            <%--Total--%>  
            <tr>
                <td rowspan="9" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="Label26" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV441" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV442" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV443" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV444" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV445" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV446" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV447" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV448" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV449" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV450" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13710"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
          
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV451" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV452" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV453" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV454" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV455" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV456" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV457" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV458" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV459" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13809"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV460" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13810"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr> 
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
             
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV461" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV462" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV463" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV464" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV465" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV466" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV467" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV468" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV469" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV470" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="13910"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label30" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV471" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV472" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV473" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV474" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV475" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV476" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV477" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV478" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV479" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14009"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV480" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14010"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
            
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV481" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV482" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV483" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV484" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV485" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV486" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV487" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV488" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV489" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14109"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV490" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14110"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblAprobadosToM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
               
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV491" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV492" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV493" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV494" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV495" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV496" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV497" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV498" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV499" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14209"></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV500" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14210"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label100" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="Label200" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV501" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV502" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV503" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV504" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV505" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV506" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV507" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV508" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV509" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV510" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14310"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Label300" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV511" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV512" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV513" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV514" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV515" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV516" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV517" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV518" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV519" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV520" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14410"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label403" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV521" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV522" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV523" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV524" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV525" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV526" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV527" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV528" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV529" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV530" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="14510"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
            <td>&nbsp;</td>
            </tr>
          </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Total_911_8N',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Total_911_8N',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('PlantelesEx_911_8N',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('PlantelesEx_911_8N',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span> 
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
        <%--Agregado--%>
           <script type="text/javascript">
                GetTabIndexes();
            </script>
    <%--Agregado--%>
</asp:Content>
