<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.5(Gastos)" AutoEventWireup="true" CodeBehind="Gasto_911_5.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_5.Gasto_911_5" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
   
   <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_5',true)"><a href="#" title=""><span>1�</span></a></li><li onclick="openPage('AG2_911_5',true)"><a href="#" title=""><span>2�</span></a></li><li onclick="openPage('AG3_911_5',true)"><a href="#" title=""  ><span>3�</span></a></li><li onclick="openPage('AGT_911_5',true)"><a href="#" title="" ><span>TOTAL</span></a></li><li onclick="openPage('AGD_911_5',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li><li onclick="openPage('Personal_911_5',true)"><a href="#" title="" ><span>PERSONAL</span></a></li><li onclick="openPage('CMYA_911_5',true)"><a href="#" title="" ><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="openPage('Gasto_911_5',true)"><a href="#" title="" class="activo" ><span>GASTO</span></a></li><li onclick="openPage('Anexo_911_5',false)"><a href="#" title="" ><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    
    <center>
           

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 800px">
            <tr>
                <td style="width: 701px;text-align:justify">
                    <asp:Label ID="lblGasto" runat="server" Font-Size="16px" CssClass="lblGrisTit" Font-Bold="True" Text="V. GASTO DE LAS FAMILIAS EN EDUCACI�N"
                        Width="800px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 701px; text-align: justify">
                    <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="a) La informaci�n de esta secci�n ser� utilizada exclusivamente para obtener indicadores de gasto educativo."
                        Width="800px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 701px; text-align: justify">
                    <asp:Label ID="lblInstrucciones2" runat="server" CssClass="lblRojo" Text="b) El punto n�mero 1 deber� ser constestado por las escuelas de todos sostenimientos. El punto n�mero 2 �nicamente por las escuelas con sostenimiento particular."
                        Width="800px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 701px;text-align: justify">
                    <asp:Label ID="lblInstrucciones3" runat="server" CssClass="lblRojo" Text="c) Presente las cifras en pesos; no utilice decimales."
                        Width="800px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 701px; text-align: justify">
                    <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="d) Para contestar, considere las definiciones siguientes. Si no cuenta con cantidades exactas, d� una aproximaci�n de las mismas"
                        Width="800px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 701px">
                </td>
            </tr>
            <tr>
                <td style="width: 701px; text-align: justify">
                    <asp:Label ID="lblInstrucciones5" runat="server" CssClass="lblGrisTit" Text="GASTO PROMEDIO ANUAL. Es el monto promedio de dinero que gasta cada alumno (o los padres del alumno) en un determinado concepto, durante el ciclo escolar. Se aplica a los siguientes conceptos: inscripci�n, paquetes de �tiles y libros (cuando �stos se soliciten) y uniformes. Asimismo, se aplican a cuotas que requieran un desembolso para las familias; por ejemplo, las aportaciones a la asociaci�n de padres de familia o alguna ayuda para el arreglo de la escuela o para equipar laboratorios y talleres, etc�tera."
                        Width="800px" Height="68px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 701px">
                </td>
            </tr>
            <tr>
                <td style="width: 701px; text-align: justify; height: 100%;">
                    <asp:Label ID="lblInstrucciones6" runat="server" CssClass="lblGrisTit" Text="GASTO PROMEDIO MENSUAL. Es el monto promedio de dinero que gasta cada alumno (o los padres del alumno) en septiembre por conceptos de colegiatura y(o) transporte escolar en escuelas particulares. Es el resultado de dividir el total de los ingresos de la escuela en septiembre entre el total de alumnos. Por ejemplo: si el ingreso de la escuela por colegiaturas pagadas por las familias durante septiembre fue de $15,000 y el n�mero de alumnos es de 100, el gasto promedio mensual en colegiaturas es de $150, cantidad que se reportar� en el rubro correspondiente."
                        Width="800px"></asp:Label></td>
            </tr>
        </table>
        <br />
    <table style="width: 800px">
        <tr>
            <td colspan="3" style="text-align: justify">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. ESCUELAS DE TODOS LOS SOSTENIMIENTOS"
                        Width="800px"></asp:Label><br />
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="En el caso de escuelas particulares, considere los gastos y compras que los alumnos y(o) padres de familia hacen directamente en la instituci�n, as� como fuera de ella."
                        Width="800px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
            </td>
            <td style="width: 86px">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GASTO PROMEDIO ANUAL EN EL PAQUETE DE �TILES Y LIBROS QUE SE SUGIERE ADQUIERA EL ALUMNO"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                <asp:TextBox ID="txtV728" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                    TabIndex="10101"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GASTO PROMEDIO ANUAL EN UNIFORMES QUE SE SUGIERE ADQUIERA EL ALUMNO"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV729" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10201"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GASTO PROMEDIO ANUAL EN CUOTAS"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV730" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10301"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td style="width: 86px">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: justify">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. ESCUELAS PARTICULARES"
                        Width="600px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
            </td>
            <td style="width: 86px">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GASTO PROMEDIO ANUAL EN INSCRIPCI�N"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV731" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10401"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GASTO PROMEDIO MENSUAL EN COLEGIATURA"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV732" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10501"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="N�MERO DE MENSUALIDADES QUE SE PAGAN"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV733" runat="server" Columns="5" CssClass="lblNegro" MaxLength="2"
                        TabIndex="10601"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:justify">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="�LA ESCUELA OFRECE SERVICIO DE TRANSPORTE ESCOLAR, PROPIO O CONCESIONADO?"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right; width: 300px;" colspan="1">
                <asp:TextBox ID="txtV734" style="visibility:hidden;" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                    ></asp:TextBox><asp:TextBox ID="txtV735" style="visibility:hidden;" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                    ></asp:TextBox>
                <asp:RadioButton ID="optV734" onclick = "OPTs2Txt();" runat="server" GroupName="TRANSPORTE" Text="SI"  />
                
                <asp:RadioButton ID="optV735" onclick = "OPTs2Txt();" runat="server" GroupName="TRANSPORTE" Text="NO"  />
                
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: justify">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Si la respuesta es S�, conteste lo siguiente:"
                        Width="600px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 21px; text-align: left">
            </td>
            <td style="width: 173px; text-align: left">
                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GASTO PROMEDIO MENSUAL DEL SERVICIO DE TRANSPORTE"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV736" runat="server" Columns="5" CssClass="lblNegro" MaxLength="4"
                        TabIndex="20101"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 21px; text-align: left">
            </td>
            <td style="width: 173px; text-align: left">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="N�MERO DE MENSUALIDADES QUE SE PAGAN"
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV737" runat="server" Columns="5" CssClass="lblNegro" MaxLength="2"
                        TabIndex="20201"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 21px; text-align: left">
            </td>
            <td style="width: 173px; text-align: left">
                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="N�MERO DE ALUMNOS QUE UTILIZAN EL SERVICIO."
                        Width="600px"></asp:Label></td>
            <td style="text-align: right" colspan="1">
                    <asp:TextBox ID="txtV738" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20301"></asp:TextBox></td>
        </tr>
    </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('CMYA_911_5',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('CMYA_911_5',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_911_5',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_911_5',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                  function OPTs2Txt(){
                     marcarTXT('734');
                     marcarTXT('735');
                } 
                function PintaOPTs(){
                     marcar('734');
                     marcar('735');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                
               PintaOPTs();
               Disparador(<%=this.hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
