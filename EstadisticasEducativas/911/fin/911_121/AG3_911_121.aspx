<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG3_911_121.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_121.AG3_911_121" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <script type="text/javascript">
        var _enter=true;
    </script>
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 6;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    
    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR IND�GENA</span></td>
        </tr>
        <tr><td>
            <asp:Label ID="Label1" runat="server" Text="FIN DE CURSOS"></asp:Label>
            <span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
        <ul class="left">
            <li onclick="openPage('Identificacion_911_121',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('AG1_911_121',true)"><a href="#" title=""><span>1�</span></a></li>
            <li onclick="openPage('AG2_911_121',true)"><a href="#" title=""><span>2�</span></a></li>
            <li onclick="openPage('AG3_911_121',true)"><a href="#" title="" class="activo"><span>3�</span></a></li>
            <li onclick="openPage('AGT_911_121',false)"><a href="#" title=""><span>TOTAL</span></a></li>
            <li onclick="denyPage('Personal_911_121')"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
            <li onclick="denyPage('Inmueble_911_121')"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
            <li onclick="denyPage('Anexo_911_121')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage('Oficializar_911_121')" ><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
        </ul>
    </div>
      <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
          

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="3�" CssClass="lblGrisTit" Font-Size="16px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl3" runat="server" Text="3 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl4" runat="server" Text="4 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl5" runat="server" Text="5 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl6" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV93" runat="server" Columns="4" ReadOnly="True" MaxLength="2" TabIndex="10101">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV94" runat="server" Columns="4" ReadOnly="True" TabIndex="10102" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV95" runat="server" Columns="4" ReadOnly="True" TabIndex="10103" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV96" runat="server" Columns="4" ReadOnly="True" TabIndex="10104" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV97" runat="server" Columns="4" ReadOnly="True" TabIndex="10105" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV98" runat="server" Columns="4" ReadOnly="True" TabIndex="10201" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV99" runat="server" Columns="4" ReadOnly="True" TabIndex="10202" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV100" runat="server" Columns="4" ReadOnly="True" TabIndex="10203" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV101" runat="server" Columns="4" ReadOnly="True" TabIndex="10204" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV102" runat="server" Columns="4" ReadOnly="True" TabIndex="10205" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblPromovidosH" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV103" runat="server" Columns="4" ReadOnly="True" TabIndex="10301" MaxLength="4">0</asp:TextBox>
                            </td>                        
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV104" runat="server" Columns="4" ReadOnly="True" TabIndex="10302" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV105" runat="server" Columns="4" ReadOnly="True" TabIndex="10303" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV106" runat="server" Columns="4" ReadOnly="True" TabIndex="10304" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV107" runat="server" Columns="4" ReadOnly="True" TabIndex="10305" MaxLength="4">0</asp:TextBox></td>
                         </tr>
                    </table><br />
               
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV108" runat="server" Columns="4" ReadOnly="True" TabIndex="10401" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV109" runat="server" Columns="4" ReadOnly="True" TabIndex="10402" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV110" runat="server" Columns="4" ReadOnly="True" TabIndex="10403" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV111" runat="server" Columns="4" ReadOnly="True" TabIndex="10404" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV112" runat="server" Columns="4" ReadOnly="True" TabIndex="10405" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV113" runat="server" Columns="4" ReadOnly="True" TabIndex="10501" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV114" runat="server" Columns="4" ReadOnly="True" TabIndex="10502" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV115" runat="server" Columns="4" ReadOnly="True" TabIndex="10503" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV116" runat="server" Columns="4" ReadOnly="True" TabIndex="10504" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV117" runat="server" Columns="4" ReadOnly="True" TabIndex="10505" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblPromovidosM" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV118" runat="server" Columns="4" ReadOnly="True" TabIndex="10601" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV119" runat="server" Columns="4" ReadOnly="True" TabIndex="10602" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV120" runat="server" Columns="4" ReadOnly="True" TabIndex="10603" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV121" runat="server" Columns="4" ReadOnly="True" TabIndex="10604" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV122" runat="server" Columns="4" ReadOnly="True" TabIndex="10605" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                    </table><br />
               
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV123" runat="server" Columns="4" ReadOnly="True" TabIndex="10701" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV124" runat="server" Columns="4" ReadOnly="True" TabIndex="10702" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV125" runat="server" Columns="4" ReadOnly="True" TabIndex="10703" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV126" runat="server" Columns="4" ReadOnly="True" TabIndex="10704" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV127" runat="server" Columns="4" ReadOnly="True" TabIndex="10705" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV128" runat="server" Columns="4" ReadOnly="True" TabIndex="10801" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV129" runat="server" Columns="4" ReadOnly="True" TabIndex="10802" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV130" runat="server" Columns="4" ReadOnly="True" TabIndex="10803" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV131" runat="server" Columns="4" ReadOnly="True" TabIndex="10804" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV132" runat="server" Columns="4" ReadOnly="True" TabIndex="10805" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblPromovidosS" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV133" runat="server" Columns="4" ReadOnly="True" TabIndex="10901" MaxLength="4">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV134" runat="server" Columns="4" ReadOnly="True" TabIndex="10902" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV135" runat="server" Columns="4" ReadOnly="True" TabIndex="10903" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV136" runat="server" Columns="4" ReadOnly="True" TabIndex="10904" MaxLength="4">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV137" runat="server" Columns="4" ReadOnly="True" TabIndex="10905" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px; height: 16px">
                            </td>
                            <td style="width: 132px; height: 16px">
                                &nbsp;</td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV138" runat="server" Columns="4" ReadOnly="True" TabIndex="11101" MaxLength="2">0</asp:TextBox></td>
                        </tr>
                    </table>
               
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG2_911_121',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG2_911_121',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGT_911_121',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_121',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>

        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
       <input id="hidListaTxtBoxs" type = "hidden" runat = "server"/>
        
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
</asp:Content>
