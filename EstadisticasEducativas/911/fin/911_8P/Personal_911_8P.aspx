<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8P (Desglose)" AutoEventWireup="true" CodeBehind="Personal_911_8P.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8P.Personal_911_8P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 2;
        MaxRow = 15;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style =" padding-left:300px;">
        <tr><td><span>PROFESIONAL T�CNICO MEDIO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8P',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li>
        <li onclick="openPage('AG1_911_8P',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_8P',true)"><a href="#" title=""><span>4�, 5� y TOTAL</span></a></li>
        <li onclick="openPage('PROCEIES_911_8P',true)"><a href="#" title=""><span>PROCEIES</span></a></li>
        <li onclick="openPage('Personal_911_8P',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="openPage('Inmueble_911_8P',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>


            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                <table style="width:100%">
                        <tr>
                            <td style="text-align: center">
                    <table>
                        <tr>
                            <td colspan="2" style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba el personal seg�n la funci�n que realiza, independientemente de su nombramiento, tipo y fuente de pago. Si una persona desempe�a dos o m�s funciones, an�tela en aqu�lla a la que dedique m�s tiempo."
                        Width="410px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left; padding-bottom: 20px;">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Nota: Considere exclusivamente al personal que labora en el turno al que se refiere este cuestionario, independientemente de que labore en otro turno."
                                    Width="410px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: left; width: 300px;">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL DIRECTIVO"
                        Width="330px"></asp:Label></td>
                <td style="width: 150px;">
                    </td>
            </tr>
            <tr>
                <td style="text-align: left; padding-left: 10px; width: 300px;">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CON GRUPO"
                        Width="300px"></asp:Label></td>
                <td style="width: 150px;">
                    <asp:TextBox ID="txtV484" runat="server" Columns="4" MaxLength="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left; padding-left: 10px; width: 300px;">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SIN GRUPO"
                        Width="300px"></asp:Label></td>
                <td style="width: 150px;">
                    <asp:TextBox ID="txtV485" runat="server" Columns="4" MaxLength="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left; width: 300px;">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL DOCENTE"
                        Width="330px"></asp:Label>
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="(No incluya personal docente especial)"
                        Width="330px"></asp:Label></td>
                <td style="width: 150px;" rowspan="">
                    <asp:TextBox ID="txtV486" runat="server" Columns="4" MaxLength="3" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left; width: 300px;">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL DOCENTE ESPECIAL"
                                    Width="330px"></asp:Label></td>
                <td style="width: 150px; height: 19px;">
                    </td>
            </tr>
                        <tr>
                            <td style="text-align: left; padding-left: 10px; width: 300px;">
                    <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE EDUCACI�N F�SICA"
                        Width="300px"></asp:Label></td>
                            <td style="width: 150px;">
                                <asp:TextBox ID="txtV487" runat="server" Columns="4" MaxLength="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
            <tr>
                <td colspan="1" style="text-align: left; padding-left: 10px; width: 300px;">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                        Width="300px"></asp:Label></td>
                <td style="width: 150px;">
                    <asp:TextBox ID="txtV488" runat="server" Columns="4" MaxLength="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; padding-left: 10px; width: 300px;">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS"
                                    Width="300px"></asp:Label></td>
                            <td style="width: 150px;">
                    <asp:TextBox ID="txtV489" runat="server" Columns="4" MaxLength="2" TabIndex="10601" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; padding-left: 10px; width: 300px;">
                    <asp:Label ID="lblEU" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE IDIOMAS"
                        Width="300px"></asp:Label></td>
                            <td style="width: 150px; height: 20px;">
                    <asp:TextBox ID="txtV490" runat="server" Columns="4" MaxLength="2" TabIndex="10701" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS"
                        Width="330px"></asp:Label></td>
                            <td style="width: 150px;">
                                <asp:TextBox ID="txtV491" runat="server" Columns="4" MaxLength="3" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblCANADA" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL DE PERSONAL"
                        Width="330px"></asp:Label></td>
                            <td style="width: 150px;">
                    <asp:TextBox ID="txtV492" runat="server" Columns="4" MaxLength="4" TabIndex="10901" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    </td>
                            <td style="text-align: center">
                    </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo de que dedica a la funci�n acad�mica."
                                    Width="410px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left; padding-bottom: 20px;">
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Nota: Si en la instituci�n no se utiliza el t�rmino tres cuartos de tiempo, no lo considere."
                                    Width="410px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblAFRICA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TIEMPO COMPLETO"
                        Width="300px"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV500" runat="server" Columns="4" MaxLength="3" TabIndex="11001" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblASIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TRES CUARTOS DE TIEMPO"
                        Width="300px"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV501" runat="server" Columns="4" MaxLength="3" TabIndex="11101" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblEUROPA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MEDIO TIEMPO"
                        Width="300px"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV502" runat="server" Columns="4" MaxLength="3" TabIndex="11201" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblOCEANIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="POR HORAS"
                        Width="300px"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV503" runat="server" Columns="4" MaxLength="3" TabIndex="11301" CssClass="lblNegro" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; width: 300px;">
                    <asp:Label ID="lblTotal3b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="300px"></asp:Label>
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="(Este total debe coincidir con la suma de personal directivo con grupo, personal docente y personal docente especial, reportado en la pregunta 1 de esta secci�n)"
                        Width="300px"></asp:Label></td>
                            <td style="text-align: center; vertical-align: top;">
                                <asp:TextBox ID="txtV504" runat="server" Columns="4" MaxLength="4" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
        </table>
                            </td>
                        </tr>
                    </table>
           
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('PROCEIES_911_8P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('PROCEIES_911_8P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Inmueble_911_8P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_8P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
        
</asp:Content>
