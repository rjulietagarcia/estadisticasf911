<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Carrera Magisterial)" AutoEventWireup="true" CodeBehind="CM_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.CM_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
    
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_1',true)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_1',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_USAER_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('CM_911_USAER_1',true)"><a href="#" title=""class="activo"><span>CARRERA MAGISTERIAL</span></a></li>
        <li onclick="openPage('Anexo_911_USAER_1',false)"><a href="#" title=""><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td >
                    <table id="TABLE1" style="text-align: center">
                        <tr>
                            <td colspan="5" rowspan="1" style="vertical-align: top; text-align: center">
                                <table style="width: 455px; text-align: center">
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 20px; text-align: center">
                                            <asp:Label ID="lblCarrera" runat="server" CssClass="lblRojo" Font-Size="16px" Text="III. CARRERA MAGISTERIAL"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; width: 3px; text-align: left">
                                            <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de profesores que se encuentran en el programa de carrera magisterial"
                                                Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: right">
                                            <asp:TextBox ID="txtV1428" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: left">
                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Desglose la cantidad anotada en el inciso anterior, seg�n la vertiente y el nivel en que se encuentran los profesores." Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl1aVertiente" runat="server" CssClass="lblRojo" Text="1a. VERTIENTE"
                                                Width="160px" Height="21px"></asp:Label>
                                            <br />
                                            <asp:Label ID="lbl1aVertiente2" runat="server"
                                                    CssClass="lblRojo" Height="21px" Text="(Profesores frente a grupo)" Width="160px"></asp:Label></td>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA1" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1429" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB1" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1430" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC1" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1431" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC1" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1432" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD1" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1433" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE1" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1434" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl2aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="2a. VERTIENTE"
                                                Width="160px"></asp:Label>
                                            <asp:Label ID="lbl2aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en funciones directivas y de supervisi�n)"
                                                Width="160px"></asp:Label></td>
                                        <td style="width: 67px; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA2" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1435" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB2" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1436" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC2" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1437" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC2" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1438" runat="server" Columns="2" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD2" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1439" runat="server" Columns="2" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE2" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1440" runat="server" Columns="2" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl3aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="3a. VERTIENTE"
                                                Width="160px"></asp:Label>
                                            <asp:Label ID="lbl3aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en actividades t�cno-pedag�gicas)"
                                                Width="160px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA3" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1441" runat="server" Columns="2" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB3" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1442" runat="server" Columns="2" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC3" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1443" runat="server" Columns="2" TabIndex="11601" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC3" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1444" runat="server" Columns="2" TabIndex="11701" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD3" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1445" runat="server" Columns="2" TabIndex="11801" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE3" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV1446" runat="server" Columns="2" TabIndex="11901" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_911_USAER_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_USAER_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_911_USAER_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_911_USAER_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
         GetTabIndexes();
        </script> 
       
</asp:Content>
