<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.3(4�, 5� y 6�)" AutoEventWireup="true" CodeBehind="AG4_911_3.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_3.AG4_911_3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
      
    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header"> 
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N PRIMARIA" Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1400px; width:expression(document.body.clientWidth < 1401? '1400px': 'auto' );">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_3',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li><li onclick="openPage('AG4_911_3',true)"><a href="#" title="" class="activo"><span>4�, 5� y 6�</span></a></li><li onclick="openPage('AGT_911_3',false)"><a href="#" title=""><span>TOTAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>      
<br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>  
  
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td style="width: 340px">
                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="13" style="text-align:center;">
                                                
                                                <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" 
                                                Text="Estad�stica de alumnos por grado, sexo, tipo de ingreso y edad"></asp:Label>
                                            </td>
                        </tr>
                       
                        <tr>
                            <td colspan="3" rowspan="1">
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="">
                                </td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl15_Mas" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td class="linaBajoAlto Orila">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td class="Orila">
                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="5" class="linaBajoAlto">
                                <asp:Label ID="lbl4" runat="server" CssClass="lblRojo" Text="4�"></asp:Label></td>
                            <td rowspan="2" class="linaBajoAlto">
                                <asp:Label ID="lblHombres4" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblNvoIngresoHombres4" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV167" runat="server" Columns="4" TabIndex="10101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV168" runat="server" Columns="4" TabIndex="10102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV169" runat="server" Columns="4" TabIndex="10103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV170" runat="server" Columns="4" TabIndex="10104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV171" runat="server" Columns="4" TabIndex="10105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV172" runat="server" Columns="4" TabIndex="10106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV173" runat="server" Columns="4" TabIndex="10107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV174" runat="server" Columns="4" TabIndex="10108" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                                <asp:TextBox ID="txtV175" runat="server" Columns="4" TabIndex="10109" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo">
                                <asp:Label ID="lblRepetidoresHombres4" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV176" runat="server" Columns="4" TabIndex="10201" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV177" runat="server" Columns="4" TabIndex="10202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV178" runat="server" Columns="4" TabIndex="10203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV179" runat="server" Columns="4" TabIndex="10204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV180" runat="server" Columns="4" TabIndex="10205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV181" runat="server" Columns="4" TabIndex="10206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV182" runat="server" Columns="4" TabIndex="10207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV183" runat="server" Columns="4" TabIndex="10208" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                                <asp:TextBox ID="txtV184" runat="server" Columns="4" TabIndex="10209" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="linaBajo">
                            <asp:Label ID="lblMujeres4" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:Label ID="lblNvoIngresoMujeres4" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV185" runat="server" Columns="4" TabIndex="10301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV186" runat="server" Columns="4" TabIndex="10302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV187" runat="server" Columns="4" TabIndex="10303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV188" runat="server" Columns="4" TabIndex="10304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV189" runat="server" Columns="4" TabIndex="10305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV190" runat="server" Columns="4" TabIndex="10306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV191" runat="server" Columns="4" TabIndex="10307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV192" runat="server" Columns="4" TabIndex="10308" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                                <asp:TextBox ID="txtV193" runat="server" Columns="4" TabIndex="10309" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" class="linaBajo">
                            <asp:Label ID="lblRepetidoresMujeres4" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV194" runat="server" Columns="4" TabIndex="10401" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV195" runat="server" Columns="4" TabIndex="10402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV196" runat="server" Columns="4" TabIndex="10403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV197" runat="server" Columns="4" TabIndex="10404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV198" runat="server" Columns="4" TabIndex="10405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV199" runat="server" Columns="4" TabIndex="10406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV200" runat="server" Columns="4" TabIndex="10407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV201" runat="server" Columns="4" TabIndex="10408" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                                <asp:TextBox ID="txtV202" runat="server" Columns="4" TabIndex="10409" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblSubtotal4" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV203" runat="server" Columns="4" TabIndex="10501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV204" runat="server" Columns="4" TabIndex="10502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV205" runat="server" Columns="4" TabIndex="10503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV206" runat="server" Columns="4" TabIndex="10504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV207" runat="server" Columns="4" TabIndex="10505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV208" runat="server" Columns="4" TabIndex="10506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV209" runat="server" Columns="4" TabIndex="10507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV210" runat="server" Columns="4" TabIndex="10508" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                                <asp:TextBox ID="txtV211" runat="server" Columns="4" TabIndex="10509" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">
                                <asp:TextBox ID="txtV212" runat="server" Columns="2" TabIndex="10510" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 54px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="">
                                </td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td class="linaBajoAlto Orila">
                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td class="Orila">
                                <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="5" class="linaBajoAlto">
                            <asp:Label ID="lbl5" runat="server" CssClass="lblRojo" Text="5�"></asp:Label></td>
                            <td rowspan="2" class="linaBajoAlto">
                            <asp:Label ID="lblHombres5" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td class="linaBajoAlto">
                            <asp:Label ID="lblNvoIngresoHombres5" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox1" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV213" runat="server" Columns="4" TabIndex="20101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV214" runat="server" Columns="4" TabIndex="20102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV215" runat="server" Columns="4" TabIndex="20103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV216" runat="server" Columns="4" TabIndex="20104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV217" runat="server" Columns="4" TabIndex="20105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV218" runat="server" Columns="4" TabIndex="20106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV219" runat="server" Columns="4" TabIndex="20107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV220" runat="server" Columns="4" TabIndex="20108" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblRepetidoresHombres5" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:TextBox ID="TextBox2" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV221" runat="server" Columns="4" TabIndex="20201" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV222" runat="server" Columns="4" TabIndex="20202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV223" runat="server" Columns="4" TabIndex="20203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV224" runat="server" Columns="4" TabIndex="20204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV225" runat="server" Columns="4" TabIndex="20205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV226" runat="server" Columns="4" TabIndex="20206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV227" runat="server" Columns="4" TabIndex="20207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV228" runat="server" Columns="4" TabIndex="20208" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="2" class="linaBajo">
                            <asp:Label ID="lblMujeres5" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:Label ID="lblNvoIngresoMujeres5" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"
                                Width="100px"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox3" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV229" runat="server" Columns="4" TabIndex="20301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV230" runat="server" Columns="4" TabIndex="20302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV231" runat="server" Columns="4" TabIndex="20303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV232" runat="server" Columns="4" TabIndex="20304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV233" runat="server" Columns="4" TabIndex="20305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV234" runat="server" Columns="4" TabIndex="20306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV235" runat="server" Columns="4" TabIndex="20307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV236" runat="server" Columns="4" TabIndex="20308" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblRepetidoresMujeres5" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:TextBox ID="TextBox4" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV237" runat="server" Columns="4" TabIndex="20401" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV238" runat="server" Columns="4" TabIndex="20402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV239" runat="server" Columns="4" TabIndex="20403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV240" runat="server" Columns="4" TabIndex="20404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV241" runat="server" Columns="4" TabIndex="20405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV242" runat="server" Columns="4" TabIndex="20406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV243" runat="server" Columns="4" TabIndex="20407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV244" runat="server" Columns="4" TabIndex="20408" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblSubtotal5" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:TextBox ID="TextBox5" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV245" runat="server" Columns="4" TabIndex="20501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV246" runat="server" Columns="4" TabIndex="20502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV247" runat="server" Columns="4" TabIndex="20503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV248" runat="server" Columns="4" TabIndex="20504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV249" runat="server" Columns="4" TabIndex="20505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV250" runat="server" Columns="4" TabIndex="20506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV251" runat="server" Columns="4" TabIndex="20507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV252" runat="server" Columns="4" TabIndex="20508" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">
                            <asp:TextBox ID="txtV253" runat="server" Columns="2" TabIndex="20509" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 54px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="">
                                </td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td class="linaBajoAlto Orila">
                                <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td class="Orila">
                                <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="5" class="linaBajoAlto">
                            <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6�"></asp:Label></td>
                            <td colspan="1" rowspan="2" class="linaBajoAlto">
                            <asp:Label ID="lblHombres6" runat="server" CssClass="lblRojo" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td rowspan="1" class="linaBajoAlto">
                            <asp:Label ID="lblNvoIngresoHombres6" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"
                                Width="100px"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox8" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox12" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV254" runat="server" Columns="4" TabIndex="30101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV255" runat="server" Columns="4" TabIndex="30102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV256" runat="server" Columns="4" TabIndex="30103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV257" runat="server" Columns="4" TabIndex="30104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV258" runat="server" Columns="4" TabIndex="30105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV259" runat="server" Columns="4" TabIndex="30106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV260" runat="server" Columns="4" TabIndex="30107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblRepetidoresHombres6" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:TextBox ID="TextBox9" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox13" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV261" runat="server" Columns="4" TabIndex="30201" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV262" runat="server" Columns="4" TabIndex="30202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV263" runat="server" Columns="4" TabIndex="30203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV264" runat="server" Columns="4" TabIndex="30204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV265" runat="server" Columns="4" TabIndex="30205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV266" runat="server" Columns="4" TabIndex="30206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV267" runat="server" Columns="4" TabIndex="30207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="2" class="linaBajo">
                            <asp:Label ID="lblMujeres6" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:Label ID="lblNvoIngresoMujeres6" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"
                                Width="100px"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox10" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox14" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV268" runat="server" Columns="4" TabIndex="30301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV269" runat="server" Columns="4" TabIndex="30302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV270" runat="server" Columns="4" TabIndex="30303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV271" runat="server" Columns="4" TabIndex="30304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV272" runat="server" Columns="4" TabIndex="30305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV273" runat="server" Columns="4" TabIndex="30306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV274" runat="server" Columns="4" TabIndex="30307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblRepetidoresMujeres6" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                            <asp:TextBox ID="TextBox11" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox7" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV275" runat="server" Columns="4" TabIndex="30401" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV276" runat="server" Columns="4" TabIndex="30402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV277" runat="server" Columns="4" TabIndex="30403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV278" runat="server" Columns="4" TabIndex="30404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV279" runat="server" Columns="4" TabIndex="30405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV280" runat="server" Columns="4" TabIndex="30406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV281" runat="server" Columns="4" TabIndex="30407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblSubtotal6" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox6" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="TextBox15" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV282" runat="server" Columns="4" TabIndex="30501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV283" runat="server" Columns="4" TabIndex="30502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV284" runat="server" Columns="4" TabIndex="30503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV285" runat="server" Columns="4" TabIndex="30504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV286" runat="server" Columns="4" TabIndex="30505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV287" runat="server" Columns="4" TabIndex="30506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Orila">
                            <asp:TextBox ID="txtV288" runat="server" Columns="4" TabIndex="30507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">
                            <asp:TextBox ID="txtV289" runat="server" Columns="2" TabIndex="30508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG1_911_3',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG1_911_3',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                   &nbsp;
                </td>
                <td ><span  onclick="openPage('AGT_911_3',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_3',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center>
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 14;
                MaxRow = 15;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		        Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
