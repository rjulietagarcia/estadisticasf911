<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7G(Egresados y repetidores)" AutoEventWireup="true" CodeBehind="Egresados_911_7G.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7G.Egresados_911_7G" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   
<div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">

   <table style="width:100%">
        <tr><td><span>BACHILLERATO GENERAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
   
   </div></div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7G',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos1_911_7G',true)"><a href="#" title=""><span>ALUMNOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_7G',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Egresados_911_7G',true)"><a href="#" title="" class="activo"><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Planteles_911_7G',false)"><a href="#" title=""><span>PLANTELES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
     
     <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>



        <table align="center" style="width: 400px">
            <tr>
                <td colspan="2" valign="top" style="text-align:justify">
                    <asp:Label ID="Label35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="III. EGRESADOS, REPROBADOS Y REGULARIZADOS DEL CICLO ANTERIOR." Width="400px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" valign="top" style="text-align:justify">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="12px"
                        Text="(CONSIDERE LA INFORMACI�N REPORTADA EN EL FIN DE CURSOS ANTERIOR)" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba la cantidad de alumnos egresados del ciclo escolar anterior (incluya los regularizados hasta el 30 de Septiembre de este a�o), desglos�ndola por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ALUMNOS EGRESADOS"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV366" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV367" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV368" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="2. Escriba la cantidad de alumnos, por grado, que reprobaron de 1 a 5 asignaturas el ciclo escolar anterior, desglos�ndola por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV369" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV370" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV371" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV372" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV373" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV374" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV375" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV376" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV377" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV378" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV379" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV380" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV381" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV382" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV383" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="3. De los alumnos reportados en el punto anterior registre el n�mero de alumnos que se regularizaron (aprobaron mediante ex�menes extraordinarios todas las asignaturas que adeudaban) al 30 de Septiembre del presente a�o, desglos�ndolo por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV384" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV385" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV386" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV387" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV388" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV389" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV390" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV391" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV392" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV393" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV394" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV395" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV396" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV397" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV398" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="4. De los alumnos reportados en la pregunta 2, escriba la cantidad de ellos que est�n inscritos en el presente ciclo escolar y contin�an como irregulares (adeudan asignaturas), seg�n sexo y el grado que cursan actualmente."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center; height: 24px;">
                    <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; height: 24px;">
                    <asp:TextBox ID="txtV399" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center; height: 24px;">
                    <asp:TextBox ID="txtV400" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center; width: 93px; height: 24px;">
                    <asp:TextBox ID="txtV401" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV402" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV403" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV404" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV405" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV406" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV407" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV408" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV409" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV410" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                    <asp:Label ID="Label28" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV411" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV412" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV413" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
            </tr>                        
        </table>
       
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos2_911_7G',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_911_7G',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Planteles_911_7G',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Planteles_911_7G',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                    MaxCol = 14;
                    MaxRow = 31;
                    TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                    GetTabIndexes();
                
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
       
</asp:Content>
