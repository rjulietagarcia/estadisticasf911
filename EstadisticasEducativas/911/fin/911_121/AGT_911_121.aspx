<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AGT_911_121.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_121.AGT_911_121" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script type="text/javascript">
        MaxCol = 6;
        MaxRow = 16;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR IND�GENA</span></td>
        </tr>
        <tr><td>
            <asp:Label ID="Label9" runat="server" Text="FIN DE CURSOS"></asp:Label>
            <span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
            <li onclick="openPage('Identificacion_911_121')"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('AG1_911_121',true)"><a href="#" title=""><span>1�</span></a></li>
            <li onclick="openPage('AG2_911_121',true)"><a href="#" title=""><span>2�</span></a></li>
            <li onclick="openPage('AG3_911_121',true)"><a href="#" title=""><span>3�</span></a></li>
            <li onclick="openPage('AGT_911_121',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
            <li onclick="openPage('Personal_911_121',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
            <li onclick="denyPage('Inmueble_911_121')"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
            <li onclick="denyPage('Anexo_911_121')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage('Oficializar_911_121')"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div>  
    <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
         

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lbl1" runat="server" Text="TOTAL" CssClass="lblGrisTit" Font-Size="16px"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl3" runat="server" Text="3 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl4" runat="server" Text="4 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl5" runat="server" Text="5 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl6" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV139" runat="server" Columns="4" ReadOnly="True" CssClass="lblNegro" MaxLength="4" TabIndex="10101">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV140" runat="server" Columns="4" ReadOnly="True" TabIndex="10102" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV141" runat="server" Columns="4" ReadOnly="True" TabIndex="10103" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV142" runat="server" Columns="4" ReadOnly="True" TabIndex="10104" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV143" runat="server" Columns="4" ReadOnly="True" TabIndex="10105" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV144" runat="server" Columns="4" ReadOnly="True" TabIndex="10201" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV145" runat="server" Columns="4" ReadOnly="True" TabIndex="10202" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV146" runat="server" Columns="4" ReadOnly="True" TabIndex="10203" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV147" runat="server" Columns="4" ReadOnly="True" TabIndex="10204" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV148" runat="server" Columns="4" ReadOnly="True" TabIndex="10205" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblPromovidosH" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV149" runat="server" Columns="4" ReadOnly="True" TabIndex="10301" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>                        
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV150" runat="server" Columns="4" ReadOnly="True" TabIndex="10302" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV151" runat="server" Columns="4" ReadOnly="True" TabIndex="10303" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV152" runat="server" Columns="4" ReadOnly="True" TabIndex="10304" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV153" runat="server" Columns="4" ReadOnly="True" TabIndex="10305" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                 </tr>
            </table>
            
            <br />
        
            <table>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV154" runat="server" Columns="4" ReadOnly="True" TabIndex="10401" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV155" runat="server" Columns="4" ReadOnly="True" TabIndex="10402" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV156" runat="server" Columns="4" ReadOnly="True" TabIndex="10403" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV157" runat="server" Columns="4" ReadOnly="True" TabIndex="10404" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV158" runat="server" Columns="4" ReadOnly="True" TabIndex="10405" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV159" runat="server" Columns="4" ReadOnly="True" TabIndex="10501" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV160" runat="server" Columns="4" ReadOnly="True" TabIndex="10502" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV161" runat="server" Columns="4" ReadOnly="True" TabIndex="10503" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV162" runat="server" Columns="4" ReadOnly="True" TabIndex="10504" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV163" runat="server" Columns="4" ReadOnly="True" TabIndex="10505" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblPromovidosM" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV164" runat="server" Columns="4" ReadOnly="True" TabIndex="10601" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV165" runat="server" Columns="4" ReadOnly="True" TabIndex="10602" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV166" runat="server" Columns="4" ReadOnly="True" TabIndex="10603" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV167" runat="server" Columns="4" ReadOnly="True" TabIndex="10604" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV168" runat="server" Columns="4" ReadOnly="True" TabIndex="10605" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
            </table>
            
            <br />
        
            <table>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="lblSubtotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV169" runat="server" Columns="4" ReadOnly="True" TabIndex="10701" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV170" runat="server" Columns="4" ReadOnly="True" TabIndex="10702" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV171" runat="server" Columns="4" ReadOnly="True" TabIndex="10703" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV172" runat="server" Columns="4" ReadOnly="True" TabIndex="10704" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV173" runat="server" Columns="4" ReadOnly="True" TabIndex="10705" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV174" runat="server" Columns="4" ReadOnly="True" TabIndex="10801" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV175" runat="server" Columns="4" ReadOnly="True" TabIndex="10802" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV176" runat="server" Columns="4" ReadOnly="True" TabIndex="10803" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV177" runat="server" Columns="4" ReadOnly="True" TabIndex="10804" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV178" runat="server" Columns="4" ReadOnly="True" TabIndex="10805" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblPromovidosS" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV179" runat="server" Columns="4" ReadOnly="True" TabIndex="10901" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV180" runat="server" Columns="4" ReadOnly="True" TabIndex="10902" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV181" runat="server" Columns="4" ReadOnly="True" TabIndex="10903" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV182" runat="server" Columns="4" ReadOnly="True" TabIndex="10904" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV183" runat="server" Columns="4" ReadOnly="True" TabIndex="10905" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="width: 75px; height: 16px">
                    </td>
                    <td style="width: 132px; height: 16px">
                        &nbsp;</td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                </tr>
                <tr>
                    <td rowspan="1" style="width: 75px">
                    </td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV184" runat="server" Columns="4" ReadOnly="True" TabIndex="11001" CssClass="lblNegro" MaxLength="2">0</asp:TextBox></td>
                </tr>
            </table>
                        <br />
                        <table>
                            <tr>
                                <td style="text-align: left; height: 19px;" colspan="3" width="480">
                                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="2. Seleccione el nombre de la lengua materna principal que se habla en la localidad."
                                        Width="480px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: left" width="160">
                                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Text="LENGUA MATERNA"></asp:Label></td>
                                <td style="width: 67px; text-align: left;" colspan="2">
                                    <asp:DropDownList ID="optV185" runat="server"  TabIndex="11101"  Width="248px"  onchange="OnChange(this)">
                                    <asp:ListItem Value="Seleccione una lengua materna" Text="Seleccione una lengua materna"></asp:ListItem>
                                    <asp:ListItem Value="1 AKATEKO VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="2 AMUZGO V FAMILIA OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="3 AWAKATEKO VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="4 AYAPANECO IX FAM. MIXE-ZOQUE" ></asp:ListItem>
                                    <asp:ListItem Value="5 CORA II FAMILIA YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="6 CUCAP� III FAM COCHIMI-YUMANA" ></asp:ListItem>
                                    <asp:ListItem Value="7 CUICATECO V FAM OTO-MANGUE"  ></asp:ListItem>
                                    <asp:ListItem Value="8 CHATINO V FAMILIA OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="9 CHICHIMECO JONAZ V FAM OTO-M" ></asp:ListItem>
                                    <asp:ListItem Value="10 CHINANTECO V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="11 CHOCHOLTECO V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="12 CHONTAL DE OAXACA X FAM CH-O" ></asp:ListItem>
                                    <asp:ListItem Value="13 CHONTAL DE TABASCO VI FAM MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="14 CHUJ VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="15 CH'OL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="16 GUARIJIO II FAM YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="17 HUASTECO VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="18 HUAVE XI FAMILIA HUAVE" ></asp:ListItem>
                                    <asp:ListItem Value="19 HUICHOL II FAM YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="20 IXCATECO V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="21 IXIL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="22 JAKALTEKO VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="23 KAQCHIKEL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="24 KICKAPOO I FAMILIA ALGICA" ></asp:ListItem>
                                    <asp:ListItem Value="25 KILIWA III FAM COCHIMI-YUMANA " ></asp:ListItem>
                                    <asp:ListItem Value="26 KUMIAI III FAM COCHIMI-YUMANA " ></asp:ListItem>
                                    <asp:ListItem Value="27 KU'AHL III FAM COCHIMI-YUMANA" ></asp:ListItem>
                                    <asp:ListItem Value="28 K'ICHE' VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="29 LACANDON VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="30 MAM VI FAMILIA MAYA " ></asp:ListItem>
                                    <asp:ListItem Value="31 MATLATZINCA V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="32 MAYA VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="33 MAYO II FAMILIA YUTO-NAHUA " ></asp:ListItem>
                                    <asp:ListItem Value="34 MAZAHUA V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="35 MAZATECO V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="36 MIXE IX FAMILIA MIXE-ZOQUE " ></asp:ListItem>
                                    <asp:ListItem Value="37 MIXTECO V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="38 NAHUATL II FAM YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="39 OLUTECO IX FAM MIXE-ZOQUE" ></asp:ListItem>
                                    <asp:ListItem Value="40 OTOMI V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="41 PAIPAI III FAM COCHIMI-YUMANA" ></asp:ListItem>
                                    <asp:ListItem Value="42 PAME V FAMILIA OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="43 PAPAGO II FAMILIA YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="44 PIMA II FAMILIA YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="45 POPOLOCA V FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="46 POPOLUCA DE LA SIERRA IX FAM" ></asp:ListItem>
                                    <asp:ListItem Value="47 QATO'K VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="48 Q'ANJOB'AL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="49 Q'EQCHI' VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="50 SAYULTECO IX FAM MIXE-ZOQUE"></asp:ListItem>
                                    <asp:ListItem Value="51 SERI IV FAMILIA SERI" ></asp:ListItem>
                                    <asp:ListItem Value="52 TARAHUMARA II FAM YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="53 TARASCO VIII FAMILIA TARASCA" ></asp:ListItem>
                                    <asp:ListItem Value="54 TEKO VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="55 TEPEHUA VII FAM TOTONACO-TEPE" ></asp:ListItem>
                                    <asp:ListItem Value="56 TEPEHUANO DEL NORTE II FAM Y-N" ></asp:ListItem>
                                    <asp:ListItem Value="57 TEPEHUANO DEL SUR II FAM Y-NA" ></asp:ListItem>
                                    <asp:ListItem Value="58 TEXISTEPEQUE�O IX FAM MIXE-ZOQUE" ></asp:ListItem>
                                    <asp:ListItem Value="59 TOJOLABAL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="60 TOTONACO VII FAM TOTONACO-T" ></asp:ListItem>
                                    <asp:ListItem Value="61 TRIQUI VI FAM OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="62 TLAHUICA V FMILIA OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="63 TLAPANECO V FAMILIA OTO-M" ></asp:ListItem>
                                    <asp:ListItem Value="64 TSELTAL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="65 TSOTSIL VI FAMILIA MAYA" ></asp:ListItem>
                                    <asp:ListItem Value="66 YAQUI II FAMILIA YUTO-NAHUA" ></asp:ListItem>
                                    <asp:ListItem Value="67 ZAPOTECO V FAMILIA OTO-MANGUE" ></asp:ListItem>
                                    <asp:ListItem Value="68 ZOQUE IX FAMILIA MIXE-ZOQUE" ></asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtV185" runat="server" Columns="1" CssClass="lblNegro" MaxLength="30"
                                        ReadOnly="True"  Width="1px" style="visibility:hidden;">0</asp:TextBox>
                                    </td>
                            </tr>
                            <tr>
                                <td style="text-align: left" width="160">
                                </td>
                                <td colspan="2" style="width: 67px; text-align: center">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="width: 132px; height: 16px; text-align: left">
                                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="3. De la existencia total, escriba el n�mero de alumnos con discapacidad, desglos�ndolo por sexo."
                                        Width="480px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 72px; height: 16px; text-align: center;">
                                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                                <td style="height: 16px; text-align: center;" width="160">
                                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                                <td style="height: 16px; text-align: center;" width="160">
                                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 72px; text-align: center">
                                    <asp:TextBox ID="txtV186" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                        ReadOnly="True" TabIndex="11201">0</asp:TextBox></td>
                                <td style="text-align: center;" width="160">
                                    <asp:TextBox ID="txtV187" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                        ReadOnly="True" TabIndex="11202">0</asp:TextBox></td>
                                <td style="text-align: center;" width="160">
                                    <asp:TextBox ID="txtV188" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                        ReadOnly="True" TabIndex="11203">0</asp:TextBox></td>
                            </tr>
                        </table>
            
            <br /> 
                
        
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG3_911_121',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG3_911_121',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_121',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_121',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
      <div class="divResultado" id="divResultado"></div>  
            <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type = "hidden" runat = "server"/>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
         
         <script type="text/javascript">
         function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    if(myindex == 0)
                    {
                    document.getElementById('ctl00_cphMainMaster_txtV185').value  = '_';
                    }else{
                    document.getElementById('ctl00_cphMainMaster_txtV185').value  = SelValue;                    
                    }
                    
                  return true;
                }
         </script>         
                 
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        </asp:Content>
