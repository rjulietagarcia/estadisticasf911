<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6(2�)" AutoEventWireup="true" CodeBehind="AG2_911_6.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6.AG2_911_6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_6',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_6',true)"><a href="#" title="" class="activo"><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_6',false)"><a href="#" title="" ><span>3�</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Revisar la cantidad de alumnos inscritos, existentes y aprobados por grado, edad y g�nero, y en caso de observar alguna inconsistencia en la informaci�n favor de realizar los ajustes pertinentes en el sistema de Control Escolar.</p>
    <p>La cantidad de alumnos aprobados se mostrar� cuando se haya concluido el ingreso de las calificaciones del quinto bimestre. Por lo tanto no deber� oficializar la estad�stica hasta que todas las calificaciones est�n en el sistema y la informaci�n en este cuestionario haya sido debidamente validada.</p>
    <p>Para calcular la edad de los alumnos se ha considerado a�os cumplidos al 8 de julio del a�o en curso, que es la fecha en que termina el ciclo escolar.</p>
    <p>En esta pantalla est�n disponibles los links para acceder al Control Escolar y realizar correcciones en los datos de los alumnos, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>

<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         <table style="text-align:center">
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="2�" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="Label1" runat="server" Text="12 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" Text="13 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" Text="14 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl16" runat="server" CssClass="lblRojo" Text="16 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl17" runat="server" CssClass="lblRojo" Text="17 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl18" runat="server" CssClass="lblRojo" Text="18 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV83" runat="server" Columns="4" ReadOnly="True" TabIndex="10101" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV84" runat="server" Columns="4" ReadOnly="True" TabIndex="10102" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV85" runat="server" Columns="4" ReadOnly="True" TabIndex="10103" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV86" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10104" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV87" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV88" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV89" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV90" runat="server" Columns="5" ReadOnly="True" TabIndex="10108" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV91" runat="server" Columns="4" ReadOnly="True" TabIndex="10201" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV92" runat="server" Columns="4" ReadOnly="True" TabIndex="10202" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV93" runat="server" Columns="4" ReadOnly="True" TabIndex="10203" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV94" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10204" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV95" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV96" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV97" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV98" runat="server" Columns="5" ReadOnly="True" TabIndex="10208" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosH" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV99" runat="server" Columns="4" ReadOnly="True" TabIndex="10301" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV100" runat="server" Columns="4" ReadOnly="True" TabIndex="10302" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV101" runat="server" Columns="4" ReadOnly="True" TabIndex="10303" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV102" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10304" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV103" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV104" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV105" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV106" runat="server" Columns="5" ReadOnly="True" TabIndex="10308" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                         </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV107" runat="server" Columns="4" ReadOnly="True" TabIndex="10401" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV108" runat="server" Columns="4" ReadOnly="True" TabIndex="10402" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV109" runat="server" Columns="4" ReadOnly="True" TabIndex="10403" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV110" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10404" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV111" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV112" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV113" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV114" runat="server" Columns="5" ReadOnly="True" TabIndex="10408" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV115" runat="server" Columns="4" ReadOnly="True" TabIndex="10501" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV116" runat="server" Columns="4" ReadOnly="True" TabIndex="10502" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV117" runat="server" Columns="4" ReadOnly="True" TabIndex="10503" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV118" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10504" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV119" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV120" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV121" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV122" runat="server" Columns="5" ReadOnly="True" TabIndex="10508" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosM" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV123" runat="server" Columns="4" ReadOnly="True" TabIndex="10601" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV124" runat="server" Columns="4" ReadOnly="True" TabIndex="10602" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV125" runat="server" Columns="4" ReadOnly="True" TabIndex="10603" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV126" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10604" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV127" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10605" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV128" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10606" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV129" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10607" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV130" runat="server" Columns="5" ReadOnly="True" TabIndex="10608" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td style="height: 102px">
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV131" runat="server" Columns="4" ReadOnly="True" TabIndex="10701" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV132" runat="server" Columns="4" ReadOnly="True" TabIndex="10702" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV133" runat="server" Columns="4" ReadOnly="True" TabIndex="10703" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV134" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10704" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV135" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10705" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV136" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10706" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV137" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10707" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV138" runat="server" Columns="5" ReadOnly="True" TabIndex="10708" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV139" runat="server" Columns="4" ReadOnly="True" TabIndex="10801" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV140" runat="server" Columns="4" ReadOnly="True" TabIndex="10802" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV141" runat="server" Columns="4" ReadOnly="True" TabIndex="10803" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV142" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10804" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV143" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10805" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV144" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10806" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV145" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10807" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV146" runat="server" Columns="5" ReadOnly="True" TabIndex="10808" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosS" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV147" runat="server" Columns="4" ReadOnly="True" TabIndex="10901" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV148" runat="server" Columns="4" ReadOnly="True" TabIndex="10902" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV149" runat="server" Columns="4" ReadOnly="True" TabIndex="10903" MaxLength="4" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV150" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10904" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV151" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10905" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV152" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10906" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV153" runat="server" Columns="4" MaxLength="4" ReadOnly="True"
                                    TabIndex="10907" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV154" runat="server" Columns="5" ReadOnly="True" TabIndex="10908" CssClass="lblNegro" MaxLength="5">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px; height: 16px">
                            </td>
                            <td style="width: 132px; height: 16px">
                                &nbsp;</td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 81px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 81px">
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV155" runat="server" Columns="2" ReadOnly="True" TabIndex="11101" CssClass="lblNegro" MaxLength="2">0</asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG1_911_6',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG1_911_6',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG3_911_6',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG3_911_6',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
       
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
          <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
        GetTabIndexes();  
         
        </script> 
</asp:Content>
